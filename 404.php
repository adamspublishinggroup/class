<?php
 switch($_SERVER['REQUEST_URI']) {
    case '/really_old_page.php':
        header('HTTP/1.1 301 Moved Permanently');
        header('Location: /new-url/...');

        exit;
    default:
        header('HTTP/1.1 404 Not Found');
        die('404 - Not Found ');
}