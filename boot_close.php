	<script>
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("active");
	});
	function keepAlive()
	{
		$.ajax({
		  url: "includes/keepAlive.php",
		  type: "POST",
		  data: {},
		  dataType: 'json',
		  success: function(response){
			 console.log(response.response);
		  }
		});
	}

	$.ready(function(){
		window.setInterval("keepAlive",10000);
	})
	</script>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '<?= GOOGLE_ANALYTICS ?>', 'auto');
      ga('send', 'pageview');

    </script>
</body>
</html>
