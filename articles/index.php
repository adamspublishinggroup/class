<?php
$page = 'articles';
$page_title = 'Articles';
$page_h1_title = 'Articles';
$page_meta = '';

require_once( '../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/article-template.php' );
require_once( '../boot_close.php' );
