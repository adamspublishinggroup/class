(function ($) {
$(function () {


	/**********************************************************/
	/* mobile nav                                             */
	/**********************************************************/
	$('#mobileNav .tab').click(function(e) {
		e.preventDefault();
		var $tab = $('#mobileNav .tab');
		var $body = $('body');
		if( $body.hasClass('showMobileNav') ) {
			$body.removeClass('showMobileNav');
			$tab.text('Menu');
		} else {
			$body.addClass('showMobileNav');
			$tab.text('Close');
		}
	});
	$('#mobileNav .subGroup').click(function(e){
		var $this = $(this);
		if( $this.hasClass('shown') ) {
			$this.removeClass('shown');
		} else {
			$this.addClass('shown');
		}
	});


	/**********************************************************/
	/* show/hide elements.                                    */
	/**********************************************************/
	$('#mobile-filter-trigger').click(function(e){
		e.preventDefault();
		if( $('#catsMenu').hasClass('shown') ) {
			$('#catsMenu').removeClass('shown');
		} else {
			$('#catsMenu').addClass('shown');
		}
	});
	$('#toggleTriggerPL').click(function(e){
		e.preventDefault();
		$('#previewListing').removeClass('shown');
		$('#previewDetail').addClass('shown');
	});
	$('#toggleTriggerPD').click(function(e){
		e.preventDefault();
		$('#previewDetail').removeClass('shown');
		$('#previewListing').addClass('shown');
	});


	/**********************************************************/
	/* show/hide sub-nav                                      */
	/**********************************************************/
	if( $('.catItem.hasChildren .subCatTrigger').length ) {
		$('.catItem.hasChildren .subCatTrigger').click(function(e){
			e.preventDefault();
			var $this = $(this);
			var $that = $this.parent();
			if( $that.hasClass('shown') ) {
				$this.text('+');
				$that.removeClass('shown');
				setTimeout(function(){
					contentResize();
				},0);
			} else {
				$this.text('−');
				$that.addClass('shown');
				setTimeout(function(){
					contentResize();
				},0);
			}
		});
	}


	/**********************************************************/
	/* package stuff                                          */
	/**********************************************************/
	if( $('#packages').length ) {
		$('.package').on('click',function () {
			var $this = $(this);
			var $all = $('.package');
			$all.removeClass('unSelected');
			$all.removeClass('selectedPackage');
			var packageValue = $this.data('id');
			$this.addClass('selectedPackage');
			var tempUrl = newAdUrl.split('&');
			newAdUrl = tempUrl[0] + '&package=' + packageValue;
			checkAck();
		});
		$('#packages .package-desc-trigger').click(function(e){
			e.preventDefault();
			var $this = $(this);
			var $that = $(this).parent();
			if( $that.hasClass('shown') ) {
				$this.text('(See Description)');
				$that.removeClass('shown');
				setTimeout(function(){
					contentResize();
				},0);
			} else {
				$this.text('(Hide Description)');
				$that.addClass('shown');
				setTimeout(function(){
					contentResize();
				},0);
			}
		});
	}


	/**********************************************************/
	/* content height                                         */
	/**********************************************************/
	if( $('.template-default').length ) {
		contentResize();
		$(window).resize(function(){
			contentResize();
		});
	}
	if( $('.template-advertiser-details').length ) {
		contentResize();
		$(window).resize(function(){
			contentResize();
		});
	}
	if( $('.template-place-ad').length ) {
		contentNoLeftResize();
		$(window).resize(function(){
			contentNoLeftResize();
		});
	}
	if( $('.template-dashboard').length ) {
		contentNoLeftResize();
		$(window).resize(function(){
			contentNoLeftResize();
		});
	}
	if( $('.template-article-details').length ) {
		contentNoLeftResize();
		$(window).resize(function(){
			contentNoLeftResize();
		});
	}
	if( $('.template-advertiser-listings').length ) {
		contentAdvertResize();
		$(window).resize(function(){
			contentAdvertResize();
		});
		titleAdvertResize();
		$(window).resize(function(){
			titleAdvertResize();
		});
	}


	/**********************************************************/
	/*   GOOGLE MAP                                           */
	/**********************************************************/
	if( $('#map').length ) {
		var myLocation = new google.maps.LatLng(43.5890111,-116.2799958);

		var draggableValue;
		if( $(document).width() <= 768 ) {
			draggableValue = false;   /*This option is used for disabling drag.*/
		} else {
			draggableValue = true;   /*This option is used for disabling drag.*/
		}

		var mapOptions = {
			center: myLocation,
			zoom: 13,
			mapTypeControl: true,
			draggable: draggableValue,
			scaleControl: false,
			scrollwheel: false,
			navigationControl: true,
			styles: [{"stylers": [{"saturation": -100}]}],
			streetViewControl: true
		};

		var marker = new google.maps.Marker({
			position: myLocation,
			title:"To be announced"
		});
		var map = new google.maps.Map(
			document.getElementById("map"),
			mapOptions
		);
		marker.setMap(map);

		// set map height
		var mapHeight = $('.map-wrap').height();
		$("#map").css('min-height', mapHeight);
	}


	/**********************************************************/
	/* smooth scroll to anchor on same page                   */
	/**********************************************************/
	// $('a[href*=#]:not([href=#])').click(function() {
	// 	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	// 		var target = $(this.hash);
	// 		target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	// 		if (target.length) {
	// 			var $tab = $('#mobileNav .tab');
	// 			var $body = $('body');
	// 			if ($body.hasClass('showMobileNav')) {
	// 				$body.removeClass('showMobileNav');
	// 				$tab.text('Menu');
	// 			}
	// 			$('html,body').animate({
	// 				scrollTop: target.offset().top - 60
	// 			}, 800);
	// 			return false;
	// 		}
	// 	}
	// });


	/**********************************************************/
	/* accordion                                              */
	/**********************************************************/
	if( $('#accordion').length ) {
		setTimeout(function(){
			$('.accd-detail').each(function(){
				var h = $(this).height();
				$(this).data('height',h);
				$(this).css('height','0px');
			});
		},0);

		$('.accd-title').click(function(e){
			e.preventDefault();
			var $allThis = $('.accd-item');
			var $allThat = $('.accd-detail');
			var $this = $(this).parent();
			var $that = $this.find('.accd-detail');
			var dh = $that.data('height') + 24;
			if( $this.hasClass('shown') ) {
				$allThis.removeClass('shown');
				$allThat.css('height','0px');
			} else {
				$allThis.removeClass('shown');
				$allThat.css('height','0px');
				$this.addClass('shown');
				$that.css('height',dh+'px');
			}
 		});
	}


	/**********************************************************/
	/* popups                                                 */
	/**********************************************************/
	// email ad popup show/hide
	$('#emailAdModal').click(function(e) {
		$('body').removeClass('popup_email_ad');
	});
	$('#emailAdSuccessModal').click(function(e) {
		$('body').removeClass('popup_email_ad_success');
	});
	$('#formSuccessModal').click(function(e) {
		$('body').removeClass('popup_form_success');
	});
	// popup close
	$('.popup-form-close').click(function(e) {
		if( e.target.id !== '#emailAdForm' ) {
			$('body').removeClass('popup_email_ad');
		}
		if( e.target.id !== '#emailAdSuccessModal' ) {
			$('body').removeClass('popup_email_ad_success');
		}
		if (e.target.id !== '#formSuccessPopup') {
			$('body').removeClass('popup_form_success');
		}
	});


	/**********************************************************/
	/* checkboxes                                             */
	/**********************************************************/
	$('.checkboxes input[type="checkbox"]').click(function() {
		if( !($(this).attr('checked') == "checked") ) {
			$(this).attr('checked', true);
			$(this).parent().addClass('selected');
		} else {
			$(this).attr('checked', false);
			$(this).parent().removeClass('selected');
		}
	});


	/**********************************************************/
	/* map marker trigger                                     */
	/**********************************************************/
	if( $('#classMap').length ) {
		$('.mapTargetTrigger').click(function(e){
			e.preventDefault();
			var target = $(this).data('target');
			new google.maps.event.trigger( markers[target], 'click' );
		});
	}


	/**********************************************************/
	/* colorbox                                               */
	/**********************************************************/
	// colorbox on video
	$('.play_video').click(wireColorbox);

	// colorbox on gallery
	$('a.adDetailSlide').colorbox({
		maxWidth : '90%',
		maxHeight : '90%',
		opacity : 0.7,
		rel:'group1',
		transition : 'none',
		current : '',
		onOpen:function(){
			prefix = 'cbox';
			$('#cboxWrapper').append(
				$prev = $('<button type="button"/>').attr("class",prefix+'Previous').attr('id',prefix+'Previous'),
				$next = $('<button type="button"/>').attr("class",prefix+'Next').attr('id',prefix+'Next')
			);
			$('#cboxContent #cboxPrevious, #cboxContent #cboxNext').remove();
			$('.cboxNext').click(function(){
				$.colorbox.next();
			});
			$('.cboxPrevious').click(function(){
				$.colorbox.prev();
			});
		}
	});


	/**********************************************************/
	/* Ad Action Ajax                                         */
	/**********************************************************/
	$('.postAction').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var gate = $this.data('gate');
		var adId = $this.parent().data('ad');
		$.ajax({
			type: 'POST',
			url: adActionsAjaxPath,
			data: {
				gate: gate,
				adId: adId
				},
			success: function(data){
				var ajData = $.parseJSON(data);
				if( ajData.status == 'success' ) {
					if( ajData.newGate > '' ) {
						$this.data('gate',ajData.newGate);
					}
					if( ajData.html > '' ) {
						$this.html(ajData.html);
					}
					if( ajData.msg > '' ) {
						alert(ajData.msg);
					}
				} else {
					if( ajData.msg > '' ) {
						alert(ajData.msg);
					}
				}
			}
		});
		return false;
	});
	$('.postEmail').click(function(e) {
		e.preventDefault();
		var $this = $(this);
		var adId = $this.parent().data('ad');
		var adTitle = $this.parent().data('title');
		$.ajax({
			type: 'POST',
			url: adActionsAjaxPath,
			data: {
				gate: 'verifyUser'
				},
			success: function(data){
				var ajData = $.parseJSON(data);
				if( ajData.status == 'success' ) {
					$('body').addClass('popup_email_ad');
					$('#email_ad_form #adid').val(adId);
					$('#emailAdForm .popup-form-subtitle').text(adTitle);
				} else {
					if( ajData.msg > '' ) {
						alert(ajData.msg);
					}
				}
			}
		});
		return false;
	});
	$('#sendEmail').click(function(e) {
		e.preventDefault();
		var adId = $('#email_ad_form #adid').val();
		var sender = $('#email_ad_form #sender_name').val();
		var recipient = $('#email_ad_form #recipient_email').val();
		var message = $('#email_ad_form #message').val();
		var token = $("#g-recaptcha-response").val();
		var mailRegex = /[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
		if ( ! mailRegex.test(recipient)) {
			// return to form
			alert('You must enter a valid email address.');
			$('#recipient_email').focus();
		} else {
			// send to server
			$.ajax({
				type: 'POST',
				url: adActionsAjaxPath,
				data: {
					gate: 'email',
					adId: adId,
					sender_name: sender,
					recipient_email: recipient,
					message: message,
					cToken: token
					},
				success: function(data){
					var ajData = $.parseJSON(data);
					if( ajData.status == 'success' ) {
						$('body').removeClass('popup_email_ad');
						$('body').addClass('popup_email_ad_success');
						if( ajData.msg > '' ) {
							alert(ajData.msg);
						}
					} else {
						if( ajData.msg > '' ) {
							alert(ajData.msg);
						}
					}
				}
			});
		}
		return false;
	});


	/**********************************************************/
	/* slick slider                                           */
	/**********************************************************/
	$('.slider_for').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		asNavFor: '.slider_nav'
	});
	$('.slider_nav').slick({
		slidesToShow: 5,
		slidesToScroll: 1,
		asNavFor: '.slider_for',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		responsive: [
			{
			breakpoint: 768,
			settings: {
				arrows: false,
				slidesToShow: 3
				}
			},
			{
			breakpoint: 480,
			settings: {
				arrows: false,
				slidesToShow: 2
				}
			}
		]
	});
	// slider for featured items
	var fcCarousel = $('.fc-wrapper');
	var fcContentCarousel = $('.content .fc-wrapper');
	if( fcContentCarousel.length ) {
		fcCarousel.find('.fc-container').slick({
			variableWidth: false,
			infinite: true,
			dots: false,
			speed: 1200,
			autoplay: true,
			autoplaySpeed: 4000,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					}
				},
				{
					breakpoint: 1050,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
					}
				},
				{
					breakpoint: 950,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					}
				},
				{
					breakpoint: 800,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					}
				},
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					}
				},
				{
					breakpoint: 400,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					}
				}
			]
		});
		// this is different than the document ready event
		$(window).bind('load', function() {
			var fcHeight = 0;
			fcCarousel.find('.fc-item').each(function() {
				var height = $(this).height();
				fcHeight = height > fcHeight ? height : fcHeight;
			});
			fcCarousel.find('.fc-container').height(fcHeight);
			fcCarousel.find('.fc-container').find('.fc-item').each(function(){
				$(this).height(fcHeight);
			});
		});
	} else if( fcCarousel.length ) {
		fcCarousel.find('.fc-container').slick({
			variableWidth: false,
			infinite: true,
			dots: false,
			speed: 1200,
			autoplay: true,
			autoplaySpeed: 4000,
			slidesToShow: 6,
			slidesToScroll: 6,
			responsive: [
				{
					breakpoint: 1200,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 5,
					}
				},
				{
					breakpoint: 1050,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					}
				},
				{
					breakpoint: 950,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					}
				},
				{
					breakpoint: 900,
					settings: {
						slidesToShow: 6,
						slidesToScroll: 6,
					}
				},
				{
					breakpoint: 800,
					settings: {
						slidesToShow: 5,
						slidesToScroll: 5,
					}
				},
				{
					breakpoint: 700,
					settings: {
						slidesToShow: 4,
						slidesToScroll: 4,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
					}
				},
				{
					breakpoint: 500,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					}
				},
				{
					breakpoint: 400,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2,
						arrows: false
					}
				}
			]
		});
		// this is different than the document ready event
		$(window).bind('load', function() {
			var fcHeight = 0;
			fcCarousel.find('.fc-item').each(function() {
				var height = $(this).height();
				fcHeight = height > fcHeight ? height : fcHeight;
			});
			fcCarousel.find('.fc-container').height(fcHeight);
			fcCarousel.find('.fc-container').find('.fc-item').each(function(){
				$(this).height(fcHeight);
			});
		});
	}


	/**********************************************************/
	/* swap advertiser's listings/displayAds                  */
	/**********************************************************/
	if( $('#alvertwiser-switch').length ) {
		$('.alvertwiser-switch-nav a').click(function(e){
			e.preventDefault();
			var $this = $(this);
			var $these = $('.alvertwiser-switch-nav a');
			var $tab = $this.data('tab');
			var $grandpa = $('#alvertwiser-switch');
			if( $this.hasClass('active') ) {
				// do nothing because you're clicking on the active tab
			} else {
				$these.removeClass('active');
				$this.addClass('active');
				$grandpa.removeClass('showList');
				$grandpa.removeClass('showDisp');
				$grandpa.addClass($tab);
			}
		});
	}


	/**********************************************************/
	/* masonry                                                */
	/**********************************************************/
	setTimeout(function(){ 
		$('.grid').masonry({
			// options
			itemSelector: '.grid-item',
			columnWidth: 200
		}); 
	}, 30);
	$('#showDispTrigger').click(function(){
		$('.grid').masonry({
			// options
			itemSelector: '.grid-item',
			columnWidth: 200
		});
	});


});
})(jQuery);


/**********************************************************/
/* package rules acknowledgement                          */
/**********************************************************/
function checkAck(e){
	if ($('#new_acknowledge').prop('checked')) {
		$('#startAd').prop('href', newAdUrl);
		$('#startAd').removeClass('disabled');
		return true;
	} else {
		$('#startAd').prop('href','#');
		$('#startAd').addClass('disabled');
		return false;
	}
}


/**********************************************************/
/* content height                                         */
/**********************************************************/
function contentResize(){
	// adjust the content height to always be bigger
	// than surrounding absolutely placed elements
	setTimeout(function(){
		var width = $(window).width();
		if( width < 700 ) {
			$('.content-wrap').css( 'height','auto' );
		} else if( width < 900 ) {
			var leftH = $('.content-nav').height() + 36;
			var contentH = $('.content').height();
			if( contentH > leftH ) {
				$('.content-wrap').css( 'height','auto' );
			} else {
				$('.content-wrap').css( 'height',( leftH ) );
			}
		} else {
			var titleH = $('.page-title-wrap').height();
			var rightH = $('.right-content').height();
			var leftH = $('.content-nav').height() + 36;
			var contentH = $('.content').height();
			var newH = 0;
			if( leftH+titleH > rightH ) {
				newH = leftH;
			} else {
				newH = rightH-titleH;
			}
			if( contentH > newH ) {
				$('.content-wrap').css( 'height','auto' );
			} else {
				$('.content-wrap').css( 'height',( newH ) );
			}
		}
	}, 0);
}
function contentNoLeftResize(){
	// adjust the content height to always be bigger
	// than surrounding absolutely placed elements
	setTimeout(function(){
		var width = $(window).width();
		if( width < 900 ) {
			$('.content-wrap').css( 'height','auto' );
		} else {
			var titleH = $('.page-title-wrap').height();
			var rightH = $('.right-content').height();
			var contentH = $('.content').height();
			if( ( contentH + titleH ) > rightH ) {
				$('.content-wrap').css( 'height','auto' );
			} else {
				$('.content-wrap').css( 'height',( rightH-titleH ) );
			}
		}
	}, 0);
}
function contentAdvertResize(){
	// adjust the content height to always be bigger
	// than surrounding absolutely placed elements
	setTimeout(function(){
		var width = $(window).width();
		if( width < 900 ) {
			$('.content-wrap').css( 'height','auto' );
		} else {
			var rightH = $('.right-content').height();
			var contentH = $('.content').height();
			if( contentH > rightH ) {
				$('.content-wrap').css( 'height','auto' );
			} else {
				$('.content-wrap').css( 'height',( rightH ) );
			}
		}
	}, 0);
}
function titleAdvertResize(){
	// adjust the map height to always be equal
	// surrounding title/logo elements
	setTimeout(function(){
		var width = $(window).width();
		if( width < 900 ) {
			$('.title-content .page-map').css( 'height','300px' );
		} else {
			var lH = $('.title-content .page-title-advertiser').height();
			if( ( lH ) >= 191 ) {
				$('.title-content .page-map').css( 'height',( lH ) );
			} else {
				$('.title-content .page-map').css( 'height','190px' );
			}
		}
	}, 0);
}


/**********************************************************/
/* colorbox                                               */
/**********************************************************/
function wireColorbox(){
	var w = window.innerWidth * .9;
	var h = window.innerHeight * .9;
	var setW = w;
	var setH = h;
	var ratio = h / w;
	if ( ratio > 0.5625 ) {
		// width takes priority
		setH = w * 9 / 16;
	} else {
		// height takes priority
		setW = h * 16 / 9;
	}

	$.colorbox({
		href: this.href,
		iframe: true,
		width: setW,
		height: setH
	});
	return false;
}


/**********************************************************/
/* remove ad                                              */
/**********************************************************/
function removeAd(adId) {
    $.ajax({
        type: 'POST',
        url: adActionsAjaxPath,
        data: {
            gate: 'deleteAd',
            id: adId
            },
        success: function(data){
            var ajData = $.parseJSON(data);
            if( ajData.status == 'success' ) {
                $('#row_'+adId).remove();
            } else {
                if( ajData.msg > '' ) {
                    alert(ajData.msg);
                }
            }
        }
    });
    return false;
}


/**********************************************************/
/* send contact email                                     */
/**********************************************************/

function sendContact(e)
{
    e.preventDefault();
    var email = $('#contact_email').val();
    var name = $('#contact_name').val();
    var message = $('#contact_message').val();
    var emailvalid = validateEmail(email);

    if( email == '' || emailvalid == false ) {
        $('#contact_form').addClass('has-errors');
        $('#contact_email').addClass('has-error');
    } else if( name == '' ) {
        $('#contact_form').addClass('has-errors');
        $('#contact_name').addClass('has-error');
    } else {
        console.log("submitting ajax");
        $.ajax({
            url: formSubmitAjaxPath,
            type: 'POST',
            data: {
                email: email,
                name: name,
                message: message
            },
            dataType: 'json',
            success: function(response){
                if(response.mail_send == 'success') {
                    $('#contact_form').removeClass('has-errors');
                    $('#contact_email').removeClass('has-error');
                    $('#contact_email').val('');
                    $('#contact_name').val('');
                    $('#contact_message').val('');
                    $('body').addClass('popup_form_success');
                } else {
                    console.log(response.message);
                }
            }
        });
    }

    return false;
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    console.log('validating...');
    return re.test(email);
}
