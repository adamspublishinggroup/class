<?php
function site_log($adID=0,$action='detail',$advertiserID = 0, $viewSource = 'page')
{
    $d = date("Y-m-d");
    $dt = date("Y-m-d H:i:s");

    //see if user is logged in
    $token = addslashes($_SESSION['token']);

    $url = addslashes($_SERVER['REQUEST_URI']);

    if(stripos($url,'css')>0 ||
    stripos($url,'js')>0 ||
    stripos($url,'admin')>0 ||
    stripos($url,'ajax')>0 ||
    stripos($url,'place-an-ad')>0 ||
    stripos($url,'account')>0 ||
    stripos($url,'ico')>0 )
    {
        //then ignore this url
    } else {
        $remoteIP = addslashes($_SERVER['REMOTE_ADDR']);
        $agent = addslashes($_SERVER['HTTP_USER_AGENT']);
        $refer = addslashes($_SERVER['HTTP_REFERER']);

        $sql="INSERT INTO site_log (ad_id, advertiser_id, action_date, action_dt, action_type, user_token, action_url,
        user_ip, user_agent, user_refer, source)
        VALUES ('$adID', '$advertiserID', '$d', '$dt', '$action', '$token', '$url',
        '$remoteIP', '$agent', '$refer', '$viewSource')";
        $dbLog = dbinsertquery($sql);
    }
}

function generate_random_string($length,$numeric=false)
{
    if($numeric)
    {
        $randstr='';
        while(strlen($randstr)<$length)
        {
            $randstr.=mt_rand(0,9);
        }
    } else {
        $randstr = "";
        for($i=0; $i<$length; $i++){
             $randnum = mt_rand(0,61);
             if($randnum < 10){
                $randstr .= chr($randnum+48);
             }else if($randnum < 36){
                $randstr .= chr($randnum+55);
             }else{
                $randstr .= chr($randnum+61);
             }
         }
    }

  return $randstr;
}


function redirect($url,$delay=0) {
   if (!headers_sent())
       if($delay>0)
       {
           header('Refresh: '.$delay.';url='.$url);
       } else {
           header('Location: '.$url);
       }
   else {
       echo '<script type="text/javascript">';
       echo 'document.location.href="'.$url.'";';
       echo '</script>';
       echo '<noscript>';
       echo '<meta http-equiv="refresh" content="'.$delay.';url='.$url.'" />';
       echo '</noscript>';
   }
}



// function to check and move file
function processFile($file,$destination,$newname)
{
    // set full path/name of file to be moved
    $upload_file = $destination.$newname;
    if(file_exists($upload_file)) {
        unlink($upload_file);
        //$error.= $file['name'].' - Filename already existed - I wrote over the original with the new one.';
    }
    if(!move_uploaded_file($file['tmp_name'], $upload_file)) {
        // failed to move file
        $error.= 'File upload failed on '.$newname.' - Please try again';
        return false;
    } else {
        // upload OK - change file permissions
        //chmod($upload_file, 0755);
        return true;
    }
}


function geocode($address)
{
    $address=urlencode($address);
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=".GOOGLE_MAP_KEY;

    $ch      = curl_init( $url );
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    $content = curl_exec( $ch );
    $status     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );
    $content=json_decode($content,true);

    $status=$content['status'];
    $lat=$content['results'][0]['geometry']['location']['lat'];
    $lon=$content['results'][0]['geometry']['location']['lng'];
    $result['status']=$status;
    $result['lat']=$lat;
    $result['lon']=$lon;

    return $result;
}


// return static page content
function staticPage($slug)
{
	$sql = "SELECT * FROM static_pages WHERE slug = '$slug'";
	$dbArr = dbselectsingle($sql);
	if( $dbArr['numrows'] > 0 ) {
		$result = $dbArr['data']['content'];
	} else {
		$result = '<p>This page appears to have no content.</p>';
	}

	return $result;
}


//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////
function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);
}


function checkCaptcha()
{
	$token = $_POST['cToken'];
	$response = getCaptchaResponse( $token );

	$json['recaptcha_status'] = $response['content']['success'];

	//ok, let's see if we have a valid captcha token first
	if( $response['content']['success'] ) {
		$json['status']='error';
		$json['message']='Complete the reCaptcha first';
		$error = true;
	} else {
		$json['status']='true';
	}
}

function getCaptchaResponse( $response )
{
	$url="https://www.google.com/recaptcha/api/siteverify";

	$fields = array(
		'secret'   => RECAPTCHA_PRIVATE,
		'response' => $response,
		'remoteip' => $_SERVER['REMOTE_ADDR']
	);

	foreach( $fields as $key => $value ) {
		$fields_string .= $key . '=' . $value . '&';
	}
	rtrim( $fields_string, '&' );

	$ch = curl_init( $url );

	curl_setopt($ch,CURLOPT_POST, count($fields));
	curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
	curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

	$content = curl_exec( $ch );
	$err     = curl_errno( $ch );
	$errmsg  = curl_error( $ch );
	$header  = curl_getinfo( $ch );

	curl_close( $ch );

	$header['errno']   = $err;
	$header['errmsg']  = $errmsg;
	$header['content'] = json_decode($content,true);
	return $header;
}



function callScript( $script )
{

    $ch = curl_init( $script );

    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);

    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );

    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = json_decode($content,true);
    return $header;
}


/*
*   Send email function
*    $params
            string $to -- email address of recipient
            string $subject --- email subject
            string $message -- rich html markup or plain text for email message
            int $adId -- id of ad (maybe used for misc)
            array $attachments -- array of arrays in the format ("filename"=>"full path to file, with DOMAIN!", "name"=>"Your invoice")
     returns a status code
        1 = 'success'
        2 = no email given
        3 = problem sending email
        4 = user was blacklisted
*/
function send_email( $to, $subject, $messageContent, $adID = 0, $attachments = array() )
{
	global $user;
	$token = generate_random_string(32);
	$optOut = SITE_URL . '/account/optout?t=' . $token;
	$flagAbuse = SITE_URL . '/account/flag?t=' . $token;

	$return[1] = array('status'=>1,'message'=>'Success');
	$return[2] = array('status'=>2,'message'=>'No email address');
	$return[3] = array('status'=>3,'message'=>'Problem sending email');
	$return[4] = array('status'=>4,'message'=>'Recipient was blacklisted');
	$return[5] = array('status'=>5,'message'=>'Too many email flags');

	// validate email field exists
	if( trim($to) == '' ) {
		return $return[2];
	}
	// check email address against blacklist
	$sql="SELECT * FROM blacklist WHERE email = '".addslashes($to)."'";
	$dbCheck = dbselectmulti($sql);
	if( $dbCheck['numrows'] > 0 ) {
		return $return[4];
	}
	// check user for abuse flags
	if( $user['email_flags'] >= EMAIL_ABUSE_LIMIT ) {
		return $return[5];
	}

	// get email options for address display
	$emailOptions = array();
	$sql = "SELECT * FROM email_info WHERE id = 1";
	$dbArr = dbselectsingle($sql);
	if( $dbArr['numrows'] > 0 ) {
		$emailOptions = $dbArr['data'];
	}


	// generate message start
	$messageStart = '';
	$messageStart .= '<html>';
	$messageStart .= '<head>';
	$messageStart .= '<title>' . $adTitle . ' | ' . SITE_NAME . '</title>';
	$messageStart .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
	$messageStart .= '<style type="text/css">';
	$messageStart .= '#outlook a { padding: 0; } ';
	$messageStart .= 'body { width: 100% !important; -webkit-text-size-adjust: none; -ms-text-size-adjust: none; margin: 0; padding: 0; } ';
	$messageStart .= '.ReadMsgBody { width: 100%; } ';
	$messageStart .= '.ExternalClass { width: 100%; } ';
	$messageStart .= '.backgroundTable { margin: 0 auto; padding: 0; width: 100% !important; } ';
	$messageStart .= 'table td { border-collapse: collapse; } ';
	$messageStart .= '.ExternalClass * { line-height: 115%; } ';
	$messageStart .= '</style>';
	$messageStart .= '</head>';
	$messageStart .= '<body bgcolor="#FFFFFF" marginwidth="0" marginheight="0" style="zoom: 100%;">';
	// generate message end
	$messageEnd = '';
	$messageEnd .= '<table border="0" cellpadding="0" width="600" cellspacing="0">';
	$messageEnd .= '<tbody>';
	$messageEnd .= '<tr><td>&nbsp;</td></tr>';
	$messageEnd .= '<tr><td>Questions? Send an email to: <a href="mailto:' . MAIL_FROM_ADDRESS . '">' . MAIL_FROM_NAME . '</a></td></tr>';
	$messageEnd .= '<tr><td>Visit us online by clicking: <a href="' . SITE_URL . '">' . SITE_NAME . '</a></td></tr>';
	$messageEnd .= '<tr><td>To stop receiving emails from ' . SITE_NAME . ', click this link to <a href="' . $optOut . '">opt-out</a>.</td></tr>';
	$messageEnd .= '<tr><td>If this email is offensive to you in any way, you may click this link to <a href="' . $flagAbuse . '">report abuse</a>.</td></tr>';
    $messageEnd .= '<tr><td>' . SITE_NAME . ' is located at ' . $emailOptions['address'] . '</td></tr>';
	$messageEnd .= '<tr><td>&nbsp;</td></tr>';
	$messageEnd .= '</tbody>';
	$messageEnd .= '</table>';
	$messageEnd .= '</body>';
	$messageEnd .= '</html>';


	// compose full message
	$message = $messageStart . $messageContent . $messageEnd;

    //for DEVELOPMENT PURPOSES
    if(MODE=='testing'){$to='jhansen@pioneernewsgroup.com';}
    
	$mail = new htmlMimeMail();
	$mail->setHtml($message);
	$mail->setFrom(MAIL_FROM_ADDRESS);
	$mail->setSubject($subject);
	$mail->setHeader('Sender',MAIL_FROM_NAME);
	
    if(!empty($attachments))
    {
       foreach($attachments as $attachID=>$attachment)
        {
            $attachment = array_merge(array('type'=>'application/octet-stream'),$attachment);
            $file = file_get_contents($attachment['filename']);
            $mail->addAttachment($file,$attachment['name'], $attachment['type']);
        }
    }
    
    if( $mail->send( array($to), 'mail' ) ) {
		//create an email log record only if we are successful in sending the message
		$sql="INSERT INTO email_log (ad_id, user_id, recipient, subject, token, send_time, message)
				VALUES ('$adID', '$user[id]', '".addslashes($to)."', '".addslashes($subject)."', '".addslashes($token)."', '".date("Y-m-d H:i:s")."', '".addslashes($message)."')";
		$dbInsert = dbinsertquery($sql);

		return $return[1];
	} else {
		return $return[3];
	}
}


/*
*  Funtion to generate rail items for promotion, etc.
*  Pass in position
        left, right-top, right-middle, leader, footer
*/
function rail_item($pos='right-top')
{
    $positions['left']=array('width'=>236,'height'=>300);
    $positions['right-top']=array('width'=>300,'height'=>250);
    $positions['right-middle']=array('width'=>300,'height'=>250);
    $positions['leader']=array('width'=>864,'height'=>90);
    $positions['footer']=array('width'=>865,'height'=>90);

    $sql="SELECT * FROM rail_items WHERE rail_position = '$pos' AND active=1";
    $dbItems = dbselectmulti($sql);
    if($dbItems['numrows']>0)
    {
        foreach($dbItems['data'] as $item)
        {
            print "<div id='rail-item-$pos-$item[id]' class='center-block rail-item-custom-wrap'>";

            //first case is an image
            if($item['filename']!='')
            {
                $imgSrc = "/uploads/rail-items/".stripslashes($item['filename']);
                $link = stripslashes($item['link']);
                print "<a href='$link'><img src='$imgSrc' class='img img-responsive' border=0 /></a>";
            } elseif($item['html_only']!='')
            {
               print html_entity_decode($item['html_only']);
            } else {
                $backgroundColor = stripslashes($item['background_color']);
                $borderColor = stripslashes($item['border_color']);
                $buttonColor = stripslashes($item['button_color']);
                $buttonHoverColor = stripslashes($item['button_hover_color']);
                $buttonTextColor = stripslashes($item['button_text_color']);
                $textColor = stripslashes($item['text_color']);
                $headlineColor = stripslashes($item['headline_color']);

                $headline = "<h4 style='color:$headlineColor;'>".stripslashes($item['headline'])."</h4>";
                $content = "<div style='color:$textColor;'>".stripslashes($item['content'])."</div>";
                $buttonText = stripslashes($item['button_text']);
                $link = stripslashes($item['link']);

                print "<style>
	                #rail-item-$pos-$item[id] .rail-item-custom-button {
	                    color:$buttonTextColor;
	                    background-color:$buttonColor;
	                }
	                #rail-item-$pos-$item[id] .rail-item-custom-button:hover {
	                    color:$buttonTextColor;
	                    background-color:$buttonHoverColor;
	                }
                </style>\n";
                print "<div class='rail-item-custom clearfix' style='background-color:$backgroundColor;border-color:$borderColor;'>\n";
                    print $headline;
                    print "<div class='rail-item-custom-content'>" . $content . "</div>\n";
                    print "<a class='rail-item-custom-button custom-button-$pos' href='$link'>$buttonText</a>\n";
                print "</div>";
            }
            print "</div>\n";
        }
    }

}

function dfp_ads($position = 'leader')
{
    global $dbDFP;
    if($dbDFP['numrows']>0)
    {
        foreach($dbDFP['data'] as $ad)
        {
            if($ad['location']==$position)
            {
                switch($ad['location'])
                    {
                        case 'leader':
                            $size = "width:728px;";
                        break;

                        case 'footer':
                            $size  = "width:728px;";
                        break;

                        case 'left':
                            $size  = "width:160px;";
                        break;

                        case 'right-top':
                            $size  = "width:300px;";
                        break;

                        case 'right-bottom':
                            $size  = "width:300px;";
                        break;
                    }
                ?>
           <div id='div-gpt-ad-<?= GOOGLE_DFP_KEY."-".$ad['id'] ?>' style='<?= $size ?>'>
            <script>
            googletag.cmd.push(function() { googletag.display('<?= GOOGLE_DFP_KEY."-".$ad['id'] ?>'); });
            </script>
            </div>
           <?php
            }
        }
    }
}


function menuItems($place='top',$parentID=0)
{
    $menuItems = array();
    $allowNest = false;
    switch($place)
    {
        case 'top':
           $filter = "AND show_top=1";
           $allowNest = true;
        break;

        case 'mobile':
            $filter = "AND show_mobile=1";
            $allowNest = true;
        break;

        case 'footer':
             $filter = "AND show_footer=1";
        break;

    }
    $sql="SELECT * FROM site_menu WHERE parent_id=$parentID $filter ORDER BY sort_order ASC";

    $dbMenuItems = dbselectmulti($sql);
    if($dbMenuItems['numrows']>0)
    {
        foreach($dbMenuItems['data'] as $menuItem)
        {
            $subMenu = array();
            //look for a few things
            if($menuItem['map_id']!=0)
            {
                $sql="SELECT * FROM maps WHERE id=$menuItem[map_id]";
                $dbMap = dbselectsingle($sql);
                $map = $dbMap['data'];
                if($map['active'])
                {
                    $name = stripslashes($menuItem['name']);
                    $url = "/maps/?map_id=$menuItem[map_id]";
                    $menuItems[]=array('name'=>$name,'url'=>$url);
                }
            } elseif($menuItem['static_id']!=0)
            {
                $sql="SELECT * FROM static_pages WHERE id=$menuItem[static_id]";
                $dbPage = dbselectsingle($sql);
                $page = $dbPage['data'];
                $name = stripslashes($page['headline']);
                $url = "/static/".stripslashes($page['slug']);
                $menuItems[]=array('name'=>$name,'url'=>$url);
            } elseif($menuItem['article_category_id']!=0)
            {
                $sql="SELECT * FROM article_categories WHERE id=$menuItem[article_category_id]";
                $dbPage = dbselectsingle($sql);
                $page = $dbPage['data'];
                $name = stripslashes($page['category']);
                $url = "/articles/".stripslashes($page['slug']);
                $menuItems[]=array('name'=>$name,'url'=>$url);
            } else {
                $name = stripslashes($menuItem['name']);

                //see if it has any children
                $sql="SELECT * FROM site_menu WHERE parent_id=$menuItem[id] $filter ORDER BY sort_order ASC";
                $dbSubMenu = dbselectmulti($sql);
                if($dbSubMenu['numrows']>0 && $allowNest)
                {
                    $url = '#';
                    $subMenu = menuItems($place,$menuItem[id]);
                } else {
                    $url = stripslashes($menuItem['url']);
                }
                $menuItems[]=array('name'=>$name,'url'=>$url, 'submenus'=>$subMenu);
            }
        }
    }
    if(($place=='top' || $place=='mobile') && $parentID==0)
    {
        if( $_SESSION['loggedin'] ) {
           $menuItems[]=array('name'=>'My Account', 'page'=>'account', 'url'=>'/account/','submenus'=>array());
        } else {
           $menuItems[]=array('name'=>'Login/Register', 'page'=>'login', 'url'=>'/account/login/','submenus'=>array());
        }
    }
    return $menuItems;
}

function postToFacebook($link,$message,$image="")
{
    //print "In function<br>";
    require_once(VENDOR_DIR."/autoload.php");
    //print "...Initializing FB Class<br>";

    $fb = new \Facebook\Facebook([
     'app_id' => FACEBOOK_APP_ID,
     'app_secret' => FACEBOOK_SECRET,
     'default_graph_version' => 'v2.2',
     'default_access_token' => FACEBOOK_TOKEN
    ]);
    //print "...Initialized FB Class<br>";
    //Post property to Facebook
    $linkData = [
     'link' => $link,
     'message' => $message
    ];
    //print "...Starting the post<br>";
    try {
     $response = $fb->post('/me/feed', $linkData);
    // print "...Successfully posted<br>";
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
    // echo '...Graph returned an error: '.$e->getMessage();
     exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
    // echo '...Facebook SDK returned an error: '.$e->getMessage();
     exit;
    }
    $graphNode = $response->getGraphNode();

}

//@TODO build out the twitter post function
function postToTwitter($link,$message)
{
    
}


/*
*   This function is called on all detail pages and adds the necessary OG information for Facebook sharing
*/
function facebookOG($ad)
{
    ?>    <meta property="og:url"         content="<?= SITE_URL ?>/detail/?ad=<?= $ad['id'] ?>" />
    <meta property="og:type"        content="product" />
    <meta property="og:title"       content="<?= $ad['headline'] ?>" />
    <meta property="og:description" content="<?= strip_tags($ad['ad_text']) ?>" />
    <?php
        if(count($ad['images']>0))
        {
            $image = $ad['images'][0]['display'];
?>    <meta property="og:image"       content="<?= SITE_URL ?>/uploads/<?= $image ?>" />
            <?php
        }
    ?>
    <?php
}

/*
*  This function takes a template id, and a passed array of additional information, and builds out the basic body of an email.
*  The email send function then appends any header/footer that is necessary to the email itself before sending it on.
 //replacements
    /*
    <p>%name% - user full name</p>
    <p>%first% - user first name</p>
    <p>%last% - user last name</p>
    <p>%date% - current date</p>
    <p>%email% - user email</p>
    <p>%site_name% - site name</p>
    <p>%site_url% - site url</p>
    <p>%contact_email% - site contact email</p>
    <p>%contact_phone% - site contact phone</p>
    <p>%confirm_link% - registration confirmation link</p>
    <p>%reset_link% - password reset link</p>
    <p>%bill% - add bill + attach a PDF</p>
    returns array('subject', 'body');
*/
function email_generator($id,$data=array())
{
    global $user;
    array_merge($user, $data);
    
    $sql="SELECT * FROM email_templates WHERE id=$id";
    $dbTemplate = dbselectsingle($sql);
    $template = $dbTemplate['data'];
    $subject = stripslashes($template['subject']);
    $body = stripslashes($template['body']);
    $date = date("F j, Y");

    $confirmLink = " <a href='".SITE_URL."/account/confirm/?t=".$data['token']."'>Click here to activate your account.</a>

      If the link does not work, copy and paste the following into your web browsers address bar:

      ".SITE_URL."/account/confirm/?t=".$data['token']."\r\n";

    $resetLink = " <a href='".SITE_URL."/account/reset/?t=".$data['token']."'>Click here to reset your account.</a>

      If the link does not work, copy and paste the following into your web browsers address bar:

      ".SITE_URL."/account/reset/?t=".$data['token']."\r\n";
    
    $approveLink = " <a href='".SITE_URL."/admin/approvePending.php/?id=".$data['id']."'>Click here to approve the ad.</a>

      If the link does not work, copy and paste the following into your web browsers address bar:

      ".SITE_URL."/account/reset/?t=".$data['token']."\r\n";

    $subject = str_replace('%name%',$data['name'],$subject);
    $subject = str_replace('%first%',$data['first'],$subject);
    $subject = str_replace('%last%',$data['last'],$subject);
    $subject = str_replace('%date%',$date,$subject);
    $subject = str_replace('%email%',$data['email'],$subject);
    $subject = str_replace('%site_name%',SITE_NAME,$subject);
    $subject = str_replace('%site_url%',SITE_URL,$subject);
    $subject = str_replace('%contact_email%',CONTACT_EMAIL,$subject);
    $subject = str_replace('%contact_phone%',CONTACT_PHONE,$subject);


    $body = str_replace('%name%',$data['name'],$body);
    $body = str_replace('%first%',$data['first'],$body);
    $body = str_replace('%last%',$data['last'],$body);
    $body = str_replace('%date%',$date,$body);
    $body = str_replace('%email%',$data['email'],$body);
    $body = str_replace('%street%',$data['street'],$body);
    $body = str_replace('%city%',$data['city'],$body);
    $body = str_replace('%state%',$data['state'],$body);
    $body = str_replace('%zip%',$data['zip'],$body);
    $body = str_replace('%phone%',$data['phone'],$body);
    $body = str_replace('%site_name%',SITE_NAME,$body);
    $body = str_replace('%site_url%',SITE_URL,$body);
    $body = str_replace('%contact_email%',CONTACT_EMAIL,$body);
    $body = str_replace('%contact_phone%',CONTACT_PHONE,$body);
    $body = str_replace("%confirm_link%",$confirmLink,$body);
    $body = str_replace("%reset_link%",$resetLink,$body);
    $body = str_replace("%approve_link%",$approveLink,$body);
    $body = str_replace("%bill%",$data['bill'],$body);
    $body = str_replace("%ad%",$data['ad'],$body);
    $body = str_replace("%message%",$data['message'],$body);

    return array('subject'=>$subject,'body'=>$body);

}


function header_icons()
{
	global $config;

	$return .= '<link rel="icon" type="image/png" sizes="16x16" href="/favicons/favicon-16x16.png">' . "\n";
	$return .= '<link rel="icon" type="image/png" sizes="24x24" href="/favicons/favicon-24x24.png">' . "\n";
	$return .= '<link rel="icon" type="image/png" sizes="32x32" href="/favicons/favicon-32x32.png">' . "\n";
	$return .= '<link rel="icon" type="image/png" sizes="48x48" href="/favicons/favicon-48x48.png">' . "\n";
	$return .= '<link rel="icon" type="image/png" sizes="64x64" href="/favicons/favicon-64x64.png">' . "\n";
	$return .= '<link rel="icon" sizes="192x192" href="/favicons/touch-icon-192x192.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="180x180" href="/favicons/apple-touch-icon-180x180-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="152x152" href="/favicons/apple-touch-icon-152x152-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="/favicons/apple-touch-icon-144x144-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="120x120" href="/favicons/apple-touch-icon-120x120-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="/favicons/apple-touch-icon-114x114-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="76x76" href="/favicons/apple-touch-icon-76x76-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="/favicons/apple-touch-icon-72x72-precomposed.png">' . "\n";
	$return .= '<link rel="apple-touch-icon-precomposed" href="/favicons/apple-touch-icon-precomposed.png"><!-- 57×57px -->' . "\n";
	$return .= '<meta name="msapplication-TileColor" content="#' . $config['color_highlight'] . '">' . "\n";
	$return .= '<meta name="msapplication-TileImage" content="/favicons/favicon-144.png">' . "\n";

	echo $return;
}

function hexToHsl($hex) {
    $hex = array($hex[0].$hex[1], $hex[2].$hex[3], $hex[4].$hex[5]);
    $rgb = array_map(function($part) {
        return hexdec($part) / 255;
    }, $hex);

    $max = max($rgb);
    $min = min($rgb);

    $l = ($max + $min) / 2;

    if ($max == $min) {
        $h = $s = 0;
    } else {
        $diff = $max - $min;
        $s = $l > 0.5 ? $diff / (2 - $max - $min) : $diff / ($max + $min);

        switch($max) {
            case $rgb[0]:
                $h = ($rgb[1] - $rgb[2]) / $diff + ($rgb[1] < $rgb[2] ? 6 : 0);
                break;
            case $rgb[1]:
                $h = ($rgb[2] - $rgb[0]) / $diff + 2;
                break;
            case $rgb[2]:
                $h = ($rgb[0] - $rgb[1]) / $diff + 4;
                break;
        }

        $h /= 6;
    }

    return array($h, $s, $l);
}

function hslToHex($hsl)
{
    list($h, $s, $l) = $hsl;

    if ($s == 0) {
        $r = $g = $b = 1;
    } else {
        $q = $l < 0.5 ? $l * (1 + $s) : $l + $s - $l * $s;
        $p = 2 * $l - $q;

        $r = hue2rgb($p, $q, $h + 1/3);
        $g = hue2rgb($p, $q, $h);
        $b = hue2rgb($p, $q, $h - 1/3);
    }

    return rgb2hex($r) . rgb2hex($g) . rgb2hex($b);
}

function hue2rgb($p, $q, $t) {
    if ($t < 0) $t += 1;
    if ($t > 1) $t -= 1;
    if ($t < 1/6) return $p + ($q - $p) * 6 * $t;
    if ($t < 1/2) return $q;
    if ($t < 2/3) return $p + ($q - $p) * (2/3 - $t) * 6;

    return $p;
}

function rgb2hex($rgb) {
    return str_pad(dechex($rgb * 255), 2, '0', STR_PAD_LEFT);
}


function themeGenerator()
{
    global $config;

    // db vars
    $fontHeader = HEADING_FONT_CSS;
    $fontBody = BODY_FONT_CSS;
    $themeAura = $config['theme_aura'];
    $customColorBackground = $config['color_background'];
    $customColorText = $config['color_text'];
    $siteLogo = stripslashes($config['site_logo_main']);
    $mobileLogo = stripslashes($config['site_logo_mobile']);
    $siteHeader = stripslashes($config['header_image']);
    $colorHighlight = str_replace("#","",$config['color_highlight']);

    // set more vars
    $colorHighlight = hexToHsl($colorHighlight);
    $clr_pri_H = ceil($colorHighlight[0]*360);
    $clr_pri_S = ceil($colorHighlight[1]*100);
    $clr_pri_L = ceil($colorHighlight[2]*100);
    $clr_pri_alt = ceil($colorHighlight[2]*66); //darken
    $clr_admin_H = 211;
    $clr_admin_S = 89;
    $clr_admin_L = 33;
    $clr_admin_D = $clr_admin_L * 0.66; // darker version of admin color

    // standard light theme (default)
    $color_bg = '#f8f8f8';
    $color_text = '#222';
    $color_pri1 = 'hsl( '.$clr_pri_H.', '.$clr_pri_S.', '.$clr_pri_L.' )';
    $color_pri2 = 'hsl( '.$clr_pri_H.', '.$clr_pri_S.', '.$clr_pri_alt.' )';
    $color_admin1 = 'hsl( '.$clr_admin_H.', '.$clr_admin_S.', '.$clr_admin_L.' )';
    $color_admin2 = 'hsl( '.$clr_admin_H.', '.$clr_admin_S.', '.$clr_admin_D.' )';
    $color_text_em = '#000';
    $color_border = '#bbb';
    $color_focus = 'rgba(255,255,255,1)';
    $color_feature = 'rgba(255,255,255,0.8)';
    $color_normal = 'rgba(255,255,255,0.2)';
    $color_theme_tr = 'rgba(255,255,255,0.6)';
    $color_btn_inv = '#fff';

    if( $themeAura == 'std_dark' ) {
        $clr_admin_L = 66;
        $clr_admin_D = $clr_admin_L * 0.66; // darker version of admin color
        $clr_pri_alt = ceil($colorHighlight[2]*133); // lighten

        // set standard dark theme
        $color_bg = '#444';
        $color_text = '#e0e0e0';
        $color_pri1 = 'hsl( '.$clr_pri_H.', '.$clr_pri_S.', '.$clr_pri_L.' )';
        $color_pri2 = 'hsl( '.$clr_pri_H.', '.$clr_pri_S.', '.$clr_pri_alt.' )';
        $color_admin1 = 'hsl( '.$clr_admin_H.', '.$clr_admin_S.', '.$clr_admin_L.' )';
        $color_admin2 = 'hsl( '.$clr_admin_H.', '.$clr_admin_S.', '.$clr_admin_D.' )';
        $color_text_em = '#fff';
        $color_border = '#888';
        $color_focus = 'rgba(0,0,0,0.3)';
        $color_feature = 'rgba(0,0,0,0.3)';
        $color_normal = 'rgba(0,0,0,0.1)';
        $color_theme_tr = 'rgba(0,0,0,0.6)';
        $color_btn_inv = '#222';
    }

    if( $customColorBackground != '' ) {
        // override theme background color
        $color_bg = $customColorBackground;
    }
    if( $customColorText != '' ) {
        // override theme text color
        $color_text = $customColorText;
    }


    $theme = "
    //THEME FONTS
    \$fnt_heading: $fontHeader;
    \$fnt_body: $fontBody;

    //THEME COLORS
    \$clr_sitebg: $color_bg;
    \$clr_text: $color_text;
    \$clr_text_em: $color_text_em;
    \$clr_border: $color_border;
    \$clr_focus: $color_focus;
    \$clr_feature: $color_feature;
    \$clr_normal: $color_normal;
    \$clr_theme_tr: $color_theme_tr;
    \$clr_btn_inv: $color_btn_inv;
    \$clr_pri1: $color_pri1;
    \$clr_pri2: $color_pri2;
    \$clr_admin1: $color_admin1;
    \$clr_admin2: $color_admin2;

    //SITE IMAGES
    \$logo: '/img/$siteLogo';
    \$logo_mobile: '/img/$mobileLogo';
    \$header_bg: '/img/$siteHeader';
    ";

    file_put_contents("../sass/_010-theme.scss",$theme);
}


function getLocations()
{
    $sql="SELECT * FROM locations ORDER BY location_name";
    $dbLocations = dbselectmulti($sql);
    return $dbLocations['data'];
}


function convertCSVtoArray($fileContent,$escape = '\\', $enclosure = '"', $delimiter = ',')
{
    $lines = array();
    $fields = array();

    if($escape == $enclosure)
    {
        $escape = '\\';
        $fileContent = str_replace(array('\\',$enclosure.$enclosure,"\r\n","\r"),
                    array('\\\\',$escape.$enclosure,"\\n","\\n"),$fileContent);
    }
    else
        $fileContent = str_replace(array("\r\n","\r"),array("\\n","\\n"),$fileContent);

    $nb = strlen($fileContent);
    $field = '';
    $inEnclosure = false;
    $previous = '';

    for($i = 0;$i<$nb; $i++)
    {
        $c = $fileContent[$i];
        if($c === $enclosure)
        {
            if($previous !== $escape)
                $inEnclosure ^= true;
            else
                $field .= $enclosure;
        }
        else if($c === $escape)
        {
            $next = $fileContent[$i+1];
            if($next != $enclosure && $next != $escape)
                $field .= $escape;
        }
        else if($c === $delimiter)
        {
            if($inEnclosure)
                $field .= $delimiter;
            else
            {
                //end of the field
                $fields[] = $field;
                $field = '';
            }
        }
        else if($c === "\n")
        {
            $fields[] = $field;
            $field = '';
            $lines[] = $fields;
            $fields = array();
        }
        else
            $field .= $c;
        $previous = $c;
    }
    //we add the last element
    if(true || $field !== '')
    {
        $fields[] = $field;
        $lines[] = $fields;
    }
    return $fields;
}
