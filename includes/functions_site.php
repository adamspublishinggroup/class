<?php
  /*
  This file has the pricing for ads. It will be unique to any particular system
  */

  /*
  *  Returns as array
            total_price = 0.00,
            word_count = 0.00,
            discount = 0.00,
            bold_count = 0.00,
            promo_code = '',
            base_cost = 0.00,
            photo_count = 0,
            word_cost = 0.00,
            photo_cost = 0.00
  */
  function adPricing($adID)
  {
      global $config;
      /*
      * This pricing is as defined by the Nickel in LaGrande, Or
      *
      *
      */
      
      //look up the ad
      $sql="SELECT * FROM ads WHERE id=$adID";
      $dbAd = dbselectsingle($sql);
      $ad = $dbAd['data'];
      
      
      $commercial = 0;
      $freeAd = false;
      $includedAddons = array();
      
      //print pricing
      $boldWordPrice = 0.50;
      $italicWordPrice = 0.50;
      $underlineWordPrice = 0.50;
      
      
      $baseWordCount = 15;
      $photoPrice = 3.00; //cost per photo
      $additionalWord = .25;
      
      $addonCharges = 0;
      $pictureCount = 0;
      
      $thirdPartyCost = 0;
      $adCost = 0;
      
      
      //get information about the package assigned to the ad
      $packageID = $ad['package_id'];
      if($packageID!=0)
      {
          $sql="SELECT * FROM packages WHERE id=$packageID";
      } else {
          //grab the default package in this case
          $sql="SELECT * FROM packages WHERE default=1";
      }
      $dbPackage = dbselectsingle($sql);
      $package = $dbPackage['data'];
      
      $basePrice = $package['package_price'];
      $basePrice = $package['package_price'];
      $daysIncluded = $package['run_length'];
      $wordsIncluded = $package['words_included'];
      $additionalWord = $package['per_word_price'];
      $commercial = $package['commercial'];
      
      //get any addons included in the package
      $sql="SELECT * FROM package_addons WHERE package_id=$packageID";
      $dbAddons = dbselectmulti($sql);
      if($dbAddons['numrows']>0)
      {
          foreach($dbAddons['data'] as $addon)
          {
            $includedAddons[]=$addon['id'];    
          }          
      }
      
      
      //get ad categories
      $catCost = 0;
      $catCount = 0;
      $sql="SELECT A.*, B.free_ads, B.category_name FROM ad_category_xref A, categories B WHERE A.category_id = B.id AND A.ad_id=$adID";
      $dbCats = dbselectmulti($sql);
      $cats = array();
      if($dbCats['numrows']>0)
      {
          foreach($dbCats['data'] as $cat)
          {
              $cats[]=stripslashes($cat['category_name']);
              if($cat['free_ads']==1){$freeAd = true;}
              $catCount++;
          }
      }
      
      if($catCount>1)
      {
          if($commercial)
          {
              $catCost = 7 * ($catCount - 1);  
          } else {
              $catCost = 5 * ($catCount - 1);
          }
      }
      //get the user
      $sql="SELECT * FROM users WHERE id='$ad[user_id]'";
      $dbUser = dbselectsingle($sql);
      $user = $dbUser['data'];
      
      
      //do we give free ads to veterans?
      if($config['veteran_free'])
      {
          //see if the user is a veteran
          if($user['veteran']) {
              //ok, user is a vertan, let's see how many ads they have placed in the last month
              $currentMonth = date("m");
              $firstOfMonth = date("Y-m-\0\1");
              $days = date("t");
              $lastOfMonth = date("Y-m")."-".$days;
              
              $sql="SELECT count(id) ad adcount FROM ads WHERE user_id = $ad[user_id] AND start_date>='$firstOfMonth' AND start_date<='$lastOfMonth' AND published = 1";
              $dbCount = dbselectsingle($sql);
              $count = $dbCount['data']['adcount'];
               
              if($count<$config['veteran_free_count'])
              {
                $return['Veteran discount'] = '100%';
                $freeAd = true;
              } else {
                $return['Veteran discount'] = 'No more free ads this month';
                  //they exceed the number of free ads
              }
          }
      }
      
      //check for other discounts
      if($user['senior'])
      {
          $seniorDiscount = $config['senior_discount'];
          $return['Senior discount'] = $seniorDiscount.'% off';
      }
      
      //if user is tied to a non-profit business
      if($user['advertiser_id']!=0)
      {
          //look up the advertiser
          $sql="SELECT * FROM advertisers WHERE id = $user[advertiser_id]";
          $dbAdvertiser = dbselectsingle($sql);
          $advertiser = $dbAdvertiser['data'];
          if($advertiser['non_profit'])
          {
               $nonprofitDiscount = $config['non_profit_discount'];
               $return['Non-profit discount'] = $nonprofitDiscount.'% off';
          }
      }
      
      //check for promo code
      $promoCode = $_POST['promo_code'];
      $promo = array();
      if($promoCode!='')
      {
          $promoCode = addslashes(trim(strtolower($promoCode)));
          $sql="SELECT * FROM promos WHERE promo_code='$promoCode' AND active=1";
          $dbPromo = dbselectsingle($sql);
          if($dbPromo['numrows']>0)
          {
              $promo = $dbPromo['data'];
              $return['Promo Code']=$promoCode;
          }   
      }elseif($ad['promo_code']!='')
      {
          $promoCode = $ad['promo_code'];
          $sql="SELECT * FROM promos WHERE promo_code='$promoCode' AND active=1";
          $dbPromo = dbselectsingle($sql);
          if($dbPromo['numrows']>0)
          {
              $promo = $dbPromo['data'];
              $return['Promo Code']=$promoCode;
          }  
      }
      
      if($promo['free_ad']){$freeAd = true; }
      
      //get ad information
      $adWordCount = $ad['word_count'];
      
      //bold words
      $adBoldCount = $ad['bold_count'];
      
      //italic words
      $adItalicCount = $ad['italic_count'];
      
      //underline words
      $adUnderlineCount = $ad['underline_count'];
      
      $onlineDays = $ad['days_online'];
      $printDays = $ad['days_print'];
      
      
      //how many pics?
      $sql="SELECT id FROM images WHERE ad_id=$adID";
      $dbPics = dbselectmulti($sql);
      $pictureCount = $dbPics['numrows'];
      
      //check for an attention getter in the ad
      if($ad['attn_getter']!='')
      {
          $pictureCount++;
      }
      
      //we start by getting only those addons that do NOT charge a percentage of the base ad
      $sql="SELECT A.* FROM addons A, ad_addons B WHERE A.id=B.addon_id AND A.percentage_charge=0 AND B.ad_id=$adID";
      $dbAddons = dbselectmulti($sql);
      if($dbAddons['numrows']>0)
      {
          $addon = $dbAddons['data'];
          
          foreach($dbAddons['data'] as $addon)
          {
              if(in_array($addon['id'],$includedAddons))
              {
                  $return[$addon['addon_short']]="Included in package"; 
              } else {
                  if($addon['charge_per_print']){
                      $addCharge = $addon['flat_charge']*$printDays;
                      $addonCharges+=$addCharge;
                  } else {
                      $addCharge = $addon['flat_charge'];
                      $addonCharges+=$addCharge;
                  }
                  $return[$addon['addon_short']]=money_format('%(#4n', $addCharge);
              }
          }
      }
      
      
      //ok, calculate without promo
      $overageWordsNoPromo = ($adWordCount<=$baseWordCount ? 0 : $adWordCount - $baseWordCount);
      $overageCostNoPromo = $overageWordsNoPromo * $additionalWord;
      
      //now word count taking promo into consideration
      $baseWordCount += $promo['free_words']; //add the free words to the base word count
      $overageWords = ($adWordCount<=$baseWordCount ? 0 : $adWordCount - $baseWordCount);
      $overageCost = $overageWords * $additionalWord;
      
      $return['Base Price']=money_format('%(#4n', $basePrice);
      
      if($overageWords > 0)
      {
          $return['Extra Words']=$overageWords;
          $return['Extra Word Cost']=money_format('%(#4n', $overageCost) ;
      }
      
      //style costs
      $boldCost = $adBoldCount * $boldWordPrice;
      $italicCost = $adItalicCount * $italicWordPrice;
      $underlineCost = $adUnderlineCount * $underlineWordPrice;
      
      $styleCost = $boldCost + $italicCost + $underlineCost;        
      
      $return['# of Publish Weeks']=ceil($printDays/7);
      if($pictureCount>0)
      {
        $return['# of pictures']=$pictureCount;
      }
      
      
      
      if($freeAd)
      {
          $adCost = 0;
          $return['Total cost'] =  "Your ad is FREE!";
          $sql = "UPDATE ads SET discount = 0, promo_code = '$promoCode', free_ad = 1, ad_total = 0, other_paper_total = 0 WHERE id = $adID";
          $dbUpdate = dbexecutequery($sql);
              
      } else {
          //see if the promo is offering photos for free
          $freePics = 0;
          if($promo['free_pictures'] > 0)
          {
              $freePics = $promo['free_pictures'];
              $return['Free pictures from promo'] = $promo['free_pictures'];
          }
          $picCost = ($freePics > $pictureCount ? 0 : ($pictureCount - $freePics) * $photoPrice);
          $hPicCost = $pictureCount * $photoPrice;
          
          
          //how many print days?
          if($promo['free_days_print']!=0)
          {
              $freeDays = $promo['free_days_print'];
              if($printDays <= $freeDays) { $totalPrintDays = 0; } else { $totalPrintDays = $printDays - $freeDays; } 
          } else {
              $totalPrintDays = $printDays;
          }
          
          if($promo['discount']!='' && $promo['discount']>0)
          {
              $discountPer = $promo['discount'];
              $overallDiscount = 0;
          } else {
              $discountPer = 0;
              $overallDiscount = 0;
          }
          
          if($catCost>0)
          {
              $return['Charge for additional categories']=money_format('%(#4n', $catCost);
          }
          
          //figure cost without any discount or addons at this point
          $noDiscountDayCost = $basePrice + $overageCostNoPromo + $styleCost + $hPicCost + $catCost;
          $noDiscountCost = round(($noDiscountDayCost * $printDays),2);
          
          //now get any addons that add a percentage of the overall cost
          $sql="SELECT A.* FROM addons A, ad_addons B WHERE A.id=B.addon_id AND A.percentage_charge>0 AND B.ad_id=$adID";
          $dbAddons = dbselectmulti($sql);
          if($dbAddons['numrows']>0)
          {
              if(in_array($addon['id'],$includedAddons))
              {
                  $return[$addon['addon_short']]="Included in package"; 
              } else {
                  foreach($dbAddons['data'] as $addon)
                  {
                      if($addon['charge_per_print']){
                          $addCharge = round((($noDiscountDayCost * $addon['percentage_charge']/100)*$printDays),2);
                      } else {
                          $addCharge = round(($noDiscountCost * $addon['percentage_charge']/100),2);
                      }
                      if($addon['third_party']){
                           $thirdPartyCost+= $addCharge;
                           $thirdParty = true;
                      } else {
                          $addonCharges+=$addCharge;
                      }
                      $return[$addon['addon_short']]=money_format('%(#4n', $addCharge);
                  }
              }
          }
          //finally we add the overall + $addonCharges to the $noDiscountCost
          $noDiscountCost = round(($noDiscountCost + $addonCharges), 2);
          
          
          //calculate the discounted ad price
          $discountDayCost = $basePrice + $overageCost + ($promo['free_bold'] ? 0 : $styleCost) + $picCost + $catCost;
          $discountAdPrintCost = ($discountDayCost * $totalPrintDays) + $addonCharges;
          $discountAdPrintCost = round($discountAdPrintCost - ($discountAdPrintCost * ($discountPer / 100)),2);
          
          
          $overallDiscount = $noDiscountCost - $discountAdPrintCost;
          
          
          $return['Subtotal'] = money_format('%(#4n', $noDiscountCost) ;
          
          //include any third party costs
          $adTotalCost = ($noDiscountCost - $overallDiscount) + $thirdPartyCost;
          
          
          if($overallDiscount > 0)
          {
              $return['Discount'] = "<span style='color:red;'>(".money_format('%(#4n', $overallDiscount).")</span>";
          }
          
          $return['Total to be charged'] =  money_format('%(#4n', $adTotalCost);
          
          //update database
          $sql = "UPDATE ads SET ad_subtotal='$noDiscountCost', discount = $overallDiscount, promo_code = '$promoCode', 
          free_ad = 0, ad_total = $adTotalCost, other_paper_total = $thirdPartyCost WHERE id = $adID";
          $dbUpdate = dbexecutequery($sql);
          
      }
      
      return array('categories'=>$cats,'pricing'=>$return);  
  }
