<?php
  require_once("bootCore.php");
  
  $imageID=intval($_POST['imageID']);
  
  $sql="SELECT * FROM images WHERE id=$imageID";
  $dbImage=dbselectsingle($sql);
  if($dbImage['numrows']>0)
  {
      $image = $dbImage['data'];
      $path = $image['path'];
      $filename = stripslashes($image['filename']);
      $output['status']='success';
      $output['filename']=$path.$filename;
  } else {
      $output['status']='error';
  }
  
  echo json_encode($output);