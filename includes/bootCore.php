<?php
session_start();
error_reporting(E_ERROR);
$scripts = array();
$docRoot = $_SERVER['DOCUMENT_ROOT'];
if($docRoot == '') $docRoot = '/var/www/';
$urlRoot = str_replace( '/var/www', '', $docRoot ) . '/';
if( strpos( strtolower( $urlRoot ), '.com' ) > 0 ) {
	$urlRoot = explode( '.com', $urlRoot );
	$urlRoot = end( $urlRoot );
}
$urlRoot = str_replace( '/html', '', $urlRoot );
define( 'ABS_PATH', $docRoot );
define( 'URL_ROOT', $urlRoot );
define( 'INCLUDE_DIR', ABS_PATH . '/includes/' );
define( 'VENDOR_DIR', ABS_PATH . '/vendor/' );
if( !file_exists( INCLUDE_DIR . 'config.json' ) ) {
	die( '..Config file not present. Aborting bootstrap.' );
} else {
	$baseConfig = json_decode( file_get_contents( INCLUDE_DIR . 'config.json' ), true );
	$baseConfig = $baseConfig['config'];
	$debugger[]=$baseConfig;
	define( 'DB_HOST', $baseConfig['database']['host'] ); // set database host
	define( 'DB_USER', $baseConfig['database']['username'] ); // set database user
	define( 'DB_PASS', $baseConfig['database']['password'] ); // set database password
	define( 'DB_NAME', $baseConfig['database']['database'] ); // set database name
    define( 'MODE', $baseConfig['mode'] ); // set mode
    define( 'ROOT_DIR', $baseConfig['root_directory'] ); // set mode
}
setlocale(LC_MONETARY, 'en_US');      
        
require_once( INCLUDE_DIR . 'functions_db.php' );
require_once( INCLUDE_DIR . 'functions_formtools.php' );
require_once( INCLUDE_DIR . 'functions_user.php' );
require_once( INCLUDE_DIR . 'functions_ads.php' );
require_once( INCLUDE_DIR . 'functions_common.php' );
if(file_exists(INCLUDE_DIR . $baseConfig['site_file'])) {
require_once( INCLUDE_DIR . $baseConfig['site_file'] );
}
require_once( INCLUDE_DIR . 'functions_advertisers.php' );
require_once( INCLUDE_DIR . 'functions_listings.php' );
require_once( INCLUDE_DIR . 'functions_rail.php' );
require_once( INCLUDE_DIR . 'functions_articles.php' );
require_once( INCLUDE_DIR . 'mail/htmlMimeMail.php' );
require_once( INCLUDE_DIR . 'definitions.php' );
 

// load config settings from database now
$sql="SELECT * FROM config WHERE id=1";
$dbConfig = dbselectsingle($sql);
$config = $dbConfig['data'];

    
define( 'SITE_NAME', stripslashes($config['site_name']) ); // set site name
define( 'SITE_URL', stripslashes($config['site_url']) ); // set site url
define( 'SEND_ERRORS_TO', stripslashes($config['send_errors_to']) ); // set error thing
define( 'MAIL_FROM_NAME',  stripslashes($config['mail_from_name']) ); // set mail from name
define( 'MAIL_FROM_ADDRESS',  stripslashes($config['mail_from_address']) ); // set mail from address
define( 'MAIL_FROM_SUBJECT',  stripslashes($config['mail_from_subject']) ); // set mail from subject
define( 'FACEBOOK_APP_ID',  stripslashes($config['facebook_app_id']) ); // set facebook app id
define( 'FACEBOOK_SECRET',  stripslashes($config['facebook_secret']) ); // set facebook app id
define( 'FACEBOOK_TOKEN',  stripslashes($config['facebook_token']) ); // set facebook app id
define( 'GOOGLE_MAP_KEY',  stripslashes($config['google_map_key']) ); // set google map API key
define( 'RECAPTCHA_PUBLIC',  stripslashes($config['recaptcha_site']) ); // set recaptcha public key
define( 'RECAPTCHA_PRIVATE',  stripslashes($config['recaptcha_secret']) ); // set recaptcha private key
define( 'STRIPE_PUBLIC',  stripslashes($config['stripe_test_public']) ); // set stripe private key
define( 'STRIPE_KEY',  stripslashes($config['stripe_test_secret']) ); // set stripe private key
define( 'GOOGLE_ANALYTICS',  stripslashes($config['google_analytics']) ); // set google analytics ID
define( 'PRINT_DOW',  stripslashes($config['print_dow']) ); // print days of week, 0 as sunday (php date("w"))
define( 'NESTED_CATEGORIES',  stripslashes($config['nested_categories']) ); // wether to allow nested categories
define( 'ALLOW_ONLINE_ONLY',  stripslashes($config['allow_online_only']) ); // allow users to place ads for online only with print as option
define( 'CONTACT_EMAIL',  stripslashes($config['contact_email']) ); // contact email
define( 'CONTACT_PHONE',  stripslashes($config['contact_phone']) ); // contact phone number
define( 'EMAIL_ABUSE_LIMIT',  stripslashes($config['email_abuse_limit']) ); // point at which a user can not send email
define( 'GOOGLE_DFP_KEY',  stripslashes($config['google_dfp_key']) ); // point at which a user can not send email
define( 'TIMEZONE',  stripslashes($config['timezone']) ); // point at which a user can not send email
if(trim(stripslashes($config['offsite_ad_order']))!='')
{
    define( 'ORDER_ENTRY_URL', trim(stripslashes($config['offsite_ad_order'])));
} else {
    define( 'ORDER_ENTRY_URL', '/place-an-ad/');
}
date_default_timezone_set(TIMEZONE);

// define fonts
$sql="SELECT * FROM fonts";
$dbfonts = dbselectmulti($sql);
$chosenFonts = array();
foreach( $dbfonts['data'] as $font ) {
    if( $font['id'] == $config['font_header'] ) {
        $chosenFonts['heading'] = $font;
    } elseif( $font['id'] == $config['font_body'] ) {
        $chosenFonts['body'] = $font;
    } elseif( $font['id'] == $config['preview_font'] ) {
        $chosenFonts['preview'] = $font;
    }
}
define( 'HEADING_FONT_LINK', stripslashes($chosenFonts['heading']['google_link']) ); // used to get font
define( 'BODY_FONT_LINK', stripslashes($chosenFonts['body']['google_link']) ); // used to get font
define( 'PREVIEW_FONT_LINK', stripslashes($chosenFonts['preview']['google_link']) ); // used to get font
define( 'HEADING_FONT_CSS', stripslashes($chosenFonts['heading']['css_family']) ); // used for styling
define( 'BODY_FONT_CSS', stripslashes($chosenFonts['body']['css_family']) ); // used for styling
define( 'PREVIEW_FONT_CSS', stripslashes($chosenFonts['preview']['css_family']) ); // used for styling

// define print preview
define( 'PRINT_COLUMN_WIDTH', stripslashes($config['print_column_width']) ); // used for styling on place-an-ad print preview
define( 'PREVIEW_FONT_SIZE', stripslashes($config['preview_font_size']) ); // used for styling on place-an-ad print preview
define( 'PREVIEW_LINE_HIEGHT', stripslashes($config['preview_line_height']) ); // used for styling on place-an-ad print preview
define( 'PREVIEW_LETTER_SPACING', stripslashes($config['preview_letter_spacing']) ); // used for styling on place-an-ad print preview
define( 'PREVIEW_DISCLAIMER', stripslashes($config['preview_disclaimer']) ); // used for styling on place-an-ad print preview
define( 'PRINT_UPPERCASE_FIRST', stripslashes($config['print_uppercase_first']) ); // used for styling on place-an-ad print preview
define( 'PRINT_BOLD_FIRST', stripslashes($config['print_bold_first']) ); // used for styling on place-an-ad print preview
define( 'PRINT_ALLOW_STYLES', stripslashes($config['print_allow_styles']) ); // used for styling on place-an-ad print preview


//are we loading in admin?
if (strpos($_SERVER['SCRIPT_NAME'],'admin')>0)
{
    //loading boot core in admin, we don't want any user cookie stuff
} else {
    //if a cookie is present, set up the session
    if( isset( $_COOKIE['pngclassUser'] ) ) {
	    $token = $_COOKIE['pngclassUser'];
	    if(!isset($_SESSION['token']) || $_SESSION['token']==$token){
            $sql = "SELECT * FROM users WHERE token = '".addslashes($token)."'";
	        $dbUser = dbselectsingle($sql);
	        if( $dbUser['numrows'] > 0 ) {
		        $user = $dbUser['data'];
                $_SESSION['loggedin'] = true;
                $_SESSION['user_id'] = $user['id'];
		        $_SESSION['token'] = $token;
		        $_SESSION['email'] = stripslashes( $user['email'] );
	        }
        }
    } else {
        if(isset($_SESSION['token']) && $_SESSION['token']!=''){
            $token = $_SESSION['token'];
            $sql = "SELECT * FROM users WHERE token = '".addslashes($token)."'";
            $dbUser = dbselectsingle($sql);
            if( $dbUser['numrows'] > 0 ) {
                $user = $dbUser['data'];
            }
        }
    }
    if( isset( $_COOKIE['pngclassAdmin'] ) ) {
        $adminToken = $_COOKIE['pngclassAdmin'];
        $sql="SELECT * FROM admins WHERE token='$adminToken'"; 
        $dbresult=dbselectsingle($sql);
        if($dbresult['numrows']>0)
        {
            define( 'ADMIN', 1);
        } else {
            define( 'ADMIN', 0);
        } 
        
    } else {
        define( 'ADMIN', 0);
    }
}

$siteMenu = menuItems();

