<?php
  include("bootCore.php");
  
  $stage=$_POST['stage'];
  if($stage=='initial')
  {
  
      $fbToken = addslashes($_POST['token']);
      $email = addslashes($_POST['email']);
      $name = explode(" ",$_POST['name']);
      $gender = addslashes($_POST['gender']);
      
      //ok, we want to check two things, if we already have this user account created with either token or email
      
      $sql="SELECT * FROM users WHERE email='$email' OR fb_token='$fbToken'";
      $dbCheck = dbselectsingle($sql);
      if( $dbCheck['numrows'] > 0 ) {
        $user = $dbCheck['data'];
        
        //user is in the system, lets log them in
        setcookie( "pngclassUser", $user['token'], time()+3600*24*30, '/' );
        $_SESSION['loggedin'] = true;
        $_SESSION['token'] = $user['token'];
        $_SESSION['email'] = stripslashes( $user['email'] );
        addUserAction($user['id'],'login-fb');
        $json['status']='success';
        $json['action']='login';
      } else {
         //going to have to create the user record, then get the user to
         $firstName = $name[0];
         array_shift($name);
         $lastName = implode(" ",$name);
         
         $password=password_hash('fb'.$lastName,PASSWORD_DEFAULT);
         $token = generate_random_string(32);
        
         $createdDT = date("Y-m-d H:i:s");
         $sql="INSERT INTO users (username, first, last, email, password, token, gender, verified, level, created_dt, 13_check, fb_token) 
         VALUES ('', '$firstName', '$lastName', '$email', '$password', '$token', '$gender', 1, 1, '$createdDT', 1, '$fbToken')";
         $dbInsert=dbinsertquery($sql);
         $userID=$dbInsert['insertid'];
         
         //user is in the system, lets log them in
         setcookie( "pngclassUser", $token, time()+3600*24*30, '/' );
         $_SESSION['loggedin'] = true;
         $_SESSION['token'] = $token;
         $_SESSION['email'] = stripslashes($email);
          
         addUserAction($userID,'register-fb');
         
         $json['status']='success';
         $json['action']='register';  
      }
  } else {
      $userName = addslashes($_POST['userName']);
      $token = $_SESSION['token'];
      
      //here we are just updating the user with the username and age check
      $sql="UPDATE users SET username='$userName', 13_check=1 WHERE token='$token'";
      $dbUpdate=dbexecutequery($sql);
      $json['status']='success';
      $json['token']=$token;
      $json['sql']=$sql;
         
  }
  echo json_encode($json);
  