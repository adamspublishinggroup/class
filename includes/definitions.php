<?php
  /*
  This file contains any global arrays or data configurations NOT stored in the site config.json file.

  */

$metaTypes=array('select'=>'List of options',
'text'=>'Text (up to 255 characters)',
'textblock'=>'Large amount of text',
'number'=>'Number',
'checkbox'=>'Checkbox',
'telephone'=>'Telephone number',
'address'=>'Address block',
'date'=>'Date'
);

$totalAdCount = 0;
$totalCategoryCount = 0;
