<?php
/*
*  Class to generate an HTML emailable / PDF generate-able invoice
*/
use Spipu\Html2Pdf\Html2Pdf;

class Invoice
{
    private $invoiceHTML = '';
    private $title = '';
    private $invoiceTotal = 0;
    private $subject;
    private $keywords;
    private $logo;
    private $invoiceNum = 0;
    private $date;
    private $dueDate; 
    private $filename;
    private $billTo;
    private $adCount = 0;
    private $ads = array();
    private $itemHTML = '';
    private $htmlHead;
    private $htmlFoot;
    private $generated = false;
    private $baseDir = '';
    
    public function __construct()
    {
        global $config;
        $this->htmlHead = <<<HEAD
           <!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>A simple, clean, and responsive HTML invoice template</title>
</head>
<body style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; text-align: center; color: #777;">
HEAD;
        
        $this->htmlFoot = <<<FOOT
        </body>
</html>
FOOT;
         //get highest invoice number
         $sql="SELECT MAX(id) as mInv FROM invoices";
         $dbInvoice = dbselectsingle($sql);
         $this->invoiceNum = $dbInvoice['data']['mInv']+1;
         
         $this->logo = "/img/".stripslashes($config['site_logo_mobile']);
         $this->date = date("Y-m-d");
         $this->dueDate = date("Y-m-d", strtotime("+".$config['bill_days_out']." days"));
         $this->filename = "invoice_".$this->date."-".$this->invoiceNum.".pdf";
        
         $this->baseDir = ABS_PATH."/uploads/bills/";
                 
         
    }
    
    public function display()
    {
        if(!$this->generated){$this->generateHTML();}
        echo $this->invoiceHTML;
    }
    
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }
    
    public function setBillTo($account)
    {
        $this->billTo = $account;
    }
    
    public function savePDF()
    {
        if(!$this->generated){$this->generateHTML();}
        //lets also save it
        $html2pdf = new Html2Pdf();
        $html2pdf->writeHTML($this->invoiceHTML);
        $html2pdf->output(ABS_PATH.'/uploads/bills/'.$this->filename, 'F');
        
        return true; 
    }
    
    public function saveInvoice()
    {
        $sql="INSERT INTO invoices (".$this->billTo['type']."_id, invoice_date, invoice_status, invoice_html) VALUES 
        ('".$this->billTo['id']."', '".date("Y-m-d H:i")."', 1, '".addslashes(htmlentities($this->invoiceHTML))."')";
        $dbInsert=dbinsertquery($sql);
        $invoiceID = $dbInsert['insertid'];
        $this->invoiceNum = $invoiceID;
        //also here we will go through the ads that have been passed to us and set billed = 1 and bill date to now
        if(count($this->adCount)>0)
        {
            $dt = date("Y-m-d H:i");
            foreach($this->ads as $ad)
            {
                $sql="UPDATE ads SET invoice_id=$invoiceID, bill_date = '$dt', billed=1 WHERE id=$ad[id]";
                $dbUpdate=dbexecutequery($sql);
            }
        }
       
        //generate the PDF at the same time
        if($this->savePDF())
        {
            $sql="UPDATE invoices SET pdf_generated = 1, pdf_filename='$this->filename' WHERE id=$invoiceID";
            $dbUpdate=dbexecutequery($sql);
        }
        
        return $invoiceID;
    }
    
    public function setTitle($title)
    {
        $this->title = $title;   
    }
    
    public function setSubject($subject)
    {
        $this->subject = $subject;   
    }
    
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;   
    }
    
    
    public function addItem($ad)
    {
        $this->ads[]=$ad;
        $this->adCount++;
        
        $adTitle = "Placed ".date("m/d/Y",strtotime($ad['created_dt']))." - ".stripslashes($ad['headline']);
        $adStart = date("m/d/Y",strtotime($ad['start_date']));
        $adStop = date("m/d/Y",strtotime($ad['end_date']));
        $adTotal = $ad['ad_total'];
        $this->invoiceTotal+=$adTotal;
        $adHTML = <<<HTML
<tr>
    <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-color: #eee; border-bottom-style: solid; padding: 5px;" valign="top">
        $adTitle
    </td>
    <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-color: #eee; border-bottom-style: solid; padding: 5px;" valign="top">
        $adStart
    </td>
    <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-color: #eee; border-bottom-style: solid; padding: 5px;" valign="top">
        $adStop
    </td>
    <td style="vertical-align: top; text-align: right; border-bottom-width: 1px; border-bottom-color: #eee; border-bottom-style: solid; padding: 5px;" align="right" valign="top">
        $adTotal
    </td>
</tr>
HTML;
        $this->itemHTML.=$adHTML;
        
    }
    
    public function generateHTML()
    {
       global $config;
       $totalDue =money_format('%(#4n', $this->invoiceTotal);
       $billName = $this->billTo['name'];
       $billStreet = ($this->billTo['street'] !='' ? $this->billTo['street']: "" );
       $billCity = ($this->billTo['city'] !='' ? $this->billTo['city']: "" );
       $billState = ($this->billTo['state'] !='' ? $this->billTo['state']: "" );
       $billZip = ($this->billTo['zip'] !='' ? $this->billTo['zip'] : "" );
       
       if($this->logo !='')
       {
           $logoTag = "<img src='".$config['site_url']."/$this->logo' style='width: 100%; max-width: 300px;'>";
       } else {
           $logoTag = "";
       }
       $this->invoiceHTML= <<<HTML
<div style="max-width: 800px; box-shadow: 0 0 10px rgba(0, 0, 0, .15); font-size: 16px; line-height: 24px; font-family: Arial, sans-serif; color: #555; margin: auto; padding: 30px; border: 1px solid #eee;">
    <table cellpadding="0" cellspacing="0" style="width: 100%; line-height: inherit; text-align: left;">
        <tr>
            <td colspan=2 style="vertical-align: top; font-size: 45px; line-height: 45px; color: #333; width: 50%; text-align: center; padding: 5px 5px 20px;" valign="top">
                $logoTag
            </td>
            <td colspan=2 style="vertical-align: top; text-align: right;width:50%" valign="top">
                <table style='float:right;'>
                    <tr>
                        <td>
                            <b>Invoice #:</b> $this->invoiceNum
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Created:</b> $this->date
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Due:</b> $this->dueDate
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr style='margin:0;padding:0;'>
            <td colspan="2" style="vertical-align: top; text-align: left;" align="left" valign="top">
                <b>Send Payment To:</b>
            </td>
            <td colspan="2" style="text-align: right;" align="right" valign="top">
                <b>Bill To:</b>
            </td>
        </tr>
        <tr style='margin:0;padding:0;'>
            <td colspan="2" style="vertical-align: top; text-align: left;" align="left" valign="top">
                $config[site_name]
            </td>
            <td colspan="2" style="text-align: right;" align="right" valign="top">
                $billName
            </td>
        </tr>
        <tr style='margin:0;padding:0;'>
            <td colspan="2" style="vertical-align: top; text-align: left;" align="left" valign="top">
                $config[billing_street]
            </td>
            <td colspan="2" style="text-align: right;" align="right" valign="top">
                $billStreet
            </td>
        </tr>
        <tr style='margin:0;padding:0;'>
            <td colspan="2" style="vertical-align: top; text-align: left;" align="left" valign="top">
                $config[billing_city], $config[billing_state] $config[billing_zip]
            </td>
            <td colspan="2" style="text-align: right;" align="right" valign="top">
                $billCity, $billState $billZip
            </td>
        </tr>
        <tr>
            <td colspan=4>&nbsp;</td>
        </tr>
        <tr>
            <td colspan=2 style="vertical-align: top; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" bgcolor="#eee" valign="top">
                Payment Method
            </td>
            <td colspan=2 style="vertical-align: top; text-align: right; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" align="right" bgcolor="#eee" valign="top">
                Check #
            </td>
        </tr>
        <tr>
            <td colspan=4>&nbsp;</td>
        </tr>
        <tr style='margin-bottom:10px;'>
            <td colspan=2 style="vertical-align: top; padding: 5px 5px 20px;border-bottom: thin solid black" valign="top">
                
            </td>
            <td colspan=2 style="vertical-align: top; text-align: right; padding: 5px 5px 20px;border-bottom: thin solid black" align="right" valign="top">
                
            </td>
        </tr>
        <tr>
            <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" bgcolor="#eee" valign="top">
                Placed Ad
            </td>
            <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" bgcolor="#eee" valign="top">
                Start Date
            </td>
            <td style="vertical-align: top; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" bgcolor="#eee" valign="top">
                Stop Date
            </td>
            <td style="vertical-align: top; text-align: right; border-bottom-width: 1px; border-bottom-style: solid; border-bottom-color: #ddd; font-weight: bold; background-color: #eee; padding: 5px;" align="right" bgcolor="#eee" valign="top">
                Price
            </td>
        </tr>
HTML;
$this->invoiceHTML.=$this->itemHTML;
$this->invoiceHTML.=<<<MORE
        <tr>
            <td colspan=2 style="vertical-align: top; padding: 5px;" valign="top"></td>
            <td colspan=2 style="vertical-align: top; text-align: right; border-top-style: solid; border-top-width: 2px; border-top-color: #eee; font-weight: bold; padding: 5px;" align="right" valign="top">
                Total: $totalDue
            </td>
        </tr>
    </table>
</div>
MORE;

        $this->generated = true;
    }
    
}
