<?php
class Export
{
    var $featuredStyle = 'Featured';
    var $headlineStyle = 'Headline';
    var $subheadStyle = 'Subhead';
    var $linerBaseStyle = 'LinerBase';
    var $linerBulletStyle = 'LinerBullets';
    var $photoStyle = 'PhotoReference';
    var $headerWords = 3;
    var $liners = array();
    var $outputType = '';
    var $output = '';
    var $images = array();
    
    public function __construct($type)
    {
        
        $this->outputType = $type;
        
        if($this->outputType=='indesign')
        {
            $this->output = "<ASCII-WIN>
<Version:5><FeatureSet:InDesign-Roman><ColorTable:=<Black:COLOR:CMYK:Process:0,0,0,1>>
";     
        } else {
            $this->output = "";
        }
    }
    
    /*
    expects an array with a minimum of body, if body, will otherwise pull first 3 words for the headline 
    $liner['headline'];
    $liner['subhead'];
    $liner['body'];
    $liner['meta'];
    $liner['photo'];
    */
    
    public function addLiner($liner)
    {
        $this->liners[]=$liner;
        
        if($liner['featured'])
        {
            switch($this->outputType)
            {
                case 'indesign':
                    $this->output.="<ParaStyle:".$this->featuredStyle.">***FEATURED AD***\n";
                break;
                
                case 'quark':
                    $this->output.="***FEATURED AD***\n";
                break;
            
                case 'text':
                    $this->output.="***FEATURED AD***\n";
                break;
            
                case 'csv':
                    $this->output.="***FEATURED AD***,";
                break;
            
            }
            
        }
        if(isset($liner['headline']) && $liner['headline']!='')
        {
            switch($this->outputType)
            {
                case 'indesign':
                    $this->output.="<ParaStyle:".$this->headlineStyle.">".$liner['headline']."\n";
                break;
                
                case 'quark':
                    $this->output.="".$liner['headline']."\n";
                break;
            
                case 'text':
                    $this->output.=$liner['headline']."\n";
                break;
            
                case 'csv':
                    $this->output.='"'.$liner['headline'].'",';
                break;
            
            }
            
        }
        /*
         else {
            //strip first $headerWords words from body to act as headline
            $words = explode(" ",stripslashes($liner['body']));
            $headline = '';
            for($i=0;$i++;$i<$this->headerWords)
            {
                $headline.=$words[$i]." ";
            }
            $headline=rtrim($headline," ");
            $this->output.="<ParaStyle:".$this->headlineStyle.">".$headline."\n";
            
            //remove those word from the start of the liner
            for($i=0;$i++;$i<$this->headerWords)
            {
                $temp = array_shift($words);
            }
            $words = implode(" ",$words);
            $liner['body']=$words;
        }
        */
        if(isset($liner['subhead']) && $liner['subhead']!='')
        {
            switch($this->outputType)
            {
                case 'indesign':
                    $this->output.="<ParaStyle:".$this->subheadStyle.">".$liner['subhead']."\n";
                break;
                
                case 'quark':
                    $this->output.=$liner['subhead']."\n";
                break;
            
                case 'text':
                    $this->output.=$liner['subhead']."\n";
                break;
            
                case 'csv':
                    $this->output.='"'.$liner['subhead'].'",';
                break;
            
            }
        }
        
        $body = $this->prepareBody(stripslashes($liner['body']));
        
        switch($this->outputType)
        {
            case 'indesign':
                $this->output.="<ParaStyle:".$this->linerBaseStyle.">".$body."\n";
            break;
            
            case 'quark':
                $this->output.=$body."\n";
            break;
        
            case 'text':
                $this->output.=$body."\n";
            break;
        
            case 'csv':
                $this->output.='"'.$body."\"\n";
            break;
        
        }
        
        
        if(isset($liner['image_filename']))
        {
            switch($this->outputType)
            {
                case 'indesign':
                    $this->output.="Images: $liner[image_filename]\n";
                    $this->output.="<ParaStyle:".$this->subheadStyle.">".$liner['subhead']."\n";
                break;
                
                case 'quark':
                    $this->output.="Image: ".$liner['image_filename']."\n";
                break;
            
                case 'text':
                    $this->output.="Image: ".$liner['image_filename']."\n";
                break;
            
                case 'csv':
                    $this->output.='"'.$liner['image_filename'].'",';
                break;
            
            }
            $this->images[] = array('image'=>$liner['image'],'filename'=>$liner['image_filename']);
        }
        
        
    }
    
    /*
    *  This function takes the body copy, does a replace on all 
    <b> with <cTypeface:Bold>
    <i> with <cTypeface:Italic>
    center: <pTextAlignment:Center>
    */
    private function prepareBody($body)
    {
        //first, let's strip out any html other than what we want to deal with
        $body=$this->stripUnwantedFontTags($body);
        $body=strip_tags($body,"<b>,<em>,<ul><i>,<li>,<br>,<br />,<br/>,<mark>");
        $body=$this->fixColor($body);
        
        //lets turn any \n or <br> into a new paragraph
        $body=str_replace("<ul>","",$body);
        $body=str_replace(array("</ul>"),"\n<ParaStyle:".$this->linerBaseStyle.">",$body);
        
        
        //lets turn any \n or <br> into a new paragraph
        $body=str_replace(array("\n","<br>","<br />","<br/>"),"\n<ParaStyle:".$this->linerBaseStyle.">",$body);
        
        
        //lets turn any <b><i> or <i></b> into a <bi>
        $body=str_replace("<b><i>","<bi>",$body);
        $body=str_replace("</b></i>","</bi>",$body); 
        $body=str_replace("<i><b>","<bi>",$body);
        $body=str_replace("</i></b>","</bi>",$body);
        
        //list items
        $body=str_replace("<li>","\n<ParaStyle:".$this->linerBulletStyle.">",$body);
        $body=str_replace("</li>","\n",$body);
        
        
        //now start doing all the regular replacements
        $body = str_replace("<b>","<cTypeface:Bold>",$body); 
        $body = str_replace("<i>","<cTypeface:Italic>",$body); 
        $body = str_replace("<bi>","<cTypeface:Bold Italic>",$body); 
        $body = str_replace("</b>","<cTypeface:>",$body); 
        $body = str_replace("</i>","<cTypeface:>",$body); 
        $body = str_replace("</bi>","<cTypeface:>",$body); 
        $body = str_replace("\n\n","\n",$body);
        $body = str_replace("<ParaStyle:".$this->linerBaseStyle."><ParaStyle:".$this->linerBaseStyle.">","<ParaStyle:".$this->linerBaseStyle.">",$body);
        return $body;
    }
    
    
    public function outputFile($filename,$category)
    {
        $toZip = array();
        //print "Generating the output<br>";
        //lets output the liners
        $output_dir = "../uploads/export/";
        
        switch($this->outputType)
        {
            case 'indesign':
                $ext='.txt';
            break;
            
            case 'quark':
                $ext='.txt';
            break;
        
            case 'text':
                $ext='.txt';
            break;
        
            case 'csv':
               $ext='.csv';
            break;
        
        }
        
        
        file_put_contents($output_dir.$filename.$ext,$this->output);
        
        //add this file to the "zip" list
        $toZip[]=$output_dir.$filename.$ext;
        
        //are there any images?
        if(!empty($this->images))
        {
            foreach($this->images as $image)
            {
                $toZip[]=$image['image'];
            }
        }
        
        $zipFile = $output_dir.$filename.".zip";
        
        if(!empty($toZip))
        {
            require_once(INCLUDE_DIR."class.zip.php");
            
            $zip = new Zip();
            
            $zip->zip_start($zipFile);
            
            foreach($toZip as $filename)
            {
                $zip->zip_add($filename);
            }
            
            $zip->zip_end();
            
            $outFiles[$category]=$zipFile;
            // http headers for zip downloads
            //used to force download the zip file
            /*
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: public");
            header("Content-Description: File Transfer");
            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=\"".$zipFile."\"");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: ".filesize($zipFile));
            ob_end_flush();
            echo $zipOutput -> file();
            */
        }
        /*
         else {
           header('Content-Type: text/plain'); // plain text file
           header('Content-Disposition: attachment; filename="'.$myFile.'"');
           print $content;
        }
        */
        
        return $outFiles;
    }
    
    private function fixColor($body)
    {
        /* 
        colors:       // colors in the color popup
                        "FFF FCC FC9 FF9 FFC 9F9 9FF CFF CCF FCF " +
                        "CCC F66 F96 FF6 FF3 6F9 3FF 6FF 99F F9F " +
                        "BBB F00 F90 FC6 FF0 3F3 6CC 3CF 66C C6C " +
                        "999 C00 F60 FC3 FC0 3C0 0CC 36F 63F C3C " +
                        "666 900 C60 C93 990 090 399 33F 60C 939 " +
                        "333 600 930 963 660 060 366 009 339 636 " +
                        "000 300 630 633 330 030 033 006 309 303",    
        <font color="#cc0000">colored</font>
        <cColor:COLOR\:CMYK\:Process\:1\,0\,1\,0>   (to get values, divide CMYK value by 255 give 0-1 range)
      
        */
        
        
        $body=str_replace('<mark color="',"<cColor:COLOR\:CMYK\:Process\:",$body);
        $body=str_replace('">',">",$body);
        $body=str_replace("</mark>","<cColor:>",$body);
        
        //replace the color value, which now should be #cc0000
        $hexColors=array("#ffffff","#ffcccc","#ffcc99","#ffff99","#ffffcc","#99ff99","#99ffff","#ccffff","#ccccff","#ffccff",
        "#cccccc","#ff6666","#ff9966","#ffff66","#ffff33","#66ff99","#33ffff","#66ffff","#9999ff","#ff99ff",
        "#bbbbb","#ff0000","#ff9900","#ffcc66","#ffff00","#33ff33","#66cccc","#33ccff","#6666cc","#cc66cc",
        "#999999","#cc0000","#ff6600","#ffcc33","#ffcc00","#33cc00","#00cccc","#3366ff","#6633ff","#cc33cc",
        "#666666","#990000","#cc6600","#cc9933","#999900","#009900","#339999","#3333ff","#6600cc","#993399",
        "#333333","#660000","#993300","#996633","#666600","#006600","#336666","#000099","#333399","#663366",
        "#000000","#330000","#663300","#663333","#333300","#003300","#003333","#000066","#330099","#330033"
        );                        
        $processColors=array(
        "0\,0\,0\,0","0\,.2\,.2\,0","0\,.2\,.4\,0","0\,0\,.4\,0","0\,0\,.2\,0",".4\,0\,.4\,0",".4\,0\,0\,0",".2\,0\,0\,0",".2\,.2\,0\,0","0\,.2\,0\,0",
        "0\,0\,0\,.2","0\,.6\,.6\,0","0\,.4\,.6\,0","0\,0\,.6\,0","0\,0\,.8\,0",".6\,0\,.4\,0",".8\,0\,0\,0",".6\,0\,0\,0",".4\,.4\,0\,0","0\,.4\,0\,0",
        "0\,0\,0\,.27","0\,1\,1\,0","0\,.4\,1\,0","0\,.2\,.6\,0","0\,0\,1\,0",".8\,0\,.8\,0",".5\,0\,0\,.2",".8\,.2\,0\,0",".5\,.5\,0\,0","0\,.5\,0\,.2",
        "0\,0\,0\,.4","0\,1\,1\,0.2","0\,0.6\,1\,0","0\,0.2\,0.8\,0","0\,0.2\,1\,0","0.75\,0\,1\,0.2","1\,0\,0\,0.2","0.8\,0.6\,0\,0","0.5\,0.5\,0\,0.2","0\,0.75\,0\,0.2",
        "0\,0\,0\,0.6","0\,1\,1\,0.4","0\,0.5\,1\,0.2","0\,0.25\,0.75\,0.2","0\,0\,1\,0.4","1\,0\,1\,0.4","0.67\,0\,0\,0.4","0.8\,0.8\,0\,0","0.5\,1\,0\,0.2","0\,0.67\,0\,0.4",
        "0\,0\,0\,0.8","0\,1\,1\,0.6","0\,0.67\,1\,0.4","0\,0.33\,0.67\,0.4","0\,0\,1\,.60","1\,0\,1\,0.6","0.5\,0\,0\,0.6","1\,1\,0\,0.4","0.67\,0.67\,0\,0.4","0\,0.5\,0\,0.6",
        "0\,0\,0\,1","0\,1\,1\,0.8","0\,0.5\,1\,0.6","0\,0.5\,0.5\,0.6","0\,0\,1\,0.8","1\,0\,1\,0.8","1\,0\,0\,0.8","1\,1\,0\,0.6","0.67\,1\,0\,0.4","0\,1\,0\,0.8"
        );
        
        $body=str_replace($hexColors,$processColors,$body);
        
        return $body;
    }
    
    public function testOutput()
    {
        /*
        foreach($this->liners as $liner)
        {
        print "<textarea rows=20 cols=100>".stripslashes($liner['body'])."</textarea><br>";
            
        }
        */
        print "<textarea rows=60 cols=100>".$this->output."</textarea>";
    }
    
    private function stripUnwantedFontTags($body){
        while(stripos($body,"<font color=")>0)
        {
            $start = stripos($body,"<font color=");
            $openend=$start+12;
            $newbody=substr($body,0,$start)."<mark color=";
            $newbody.=substr($body,$openend,10);
            $colorend=$openend+10;
            //crawl from this point until we find the closing </font>
            for($i=$openend;$i<strlen($body);$i++)
            {
                if(substr($body,$i,7)=="</font>")
                {
                    $newbody.=substr($body,$colorend,($i-$colorend))."</mark>";
                    $newbody.=substr($body,($i));
                    break;
                }
            }
            $body=$newbody;
            
        }
        return $body;         
    }
}