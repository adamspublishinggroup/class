<h4>Basic Content</h4>
<form id="adBasics" name="adBasics" method="post" action="/includes/handler/basics.php">
	<fieldset>
    <?php
	/*
	*   We also don't allow creating an ad without the user being logged in. However, we'll cheat around this and collect user information
	*   at the end. Instead we'll create a temporary user account for them
	*/


	/*
	*  Check for a passed ad id, which would be a user editing an existing ad
	*/

    // set some vars
	$adID = 0;
    $cats = array();
    $copy = false;
    $access = false;
    $allowExistingEdit = false;
    $categories = categories(0); 
    $ad = array();
    

    if( isset( $_GET['ad'] ) && $_GET['ad'] != 0 ) {
        // ...then this is a previously created ad
        $adID = intval( $_GET['ad'] );
        $sql = "SELECT * FROM ads WHERE id=$adID";
        $dbAd = dbselectsingle($sql);
        $testAd = $dbAd['data'];
        if( $testAd['published'] == 1 || $testAd['status'] == 3 ) {
            // was published, so we set status to 3 and published to 0
            $editStatus = 3;
        } else {
            $editStatus = 2;
        }
        $sql = "UPDATE ads SET status=$editStatus, published=0 WHERE id=$adID";
        $dbUpdate = dbexecutequery($sql);
        $editing = true;
    } elseif( isset( $_SESSION['ad_id'] ) && $_SESSION['ad_id'] != 0) {
        // ...then this is a new ad that was never finished
        $adID = intval($_SESSION['ad_id']);
    } else {
        // ...then this is a new ad
        $adID = 0;
    }
    if( isset( $_GET['new'] ) ) {
        // ...then this is a new ad
        $adID = 0;
        $_SESSION['ad_id'] = 0;
        $packageID = intval( $_GET['package'] );
        
        //now we need to see if package ID is 0, if so, set it to the default package id, if there are packages
        if($packageID==0)
        {
            $sql="SELECT id FROM packages WHERE active=1 AND default=1";
            $dbDefaultPackage = dbselectsingle($sql);
            if($dbDefaultPackage['numrows']>0)
            {
                $packageID = $dbDefaultPackage['data']['id'];
            }
        }
    }
    if( $adID != 0 ) {
		// ok, we have an ad id, let's make sure it belongs to this user
		$sql = "SELECT * FROM ads WHERE id='$adID'";
		$dbAd = dbselectsingle($sql);
		$ad = $dbAd['data'];
        if( $dbAd['numrows'] > 0 ) {
			//get the user id
			if( ADMIN ) {
                if( $_GET['pickup'] == true ) {
                    print "<div class='alert alert-info' role='alert'>You are duplicating an existing ad.</div>\n";
                } else {
                    print "<div class='alert alert-success' role='alert'>You are editing a previously created ad. The ad has been 'un-published' so you will need to <a href='/includes/place-ad-content/checkout.php?action=republishedited&ad_id=$adID'>re-publish the ad</a> to make it available online again.</div>\n";
                }
                $allowExistingEdit = true;
                $access = true;
            } else if ($_SESSION['token']=='' && $adID!=0) {
                //valid ad
                $ad = $dbAd['data'];
                $allowExistingEdit = true;
                $access=true;
            } else {
                $sql = "SELECT * FROM users WHERE token='".$_SESSION['token']."'";
			    $dbUser = dbselectsingle($sql);
			    $user = $dbUser['data'];
			    $userID = $user['id'];
			    //valid ad
			    $ad = $dbAd['data'];
			    if( $ad['user_id'] == $userID ) {
				    //we are good!
                    if( $_GET['pickup'] == true ) {
                        print "<div class='alert alert-info' role='alert'>You are duplicating an existing ad.</div>\n";
                    } else {
                        if( $ad['status'] == 3 ) {
                            print "<div class='alert alert-success' role='alert'>You are editing a previously created ad. The headline and print ad contents can not be edited. If you need to make a change, please contact us at ".CONTACT_EMAIL." or ".CONTACT_PHONE.". The ad has been 'un-published' so you will need to re-publish the ad to make it available online again.</div>\n";
                            $allowExistingEdit = false;
                        } else {
                            $allowExistingEdit = true;
                        }
                    }
                    $access = true;
                } else {
				    $access = false;
				    print "<div class='alert alert-danger' role='alert'>You do not have access to edit this ad.</div>\n";
                }
            }
		} else {
            $access = false;
            print "<div class='alert alert-danger' role='alert'>There was a problem retrieving this ad.</div>\n";
        }
	} else {
		$access = true;
        $adID = 0;
	}
	if( $access ) {
        // ok, we have access, either as a new ad, or as one of our own ads
        $_SESSION['ad_id'] = $adID;
        
        //let's check to see if the user is picking up an old ad 
        if( $_GET['pickup'] ) {
            pickupAd($adID);
        } 
        
        if( isset( $_SESSION['error'] ) && $_SESSION['error'] != '' ) {
            ?>
            <div class='alert alert-danger clearfix' role='alert'><b>Error</b> <?= $_SESSION['error'] ?></div>
            <?php
            $_SESSION['error'] = '';
        }
        if( !isset( $_SESSION['token'] ) || $_SESSION['token'] == '' ) {
            ?>
            <div class='alert alert-info clearfix' role='alert'><b>Do you have an account?</b> If you have an account with us, <a href='/account/login/?r=<?= urlencode("/place-an-ad/place-ad-basics/?new") ?>'>please log in first.</a> Otherwise you will be prompted to create an account when you have finished creating your ad.</div>
            <?php
            $_SESSION['error'] = '';
        }
        if( $_GET['nav'] == 'true' ) {
            ?>
            <div class='alert alert-info clearfix' role='alert'><b>Whoops!</b> You must complete the basics before anything else.</div>
            <?php
        }
        if( ADMIN ) {
            //pull in a list of all users, allow admin to set the user to be attached to the ad
            $sql = "SELECT id, first, last, username FROM users ORDER BY last DESC";
            $dbUsers = dbselectmulti($sql);
            $users = array();
            print "<div class='col-xs-12 clearfix' style='margin-bottom:20px;'>";
            ?>
            <div class="input-group">
                <label>Assign this ad to this user</label>
                <select id="user_id" name="user_id">
                    <?php
                    if( $dbUsers['numrows'] > 0 ) {
                        foreach( $dbUsers['data'] as $user ) {
                            $selected = '';
                            if( in_array( $ad['user_id'], $users ) ) {
                                $selected = 'selected';
                            } else {
                                $selected = '';
                            }
                            print "<option value='$user[id]' $selected>".stripslashes($user['username']." - ".$user['first'].' '.$user['last'])."</option>\n";
                        
                        }
                    }
                    ?>
                </select>
                <div class="input-group-btn"><a href='/admin/users.php?action=add' class='btn'><i class='fa fa-2x fa-user-plus'></i></a></div>
            </div>
            <br>
            <?php
            //now allow admin to select the package for the ad
            $sql="SELECT id, package_name FROM packages WHERE active=1 ORDER BY default DESC";
            $dbPackages = dbselectsingle($sql);
            $packages[0]="No package specified";
            if($dbPackages['numrows']>0)
            {
                ?>
            <div class="input-group">
                <label>Select a package for this ad</label>
                <select id="package_id" name="package_id" class="form-control">
                    <?php
                    foreach( $dbPackages['data'] as $pack ) {
                        $selected = '';
                        if( $ad['package_id']==$pack['id']) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        print "<option value='$pack[id]' $selected>".stripslashes($pack['package_name'])."</option>\n";
                    }
                    ?>
                </select>
            </div>
                <?php   
            }
            
            
            print "</div>";
        }
        $adHeadline = stripslashes($ad['headline']);
        $adText = stripslashes($ad['ad_text']);
        $adGeo = stripslashes($ad['ad_geo']);
        $iText = stripslashes($ad['internet_text']);
        $wordCount = ($ad['word_count']!='' ? $ad['word_count'] : 0);
        $featureHeadline = $ad['feature_headline'];
        $adKeywords = stripslashes($ad['keywords']);
        
        //pull in all ad categories
        $cats = array();
        if( $adID != 0 ) {
            $sql = "SELECT category_id FROM ad_category_xref WHERE ad_id='$adID'";
            $dbCats = dbselectmulti($sql);
            if( $dbCats['numrows'] > 0 ) {
                foreach( $dbCats['data'] as $cat ) {
                    $cats[]=$cat['category_id'];
                }
            }
        }
        if( ADMIN || $adID == 0 || $copy == true || $allowExistingEdit ) {
            ?>
            <br>
            <input type="text" id="ad_headline" name="ad_headline" value="<?= $adHeadline ?>" placeholder="Headline" maxlength="255" /><?= ($config['require_headline'] ? ' (*required)' : '' )?>
            <div id="ad_headline_error" style='display:none;'>You must enter a headline to proceed.</div>
            <div class="form-group1 checkboxes">
                <div class="col-sm-12">
                  <div class="checkbox">
                    <label <?php echo ($ad['feature_headline']==1 ? 'class="selected"' : '' )?>>
                      <input type="checkbox" name='feature_headline' <?php echo ($ad['feature_headline']==1 ? 'checked="checked"' : '' )?> /> <?php if($config['include_headline_message']!='') { echo stripslashes($config['include_headline_message']); } else { print "Include headline bold and centered above the ad in print to draw attention (additional charges)"; } ?>
                    </label>
                  </div>
                </div>
            </div>
            <br><br>
            <small>What would you like to say in your print ad?<?= ($config['require_body'] ? ' (*required)' : '' )?></small> (Current print word count: <span id='adWordCount'><?= $wordCount ?></span> )
            <div id="ad_text_error" style='display:none;'>You must enter ad text to proceed.</div>
            <textarea id="ad_text" name="ad_text" rows="10"><?= $adText ?></textarea>
            <?php
        } else {
            ?>
            <br>
            <b>Headline:</b> <?= $adHeadline ?><br><br>
            <b>Print ad:</b> <?= $adText ?><br><br>
            <?php                     
        }
    if($config['online_text']){
    ?>
        <small>Do you want to say more in your online ad? Enter your text here and it will be shown instead of the print ad text.</small>
        <textarea id="internet_text" name="internet_text" rows="15"><?= $iText ?></textarea>
     <?php } 
     if($config['ad_geographic_field']){?>
        <small>Add a geographic location to your ad (ex: name of city).<?= ($config['require_geo'] ? ' (*required)' : '' )?></small>
        <input type="text" id="ad_geo" name="ad_geo" value="<?= $adGeo ?>"> <br>
        <div id="ad_geo_error" style='display:none;'>You must enter a geographic tag to proceed.</div>
     <?php } ?>
        <small>Add some keywords relevant to your ad to help it be found.<?= ($config['require_tags'] ? ' (*required)' : '' )?></small>
        <input type="text" id="ad_keywords" name="ad_keywords" value="<?= $adKeywords ?>"> <br>
        <div id="ad_keywords_error" style='display:none;'>You must enter at least one keyword to proceed.</div>
        <?php if( ADMIN || $adID == 0 || $allowExistingEdit ) { ?>
            <small><?php if($config['categories_message']!=''){print stripslashes($config['categories_message']);} else { print "Select one or more categories for your ad to appear in online and in print. Some categories are exclusive and will cause all other categories to be removed. (Ex: free ad categories)";} ?><?= ($config['require_category'] ? ' (*required)' : '' )?></small>
			<br>
			<select id="ad_categories" name="ad_categories[]" multiple="multiple">
				<?php
				$selected = '';
                if( count( $categories ) > 0 ) {
					foreach( $categories as $cat ) {
						if( in_array( $cat['category_id'],$cats ) ) {
                            $selected = 'selected';
                        } else {
                            $selected = '';
                        }
                        print "<option value='$cat[category_id]' $selected>$cat[category_name]</option>\n";
						if( count( $cat['subcats'] ) > 0 ) {
							foreach( $cat['subcats'] as $subcat ) {
						        if( in_array( $subcat['category_id'], $cats ) ) {
                                    $checked = 'selected';
                                } else {
                                    $checked = '';
                                }
                        		print "<option value='$subcat[category_id]' $checked>&nbsp;&nbsp;&nbsp;&nbsp;$subcat[category_name]</option>\n";
							}
						}
					}
				}
				?>
			</select>
            <div id="ad_categories_error" style='display:none;'>You must select a category to proceed.</div>
		<?php } ?>
        </fieldset>
        <input type="hidden" id="ad_id" name="ad_id" value="<?= $adID ?>">
		<input type="hidden" id="package_id" name="package_id" value="<?= $packageID ?>">
		<input type="hidden" id="ad_stage" name="ad_stage" value="basics">
		<?php if( ADMIN ) { ?>
            <?php make_hidden('admin_edit',1); ?>
        <?php } else { ?>
            <input type="hidden" id="user_token" name="user_token" value="<?= $_SESSION['token'] ?>">
        <?php } ?>
        <div class="clearFix"></div>
        <button type="button" id="nextStep" class="pull-right">Next Step <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
	<?php } ?>
</form>


<script>
    $("#ad_categories").select2();
    $("#ad_keywords").tagsInput({
        'defaultText':'add a keyword',
        'delimiter': ['|'],   // Or a string with a single delimiter. Ex: ';'
        'removeWithBackspace' : true,
        'placeholderColor' : '#666666',
        'height':'40px',
        'width':'100%'
    });
    $('#nextStep').on('click',function(){
        /*
        //handle validation based on config settings
        */
        var validNext = true;
        <?php
            if($config['require_headline'])
            {
                ?>
                if($('#ad_headline').val()=='')
                {
                    validNext = false;
                    $('#ad_headline').addClass("has-error");
                    $('#ad_headline_error').addClass("shown");
                }
                <?php
            }
            if($config['require_body'])
            {
                ?>
                if($('#ad_text').val()=='')
                {
                    validNext = false;
                    $('#ad_text').addClass("has-error");
                    $('#ad_text_error').addClass("shown");
                }
                <?php
            }
            if($config['require_geo'] && $config['ad_geographic_field'])
            {
                ?>
                if($('#ad_geo').val()=='')
                {
                    validNext = false;
                    $('#ad_geo').addClass("has-error");
                    $('#ad_geo_error').addClass("shown");
                }
                <?php
            }
            if($config['require_tags'])
            {
                ?>
                if($('#ad_keywords').val()=='')
                {
                    validNext = false;
                    $('#ad_keywords').addClass("has-error");
                    $('#ad_keywords_error').addClass("shown");
                }
                <?php
            }
            if($config['require_category'])
            {
                ?>
                if($('#ad_categories').val()=='')
                {
                    validNext = false;
                    $('#ad_categories').next().addClass("has-error");
                    $('#ad_categories_error').addClass("shown");
                }
                <?php
            }
        ?>
        
        if(validNext)
        {
            $('#adBasics').submit();
        } else {
            return false;
        }

    })

    <?php if($config['online_text']){?>
    $('#internet_text').summernote({
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']]
        ],
        callbacks: {
            onPaste: function (e) {
                var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                e.preventDefault();
                var stripedHtml = bufferText.replace(/<[^>]+>/g, '');
                // Firefox fix
                setTimeout(function () {
                    document.execCommand('insertText', false, stripedHtml);
                }, 10);
                counter();
            }
        }
    });
    <?php } ?>
    function counter() {
        var headline = $('#ad_headline').val();
        var adText = $('#ad_text').val();
        var headlineWordCount = 0;
        var headlineCharCount = 0;
        var textWordCount = 0;
        var textCharCount = 0;
        var allowedWordLength = 15;
        var regex = /<\/?\w+[^>]*\/?>/g;
        <?php if ($config['include_headline_adcount']) { ?>
            if( headline.trim() != '' ) {
                headlineWordCount = headline.trim().replace(regex, '').split(' ').length;
                headlineCharCount = headline.trim().replace(regex, '').length;
            }
        <?php } ?>
        if( adText.trim() != '' ) {
            textWordCount = adText.trim().replace(regex, '').split(' ').length;
            textCharCount = adText.trim().replace(regex, '').length;
        }
        var wordCount = parseInt( headlineWordCount ) + parseInt( textWordCount );
        var textLength = parseInt( headlineCharCount ) + parseInt( textCharCount );
        if( ( wordCount * allowedWordLength ) / textLength >= 1 || textLength == 0 ) {
            $('#adWordCount').html('<b>'+wordCount+'</b>');
        } else {
            $('#adWordCount').html('<span class="busted"><b>'+wordCount+'</b></span><span class="bustedText"> -Please refrain from stringing words together.</span>');
        }
    };

    <?php if( $config['include_headline_adcount'] ) { ?>
        $('#ad_headline').on("change keydown keypress keyup blur focus",counter);
    <?php } else { ?>
        // do nothing
    <?php } ?>

    <?php if( $config['rich_ad_text'] ) { ?>
        $('#ad_text').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']]
            ],
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var stripedHtml = bufferText.replace(/<[^>]+>/g, '');
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, stripedHtml);
                    }, 10);
                    counter();
                },
                onKeyup: function(e){counter()}
            }
        });
        /*
        $('#ad_text').trumbowyg({resetCss: true,btns: [['bold', 'italic', 'underlilne']]}).on('tbwchange',counter);
        */
    <?php } else { ?>
        $('#ad_text').on("change keydown keypress keyup blur focus",counter);
    <?php } ?>
</script>


<?php
function pickupAd($adID)
{
    //ok, we'll duplicate the ad record, set dates to null, duplicate categories    
    $oldID = $adID;
    $sql = "SELECT * FROM ads WHERE id=$oldID";
    $dbOldAd = dbselectsingle($sql);
    $oldAd = $dbOldAd['data'];
    
    //see if this has already been copied (user clicked back button in browser for example)
    $sql = "SELECT * FROM ads WHERE copied_from = $adID";
    $dbCheck = dbselectsingle($sql);
    if( $dbCheck['numrows'] > 0 ) {
        //it's already been copied
        $ad = $dbCheck['data'];
        $adID = $ad['id'];
        $_SESSION['ad_id'] = $adID;
    } else {
        $newAdSql = "INSERT INTO ads (user_id, advertiser_id, headline, ad_text, internet_text, keywords, status, 
        created_dt, featured, print, other_paper, facebook, lat, lon, published, copied_from, admin_placed, package_id) VALUES ('$oldAd[user_id]', '$oldAd[advertiser_id]', '$oldAd[headline]', '$oldAd[ad_text]', '$oldAd[internet_text]', '$oldAd[keywords]', 1, '".date("Y-m-d H:i:s")."', $oldAd[featured], $oldAd[print], $oldAd[other_paper], $oldAd[facebook], '$oldAd[lat]', '$oldAd[lon]', 0, $oldID,".(ADMIN ? '1':'0').", $oldAd[package_id])";
        $dbInsertAd = dbinsertquery($newAdSql);
        $adID = $dbInsertAd['insertid'];           
        
        $_SESSION['ad_id'] = $adID;
        
        if( $dbInsertAd['error'] == '' ) {
            $ad = $oldAd;
            //now duplicate the ad_meta records
            $sql = "SELECT * FROM ad_meta WHERE ad_id=$oldID";
            $dbMeta = dbselectmulti($sql);
            if( $dbMeta['numrows'] > 0 ) {
                $insertMeta = array();
                foreach( $dbMeta['data'] as $meta ) {
                    $insertMeta[] = "($adID,$meta[meta_id],'$meta[meta_value_short]','$meta[meta_value_long]')";
                }
                if( !empty( $insertMeta ) ) {
                    $metaSql = "INSERT INTO ad_meta (ad_id, meta_id, meta_value_short, meta_value_long) VALUES ".implode(",",$insertMeta);
                    $dbInsert = dbinsertquery($metaSql);
                }
            }
            
            //now duplicate the ad_category records
            $sql = "SELECT * FROM ad_category_xref WHERE ad_id=$oldID";
            $dbCategories = dbselectmulti($sql);
            if( $dbCategories['numrows'] > 0 ) {
                $insertCats = array();
                foreach( $dbCategories['data'] as $cat ) {
                    $insertCats[] = "($adID,$cat[category_id])";
                }
                if( !empty( $insertCats ) ) {
                    $catSql = "INSERT INTO ad_category_xref (ad_id, category_id) VALUES ".implode(",",$insertCats);
                    $dbInsert = dbinsertquery($catSql);
                }
            }
            
            //now the images, we'll need to duplicate the actual images as well
            $sql = "SELECT * FROM images WHERE ad_id=$oldID";
            $dbImages = dbselectmulti($sql);
            if( $dbImages['numrows'] > 0 ) {
                $year = date ("Y");
                $month = date ("m");

                if( !file_exists( "../../uploads/$year" ) ) {
                   mkdir("../../uploads/$year", 0755);
                }

                if( !file_exists( "../../uploads/$year/$month" ) ) {
                   mkdir("../../uploads/$year/$month", 0755);
                }
                $newPath = "$year/$month/";
                $newImageDirectory = "../../uploads/".$newPath;
                
                $sql = "SELECT MAX(id) as photoID from images WHERE ad_id=$adID";
                $dbMax = dbselectsingle($sql);
                $max = $dbMax['data']['photoID'];
                $max++;
                
                $insertImages = array();
                foreach( $dbImages['data'] as $image ) {
                    $filename = stripslashes($image['filename']);
                    //$newName = str_replace("$oldID-","$adID-",$filename);
                    $ext = end( explode( ".", $filename ) );
                    $newName = "image_" . $adID . "_" . $max . "." . $ext;
                    
                    $path = "../../uploads/" . stripslashes( $image['path'] );
                    if( copy( $path . $filename, $newImageDirectory . $newName ) ) {
                        //now copy thumb and display
                        copy( $path . "display_" . $filename, $newImageDirectory . "display_" . $newName );
                        copy( $path . "thumb_" . $filename, $newImageDirectory . "thumb_" . $newName );
                        
                        //and now create the DB insert
                        $insertImages[] = "($adID,'" . addslashes( $newName ) . "','$newPath')"; 
                    } else {
                        print "Tried to copy from " . $path . $filename . " to " . $newPath . $newName . "<br>";
                    }
                    $max++;
                }
                if( count( $insertImages) > 0 ) {
                    $sql = "INSERT INTO images (ad_id, filename, path) VALUES " . implode(",",$insertImages);
                    $dbInsert = dbinsertquery($sql);
                }
            }
            $copy = true;
            print "<div class='alert alert-info' role='alert'><b>Success! </b>Your ad has been successfully copied. You'll need to set new dates and complete the ad as normal. Old id: $oldID, new id: $adID</div>\n";
        } else {
            print "<div class='alert alert-danger' role='alert'><b>Error! </b>Something went wonky and we were unable to copy your old ad. Please contact us and we'll help get it sorted out.</div>\n";
        }
    }
}