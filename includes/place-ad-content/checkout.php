<?php
global $config;
$adID = intval($_SESSION['ad_id']);
$sql="SELECT * FROM ads WHERE id=$adID";
$dbAd = dbselectsingle($sql);
$ad = $dbAd['data'];
if(!isset($_SESSION['ad_id']) || $_SESSION['ad_id']==0)
 {
    redirect("/place-an-ad/place-ad-basics/?new&nav=true");
 }
 
$totalDue = $ad['ad_total']; 
if($ad['last_error']!='')
{
    print "<div class='alert alert-danger' role='alert'>";
    print "<b>There was a problem processing your order.</b> ".stripslashes($ad['last_error']);
    print "</div>\n";
    $sql="UPDATE ads SET last_error='' WHERE id=$adID";
    $dbUpdate=dbexecutequery($sql);
}

if($ad['status']==3) //means we are in edit mode of a paid ad
{
    ?>
    <form method=post action = '/includes/handler/payment.php'>
    <h3>If you have finished editing your ad, click the button below to re-publish it, otherwise it will remain unpublished.</h3>
    <input type=submit name='submit' value='Re-publish my ad' class='btn' />
    </form>
    <?php
    //redirect("/place-an-ad/place-ad-basics/");
} else {
print "<h4>Total due for this ad is: <span style='color:green;'>".money_format('%(#4n', $totalDue)."</h4>";


?>
<div id='checkoutForm'>
    <?php
        if($config['checkout_message']!='') {
            print "<div class='alert alert-primary'>";  
            print stripslashes($config['checkout_message']);
            print "</div>";
        }
    ?>
    <form name='checkout' id='checkout' method=post class='form-horizontal' action = '/includes/handler/payment.php'>
        <div class="errors">Please fix the indicated errors.</div>
        <?php if ($_SESSION['loggedin'] || ADMIN)
          {
              $onlyCard = true;
              print "<input type=hidden id='logged_in' value='true' />"; 
              print "<input type=hidden id='user_token' value='$_SESSION[token]' />";
              //if the user has the "bill me" option on their account, let that be a choice here
              //look up user
              $token = addslashes($_SESSION['token']); 
              $sql="SELECT * FROM users WHERE token='$token'";
              $dbUser = dbselectsingle($sql);
              $user=$dbUser['data'];
              if($user['bill_me'] && $config['allow_bill_me'])
              {
               ?>
               <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <small>Your account has the ability to be billed later.</small>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='bill_me' id='bill_me' />  Please send me a bill for this ad <?php if($config['bill_me_fee']>0){ print "(additional fee of \$$config[bill_me_fee] applies)";} ?>
                    </label>
                  </div>
                </div>
            </div>
            <?php   
              }
              if($user['advertiser_id']!=0)
              {
                 //user belongs to an advertiser, let's look that company up
                 $sql="SELECT * FROM advertisers WHERE id=".$user['advertiser_id'];
                 $dbAdvertiser = dbselectsingle($sql);
                 if($dbAdvertiser['numrows']>0)
                 {
                  
                     ?>
               <input type='hidden' name='advertiser_id' value='<?= $dbAdvertiser['data']['id'] ?>' />
               <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <small>You are enabled to place ads for <?= stripslashes($dbAdvertiser['data']['name']) ?>.</small>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='bill_advertiser' id='bill_advertiser' />  Please bill my company for this ad
                    </label>
                  </div>
                </div>
            </div>
            <?php 
                 }  
              }
              ?>
              <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button id='billMeSubmit' type="submit" class="btn btn-success" style='display:none;'>Process Ad</button>
                </div>
            </div>
            
              <?php
              if(ADMIN) {
              $onlyCard = true;
              //look up the user this ad was assigned to
              $adID = intval($_SESSION['ad_id']);
              $sql="SELECT user_id FROM ads WHERE id=$adID";
              $dbAd = dbselectsingle($sql);
              $ad = $dbAd['data'];
              $sql="SELECT * FROM users WHERE id=$ad[user_id]";
              $dbUser = dbselectsingle($sql);
              $user = $dbUser['data'];
              print "<input type=hidden id='logged_in' value='true' />"; 
              print "<input type=hidden id='user_token' value='$user[token]' />"; 
              ?>
              
            <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <small>As an admin, you can choose to complete this ad without payment if you so chose, or specify the payment method. If you want to charge their card, do so in the form below.</small>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='admin_free' id='admin_free' class='admin_payment'/>  Process ad with no charges
                    </label>
                  </div>
                </div>
            </div>
            <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='admin_check' id='admin_check' class='admin_payment' />  Customer paid with check<br>
                    </label>
                  </div>
                  <input type='text' id='check_number' name='check_number' value = '' placeholder='Check number' class='form-control'/>
                </div>
            </div>
            <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='admin_cash' id='admin_cash' class='admin_payment'/>  Customer paid with cash
                    </label>
                  </div>
                </div>
            </div>
            <div class="form-group checkboxes">
                <div class="col-sm-offset-3 col-sm-9">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name='admin_card' id='admin_card' class='admin_payment'/>  Customer paid with card
                    </label>
                  </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button id='adminSubmit' type="submit" class="btn btn-success" style='display:none;'>Process Ad</button>
                </div>
            </div>
            <hr> 
              <?php
              }
          } else {
             ?>
             <input type=hidden id='logged_in' value='false' />
             <h4>Create an account now</h4>
                    <input id="firstname" name="firstname" placeholder="First Name" type="text">
                    <input id="lastname" name="lastname" placeholder="Last Name" type="text">
                    <input id="username" name="username" placeholder="Username" type="text">
                    <input id="user_password" name="user_password" placeholder="Password" type="password">
                    <input id="confirm_password" name="confirm_password" placeholder="Confirm Password" type="password">
                    <input id="email" name="email" placeholder="E-Mail Address" type="email">
                    <input id="street" name="street" placeholder="Street Address" type="text">
                    <input id="city" name="city" placeholder="City" type="text">
                    <select id='state' name='state' style='margin-bottom:8px;'>
                        <?php
                            $states=array(
                                'AL'=>'ALABAMA',
                                'AK'=>'ALASKA',
                                'AZ'=>'ARIZONA',
                                'AR'=>'ARKANSAS',
                                'CA'=>'CALIFORNIA',
                                'CO'=>'COLORADO',
                                'CT'=>'CONNECTICUT',
                                'DE'=>'DELAWARE',
                                'DC'=>'DISTRICT OF COLUMBIA',
                                'FL'=>'FLORIDA',
                                'GA'=>'GEORGIA',
                                'GU'=>'GUAM',
                                'HI'=>'HAWAII',
                                'ID'=>'IDAHO',
                                'IL'=>'ILLINOIS',
                                'IN'=>'INDIANA',
                                'IA'=>'IOWA',
                                'KS'=>'KANSAS',
                                'KY'=>'KENTUCKY',
                                'LA'=>'LOUISIANA',
                                'ME'=>'MAINE',
                                'MD'=>'MARYLAND',
                                'MA'=>'MASSACHUSETTS',
                                'MI'=>'MICHIGAN',
                                'MN'=>'MINNESOTA',
                                'MS'=>'MISSISSIPPI',
                                'MO'=>'MISSOURI',
                                'MT'=>'MONTANA',
                                'NE'=>'NEBRASKA',
                                'NV'=>'NEVADA',
                                'NH'=>'NEW HAMPSHIRE',
                                'NJ'=>'NEW JERSEY',
                                'NM'=>'NEW MEXICO',
                                'NY'=>'NEW YORK',
                                'NC'=>'NORTH CAROLINA',
                                'ND'=>'NORTH DAKOTA',
                                'OH'=>'OHIO',
                                'OK'=>'OKLAHOMA',
                                'OR'=>'OREGON',
                                'PW'=>'PALAU',
                                'PA'=>'PENNSYLVANIA',
                                'PR'=>'PUERTO RICO',
                                'RI'=>'RHODE ISLAND',
                                'SC'=>'SOUTH CAROLINA',
                                'SD'=>'SOUTH DAKOTA',
                                'TN'=>'TENNESSEE',
                                'TX'=>'TEXAS',
                                'UT'=>'UTAH',
                                'VT'=>'VERMONT',
                                'VA'=>'VIRGINIA',
                                'VI'=>'VIRGIN ISLANDS',
                                'WA'=>'WASHINGTON',
                                'WV'=>'WEST VIRGINIA',
                                'WI'=>'WISCONSIN',
                                'WY'=>'WYOMING'
                                );
                                foreach($states as $abbr=>$state)
                                {
                                    print "<option value='$abbr' ".($abbr == 'OR' ? 'selected' : '').">$state</option>\n";
                                }
                        ?>
                    </select>
                    <input id="zip" name="zip" placeholder="Zip" type="text">
                    <input id="phone" name="phone" placeholder="(000) 000-0000" type="tel">
                   
             <?php 
          }
          ?>
          <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                  <button id='billMeSubmit' type="submit" class="btn btn-success" style='display:none;'>Process Ad</button>
                </div>
            </div>
           
      <fieldset id='payInfo' style='display:block;'>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="cardHolderName">Name on Card</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="cardHolderName" id="cardHolderName" placeholder="Card Holder's Name">
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-sm-3 control-label" for="card-number">Card Number</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" name="cardNumber" id="cardNumber" placeholder="Debit/Credit Card Number" value="<?=(MODE=='testing'?'4242424242424242':'') ?>">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="expireMonth">Expiration Date</label>
            <div class="col-sm-9">
              <div class="row">
                <div class="col-xs-3">
                  <select class="form-control col-sm-2" name="expireMonth" id="expireMonth">
                    <option>Month</option>
                    <option value="01">Jan (01)</option>
                    <option value="02">Feb (02)</option>
                    <option value="03">Mar (03)</option>
                    <option value="04">Apr (04)</option>
                    <option value="05">May (05)</option>
                    <option value="06">June (06)</option>
                    <option value="07">July (07)</option>
                    <option value="08">Aug (08)</option>
                    <option value="09">Sep (09)</option>
                    <option value="10">Oct (10)</option>
                    <option value="11">Nov (11)</option>
                    <option value="12" <?=(MODE=='testing'?'selected':'' )?>>Dec (12)</option>
                  </select>
                </div>
                <div class="col-xs-3">
                  <select class="form-control" name="expireYear">
                    <?php
                        for($i=date("y");$i<=(date("y")+10);$i++)
                        {
                            if(MODE=='testing' && $i==17){$sel='selected';}else{$sel='';}
                            print "<option value='$i' $sel>20$i</option>\n";   
                        }
                    ?>
                    
                  </select>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
            <div class="col-sm-3">
              <input type="number" class="form-control" name="cvv" id="cvv" placeholder="Security Code" value="<?=(MODE=='testing' ? '123':'')?>">
            </div>
          </div>
          <input type=hidden id='amount' name='amount' value='<?= $totalDue ?>' />
          <div class="form-group">
            <div class="col-sm-offset-3 col-sm-9">
              <?php
                  if($config['billing_interface']=='Stripe')
                  {
                      ?>
                      <button type="submit" class="btn btn-success">Pay Now</button>
                      <?php
                  }elseif($config['billing_interface']=='Authorize.net')
                  {
                      ?>
                      <button type="button" id="submitButton" onclick="sendPaymentDataToAnet()">Pay Now</button>
                      <?php
                  }
              ?>
              
            </div>
          </div>
          <small>*Your credit card information is transmitted securely to our processor and not stored by us.</small>
          
        </fieldset>
    </form>
</div> 
<script>
    $(document).ready(function() {
        var $validator = $("#checkout").validate({
            rules: {
                <?php if (! $onlyCard ){ ?>
                email: {
                    required: true,
                    email: true,
                    remote: "/includes/checkEmail.php"
                },
                firstname: {
                    required: true,
                },
                lastname: {
                    required: true,
                },
                lastname: {
                    required: true,
                },
                street: {
                    required: true,
                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                },
                zip: {
                    required: true,
                },
                phone: {
                    required: true,
                    phoneUS: true,
                },
                username: {
                    required: true,
                    minlength: 3,
                    remote: "/includes/checkUsername.php"
                },
                confirm_password: {
                    equalTo: "#user_password"
                },
                <?php } ?>cardHolderName: {
                    required: true,
                },
                expireMonth: {
                    required: true
                },
                cardNumber: {
                    required: true,
                    creditcard: true
                },
                cvv: {
                    required: true,
                    number: true
                }
            },
            submitHandler: function(form) {
                <?php
                if($config['billing_interface']=='Stripe')
                {
                   print "              form.submit();\n"; 
                }elseif($config['billing_interface']=='Authorize.net')
                {
                   print "              sendPaymentDataToAnet();\n"; 
                }
                ?>
            },
            invalidHandler: function(event, validator) {
                // 'this' refers to the form
                var errors = validator.numberOfInvalids();
                if (errors) {
                  var message = errors == 1
                    ? 'You missed 1 field. It has been highlighted'
                    : 'You missed ' + errors + ' fields. They have been highlighted';
                  $("div.errors span").html(message);
                  $("div.errors").show();
                } else {
                  $("div.errors").hide();
                }
              }
        });
        $("#cardNumber").mask("9999-9999-9999-9999",{placeholder:"X"});
        $('#zip').mask('99999');
        $('#phone').mask('(999) 999-9999');
        
        $('#user_password').pwstrength({
            ui: { showVerdictsInsideProgressBar: true }
        });
        
        $('.admin_payment').on('click',function(){
            if ($('#admin_free').prop('checked') || $('#admin_check').prop('checked') || $('#admin_card').prop('checked') || $('#admin_cash').prop('checked')){
                $validator.destroy();
                $('#payInfo').hide();
                $('#adminSubmit').show();         
            } else {
                document.location.href="?";
            }
        });
        $('#bill_me').on('click',function(){
            if ($('#bill_me').prop('checked')){
                $validator.destroy();
                $('#payInfo').hide();
                $('#billMeSubmit').show();         
            } else {
                document.location.href="?";
            }
        });
        $('#bill_advertiser').on('click',function(){
            if ($('#bill_advertiser').prop('checked')){
                $validator.destroy();
                $('#payInfo').hide();
                $('#billMeSubmit').show();         
            } else {
                document.location.href="?";
            }
        });
    });
    
    function sendPaymentDataToAnet() {
        var secureData = {}; authData = {}; cardData = {};

        // Extract the card number, expiration date, and card code.
        cardData.cardNumber = $("#cardNumber").val();
        cardData.month = $("#expireMonth").val();
        cardData.year = $("#expireYear").val();
        cardData.cardCode = $("#cvv").val();
        secureData.cardData = cardData;

        // The Authorize.Net Client Key is used in place of the traditional Transaction Key. The Transaction Key
        // is a shared secret and must never be exposed. The Client Key is a public key suitable for use where
        // someone outside the merchant might see it.
        authData.clientKey = "<?= $config['authorize_net_client_key'] ?>";
        authData.apiLoginID = "<?= $config['authorize_net_login_id'] ?>";
        secureData.authData = authData;

        // Pass the card number and expiration date to Accept.js for submission to Authorize.Net.
        Accept.dispatchData(secureData, responseHandler);

        // Process the response from Authorize.Net to retrieve the two elements of the payment nonce.
        // If the data looks correct, record the OpaqueData to the console and call the transaction processing function.
        function responseHandler(response) {
            if (response.messages.resultCode === "Error") {
                for (var i = 0; i < response.messages.message.length; i++) {
                    console.log(response.messages.message[i].code + ": " + response.messages.message[i].text);
                }
                alert("acceptJS library error!")
            } else {
                console.log(response.opaqueData.dataDescriptor);
                console.log(response.opaqueData.dataValue);
                processTransaction(response.opaqueData);
            }
        }
    }
    
    function processTransaction(responseData) {
    
        //create the form and attach to the document
        var transactionForm = document.createElement("form");
        transactionForm.Id = "transactionForm";
        transactionForm.action = "/includes/handler/payment.php";
        transactionForm.method = "POST";
        document.body.appendChild(transactionForm);

        //create form "input" elements corresponding to each parameter
        amount = document.createElement("input")
        amount.hidden = true;
        amount.value = document.getElementById('amount').value;
        amount.name = "amount";
        transactionForm.appendChild(amount);

        dataDesc = document.createElement("input")
        dataDesc.hidden = true;
        dataDesc.value = responseData.dataDescriptor;
        dataDesc.name = "dataDesc";
        transactionForm.appendChild(dataDesc);

        dataValue = document.createElement("input")
        dataValue.hidden = true;
        dataValue.value = responseData.dataValue;
        dataValue.name = "dataValue";
        transactionForm.appendChild(dataValue);

        //submit the new form
        transactionForm.submit();
    }
</script>
<?php } ?>
