<?php
// skipping steps, go to jail
if( !isset($_SESSION['ad_id']) || $_SESSION['ad_id'] == 0 ){
    redirect('/place-an-ad/place-ad-basics/?new&nav=true');
}

// set ad id
$adId = intval($_SESSION['ad_id']);

// get ad
$sql = "SELECT * FROM ads WHERE id=$adId";
$dbAd = dbselectsingle($sql);
$ad = $dbAd['data'];
$status = $ad['status'];
$packageID = $ad['package_id'];
?>
<form method="post" action="/includes/handler/dates.php" class="form-horizontal">
    <button type="submit" class="pull-right">Add-ons <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <div class='cleareFix'></div>
    <h4>Please select the start and end dates for your ad</h4>
    <?php
    if( $status == 3 && !ADMIN ) { ?>
        <div class='alert alert-info' role='alert'><b>Attention</b> As you have already paid for this ad, you can not alter the start/stop dates or any of the basic add-ons. If you need to change this information, please contact us at <?= CONTACT_EMAIL ?> or <?= CONTACT_PHONE ?>.</div>
        <b>Scheduled Start Date: </b><?= date("m/d/Y",strtotime($ad['start_date'])) ?><br>
        <b>Scheduled End Date: </b><?= date("m/d/Y",strtotime($ad['end_date'])) ?><br><br>

        <?php
    } else {
        $packageRun = DEFAULT_RUN_LENGTH;
        if( $ad['package_id'] != 0 ) {
            //look up the package to find out how many days it is running
            $sql = "SELECT * FROM packages WHERE id=".intval($ad['package_id']);
            $dbPackage = dbselectsingle($sql);
            $package = $dbPackage['data'];
        }
        if( $ad['start_date'] == '' ){
            $ad['start_date'] = date("m/d/Y",strtotime("+1 day"));
        }
        if( $ad['end_date'] == '' ) {
            //set run length based on the package by default
            if( $ad['package_id'] != 0 ) {
                $packageRun = $package['run_length']+1;
                $ad['end_date'] = date("m/d/Y",strtotime("+$packageRun days"));
            } else {
                $packageRun = 365;
                $ad['end_date'] = date("m/d/Y",strtotime("+1 week"));
            }
        }
        ?>
        <input type='hidden' id='packageDays' value='<?= $packageRun ?>' />

        <div class="form-group">
            <label for="startDate" class="col-sm-2 control-label">Start Date</label>
            <div class="col-sm-10">
                <div class="input-group date" id="startDate" style="width:150px;">
                    <input type="text" name="startDate" value='<?= date("m/d/Y",strtotime($ad['start_date'])) ?>'
                    /><span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="endDate" class="col-sm-2 control-label">End Date</label>
            <div class="col-sm-10">
            <div class="input-group date" id="endDate" style="width:150px;">
                <input type="text" name="endDate" value='<?= date("m/d/Y",strtotime($ad['end_date'])) ?>'
                /><span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            </div>
        </div>
        <div class="form-group">
            <label for="endDate" class="col-sm-2 control-label">Total run length</label>
            <div class="col-sm-10">
                <span id="totalRunDays"><?php 
                $date1 = new DateTime($ad['start_date']);
                $date2 = new DateTime($ad['end_date']);
                $diff = $date2->diff($date1)->format("%a");
                if($config['display_run_length_mode']=='weeks')
                {
                    print ceil($diff/7)." weeks"; 
                } else {
                    print "$diff days";
                }
                ?></span>
            </div>
        </div>
        <?php
        //build the list of dis-allowed dates, to be used if date_selection_mode is 'weeks'
        $dow = explode(",",$config['print_dow']);
        //set up full week
        $week = array(0,1,2,3,4,5,6);
        $invalidDays = array_diff($week,$dow);
        $invalidStartDaysString = implode(",",$invalidDays);
        //we need to back off by one day here so that it starts on "wed" and ends on "tue" for example for the end day.
        foreach($invalidDays as &$day)
        {
            if($day==0)
            {
                $day=6;
            } else {
                $day--;
            }
        }
        $invalidEndDaysString = implode(",",$invalidDays);
        
        //figure out the miniumum date (can't just use today or tomorrow since it needs to be in the selectable dates
        $minDate=date("m/d/Y",strtotime("+1 day")); //start with tomorrow
        while(in_array(date("w",strtotime($minDate)),$invalidStartDaysString))
        {
            $minDate = date("m/d/Y",strtotime($minDate."+1 day"));
        }
        ?>
        <script type="text/javascript">
            $(function () {
                $('#startDate').datetimepicker({
                    format: 'MM/DD/YYYY',
                    minDate: '<?= $minDate ?>'
                    <?php if ($config['date_selection_mode']=='weeks'){ print ",daysOfWeekDisabled: [$invalidStartDaysString]";}
                    if($ad['start_date']!='') {print ", defaultDate: '".date("m/d/Y",strtotime($ad['start_date']))."'";} ?>
                });
                $('#endDate').datetimepicker({
                    format: 'MM/DD/YYYY',
                    <?php if ($config['date_selection_mode']=='weeks'){ print "daysOfWeekDisabled: [$invalidEndDaysString],";}
                    if($ad['end_date']!='') {print "defaultDate: '".date("m/d/Y",strtotime($ad['end_date']))."',";} ?>
                    useCurrent: false
                });
                $("#startDate").on("dp.change", function (e) {
                    $('#endDate').data("DateTimePicker").minDate(e.date);
                    showRunLength();
                });
                $("#endDate").on("dp.change", function (e) {
                    $('#startDate').data("DateTimePicker").maxDate(e.date);
                    showRunLength();
                });
            });
            function showRunLength()
            {
                var packageDays = $('#packageDays').val();
                var dateEnd = moment($('#endDate').datetimepicker('date'));
                var dateStart = moment($('#startDate').datetimepicker('date'));
                var runDays = dateEnd.diff(dateStart, 'days');
                
                console.log(dateStart);
                var dayHolder = $('#totalRunDays');
                
                <?php
                if($config['display_run_length_mode']=='weeks')
                {
                    print "dayHolder.html(Math.ceil(runDays/7)+' weeks.');";
                } else {
                    print "dayHolder.html(runDays+' days.');";
                }?>
            }
        </script>
    <?php
    }
    
    make_hidden('ad_id',$adId);
    make_hidden('ad_stage','dates');
    ?>

    <div class="clearFix"></div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <button type="submit" class="pull-right">Add-ons <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <div class='cleareFix'></div>

</form>

