<?php
// skipping steps, go to jail
if( !isset($_SESSION['ad_id']) || $_SESSION['ad_id'] == 0 ){
    redirect('/place-an-ad/place-ad-basics/?new&nav=true');
}

// set ad id
$adId = intval($_SESSION['ad_id']);

//create the billing record for the ad
$ad = getSingleAd($adId);
if( $ad['status'] == 3 && !ADMIN ) {
    //means we are in edit mode of a paid ad
    redirect('/place-an-ad/place-ad-basics/');
}

// get previous choices made
$imgChoice = $ad['image_choice'];
$attnChoice = $ad['attn_getter'];
?>
<div>
<a href="/place-an-ad/place-ad-basics" class="formButton pull-left"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Change Ad </a>
<a href="/place-an-ad/place-ad-checkout" class="formButton pull-right">Checkout <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
</div>
<div class="clearFix"></div>
<h4>Summary of ad</h4>
<div class="row">
    <div class="col-xs-12 col-md-6">
        <form method="post">
            <p><b>Do you have a promo code?</b></p>
            <input type="text" id="promo_code" name="promo_code" placeholder="Enter promo code" value="<?= $_POST['promo_code'] ?>">
            <input class="formButton" type="submit" value="Apply Code">
        </form>
        <?php
        $adSummary = adPricing($adId);
        $cats = $adSummary['categories'];
        if( count($cats) > 0 ) { ?>
            <h4>Your ad will appear in the following categories</h4>
            <ul>
                <?php foreach( $cats as $cat ) { ?>
                    <li><?= $cat ?></li>
                <?php } ?>
            </ul>
        <?php } ?>

        <?php if( $ad['print'] ) {
            $printDates = explode(",",trim($ad['print_days']));
            if( count($printDates) > 0 ) { ?>
                <h4>Publishing dates</h4>
                <ul>
                <?php foreach( $printDates as $pd ) { ?>
                    <li><?= date("m/d/Y",strtotime($pd)) ?></li>
                <?php } ?>
                </ul>
            <?php }
        } 
 
        //show the addons
        $sql = "SELECT A.addon, A.addon_short FROM addons A, ad_addons B WHERE B.ad_id=$adId AND B.addon_id = A.id";
        $dbAddons = dbselectmulti($sql);
        if( $dbAddons['numrows'] > 0 ) { ?>
            <h4>Addons for your ad</h4>
            <ul>
            <?php foreach( $dbAddons['data'] as $addon ) { ?>
                <li><?= stripslashes($addon['addon_short']) ?></li>
            <?php } ?>
            </ul>
        <?php } ?>
    </div>
    <div class='col-xs-12 col-md-6'>
        <table id="adSummaryTable" class='table table-bordered table-condensed'>
            <?php foreach( $adSummary['pricing'] as $key => $value ) { ?>
                <tr><td><?= $key ?></td><td style='text-align:right;'><?= $value ?></td></tr>
            <?php } ?>
        </table>
    </div>
</div>
