<?php
  /*
  * at this point the payment and checkout process is complete
  */
  $_SESSION['ad_id']=0;
  ?>
  <h4>Thank you for your order.</h4>
  <p>Your ad is now available for viewing online as soon as it is approved and will appear in print according the the schedule you have set.</p>
  <p>Should you have any questions about your ad, please contact us at <?= MAIL_FROM_ADDRESS ?></p>
  <p>You can edit the online text and certain details about your ad by going to your account dashboard. Further changes to the print version will require you to contact us.</p>