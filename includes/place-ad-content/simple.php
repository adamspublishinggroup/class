<?php
placeAd();


function placeAd(){

    $packageID = intval($_GET['package']);
    
    ?>
<h4>Create your ad</h4>
<form id="adBasics" name="adBasics" method="post" action="/includes/handler/simple.php" class="form-horizontal" enctype="multipart/form-data"><?php
global $config;
$adID = 0;
$cats = array();
$copy = false;
$access = false;
$allowExistingEdit = false;
$categories = categories(0); 
$ad = array();
if( isset( $_GET['ad'] ) && $_GET['ad'] != 0 ) {
    // ...then this is a previously created ad
    $adID = intval( $_GET['ad'] );
    $_SESSION['ad_id']=$adID;
} elseif( isset( $_SESSION['ad_id'] ) && $_SESSION['ad_id'] != 0) {
    // ...then this is a new ad that was never finished
    $adID = intval($_SESSION['ad_id']);
} else {
    // ...then this is a new ad
    $adID = 0;
}
if($adID!=0)
{
    $sql = "SELECT * FROM ads WHERE id=$adID";
    $dbAd = dbselectsingle($sql);
    $ad = $dbAd['data'];
    if( $testAd['published'] == 1 || $testAd['status'] == 3 ) {
        // was published, so we set status to 3 and published to 0
        $editStatus = 3;
    } else {
        $editStatus = 2;
    }
    $sql = "UPDATE ads SET status=$editStatus, published=0 WHERE id=$adID";
    $dbUpdate = dbexecutequery($sql);
    $editing = true;
    //get the package
    $packageID=$ad['package_id'];
    
} 
    $sql="SELECT * FROM packages WHERE id=$packageID";
    $dbPackage = dbselectsingle($sql);  
    $package = $dbPackage['data'];
    
    $adHeadline = stripslashes($ad['headline']);
    $adText = stripslashes($ad['ad_text']);
    $adGeo = stripslashes($ad['ad_geo']);
    $iText = stripslashes($ad['internet_text']);
    $wordCount = ($ad['word_count']!='' ? $ad['word_count'] : 0);
    $featureHeadline = $ad['feature_headline'];
    $adKeywords = stripslashes($ad['keywords']);
    $wordsIncluded = $package['words_included']>0 ? $package['words_included'] : 0;
    $perWordCost = $package['per_word_price']>0 ? $package['per_word_price'] : 0;
    $overCost = ($includedWords-$wordCount)*$perWordCost;
    
    if($overCost<0){$overCost = 0;}
    //pull in all ad categories
    $cats = array();
    if( $adID != 0 ) {
        $sql = "SELECT category_id FROM ad_category_xref WHERE ad_id='$adID'";
        $dbCats = dbselectmulti($sql);
        if( $dbCats['numrows'] > 0 ) {
            foreach( $dbCats['data'] as $cat ) {
                $cats[]=$cat['category_id'];
            }
        }
    }
    
    
    
    //build the list of dis-allowed dates, to be used if date_selection_mode is 'weeks'
    $dow = explode(",",$config['print_dow']);
    //set up full week
    $week = array(0,1,2,3,4,5,6);
    $invalidDays = array_diff($week,$dow);
    $invalidStartDaysString = implode(",",$invalidDays);
    //we need to back off by one day here so that it starts on "wed" and ends on "tue" for example for the end day.
    foreach($invalidDays as &$day)
    {
        if($day==0)
        {
            $day=6;
        } else {
            $day--;
        }
    }
    $invalidEndDaysString = implode(",",$invalidDays);
    
    //figure out the miniumum date (can't just use today or tomorrow since it needs to be in the selectable dates
    $minDate=date("m/d/Y",strtotime("+1 day")); //start with tomorrow
    while(in_array(date("w",strtotime($minDate)),$invalidStartDaysString))
    {
        $minDate = date("m/d/Y",strtotime($minDate."+1 day"));
    }
    $startDate = $minDate;
?>
<fieldset>
<div class="form-group">
    <label for="ad_categories" class="col-sm-2 control-label">Category</label>
    <div class="col-sm-10">
   
<small><?php if($config['categories_message']!=''){print stripslashes($config['categories_message']);} else { print "Select one or more categories for your ad to appear in online and in print. Some categories are exclusive and will cause all other categories to be removed. (Ex: free ad categories)";} ?><?= ($config['require_category'] ? ' (*required)' : '' )?></small>
<br>
<select id="ad_categories" name="ad_categories[]" multiple="multiple">
    <?php
    $selected = '';
    if( count( $categories ) > 0 ) {
        foreach( $categories as $cat ) {
            if( in_array( $cat['category_id'],$cats ) ) {
                $selected = 'selected';
            } else {
                $selected = '';
            }
            print "<option value='$cat[category_id]' $selected>$cat[category_name]</option>\n";
            if( count( $cat['subcats'] ) > 0 ) {
                foreach( $cat['subcats'] as $subcat ) {
                    if( in_array( $subcat['category_id'], $cats ) ) {
                        $checked = 'selected';
                    } else {
                        $checked = '';
                    }
                    print "<option value='$subcat[category_id]' $checked>&nbsp;&nbsp;&nbsp;&nbsp;$subcat[category_name]</option>\n";
                }
            }
        }
    }
    ?>
</select>
<div id="ad_categories_error" style='display:none;'>You must select a category to proceed.</div>
 </div>
</div>
<!--
<div class="form-group">
    <label for="ad_type" class="col-sm-2 control-label">Ad Type</label>
    <div class="col-sm-10">
      <select id='ad_type' name='ad_type' class=''>
        <option value='personal'>Personal</option>
        <option value='business'>Business</option>
      </select>
    </div>
</div>
-->
<div class="form-group">
    <label for="heading" class="col-sm-2 control-label">Heading</label>
    <div class="col-sm-10">
      <select id='heading' name='heading' class=''>
        <option value='none' <?= ($featureHeadline==0 ? "selected" : "") ?>>None</option>
        <option value='bold' <?= ($featureHeadline==1 ? "selected" : "") ?>>Bold Heading (+$1.25)</option>
        <option value='superbold' <?= ($featureHeadline==2 ? "selected" : "") ?>>Super Bold Heading (+$2.00)</option>
      </select>
    </div>
</div>
</fieldset>
<fieldset>

<div class="form-group">
    <label for="ad_text" class="col-sm-2 control-label">Ad Text<br>
        <div style='font-size:12px; text-align:left; padding: 2px;background-color:white;border: thin solid black;color:black;'><span >Includes <?= $wordsIncluded ?> words. Additional words are $<?= $perWordCost ?> each.</span><br>
        <b>Word Count: </b><span id='adWordCount'><?= $wordCount ?></span><br>
        <b>Overage: </b>$<span id='overCost'><?= $overCost ?></span></div>
    </label>
    <div class="col-sm-10">
      <textarea class='' id='ad_text' name='ad_text' rows=5 onKeyup='counter()'><?=  $adText ?></textarea>
    </div>
</div>
<div class="form-group">
    <label for="ad_files" class="col-sm-2 control-label">Add a photo to your ad<br>
        <?php
            if($package['photos_included']>0)
            {
                ?>
                <small>This package includes <?= $package['photos_included'] ?> photos.</small>
                <?php   
            } else {
        ?>
        <small>Cost per photo $<?= $package['per_photo_price']>0 ? $package['per_photo_price'] : '0.00' ?></small>
        <?php } ?>
    </label>
    <div class="col-sm-10">
      <input type='file' class='' id='ad_files' name='ad_files'>
    </div>
</div>
</fieldset>
<fieldset>
<h4>Please select add-ons for your ad</h4>
        <?php if( ALLOW_ONLINE_ONLY == "true" ) { ?>
            <div class="form-group1 checkboxes">
                <div class="col-sm-offset-2 col-sm-10">
                  <small></small>
                  <div class="checkbox">
                    <label <?php echo ($ad['print']==1 ? 'class="selected"' : '' )?>>
                      <input type="checkbox" name='print' <?php echo ($ad['print']==1 ? 'checked="checked"' : '' )?> /> Include my ad in print each week during my ads run dates.
                    </label>
                  </div>
                </div>
            </div>
        <?php } 
        $sql = "SELECT * FROM addons WHERE active=1";
        $dbAddons = dbselectmulti($sql);
        if( $dbAddons['numrows'] > 0 ) {
            $sql="SELECT addon_id FROM ad_addons WHERE ad_id = $adId";
            $dbAdAddons = dbselectmulti($sql);
            $adAddons = array();
            if( $dbAdAddons['numrows'] > 0 ) {
                foreach( $dbAdAddons['data'] as $a ) {
                    $adAddons[]=$a['addon_id'];
                }
            }
            $packageAddons = array();
            if($packageID!=0)
            {
                $sql="SELECT addon_id FROM package_addons WHERE package_id = $packageID";
                $dbpackageAddons = dbselectmulti($sql);
                if( $dbpackageAddons['numrows'] > 0 ) {
                    foreach( $dbpackageAddons['data'] as $a ) {
                        $packageAddons[]=$a['addon_id'];
                    }
                }
            }
            foreach( $dbAddons['data'] as $addon ) { ?>
                <div class="form-group1 checkboxes">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label <?php echo (in_array($addon['id'],$adAddons) || in_array($addon['id'],$packageAddons) ? 'class="selected"' : '' )?>>
                          <input type="checkbox" name='_addon_<?=$addon['id'] ?>' <?php echo (in_array($addon['id'],$adAddons)  || in_array($addon['id'],$packageAddons) ? 'checked="checked"' : '' )?> /><?= stripslashes($addon['addon']); if (in_array($addon['id'],$packageAddons)) { print " (<i>included in the package</i>)"; } ?>
                        </label>
                      </div>
                    </div>
                </div>
            <?php }
        } 
        
?>
</fieldset>


<fieldset>
<h4>Schedule your ad</h4>
    <div class="form-group">
        <label for="ad_files" class="col-sm-2 control-label">Start Date</label>
        <div class="col-sm-10">
           <select id='start_date' name='start_date' class=''>
             <?php
                 for($i=0;$i<=10;$i++)
                 {
                     print "<option value='".date("Y-m-d",strtotime($startDate." +$i weeks"))."'>".date("D, m/d/Y",strtotime($startDate." +$i weeks"))."</option>\n";
                 }
             ?>
           
           </select>
        </div>
    </div>
    <div class="form-group">
        <label for="ad_files" class="col-sm-2 control-label">How many weeks</label>
        <div class="col-sm-10">
           <small>How many weeks would you like the ad to run for?</small>
           <select id='end_date' name='end_date' class=''>
             <?php
                 //figure out number of weeks
                 
                 for($i=1;$i<=12;$i++)
                 {
                     print "<option value='$i' ".($i==9 ? "selected": "").">".($i==1 ? "1 week" : $i." weeks")."</option>\n";
                 }
             ?>
           
           </select>
        </div>
    </div>










<input type="hidden" id="ad_id" name="ad_id" value="<?= $adID ?>">
<input type="hidden" id="package_id" name="package_id" value="<?= $packageID ?>">
<input type="hidden" id="ad_stage" name="ad_stage" value="basics">
<?php if( ADMIN ) { ?>
    <?php make_hidden('admin_edit',1); ?>
<?php } else { ?>
    <input type="hidden" id="user_token" name="user_token" value="<?= $_SESSION['token'] ?>">
    <?php } ?>
    <div class="form-group">
        <label for="ad_files" class="col-sm-2 control-label"></label>
        <div class="col-sm-10">
           <input type='submit' name='submit' value='Summary' />
        </div>
    </div>
</form>    
    <script>
function counter() {
    console.log('firing counter');
    var adText = $('#ad_text').val();
    var textWordCount = 0;
    var textCharCount = 0;
    var allowedWordLength = 15;
    var overWordPrice = <?= $perWordCost ?>;
    var includedWords = <?= $wordsIncluded ?>;
    var overCost = 0;
    var regex = /<\/?\w+[^>]*\/?>/g;
    if( adText.trim() != '' ) {
        textWordCount = adText.trim().replace(regex, '').split(' ').length;
        textCharCount = adText.trim().replace(regex, '').length;
    }
    var wordCount = parseInt( textWordCount );
    var textLength = parseInt( textCharCount );
    if( ( wordCount * allowedWordLength ) / textLength >= 1 || textLength == 0 ) {
        if(wordCount>includedWords)
        {
            overCost = (wordCount - includedWords)*overWordPrice;
        } else {
            overCost = 0;
        }
        $('#adWordCount').html('<b>'+wordCount+'</b>');
        $('#overCost').html('<b>'+overCost.toFixed(2)+'</b>');
    } else {
        $('#adWordCount').html('<span class="busted"><b>'+wordCount+'</b></span><span class="bustedText"> -Please refrain from stringing words together.</span>');
    }
};
</script>
<?php }  /* closing the placead function */ 




?>