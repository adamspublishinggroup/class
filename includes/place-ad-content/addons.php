<?php
// skipping steps, go to jail
if( !isset($_SESSION['ad_id']) || $_SESSION['ad_id'] == 0 ){
    redirect('/place-an-ad/place-ad-basics/?new&nav=true');
}

// set ad id
$adId = intval($_SESSION['ad_id']);

// get ad
$sql = "SELECT * FROM ads WHERE id=$adId";
$dbAd = dbselectsingle($sql);
$ad = $dbAd['data'];
$status = $ad['status'];
$packageID = $ad['package_id'];
?>
<form method="post" action="/includes/handler/addons.php" class="form-horizontal">
    <?php if( $status == 3 ) { ?>
        <button type="submit" class="pull-right">Republish your ad <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <?php } else { ?>
        <button type="submit" class="pull-right">Preview your ad <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <?php } ?>
    <div class="clearFix"></div>
    <?php
    if( $status == 3 && !ADMIN ) { ?>
        <div class='alert alert-info' role='alert'><b>Attention</b> As you have already paid for this ad, you can not alter the basic add-ons. If you need to change this information, please contact us at <?= CONTACT_EMAIL ?> or <?= CONTACT_PHONE ?>.</div>
        
        <?php
        //go through any addons
        $sql = "SELECT A.* FROM addons A, ad_addons B WHERE B.ad_id=$adId AND B.addon_id=A.id";
        $dbAddons = dbselectmulti($sql);
        if( $dbAddons['numrows'] > 0 ) { ?>
            <ul><b>Selected addons</b>
            <?php foreach( $dbAddons['data'] as $addon ) { ?>
                <li><?= $addon['addon_short'] ?></li>
            <?php } ?>
            </ul>
        <?php }
    } else {
        ?>

        <h4>Please select add-ons for your ad</h4>
        <?php if( ALLOW_ONLINE_ONLY == "true" ) { ?>
            <div class="form-group1 checkboxes">
                <div class="col-sm-offset-2 col-sm-10">
                  <small></small>
                  <div class="checkbox">
                    <label <?php echo ($ad['print']==1 ? 'class="selected"' : '' )?>>
                      <input type="checkbox"  name='print' id='print' <?php echo ($ad['print']==1 ? 'checked="checked"' : '' )?> /> Include my ad in print each week during my ads run dates.
                    </label>
                  </div>
                </div>
            </div>
        <?php } 
        
        $sql = "SELECT * FROM addons WHERE active=1";
        $dbAddons = dbselectmulti($sql);
        if( $dbAddons['numrows'] > 0 ) {
            $sql="SELECT addon_id FROM ad_addons WHERE ad_id = $adId";
            $dbAdAddons = dbselectmulti($sql);
            $adAddons = array();
            if( $dbAdAddons['numrows'] > 0 ) {
                foreach( $dbAdAddons['data'] as $a ) {
                    $adAddons[]=$a['addon_id'];
                }
            }
            $packageAddons = array();
            if($packageID!=0)
            {
                $sql="SELECT addon_id FROM package_addons WHERE package_id = $packageID";
                $dbpackageAddons = dbselectmulti($sql);
                if( $dbpackageAddons['numrows'] > 0 ) {
                    foreach( $dbpackageAddons['data'] as $a ) {
                        $packageAddons[]=$a['addon_id'];
                    }
                }
            }
            $previews="";
            foreach( $dbAddons['data'] as $addon ) { 
                //build pricing element
                $notCharging = false;
                $charge = ". No charge.";
                if($addon['word_charge']!=0)
                {
                    $charge=". Charge is ".money_format('%.2n', $addon['word_charge'])." per word";    
                }elseif($addon['flat_charge']!=0)
                {
                    $charge=". Charge is ".money_format('%.2n', $addon['flat_charge'])." ";
                }elseif($addon['percentage_charge']!=0)
                {
                    $charge=". Charge is an additional ".money_format('%.2n', $addon['word_charge'])."% of the total ad";
                } else {
                    $notCharging = true;
                }
                if(!$notCharging) {
                    if($addon['charge_per_print'])
                    {
                        $charge.=" for each publication."; 
                    } else {
                        $charge.=" for each ad.";
                    }
                }
                ?>
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkboxes">
                        <label <?php echo (in_array($addon['id'],$adAddons) || in_array($addon['id'],$packageAddons) ? 'class="selected"' : '' )?>>
                          <input type="checkbox" id="_addon_<?=$addon['id'] ?>" name="_addon_<?=$addon['id'] ?>" <?php echo (in_array($addon['id'],$adAddons)  || in_array($addon['id'],$packageAddons) ? 'checked="checked"' : '' )?> /><?= rtrim(stripslashes($addon['addon']),"."); 
                          if (in_array($addon['id'],$packageAddons)) { print " (<i>included in the package</i>)"; } else { print "<small>$charge</small>"; } ?>
                          
                          <?php
                              if($addon['preview_image']!='')
                              {
                                  print "<span class='previewTip' title=\"<img src='/uploads/addons/$addon[preview_image]' />\">(Hover for example) </span>";
                              }
                          ?>
                        </label>
                        
                      </div>
                    </div>
            <?php }
            
            ?>
            <script>
                $('.previewTip').tooltipster({
                    contentAsHTML: true,
                    trigger: 'hover'
                });
            </script>
            <?php
        } 
        
    }
     
    //get the categories the ad has been selected for, then the distinct meta_tags for those categories
    $sql = "SELECT DISTINCT(A.id), A.meta_name, A.meta_type, A.meta_default, A.required 
            FROM metadata AS A, ad_category_xref B, meta_category_xref C 
            WHERE B.ad_id=$adId 
            AND B.category_id=C.category_id 
            AND A.id=C.meta_id";
    $dbMetaTags = dbselectmulti($sql);

    if( $dbMetaTags['numrows'] > 0 ) { ?>
        <fieldset>
            <h4>Details for your selected categories</h4>
            <?php
            $sql = "SELECT * FROM ad_meta WHERE ad_id=$adId";
            $dbAdMeta = dbselectmulti($sql);
            $adDetails = array();
            if( $dbAdMeta['numrows'] > 0 ) {
                foreach( $dbAdMeta['data'] as $adMeta ) {
                    $adDetails[$adMeta['meta_id']]['short']=stripslashes($adMeta['meta_value_short']);   
                    $adDetails[$adMeta['meta_id']]['long']=stripslashes($adMeta['meta_value_long']);   
                }
            }
            /*
            Possible types
            'select'=>'List of options',
            'text'=>'Text (up to 255 characters)',
            'textblock'=>'Large amount of text',
            'number'=>'Number',
            'checkbox'=>'Checkbox',
            'telephone'=>'Telephone number',
            'address'=>'Address block',
            'date'=>'Date'
            */

            foreach( $dbMetaTags['data'] as $metaTag ) {
                switch( $metaTag['meta_type'] ) {
                    case 'select': meta_select($metaTag,$adDetails); break;
                    case 'text': meta_text($metaTag,$adDetails); break;
                    case 'textblock': meta_textblock($metaTag,$adDetails); break;
                    case 'number': meta_number($metaTag,$adDetails); break;
                    case 'checkbox': meta_checkbox($metaTag,$adDetails); break;
                    case 'telephone': meta_telephone($metaTag,$adDetails); break;
                    case 'address': meta_address($metaTag,$adDetails); break;
                    case 'date': meta_date($metaTag,$adDetails); break;
                }
            }
            ?>
        </fieldset>
    <?php } ?>
    <?php
    make_hidden('ad_id',$adId);
    make_hidden('ad_stage','addons');
    ?>

    <div class="clearFix"></div>
    <br>
    <?php if( $status == 3 ) { ?>
        <button type="submit" class="pull-right">Republish your ad <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <?php } else { ?>
        <button type="submit" class="pull-right">Preview your ad <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></button>
    <?php } ?>
    
</form>


<?php
/*
All the functions to generate the various form elements
*/    
function meta_select($metaTag,$adDetails) {
    $options = explode("|",stripslashes($metaTag['meta_default']));
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_select('meta_'.$metaTag['id'],$options[$currentVal],$options,$metaTag['meta_name']);  
}
function meta_text($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_text('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']); 
}
function meta_textblock($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['long'];
    make_textarea('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']);  
}
function meta_number($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_number('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']);  
}
function meta_checkbox($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_checkbox('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']);  
}
function meta_telephone($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_phone('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']); 
}
function meta_address($metaTag,$adDetails) {
    global $config;
    $currentVal=explode("|",$adDetails[$metaTag['id']]['long']);
    $street=$currentVal[0];
    $street2=$currentVal[1];
    $city=$currentVal[2];
    $state=$currentVal[3];
    if($state==''){$state = $config['default_state'];}
    $zip=$currentVal[4];
    make_address('meta_'.$metaTag['id'],$street,$street2,$city,$state,$zip,$metaTag['meta_name']);       
}
function meta_date($metaTag,$adDetails) {
    $currentVal=$adDetails[$metaTag['id']]['short'];
    make_date('meta_'.$metaTag['id'],$currentVal,$metaTag['meta_name']);  
}
