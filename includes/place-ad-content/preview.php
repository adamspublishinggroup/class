<?php
// skipping steps, go to jail
if( !isset($_SESSION['ad_id']) || $_SESSION['ad_id'] == 0 ){
    redirect('/place-an-ad/place-ad-basics/?new&nav=true');
}

// set ad id
$adId = intval($_SESSION['ad_id']);

//create the billing record for the ad
$ad = getSingleAd($adId,true,true);
if( $ad['status'] == 3 && !ADMIN ) {
    //means we are in edit mode of a paid ad
    redirect('/place-an-ad/place-ad-basics/');
}

// get previous choices made
$imgChoice = $ad['image_choice'];
$attnChoice = $ad['attn_getter'];

// set some vars for print preview
$previewStyle = 'width:' . PRINT_COLUMN_WIDTH . ';'
                . 'font-family:' . PREVIEW_FONT_CSS . ';'
                . 'font-size:' . PREVIEW_FONT_SIZE . ';'
                . 'line-height:' . PREVIEW_LINE_HIEGHT . ';'
                . 'letter-spacing:' . PREVIEW_LETTER_SPACING . ';';
?>
<a href="/place-an-ad/place-ad-summary" class="formButton pull-right">Summary <i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
<div class="clearFix"></div>
<h4>Preview of ad</h4>
<div class="row">
    <div class="col-xs-12" style="padding-top:12px;padding-bottom:30px;">
        <p><b>This is how your ad will appear in print</b></p>
        <div class="col-md-4 col-xs-12 pull-right">
            <p><?= PREVIEW_DISCLAIMER ?></p>
        </div>
        <div id="previewPaperWrap" class="col-md-8 col-xs-12 pull-left">
            <div id="previewPaper" class="<?= getAddons($ad['id']) ?><?= ( $ad['feature_headline'] ? ' isHeadlineHC' : '' ) ?>" style="<?= $previewStyle ?>">
                <?php
                // get generic list of attention-getters
                $attns = array();
                $sql = "SELECT * FROM attention_getters";
                $dbRecord = dbselectmulti($sql);
                if( $dbRecord['numrows'] >= 1 ) {
                    foreach( $dbRecord['data'] as $row ) {
                        $attns[$row['id']] = $row['image'];
                    }
                }
                $mainImg = false;
                $mainImgTop = false;
                $mainImgBot = false;
                $adTitle = $ad['headline'];
                $adText = $ad['ad_text'];
                if( !PRINT_ALLOW_STYLES ) {
                    $adText = strip_tags($adText, '');
                } else {
                    $adText = strip_tags($adText, '<b><i><u><em><strong><br>');    
                }
                $adImages = $ad['images'];
                $adAttns = $ad['attn_getter'];
                $printImgString = '';
                $printImgStringTop = '';
                $printImgStringBot = '';
                if( !empty($adImages) ) {
                    // get print image
                    $sql="SELECT * FROM images WHERE for_print=1 AND ad_id=$adId";
                    $dbPrintImgArr = dbselectmulti($sql);
                    if( $dbPrintImgArr['numrows'] > 0 ) {
                        foreach( $dbPrintImgArr['data'] as $dbPrintImgRow ) {
                            if( $dbPrintImgRow['placement'] == 'top' ) {
                                $printImgStringTop = $dbPrintImgRow['path'] . 'thumb_' . $dbPrintImgRow['filename'];
                            }
                            if( $dbPrintImgRow['placement'] == 'bottom' ) {
                                $printImgStringBot = $dbPrintImgRow['path'] . 'thumb_' . $dbPrintImgRow['filename'];
                            }
                        }
                    }
                }
                if( $imgChoice != 0 ) {
                    if( !empty($adImages) && $imgChoice == 1 ) {
                        $mainImgTop = '<div class="printImg"><img src="/uploads/' . $printImgStringTop . '" alt="' . $adTitle . '" ></div>';
                        $mainImgBot = '<div class="printImg"><img src="/uploads/' . $printImgStringBot . '" alt="' . $adTitle . '" ></div>';
                    } elseif( !empty($adAttns) && !empty($attns) && ( $imgChoice == 2 || $imgChoice == 3 ) ) {
                        $adAttnGetter = $attns[$adAttns];
                        $mainImg = '<div class="printImg attnGtr"><img src="/uploads/attention/' . $adAttnGetter. '" alt="' . $adTitle . '" ></div>';
                    }
                }
                ?>
                <div class="printPreview-wrap">
                    <?= $mainImg ?>
                    <?= $mainImgTop ?>
                    <?php if( $ad['feature_headline'] ) { ?>
                        <div class="printTitle"><?= $adTitle ?></div>
                        <div class="printText"><?= $adText ?></div>
                    <?php } else { 
                        $adText = strip_tags($adText,'');
                        $comboText = $adTitle . ' ' . $adText;
                        $pieces = explode(" ", $comboText);
                        $first_part = implode(" ", array_splice($pieces, 0, PRINT_UPPERCASE_FIRST));
                        $other_part = implode(" ", array_splice($pieces, PRINT_UPPERCASE_FIRST));
                        $newAdText = strtoupper($first_part) . ' ' . $other_part;
                        ?>
                        <div class="printText"><?= $newAdText ?></div>
                    <?php } ?>
                    <?= $mainImgBot ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row clearfix">
    <div class="col-xs-12" style="padding-top:30px;">
        <p><b>This is how your ad will appear online</b></p>
        <div id="previewListing" style="width:100%;max-width:604px;">
            <div id="toggleTriggerPL">Show More (Detail View)</div>
            <?php
            // get generic list of attention-getters
            $attns = array();
            $sql = "SELECT * FROM attention_getters";
            $dbRecord = dbselectmulti($sql);
            if( $dbRecord['numrows'] >= 1 ) {
                foreach( $dbRecord['data'] as $row ) {
                    $attns[$row['id']] = $row['image'];
                }
            }
            // set empty list of saved ads
            $savedAds = array();
            ?>
            <?php classListing( $ad, 'preview', $savedAds, $attns ); ?>
        </div>
        <div id="previewDetail" class="post-detail-wrap classPost shown <?= getAddons($ad['id']) ?>" style="width:100%;max-width:604px;">
            <div id="toggleTriggerPD">Show Less (Listing View)</div>
            <div class="detail-title">
                <div class="h1-like">
                    <?= $ad['headline'] ?>
                </div>
            </div>
            <?php classDetail( $ad, 'preview', $savedAds, $attns ); ?>
        </div>
    </div>
</div>
