<?php
// skipping steps, go to jail
if( !isset($_SESSION['ad_id']) || $_SESSION['ad_id'] == 0 ){
    redirect('/place-an-ad/place-ad-basics/?new&nav=true');
}

// set ad id
$adId = intval($_SESSION['ad_id']);
$hasTop = 0;
$hasBottom = 0;
// get previous choices made
$imgChoice = 0;
$attnChoice = 0;
$sql = "SELECT image_choice, attn_getter FROM ads WHERE id = $adId";
$dbChoices = dbselectsingle($sql);
if( $dbChoices['numrows'] > 0 ) {
    $imgChoice = $dbChoices['data']['image_choice'];
    $attnChoice = $dbChoices['data']['attn_getter'];
}
?>
<a href="/place-an-ad/place-ad-dates" class="formButton pull-right">Dates<i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
<div class="clearFix"></div>
<div class="row">
    <div class="col-xs-12">

        <!-- Choice Section -->
        <h4>What sort of images would you like?</h4>
        <div class="img-attn-choice-radios-wrap row">
            <div id="imgAttnChoiceRadios" class="img-attn-choice-radios col-xs-12">

                <label><input class="img-attn-choice-radio" type="radio" name="img-attn-choice" value="none"<?= ( $imgChoice == 0 ? ' checked="checked"' : '' ) ?>> I don't want any images on my ad.</label>
                <label><input class="img-attn-choice-radio" type="radio" name="img-attn-choice" value="imageOnly"<?= ( $imgChoice == 1 ? ' checked="checked"' : '' ) ?>> I'd like to upload some images. (This will appear in the online ad and in the print ad.)</label>

                <?php if($config['show_attention_getters']) { ?>
                    <label><input class="img-attn-choice-radio" type="radio" name="img-attn-choice" value="attnGetterOnly"<?= ( $imgChoice == 2 ? ' checked="checked"' : '' ) ?>> I don't have of my own images, I'd like to use an Attention Getter image. (This will appear in the online ad and in the print ad.)</label>
                    <label><input class="img-attn-choice-radio" type="radio" name="img-attn-choice" value="both"<?= ( $imgChoice == 3 ? ' checked="checked"' : '' ) ?>> I'd like to upload my images and use an Attention Getter. (Uploaded images will appear online and the Attention Getter image will appear in print.)</label>
                <?php } ?>

            </div>
        </div>
        <br>

        <!-- Attention Getters Section -->
        <div id="attentionGetterWrap" class="attention-getter-wrap<?= ( ( $imgChoice == 2 || $imgChoice == 3 ) ? ' shown' : '' ) ?>">
            <div id="setAttnGtr-wrap" data-selected="0">
                <div id="setAttnGtr">
                    <h4>Select an attention getting image from our gallery</h4>
                    <div class="attnGtrs">
                        <div class="attnGtr attnGtrClear<?= ( $attnChoice == 0 ? ' selected' : '' ) ?>" data-id="0" style="background-image:url();"><br>Clear<br>Selection<br>(No Image)</div>
                        <?php
                        $sql="SELECT * FROM attention_getters ORDER BY name";
                        $dbAttentionGetters = dbselectmulti($sql);
                        if($dbAttentionGetters['numrows']>0) {
                            foreach($dbAttentionGetters['data'] as $aget) {
                                ?><div class="attnGtr<?= ( $attnChoice == $aget['id'] ? ' selected' : '' ) ?>" data-id="<?= $aget['id'] ?>" style="background-image:url(/uploads/attention/<?= $aget['image'] ?>);"></div><?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <br>

        <!-- Images Section -->
        <div id="imageUploaderWrap" class="image-uploader-wrap<?= ( ( $imgChoice == 1 || $imgChoice == 3 ) ? ' shown' : '' ) ?>">
            <!-- Image Upload Area -->
            <h4>Upload your ad images here (max size 4mb, in jpeg format)</h4>
            <p>After uploading, you can use the <i class="fa fa-pencil"></i> icon to edit your image. You'll be able to crop and rotate it.</p>
            <form method="post" action="/includes/handler/images.php" class="form-horizontal">
                <div class="form-group">
                    <label for="imageUploader" class="col-sm-2 control-label"><b>Uploader</b></label>
                    <div class="col-sm-10">
                        <input type="file" class="form-control" id="imageUploader" name="imageUploader">
                    </div>
                </div>
                <?php
                make_hidden('ad_id',$adId);
                make_hidden('ad_stage','images');
                ?>
            </form>
            <button type="button" onClick="$('#imageUploader').fileinput('upload');" class="formButton pull-right">Upload this photo</button>
            <!-- Image Edit Area -->
            <div id="cropperTool" class="row" style="display:none;">
                <input id="croppingImageID" val=0 type="hidden" />
                <div id="cropper" class="col-xs-9">
                    <img id="cropperImage" src="" />
                </div>
                <div class="col-xs-3">
                    <div class="docs-preview clearfix">
                        <div class="img-preview preview-md"></div>
                        <div class="img-preview preview-sm"></div>
                    </div>
                    <div class="toolButtons">
                        <?php 
                        $editBtnHtml = '';
                        $editButtons = array(
                                    array( array('method'=>'setDragMode', 'option'=>'move', 'title'=>'Move', 'fa_icon'=>'fa-arrows'), 
                                           array('method'=>'setDragMode', 'option'=>'crop', 'title'=>'Crop', 'fa_icon'=>'fa-crop') ),
                                    array( array('method'=>'zoom', 'option'=>'0.1', 'title'=>'Zoom In', 'fa_icon'=>'fa-search-plus'), 
                                           array('method'=>'zoom', 'option'=>'-0.1', 'title'=>'Zoom Out', 'fa_icon'=>'fa-search-minus') ),
                                    array( array('method'=>'rotate', 'option'=>'-90', 'title'=>'Rotate Left', 'fa_icon'=>'fa-rotate-left'), 
                                           array('method'=>'rotate', 'option'=>'90', 'title'=>'Rotate Right', 'fa_icon'=>'fa-rotate-right') ),
                                    array( array('method'=>'scaleX', 'option'=>'-1', 'title'=>'Flip Horizontal', 'fa_icon'=>'fa-arrows-h'), 
                                           array('method'=>'scaleY', 'option'=>'-1', 'title'=>'Flip Vertical', 'fa_icon'=>'fa-arrows-v') ),
                                    array( array('method'=>'crop', 'option'=>'', 'title'=>'Crop', 'fa_icon'=>'fa-check'), 
                                           array('method'=>'clear', 'option'=>'', 'title'=>'Clear', 'fa_icon'=>'fa-remove') ),
                                );
                        foreach( $editButtons as $btnGroup ) {
                            $editBtnHtml .= '<div class="btn-group">';
                            foreach( $btnGroup as $btn ) {
                                $editBtnHtml .= '<button type="button" class="formButton"';
                                $editBtnHtml .= ' data-method="' . $btn['method'] . '"';
                                $editBtnHtml .= ' data-option="' . $btn['option'] . '"';
                                $editBtnHtml .= ' title="' . $btn['title'] . '"';
                                $editBtnHtml .= '>';
                                $editBtnHtml .= '<span class="fa ' . $btn['fa_icon'] . '"></span>';
                                $editBtnHtml .= '</button>&nbsp';
                            }
                            $editBtnHtml .= '</div><br>';
                        }
                        echo $editBtnHtml;
                        ?>
                        <button type="button" class="formButton" onClick="saveCroppedImage();">Save Image</button>
                    </div>
                </div>
            </div>
            <div class="clearFix"></div>
            <!-- Image Preview Area -->
            <div><b>Uploaded Images</b></div>
            <div id="postImages">
                <?php
                //see if there are any existing images for this ad
                $sql = "SELECT * FROM images WHERE ad_id=" . intval($adId);
                $dbImages = dbselectmulti($sql);
                if( $dbImages['numrows'] > 0 ) {
                    foreach( $dbImages['data'] as $image ) { ?>
                        <div id="image_<?= $image['id'] ?>" class="adPreview">
                            <div class="col-xs-3"><img src="/uploads/<?= $image['path'] ?>thumb_<?= $image['filename'] ?>" class="img-thumbnail postPreviewImage" /></div>
                            <div class="col-xs-9">
                                <div class="btn-group">
                                    <button type="button" onClick="editImage(<?= $image['id'] ?>)" class="btn btn-success"><i class="fa fa-pencil"></i> Edit</button>
                                    <?php if($image['for_print']) { 
                                        if($image['placement']=='top'){$hasTop = 1;}
                                        if($image['placement']=='bottom'){$hasBottom = 1;}
                                        ?>
                                        <button type="button" id="pimage_<?= $image['id'] ?>" onClick="setPrintImage(<?= $image['id'] ?>,<?= $adId ?>)" class="btn btn-primary pimage"><i class="fa fa-check "></i> This is the print image</button>
                                        <button type="button" id="imageid_<?= $image['id'] ?>_top" onClick="setPrintPlacement(<?= $image['id'] ?>,<?= $adId ?>,'top')" data-selected=<?= $hasTop ?> class="btn btn-info placement_<?= $image['id'] ?>"><?= ($image['placement']=='top' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-square-o"></i>' ) ?> Top of Ad</button>
                                        <button type="button" id="imageid_<?= $image['id'] ?>_bottom" onClick="setPrintPlacement(<?= $image['id'] ?>,<?= $adId ?>,'bottom')" data-selected=<?=$hasBottom ?> class="btn btn-info placement_<?= $image['id'] ?>"><?= ($image['placement']=='bottom' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-square-o"></i>' ) ?> Bottom of Ad</button>
                                       
                                    <?php } else { ?>
                                        <button type="button" id="pimage_<?= $image['id'] ?>" onClick="setPrintImage(<?= $image['id'] ?>,<?= $adId ?>)" class="btn btn-primary pimage">Set as the print image</button>
                                        <button type="button" id="imageid_<?= $image['id'] ?>_top" onClick="setPrintPlacement(<?= $image['id'] ?>,<?= $adId ?>,'top')" data-selected=0  class="btn btn-info placement_<?= $image['id'] ?>" style="display:none;"><?= ($image['placement']=='top' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-square-o"></i>' ) ?> Top of Ad</button>
                                        <button type="button" id="imageid_<?= $image['id'] ?>_bottom" onClick="setPrintPlacement(<?= $image['id'] ?>,<?= $adId ?>,'bottom')" data-selected=0 class="btn btn-info placement_<?= $image['id'] ?>" style="display:none;"><?= ($image['placement']=='bottom' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-square-o"></i>' ) ?> Bottom of Ad</button>
                                    <?php } ?>
                                    <button type="button" onClick="removeImage(<?= $image['id'] ?>)" class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearFix"></div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

    </div>
</div>


<script>
    var hasTop = <?= $hasTop ?>;
    var hasBottom = <?= $hasBottom ?>;
    var maxImages = <?= ($config['max_images']!='' ? $config['max_images'] : 99 ) ?>;
    var imageCount = <?= $dbImages['numrows'] ?>; 
    var $image;
    var adID = <?= $adId ?>;
    $("#imageUploader").fileinput({
        showUpload: false,
        uploadAsync: false,
        dropZoneEnabled: false,
        uploadUrl: "/includes/handler/images.php",
        allowedFileTypes: ["image"],
        allowedFileExtensions:['jpg', 'gif', 'png', 'jpeg'],
        maxFileSize: 8192,
        uploadExtraData: function() {
            return {
                adID: adID
            };
        }
    }).on('filebatchuploadsuccess', function(event, data, previewId, index) {
        addImage(data);
    });

    // set image choice on the ad
    $('.img-attn-choice-radios input').click(function(){
        var $imgWrap = $('#imageUploaderWrap');
        var $attnWrap = $('#attentionGetterWrap');
        var $this = $(this);
        var $thisChoice = $this.val();
        var $dbChoice = 0;
        <?php if(MODE=='testing'){ ?>console.log($thisChoice);<?php } ?>

        if( $thisChoice == 'none' ) {
            $dbChoice = 0;
            $imgWrap.removeClass('shown');
            $attnWrap.removeClass('shown');
        } else if( $thisChoice == 'imageOnly' ) {
            $dbChoice = 1;
            $imgWrap.addClass('shown');
            $attnWrap.removeClass('shown');
        } else if( $thisChoice == 'attnGetterOnly' ) {
            $dbChoice = 2;
            $imgWrap.removeClass('shown');
            $attnWrap.addClass('shown');
        } else if( $thisChoice == 'both' ) {
            $dbChoice = 3;
            $imgWrap.addClass('shown');
            $attnWrap.addClass('shown');
        }
        $.ajax({
            url: "/ajax/setAttnGetter.php",
            type: "POST",
            data: {
                gate: 'choice',
                this_choice: $dbChoice, 
                ad_id: <?= $adId ?>
            },
            dataType: 'json',
            success: function(response){
                console.log(response);
            },
            error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
            }
       });
    });

    // set attention getter on the ad
    $('.attnGtr').click(function(e){
        e.preventDefault();
        var $this = $(this);
        var $thisId = $this.data('id');
        var $wrap = $('#setAttnGtr-wrap');
        var $all = $('.attnGtr');
        $wrap.data('selected',$thisId);
        $all.removeClass('selected');
        $this.addClass('selected');
        $.ajax({
            url: "/ajax/setAttnGetter.php",
            type: "POST",
            data: {
                gate: 'attnGtr',
                attn_gtr_id: $thisId, 
                ad_id: <?= $adId ?>
            },
            dataType: 'json',
            success: function(response){
                console.log(response);
            },
            error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
            }
       });
    });

    function addImage(data)
    {
        var form = data.form, files = data.files, extra = data.extra, response = data.response, reader = data.reader;

        if(response.status=='success'){
            if(response.count==0){
                
                <?php if ($config['show_top_bottom_placement']) { ?>
                var $placementButtons = '<button type="button" id="imageid_'+response.image_id+'_top" onClick="setPrintPlacement('+response.image_id+','+adID+',\'top\')" data-selected=0 class="btn btn-info placement_'+response.image_id+'"><i class="fa fa-square-o"></i> Top of Ad</button><button type="button" id="imageid_'+response.image_id+'_bottom" data-selected=0 onClick="setPrintPlacement('+response.image_id+','+adID+',\'bottom\')" class="btn btn-info placement_'+response.image_id+'"><i class="fa fa-square-o"></i> Bottom of Ad</button>';            
                <?php } else { ?>
                var $placementButtons = '';
                <?php } ?>
                var $newDiv = $("<div />").prop('id',"image_"+response.image_id).addClass('col-xs-12 adPreview');
                $newDiv.html("<div class='col-xs-3'><img src='/uploads/"+response.filename+"' class='img-thumbnail postPreviewImage' /></div><div class='col-xs-9'><div class='btn-group'><button type='button' onClick='editImage("+response.image_id+")' class='btn btn-success'><i class='fa fa-pencil'></i> Edit</button><button type='button' id='pimage_"+response.image_id+"' onClick='setPrintImage("+response.image_id+","+adID+")' class='btn btn-primary pimage'><i class='fa fa-check'></i> This is a print image</button>"+$placementButtons+"<button type='button' onClick='removeImage("+response.image_id+")' class='btn btn-danger'><i class='fa fa-trash'></i> Delete</button></div></div></div>");
            } else {
                <?php if ($config['show_top_bottom_placement']) { ?>
                var $placementButtons = '<button type="button" id="imageid_'+response.image_id+'_top" data-selected=0 onClick="setPrintPlacement('+response.image_id+','+adID+',\'top\')" class="btn btn-info placement_'+response.image_id+'" style="display:none;"><i class="fa fa-square-o"></i> Top of Ad</button><button type="button" id="imageid_'+response.image_id+'_bottom" data-selected=0 onClick="setPrintPlacement('+response.image_id+','+adID+',\'bottom\')" class="btn btn-info placement_'+response.image_id+'" style="display:none;"><i class="fa fa-square-o"></i> Bottom of Ad</button>'; 
                <?php } else { ?>
                var $placementButtons = '';
                <?php } ?>
                           
                var $newDiv = $("<div />").prop('id',"image_"+response.image_id).addClass('col-xs-12 adPreview');
                $newDiv.html("<div class='col-xs-3'><img src='/uploads/"+response.filename+"' class='img-thumbnail postPreviewImage' /></div><div class='col-xs-9'><div class='btn-group'><button type='button' onClick='editImage("+response.image_id+")' class='btn btn-success'><i class='fa fa-pencil'></i> Edit</button><button type='button' id='pimage_"+response.image_id+"' onClick='setPrintImage("+response.image_id+","+adID+")' class='btn btn-primary pimage'>Set as a print image</button>"+$placementButtons+"<button type='button' onClick='removeImage("+response.image_id+")' class='btn btn-danger'><i class='fa fa-trash'></i> Delete</button></div></div></div>");
            }
            $clearFix = $("<div />").addClass('clearFix');
            $('#postImages').append($newDiv);
            $('#postImages').append($clearFix);
            $('#imageUploader').fileinput('reset');
            imageCount++;
            if(imageCount>=maxImages)
            {
                $('#imageUploaderWrap').hide();
            }
        } else {
            
        }

    }

    function editImage(imageID)
    {
        //get image details and load them into the #cropper div and enable the cropper plugin
        $.ajax({
          url: "/includes/imageInfo.php",
          type: "POST",
          data: {imageID: imageID},
          dataType: 'json',
          success: function(response){
              <?php if(MODE=='testing'){ ?>console.log(response);<?php } ?>
              if(response.status=='success'){
                  $('#cropperImage').attr('src','/uploads/'+response.filename);
                  $('#cropperTool').show();
                  $('#croppingImageID').val(imageID);
                  initCropper();
              }
          },
          error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
          }
       });
    }

    function removeImage(imageID)
    {
        //get image details and load them into the #cropper div and enable the cropper plugin
        $.ajax({
          url: "/includes/imageDelete.php",
          type: "POST",
          data: {imageID: imageID},
          dataType: 'json',
          success: function(response){
              <?php if(MODE=='testing'){ ?>console.log(response);<?php } ?>
              if(response.status=='success'){
                  $('#image_'+imageID).remove();
                  imageCount--;
                  $('#imageUploaderWrap').show();
              }
          },
          error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
          }
       });
    }

    function setPrintImage(imageID,adID)
    {
        //set the selected image as the image to be used in the print ad, defaults to the first image uploaded
        $.ajax({
            url: "/includes/handler/imageForPrint.php",
            type: "POST",
            data: {image_id: imageID, ad_id:adID},
            dataType: 'json',
            success: function(response){
                <?php if(MODE=='testing'){ ?>console.log(response);<?php } ?>
                if(response.status=='success'){
                    //set the one clicked as print image
                    if(response.for_print==1)
                    {
                        $('#pimage_'+imageID).html("<i class='fa fa-check'></i> This is a print image");
                        $('.placement_'+imageID).show();
                    } else {
                        $('#pimage_'+imageID).html("Set as a print image");
                        $('#placement_'+imageID).hide();
                    }
                }
            },
            error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
            }
       });
    }

    function setPrintPlacement(imageID,adID,placement)
    {
        <?php if(MODE=='testing'){ ?>console.log('placement: '+placement);<?php } ?>
        //get this element
        var $item = $('#imageid_'+imageID+'_'+placement);
        if($item.data('selected')==1)
        {
            //means we're toggling something off
            var sel = 1;
            var originPlacement = placement;
            placement = '';
        } else {
            var sel = 0;
            if(placement == 'top' && hasTop)
            {
                alert("You may only have one image each at the top and/or bottom of your print ad.")
                return false;
            }
            if(placement =='bottom' && hasBottom)
            {
                alert("You may only have one image each at the top and/or bottom of your print ad.")
                return false;
            }
        }
        $.ajax({
            url: "/includes/handler/imageForPlacement.php",
            type: "POST",
            data: {image_id: imageID, ad_id:adID, placement: placement, selected: sel},
            dataType: 'json',
            success: function(response){
                <?php if(MODE=='testing'){ ?>console.log(response);<?php } ?>
                if(response.status=='success'){
                    if(sel == 1)
                    {
                        if(originPlacement=='top')
                        {
                            $('#imageid_'+imageID+'_top').html("<i class='fa fa-square-o'></i> Top of Ad").data('selected',0);
                        } else {
                            $('#imageid_'+imageID+'_bottom').html("<i class='fa fa-square-o'></i> Bottom of Ad").data('selected',0);
                        }
                    } else {
                        if(placement=='top')
                        {
                            $('#imageid_'+imageID+'_top').html("<i class='fa fa-check'></i> Top of Ad").data('selected',1);
                            $('#imageid_'+imageID+'_bottom').html("<i class='fa fa-square-o'></i> Bottom of Ad").data('selected',0);
                        } else {
                            $('#imageid_'+imageID+'_top').html("<i class='fa fa-square-o'></i> Top of Ad").data('selected',0);
                            $('#imageid_'+imageID+'_bottom').html("<i class='fa fa-check'></i> Bottom of Ad").data('selected',1);
                        }
                    }
                    hasTop = response.hasTop;
                    hasBottom = response.hasBottom;
                }
            },
            error: function (xhr, desc, er) {
              console.log('An error occurred: '+desc);
              return false
            }
       });
       return false;
    }

    function saveCroppedImage(imageID)
    {
        var imageID = $('#croppingImageID').val();
        result = $image.cropper('getCanvasData');
        <?php if(MODE=='testing'){ ?>console.log("Canvas Data");<?php } ?>
        <?php if(MODE=='testing'){ ?>console.log(result);<?php } ?>

        var canvasX = result.left;
        var canvasY = result.top;
        var canvasWidth = result.width;
        var canvasHeight = result.height;
        var naturalWidth = result.naturalWidth;
        var naturalHeight = result.naturalHeight;

        result = $image.cropper('getImageData');
        <?php if(MODE=='testing'){ ?>console.log("Image Data");<?php } ?>
        <?php if(MODE=='testing'){ ?>console.log(result);<?php } ?>

        var rotation = result.rotate;
        var scaleX = result.scaleX;
        var scaleY = result.scaleY;

        result = $image.cropper('getCropBoxData');
        <?php if(MODE=='testing'){ ?>console.log("Crop Box Data");<?php } ?>
        <?php if(MODE=='testing'){ ?>console.log(result);<?php } ?>

        var cropX = result.left;
        var cropY = result.top;
        var cropWidth = result.width;
        var cropHeight = result.height;

        $.ajax({
            url: "/includes/imageManipulator.php",
            type: "POST",
            data: {imageID: imageID,
            canvasX: canvasX,
            canvasY: canvasY,
            canvasWidth: canvasWidth,
            canvasHeight: canvasHeight,
            naturalWidth: naturalWidth,
            naturalHeight: naturalHeight,
            rotation: rotation,
            scaleX: scaleX,
            scaleY: scaleY,
            cropX: cropX,
            cropY: cropY,
            cropWidth: cropWidth,
            cropHeight: cropHeight},
            dataType: 'json',
            success: function(response){
                <?php if(MODE=='testing'){ ?>console.log(response);<?php } ?>
                if(response.status=='success')
                {
                    var unique = $.now();
                    var currentSrc = $('#image_'+imageID+' img').attr('src');
                    $('.captcha_pic').attr('src', 'http://localhost/captcha.php?' + unique);
                    $('#image_'+imageID+' img').attr('src', currentSrc +'?' + unique);
                    $('#cropperTool').hide();
                    $('#cropperImage').attr('src','#');
                    $('#croppingImageID').val(0);
                }
            },
            error: function (xhr, desc, er) {
                console.log('An error occurred: '+desc);
                return false
            }
        });
    }

    function initCropper()
    {
        var console = window.console || { log: function () {} };
        var URL = window.URL || window.webkitURL;
        $image = $('#cropperImage');
        var $dataX = 0;
        var $dataY = 0;
        var $dataHeight = 0;
        var $dataWidth = 0;
        var $dataRotate = 0;
        var $dataScaleX = 100;
        var $dataScaleY = 100;

        var options = {
            preview: '.img-preview',
            crop: function (e) {
              $dataX=(Math.round(e.x));
              $dataY=(Math.round(e.y));
              $dataHeight=(Math.round(e.height));
              $dataWidth=(Math.round(e.width));
              $dataRotate=(e.rotate);
              $dataScaleX=(e.scaleX);
              $dataScaleY=(e.scaleY);
              <?php if(MODE=='testing'){ ?>console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);<?php } ?>
            }
        };

        var originalImageURL = $image.attr('src');
        var uploadedImageType = 'image/jpeg';
        var uploadedImageURL;

        // Cropper
        $image.on({
            ready: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type);<?php } ?>
            },
            cropstart: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type, e.action);<?php } ?>
            },
            cropmove: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type, e.action);<?php } ?>
            },
            cropend: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type, e.action);<?php } ?>
            },
            crop: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type, e.x, e.y, e.width, e.height, e.rotate, e.scaleX, e.scaleY);<?php } ?>
            },
            zoom: function (e) {
                <?php if(MODE=='testing'){ ?>console.log(e.type, e.ratio);<?php } ?>
            }
        }).cropper(options);


        if (typeof document.createElement('cropper').style.transition === 'undefined') {
            $('button[data-method="rotate"]').prop('disabled', true);
            $('button[data-method="scale"]').prop('disabled', true);
        }

        // Methods
        $('.toolButtons').on('click', '[data-method]', function () {
            var $this = $(this);
            var data = $this.data();
            var $target;
            var result;

            if ($this.prop('disabled') || $this.hasClass('disabled')) {
                return;
            }

            if ($image.data('cropper') && data.method) {
                data = $.extend({}, data); // Clone a new one

                if (typeof data.target !== 'undefined') {
                    $target = $(data.target);
                    if (typeof data.option === 'undefined') {
                        try {
                            data.option = JSON.parse($target.val());
                        } catch (e) {
                            console.log(e.message);
                        }
                    }
                }

                switch (data.method) {
                    case 'rotate':
                        $image.cropper('clear');
                        break;
                }

                result = $image.cropper(data.method, data.option, data.secondOption);

                switch (data.method) {
                    case 'rotate':
                        $image.cropper('crop');
                        break;
                    case 'scaleX':
                    case 'scaleY':
                        $(this).data('option', -data.option);
                        break;
                    case 'destroy':
                        if (uploadedImageURL) {
                            URL.revokeObjectURL(uploadedImageURL);
                            uploadedImageURL = '';
                            $image.attr('src', originalImageURL);
                        }
                        break;
                }

                if ($.isPlainObject(result) && $target) {
                    try {
                        console.log(result);
                        $target.val(JSON.stringify(result));
                    } catch (e) {
                        console.log(e.message);
                    }
                }

            }
        });
    }

</script>