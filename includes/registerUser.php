<?php
  require_once("bootCore.php");
  $json['status']='error';
  
  $userName=addslashes($_POST['userName']);
  $firstName=addslashes($_POST['firstName']);
  $lastName=addslashes($_POST['lastName']);
  $email=addslashes($_POST['email']);
  $password=addslashes($_POST['pass']);
  $regmessage=addslashes($_POST['message']);
  $street=addslashes($_POST['street']);
  $city=addslashes($_POST['city']);
  $state=addslashes($_POST['state']);
  $zip=addslashes($_POST['zip']);
  $phone=addslashes($_POST['phone']);
  $createdDT = date("Y-m-d H:i");
  $password=password_hash($password,PASSWORD_DEFAULT);
  $token = generate_random_string(32);
  $sql="INSERT INTO users (username, first, last, email, password, street, city, sate, zip, phone, token, verified, level, created_dt, 13_check) VALUES ('$userName', '$firstName', '$lastName','$email', '$password', '$street', '$city', '$state', '$zip', '$phone', '$token', 0, 1, '$createdDT', 1)";
  $dbInsert=dbinsertquery($sql);
  $userID=$dbInsert['insertid'];
  
  if($dbInsert['error']=='')
  {
      addUserAction($userID,'register');
      
      $emailData = array('name'=>"$firstName $lastName",
      'first'=>$firstName,
      'last'=>$lastName,
      'token'=>$token,
      'email'=>$email);
      
      $emailContent = email_generator(3,$emailData);
      
      $subject = $emailContent['subject'];
      $message = $emailContent['body'];
      
      
      //send confirmation email to user
      if(send_email($email,$subject,$message))
      {
          $json['status']='success';  
      } else {
          $json['message']='Unable to send email';
      } 
      
      //admin also gets user signup email
    $emailData = array('name'=>"$firstName $lastName",
      'first'=>$firstName,
      'last'=>$lastName,
      'street'=>$street,
      'city'=>$city,
      'state'=>$state,
      'zip'=>$zip,
      'phone'=>$phone,
      'email'=>$email,
      'message'=>$regmessage);
      
    $emailContent = email_generator(9,$emailData);

    $subject = $emailContent['subject'];
    $message = $emailContent['body'];
    //send new user email to admin
    send_email(CONTACT_EMAIL,$subject,$message);
      
      
  } else {
      $json['message']='Unable to create user';
  }
  
  echo json_encode($json); 