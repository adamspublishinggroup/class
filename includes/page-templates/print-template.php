<?php //require_once( INCLUDE_DIR . 'site_header.php' ); ?>

<main class="template-print">
	<?php require_once( INCLUDE_DIR . 'page-sections/print-title.php' ); ?>
	<?php require_once( INCLUDE_DIR . 'page-content/' . $page . '.php' ); ?>
	<div class="clearFix"></div>
</main>

<?php //require_once( INCLUDE_DIR . 'site_footer.php' ); ?>

<script>(function ($) { $(function () { window.print(); }); })(jQuery);</script>
