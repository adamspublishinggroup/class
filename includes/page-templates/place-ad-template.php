<?php require_once( INCLUDE_DIR . 'site_header.php' ); ?>

<main class="template-place-ad">
	<?php require_once( INCLUDE_DIR . 'page-sections/site_changes.php' ); ?>
	<div class="mainContainer">
		<div class="left-content">
			<?php require_once( INCLUDE_DIR . 'page-sections/page-title.php' ); ?>
			<?php 
            if($config['ad_order_mode']=='simple' && $currPage!='checkout' && $currPage!='summary')
            {
                ?>
                <div class="content-wrap-wrap">
                <div class="content-wrap"><div class="content">
                    <?php require_once( INCLUDE_DIR . 'place-ad-content/simple.php' ); ?>
                    <div class="clearFix"></div>
                </div></div>
            </div>
                <?php
            } else {
                if($config['ad_order_mode']!='simple') require_once( INCLUDE_DIR . 'page-sections/place-ad-nav.php' ); 
                ?>
                <div class="content-wrap-wrap">
                <div class="content-wrap"><div class="content">
                    <?php require_once( INCLUDE_DIR . 'place-ad-content/' . $currPage . '.php' ); ?>
                    <div class="clearFix"></div>
                </div></div>
            </div>
                <?php
            }
            ?>
			
		</div>
		<div class="right-content">
			<?php require_once( INCLUDE_DIR . 'page-sections/right-rail.php' ); ?>
		</div>
	</div>
</main>

<?php require_once( INCLUDE_DIR . 'site_footer.php' ); ?>
