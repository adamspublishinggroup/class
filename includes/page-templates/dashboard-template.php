<?php require_once( INCLUDE_DIR . 'site_header.php' ); ?>

<main class="template-dashboard">
	<?php require_once( INCLUDE_DIR . 'page-sections/site_changes.php' ); ?>
	<div class="mainContainer">
		<div class="left-content">
			<?php require_once( INCLUDE_DIR . 'page-sections/page-title.php' ); ?>
			<?php require_once( INCLUDE_DIR . 'page-sections/dashboard-nav.php' ); ?>
			<div class="content-wrap-wrap">
				<div class="content-wrap"><div class="content">
					<?php require_once( INCLUDE_DIR . 'dashboard-content/' . $currPage . '.php' ); ?>
					<div class="clearFix"></div>
				</div></div>
			</div>
		</div>
		<div class="right-content">
			<?php require_once( INCLUDE_DIR . 'page-sections/right-rail.php' ); ?>
		</div>
	</div>
</main>

<?php require_once( INCLUDE_DIR . 'site_footer.php' ); ?>
