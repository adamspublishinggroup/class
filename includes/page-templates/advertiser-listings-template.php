<?php require_once( INCLUDE_DIR . 'site_header.php' ); ?>

<main class="template-advertiser-listings">
	<?php require_once( INCLUDE_DIR . 'page-sections/site_changes.php' ); ?>
	<div class="titleContainer">
		<?php require_once( INCLUDE_DIR . 'page-sections/page-title-advertiser.php' ); ?>
	</div>
	<div class="mainContainer">
		<div class="left-content">
			<div class="content-wrap-wrap">
				<div class="content-wrap"><div class="content">
					<?php require_once( INCLUDE_DIR . 'page-content/' . $page . '.php' ); ?>
					<div class="clearFix"></div>
				</div></div>
			</div>
		</div>
		<div class="right-content">
			<?php require_once( INCLUDE_DIR . 'page-sections/right-rail.php' ); ?>
		</div>
	</div>
</main>

<?php require_once( INCLUDE_DIR . 'site_footer.php' ); ?>
