<?php
/*
*    create some form variables that are global in score

*/
$tableSearch = '';
$tableOptions = '';

//print a text box
function labelStart($label, $id, $type='normal')
{
    if ($type=='normal') { ?>
        <div class="form-group">
            <label for="<?= $id ?>" class="col-sm-2 control-label"><?= $label ?></label>
            <div class="col-sm-10">
    <?php } elseif ($type=='checkbox') { ?>
        <div class="form-group checkboxes">
            <label for="<?= $id ?>" class="col-sm-2 control-label"><?= $label ?></label>
            <div class="col-sm-10">
                <div class="checkbox">
                    <label>
    <?php } else { ?>
        <div class="form-group">
            <label for="<?= $id ?>" class="col-sm-2 control-label"><?= $label ?></label>
            <div class="col-sm-10">
                <div>
                    <label>
    <?php }
}

function labelEnd($inputHTML='',$type='normal',$explain='')
{
    if ($type=='normal') { ?>
                <?= $inputHTML ?>
                <?= ($explain!='' ? '<br><small>'.$explain.'</small>' : '') ?>
            </div>
        </div>
    <?php } else { ?>
                        <?= $inputHTML ?>
                    </label>
                </div>
            </div>
        </div>
    <?php }
}

function tableStart($formoptions,$headers,$cols=0,$searchblock='',$sortcol='false',$hasAction=true)
{
    global $tableSearch, $tableOptions;
    $tableOptions = $formoptions;
    $tableSearch = $searchblock;
    ?>
    <div class="row">
        <div class="col xs-12 col-sm-9"> <!-- opens up the div for the table! -->
            <div class="table-responsive" style="min-height:75vh !important;">
                <table id="stable" class="table table-striped table-bordered table-hover display responsive no-wrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <?php
                            $headers = explode(",", $headers);
                            $h = 0;
                            foreach($headers as $key => $header) {
                                echo '<th>'.$header.'</th>';
                                $h++;
                            }
                            if ($hasAction) {
                                echo '<th style="min-width:130px;">Actions</th>';
                            }
                            ?>
                        </tr>
                    </thead>
                    <tbody>
    <?php
}


function tableEnd($set=array('numrows'=>'0'),$extrascript='',$sortcol=0,$sortdir='asc',$stateSave='false')
{
    global $tableSearch, $tableOptions;
    if ($_POST['stateSave']) { $stateSave = 'true'; }
    if ($set['numrows'] > 10) { $limit = 10; } else { $limit = $set['numrows']; }
    // wrap up, we're not using anything fancy other than the delete box
    ?>
                    </tbody>
                </table>
                <script>
                    $('#stable').dataTable( {
                        paging: true,
                        responsive: true,
                        columnDefs: [
                            { responsivePriority: 1, targets: 0 },
                            { responsivePriority: 2, targets: -1 }
                        ],
                        colReorder: true,
                        pageLength: 25,
                        pagingType: "full_numbers",
                        lengthChange: true,
                        search: {
                            smart: true
                          },
                        stateSave: <?= $stateSave ?>,
                        order: [[ 
                            <?= $sortcol ?>,
                            <?= '"' . $sortdir . '"'; ?> 
                        ]]
                    });
                    <?= $extrascript; ?>
                </script>
            </div>
        </div>
        <div class="col xs-12 col-sm-3 hidden-print">
            <div id="tableoptions" class="well">
                <h4>Actions:</h4>
                <div class="btn-group-vertical btn-justified btn-block" role="group" aria-label="actions">
                    <?php
                    if (count($tableOptions) > 0) {
                        if (!is_array($tableOptions)) {
                            $tableOptions = explode(",", $tableOptions);
                        }
                        foreach($tableOptions as $opt) {
                            if (strpos($opt, "class") > 0) {
                                // handle classes a bit differently
                                $opt = str_replace('class="', 'class="btn btn-primary ', $opt);
                            } else {
                                $opt = str_replace('<a ', '<a class="btn btn-primary" ', $opt);
                            }
                            print $opt . "\n";
                        }
                    }
                    ?>
                </div>
                <?php if ($tableSearch != '') { ?>
                    <h4>Search:</h4>
                    <?= $tableSearch ?>
                <?php } ?>
            </div>
        </div>
    </div> <!-- close row -->
    <?php
}

function make_text($element_name, $value, $label = '', $explain_text = '', $size = 50, $validation = '', $disabled = false, $onchange = '', $onfocus = '', $onblur = '', $onkeypress = '', $onkeydown = '', $onkeyup = '', $onclick = '', $maxlength = 255, $placeholder = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp = '<input type="text" id="' . $element_name . '" name="' . $element_name . '"';
    $temp.= ' value="'.$value.'"';
    if ($disabled) { $temp.= ' readonly'; }
    if ($size != '') { $temp.= ' size="'.$size.'"'; }
    if ($placeholder != '') { $temp.= ' placeholder="'.$placeholder.'"'; } else { $temp.= ' placeholder="'.$label.'"'; }
    if ($onchange != '') { $temp.= ' onchange="'.$onchange.'"'; }
    if ($onfocus != '') { $temp.= ' onfocus="'.$onfocus.'"'; }
    if ($onblur != '') { $temp.= ' onblur="'.$onblur.'"'; }
    if ($onkeypress != '') { $temp.= ' onkeypress="'.$onkeypress.'"'; }
    if ($onkeydown != '') { $temp.= ' onkeydown="'.$onkeydown.'"'; }
    if ($onkeyup != '') { $temp.= ' onkeyup="'.$onkeyup.'"'; }
    if ($onclick != '') { $temp.= ' onclick="'.$onclick.'"'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= ' maxlength="'.$maxlength.'"';
    $temp.= '/>';
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

function make_email($element_name, $value, $label='',$explain_text='',$confirm=false)
{
    if ($label!='') {
        labelStart($label,$element_name,'normal');
    }
    $temp='<input type="email" id="'.$element_name.'" name="'.$element_name.'" value="'.$value.'" size=50 maxlength=255 />';
    if ($confirm) {
        $temp.='<br><input type="email" size=50 id="'.$element_name.'_confirm" name="'.$element_name.'_confirm"
        value="'.$value.'" maxlength=255 onblur="if(this.value!=document.getElementById(\''.$element_name.'\').value){alert(\'Emails do not match!\');}"/>';
    }
    if ($label!="") {
        labelEnd($temp,'normal',$explain_text);
    } else {
       return $temp;
    }
}

function make_number($element_name, $value, $label = '', $explain_text = '', $validation = '', $disabled = false, $onchange = '', $onfocus = '', $onblur = '', $onkeydown = '', $onkeyup = '', $onclick = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp = '<input type="number" id="' . $element_name . '" name="' . $element_name . '"';
    $temp.= ' value="'.$value.'"';
    $temp.= ' size=10';
    if ($disabled) { $temp.= ' readonly'; }
    if ($onchange != '') { $temp.= ' onChange="'.$onchange.'"'; }
    if ($onfocus != '') { $temp.= ' onFocus="'.$onfocus.'"'; }
    if ($onblur != '') { $temp.= ' onBlur="'.$onblur.'"'; }
    if ($onkeydown != '') { $temp.= ' onKeydown="'.$onkeydown.'"'; }
    if ($onkeyup != '') { $temp.= ' onKeyup="'.$onkeyup.'"'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= " step='0.01'></input>";
    if ($label != "") {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

function make_number_range($low_element, $high_element, $low_value, $high_value, $label = '', $explain_text = '', $step = 1, $disabled = false, $onchange = '', $onfocus = '', $onblur = '', $onkeydown = '', $onkeyup = '', $onclick = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp.= '<span><b>Low:</b> <input type="number" id="' . $low_element . '" name="' . $low_element . '"';
    $temp.= ' value="'.$low_value.'"';
    if ($disabled) { $temp.= ' readonly'; }
    if ($onchange != '') { $temp.= ' onChange="'.$onchange.'"'; }
    if ($onfocus != '') { $temp.= ' onFocus="'.$onfocus.'"'; }
    if ($onblur != '') { $temp.= ' onBlur="'.$onblur.'"'; }
    if ($onkeydown != '') { $temp.= ' onKeydown="'.$onkeydown.'"'; }
    if ($onkeyup != '') { $temp.= ' onKeyup="'.$onkeyup.'"'; }
    $temp.= ' step="'.$step.'"></input></span>';
    
    $temp.= '<span style="margin-left:20px;"><b>High:</b> <input type="number" id="' . $high_element . '" name="' . $high_element . '"';
    $temp.= ' value="'.$high_value.'"';
    if ($disabled) { $temp.= ' readonly'; }
    if ($onchange != '') { $temp.= ' onChange="'.$onchange.'"'; }
    if ($onfocus != '') { $temp.= ' onFocus="'.$onfocus.'"'; }
    if ($onblur != '') { $temp.= ' onBlur="'.$onblur.'"'; }
    if ($onkeydown != '') { $temp.= ' onKeydown="'.$onkeydown.'"'; }
    if ($onkeyup != '') { $temp.= ' onKeyup="'.$onkeyup.'"'; }
    $temp.= ' step="'.$step.'"></input></span>';
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

//print a submit button
function make_submit($element_name, $button_name, $label='',$disabled=false,$onclick='',$buttonclass='btn-dark')
{
    ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" id="submit" name="submit" class="<?= $buttonclass ?>" 
                <?= ($disabled ? 'disabled' : '') ?>
                <?= ($onclick!='' ? 'onclick="'.$onclick.'"' : '') ?>
                value="<?= $button_name ?>" />
        </div>
    </div>
    <?php
}

//print a regular button
function make_button($element_name, $button_name, $label='',$disabled=false,$onclick='',$buttonclass='btn-dark')
{
    ?>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="button" class="btn <?= $buttonclass ?>" 
                <?= ($disabled ? 'disabled' : '') ?>
                <?= ($onclick!='' ? 'onclick="'.$onclick.'"' : '') ?>
                ><?= $button_name ?></button>
        </div>
    </div>
    <?php
}

//print a textarea
function make_textarea($element_name, $value, $label = '', $explain_text = '', $cols = '80', $rows = '15', $mce = true, $validation = '', $disabled = false, $onchange = '', $onfocus = '', $onblur = '', $onkeypress = '', $onkeydown = '', $onkeyup = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    if ($mce) { $meditor = ' class="GuiEditor"'; } else { $meditor = ' class="noGuiEditor"'; }
    $temp.= '<textarea id="'.$element_name.'" name="'.$element_name.'" cols="'.$cols.'" rows="'.$rows.'" '.$meditor;
    if ($disabled) { $temp.= ' readonly'; }
    if ($onchange != '') { $temp.= ' onchange="'.$onchange.'"'; }
    if ($onfocus != '') { $temp.= ' onfocus="'.$onfocus.'"'; }
    if ($onblur != '') { $temp.= ' onblur="'.$onblur.'"'; }
    if ($onkeypress != '') { $temp.= ' onkeypress="'.$onkeypress.'"'; }
    if ($onkeydown != '') { $temp.= ' onkeydown="'.$onkeydown.'"'; }
    if ($onkeyup != '') { $temp.= ' onkeyup="'.$onkeyup.'"'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= '>'.$value.'</textarea>';
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

//print a radio button or checkbox
function make_radiocheck($type, $element_name, $element_value = 0, $label = '', $explain_text = '', $validation = '', $onclick = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'checkbox');
    }
    $temp = '<input type="'.$type.'" id="'.$element_name.'" name="'.$element_name.'"';
    if ($element_value) { $temp.= ' checked="checked"'; }
    if ($onclick != '') { $temp.= ' onclick="'.$onclick.'"'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= '/>';
    if ($label != '') {
        labelEnd($temp, 'checkbox', $explain_text);
    } else {
        return $temp;
    }
}

// print a checkbox
function make_checkbox($element_name, $element_value = 0, $label = '', $explain_text = '', $validation = '', $onclick = '', $groupclass = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'checkbox', '');
    }
    $temp = '<input type="checkbox" id="'.$element_name.'" name="'.$element_name.'"';
    if ($groupclass != '') { $temp.= ' class="'.$groupclass.'"'; }
    if ($element_value) { $temp.= ' checked="checked"'; }
    if ($onclick != '') { $temp.= ' onclick="'.$onclick.'"'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= '/>';
    if ($explain_text != '') { $temp.= $explain_text; }
    if ($label != '') {
        labelEnd($temp, 'checkbox', $explain_text);
    } else {
        return $temp;
    }
}

// print a password text field
function make_password($element_name, $value, $label = '', $explain_text = '', $confirm = false)
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp = '<input class="input" type="password" id="'.$element_name.'" name="'.$element_name.'" value="'.$value.'" ></input>';
    if ($confirm) {
        $temp.= '<br><input class="input" type="password" id="' . $element_name . '_confirm" name="' . $element_name . '_confirm"
         value="'.$value.'" onblur="if(this.value!=document.getElementById(\''.$element_name.'\').value){alert(\'Passwords do not match!\');}" />';
    }
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

//print a hidden text field
function make_hidden($element_name, $value)
{
    echo '<input type="hidden" id="'.$element_name.'" name="'.$element_name.'" value="'.$value.'" />';

}

function make_file($element_name, $label = '', $explain_text = '', $filetoshow = '', $buttonclass = 'submit')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal', $explain_text);
    }
    $temp = '<input class="input" type="file" id="'.$element_name.'" name="'.$element_name.'" class="'.$buttonclass.'" 
                onblur="document.getElementById(\'' . $element_name . '_hidden\').value=this.value;">';
    $temp.= '<input type="hidden" id="'.$element_name.'_hidden" name="'.$element_name.'_hidden" value=""/>';
    if ($label != '') {
        echo $temp;
        $fileparts = explode("/", $filetoshow);
        if ($filetoshow != '' && file_exists($filetoshow) && end($fileparts) != '') {
            echo '<br><img src="'.$filetoshow.'" height=100 border=0>';
        }
        labelEnd('');
    } else {
        return $temp;
    }
}

function make_multifile($element_name, $path, $label, $explain_text)
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp = '<div id="'.$element_name.'"></div>';
    $temp.= '<script type="text/javascript">';
    $temp.= '$(document).ready(function () {';
    $temp.= 'var currentDir="'.$path.'";';
    $temp.= '$("#'.$element_name.'").fineUploader({';
    $temp.= 'request: { endpoint: "includes/ajax_handlers/fineUploaderHandler.php" },';
    $temp.= 'debug: true,';
    $temp.= 'validation: {';
    $temp.= 'allowedExtensions: ["jpeg", "jpg", "gif", "png"],';
    $temp.= 'sizeLimit: 5000 * 1024 //kb size';
    $temp.= '},';
    $temp.= 'failedUploadTextDisplay: {';
    $temp.= 'mode: "custom",';
    $temp.= 'maxChars: 40,';
    $temp.= 'responseProperty: "error",';
    $temp.= 'enableTooltip: true';
    $temp.= '},';
    $temp.= 'debug: true';
    $temp.= '}).on("submit", function(event, id, filename) {';
    $temp.= '$(this).fineUploader("setParams", {"dir": "'.$path.'"});';
    $temp.= '})';
    $temp.= '});';
    $temp.= '</script>';

    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

//print a <select> menu
function make_select($element_name, $selected, $options, $label = '', $explain_text = '', $validation = '', $editable = false, $action = '', $disabled = false, $multiple = false, $handler = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }
    $temp = '';
    if ($editable) {
        $temp.= '<div class="input-group">';
    }
    // print out the <select> tag
    $temp.= '<select name="' . $element_name . ($multiple ? '[]' : '') . '" id="'.$element_name.'" autocomplete="off" ';
    if ($action != '') { $temp.= ' onChange="' . $action . '"'; }
    if ($multiple) { $temp.= ' multiple'; }
    if ($disabled) { $temp.= ' disabled'; }
    if ($validation != '') { $temp.= ' '.$validation; }
    $temp.= ">";
    // print out the <option> tags
    foreach($options as $option => $option_label) {
        $temp.= '<option value="' . $option . '"';
        if (is_array($selected)) {
            if (in_array($option_label, $selected)) {
                $temp.= ' selected';
            }
        } else {
            if ($selected == $option_label) {
                $temp.= ' selected';
            }
        }
        $temp.= '>' . $option_label . '</option>';
    }
    $temp.= '</select>';
    if ($editable && $handler != '') {
        $temp.= '<span class="input-group-addon" id="' . $element_name . '_mod" data-toggle="modal" data-target="#' . $element_name . '_selectCustomOptionModal"><i class="fa fa-plus"></i></span></div>';
        // going to add a script to the global script array
        $globMod = '<!-- modal for select box '.$element_name.' custom option -->';
        $globMod .= '<div id="'.$element_name.'_selectCustomOptionModal" class="modal fade" tabindex="-1" role="dialog"';
        $globMod .= ' aria-labelledby="selectCustomOptionModalLabel'.$element_name.'">';
        $globMod .= '<div class="modal-dialog" role="document">';
        $globMod .= '<div class="modal-content">';
        $globMod .= '<div class="modal-header">';
        $globMod .= '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>';
        $globMod .= '<h4 class="modal-title" id="selectCustomOptionModalLabel'.$element_name.'">Add custom option</h4>';
        $globMod .= '</div>';
        $globMod .= '<div class="modal-body form-horizontal">';
        $globMod .= '<input id="' . $element_name . '_custom" name="' . $element_name . '_custom" type="text" class="form-control" />';
        $globMod .= '</div>';
        $globMod .= '<div class="modal-footer">';
        $globMod .= '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>';
        $globMod .= '<button type="button" class="btn btn-primary" onclick="saveSelectOption(\'$element_name\',\'$handler\');">Save changes</button>';
        $globMod .= '</div>';
        $globMod .= '</div>';
        $globMod .= '</div>';
        $globMod .= '</div>';

        $GLOBALS['modals'][] = $globMod;

    }
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}


function make_state($element_name, $selected, $label='',$explain_text='')
{
    $states=array(
            'AL'=>'ALABAMA',
            'AK'=>'ALASKA',
            'AZ'=>'ARIZONA',
            'AR'=>'ARKANSAS',
            'CA'=>'CALIFORNIA',
            'CO'=>'COLORADO',
            'CT'=>'CONNECTICUT',
            'DE'=>'DELAWARE',
            'DC'=>'DISTRICT OF COLUMBIA',
            'FL'=>'FLORIDA',
            'GA'=>'GEORGIA',
            'GU'=>'GUAM',
            'HI'=>'HAWAII',
            'ID'=>'IDAHO',
            'IL'=>'ILLINOIS',
            'IN'=>'INDIANA',
            'IA'=>'IOWA',
            'KS'=>'KANSAS',
            'KY'=>'KENTUCKY',
            'LA'=>'LOUISIANA',
            'ME'=>'MAINE',
            'MD'=>'MARYLAND',
            'MA'=>'MASSACHUSETTS',
            'MI'=>'MICHIGAN',
            'MN'=>'MINNESOTA',
            'MS'=>'MISSISSIPPI',
            'MO'=>'MISSOURI',
            'MT'=>'MONTANA',
            'NE'=>'NEBRASKA',
            'NV'=>'NEVADA',
            'NH'=>'NEW HAMPSHIRE',
            'NJ'=>'NEW JERSEY',
            'NM'=>'NEW MEXICO',
            'NY'=>'NEW YORK',
            'NC'=>'NORTH CAROLINA',
            'ND'=>'NORTH DAKOTA',
            'OH'=>'OHIO',
            'OK'=>'OKLAHOMA',
            'OR'=>'OREGON',
            'PW'=>'PALAU',
            'PA'=>'PENNSYLVANIA',
            'PR'=>'PUERTO RICO',
            'RI'=>'RHODE ISLAND',
            'SC'=>'SOUTH CAROLINA',
            'SD'=>'SOUTH DAKOTA',
            'TN'=>'TENNESSEE',
            'TX'=>'TEXAS',
            'UT'=>'UTAH',
            'VT'=>'VERMONT',
            'VA'=>'VIRGINIA',
            'VI'=>'VIRGIN ISLANDS',
            'WA'=>'WASHINGTON',
            'WV'=>'WEST VIRGINIA',
            'WI'=>'WISCONSIN',
            'WY'=>'WYOMING'
            );

    if ($label!='') {
        labelStart($label,$element_name,'normal');
    }

    // print out the <select> tag
    $temp = '<select name="'.$element_name.'" id="'.$element_name.'">';

    // print out the <option> tags
    foreach ($states as $option => $option_label) {
        $temp.= '<option value="'. $option . '"';
        if ($selected==$option_label || $selected==$option) {
            $temp.= ' selected="selected"';
        }
        $temp.= '>' . $option_label . '</option>';
    }
    $temp.= '</select>';
    if ($label!='') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

function make_date($element_name, $date, $label = '', $explain_text = '', $step = 1, $minDate = '', $maxDate = '', $empty = false)
{
    // just in case date is empty, set to today
    if ($date == '' && !$empty) {
        $date = date('Y-m-d');
    }
    // just in case, we're breaking date to two items and using only the first
    $date = explode(' ', $date);
    $date = $date[0];

    // build options var for script
    if (!$empty) $options = ', defaultDate: "'.$date.'"';
    if ($minDate != '') {
        $options.= ', minDate: "'.date("Y-m-d", strtotime($minDate)).'"';
    }
    if ($maxDate != '') {
        $options.= ', maxDate: "'.date("Y-m-d", strtotime($maxDate)).'"';
    }

    $temp = '';
    if ($label != '') {
        $temp .= '<div class="form-group">';
        $temp .= '<label for="'.$id.'" class="col-sm-2 control-label">'.$label.'</label>';
        $temp .= '<div class="col-sm-10">';
    }
    $temp .= '<div class="input-group date col-xs-3" style="min-width:200px;" id="'.$element_name.'_tp">';
    $temp .= '<input type="text" class="" name="'.$element_name.'" id="'.$element_name.'" value="'.$date.'"/>';
    $temp .= '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
    $temp .= '</div>';
    $temp .= '<script>';
    $temp .= '$("#'.$element_name.'_tp").datetimepicker({';
    $temp .= 'format:"YYYY-MM-DD",';
    $temp .= 'useCurrent: true,';
    $temp .= 'showTodayButton: true,';
    $temp .= 'icons: {time: "fa fa-clock-o",}';
    $temp .= $options;
    $temp .= '})';
    $temp .= '</script>';

    if ($explain_text != '') {
        $temp.= '<small>'.$explain_text.'</small><br>';
    }
    if ($label != '') {
        $temp.= '</div></div>';
        echo $temp;
    } else {
        return $temp;
    }
}


function make_time($element_name, $time, $label = '', $explain_text = '')
{
    // just in case date is empty, set to today
    if ($time == '') {
        $time = date('H:i');
    }
    
    $temp = '';
    if ($label != '') {
        $temp .= '<div class="form-group">';
        $temp .= '<label for="'.$id.'" class="col-sm-2 control-label">'.$label.'</label>';
        $temp .= '<div class="col-sm-10">';
    }
    $temp .= '<div class="input-group date col-xs-3" style="min-width:200px;" id="'.$element_name.'_tp">';
    $temp .= '<input type="text" class="" name="'.$element_name.'" id="'.$element_name.'" value="'.$time.'"/>';
    $temp .= '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
    $temp .= '</div>';
    $temp .= '<script>';
    $temp .= '$("#'.$element_name.'_tp").datetimepicker({';
    $temp .= 'format:"HH:mm",';
    $temp .= 'useCurrent: true,';
    $temp .= 'showTodayButton: true,';
    $temp .= 'icons: {time: "fa fa-clock-o",}';
    $temp .= '})';
    $temp .= '</script>';

    if ($explain_text != '') {
        $temp.= '<small>'.$explain_text.'</small><br>';
    }
    if ($label != '') {
        $temp.= '</div></div>';
        echo $temp;
    } else {
        return $temp;
    }
}


function make_datetime($element_name, $datetime, $label = '', $explain_text = '', $stepMinute = 1, $minDate = '', $maxDate = '')
{
    // just in case date is empty, set to today
    if ($datetime == '') {
        $datetime = date('Y-m-d');
    }

    // build options var for script
    if (!$empty) $options = ', defaultDate: "'.$datetime.'"';
    if ($minDate != '') {
        $options.= ', minDate: "'.date("Y-m-d H:i", strtotime($minDate)).'"';
    }
    if ($maxDate != '') {
        $options.= ', maxDate: "'.date("Y-m-d H:i", strtotime($maxDate)).'"';
    }

    $temp = '';
    if ($label != '') {
        $temp .= '<div class="form-group">';
        $temp .= '<label for="'.$id.'" class="col-sm-2 control-label">'.$label.'</label>';
        $temp .= '<div class="col-sm-10">';
    }
    $temp .= '<div class="input-group date col-xs-3" style="min-width:200px;" id="'.$element_name.'_tp">';
    $temp .= '<input type="text" class="" name="'.$element_name.'" id="'.$element_name.'" value="'.$date.'"/>';
    $temp .= '<span class="input-group-addon"><i class="fa fa-calendar"></i></span>';
    $temp .= '</div>';
    $temp .= '<script>';
    $temp .= '$("#'.$element_name.'_tp").datetimepicker({';
    $temp .= 'format:"YYYY-MM-DD HH:mm",';
    $temp .= 'useCurrent: true,';
    $temp .= 'showTodayButton: true,';
    $temp .= 'sideBySide: true,';
    $temp .= 'icons: {time: "fa fa-clock-o",}';
    $temp .= $options;
    $temp .= '})';
    $temp .= '</script>';

    if ($explain_text != '') {
        $temp.= '<small>'.$explain_text.'</small><br>';
    }
    if ($label != '') {
        $temp.= '</div></div>';
        echo $temp;
    } else {
        return $temp;
    }
}

function make_color($element_name, $color, $label = '', $explain_text = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }

    $temp = '';
    $temp.= '<input type="text" id="'.$element_name.'" name="'.$element_name.'" value="'.$color.'" size="10" style="background-color:'.$color.';"></input>';
    $temp.= '<script type="text/javascript">';
    $temp.= '$("#'.$element_name.'").ColorPicker({';
    $temp.= 'onSubmit: function(hsb, hex, rgb, el) { $(el).val(hex); $(el).ColorPickerHide(); },';
    $temp.= 'onBeforeShow: function () { $(this).ColorPickerSetColor(this.value); },';
    $temp.= 'onChange: function (hsb, hex, rgb) { $("#'.$element_name.'").css("backgroundColor", "#" + hex); $("#'.$element_name.'").val("#" + hex); }';
    $temp.= '}).bind("keyup", function(){ $(this).ColorPickerSetColor(this.value); });';
    $temp.= '</script>';
    
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

function make_address($element_name, $location_street = '', $location_street2 = '', $location_city = '', $location_state = 'ID', $location_zip = '', $label = '', $explain_text = '')
{
    if ($label != '') {
        labelStart($label, $element_name, 'normal');
    }

    $temp = 'Street Address: ';
    $temp.= make_text($element_name . '_street', $location_street);
    $temp.= 'Street 2 (Suite, Apartment, PO Box, etc): ';
    $temp.= make_text($element_name . '_street2', $location_street2);
    $temp.= '<br><div class="row"><div class="col-sm-12 col-md-6">City:<br>';
    $temp.= make_text($element_name . '_city', $location_city);
    $temp.= '</div><div class="col-xs-6 col-md-4">State:<br>';
    $temp.= make_state($element_name . '_state', $location_state);
    $temp.= '</div><div class="col-xs-6 col-md-2">Zip: ';
    $temp.= make_number($element_name . '_zip', $location_zip);
    $temp.= '</div></div>';
    
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
        return $temp;
    }
}

function make_phone($element_name,$phonenumber,$label='',$explain_text='')
{
    if ($label!='') {
        labelStart($label, $element_name, 'normal');
    }

    $temp.= '<input type="text" id="'.$element_name.'" name="'.$element_name.'"';
    if ($disabled) { $temp.= ' readonly'; }
    $temp.= ' value="'.$phonenumber.'" />';
    $temp.= '<script>$("#'.$element_name.'").mask("(999) 999-9999");</script>';
    
    if ($label != '') {
        labelEnd($temp, 'normal', $explain_text);
    } else {
       return $temp;
    }
}

function make_slider($element_name,$value,$label,$explain_text,$min=0,$max=100,$increment=1)
{
    if ($label != '') {
        labelStart($label,$element_name,'normal');
    }

    $temp = '';
    $temp.= '<input type="text" id="'.$element_name.'" name="'.$element_name.'" style="border:0;color:#f6931f;font-weight:bold;" />';
    $temp.= '<div id="'.$element_name.'_slider"></div>';
    $temp.= '<script>';
    $temp.= '$(function() {';
    $temp.= '$("#'.$element_name.'_slider").slider({';
    $temp.= 'range: "min",';
    $temp.= 'value: '.intval($value).',';
    $temp.= 'min: '.intval($min).',';
    $temp.= 'max: '.intval($max).',';
    $temp.= 'step: '.intval($increment).',';
    $temp.= 'slide: function( event, ui ) {';
    $temp.= '$( "#'.$element_name.'" ).val(ui.value);';
    $temp.= '}';
    $temp.= '});';
    $temp.= '$("#'.$element_name.'").val($("#'.$element_name.'_slider").slider("value"));';
    $temp.= '});';
    $temp.= '</script>';

    if ($label != '') {
        labelEnd($temp,'normal',$explain_text);
    }
    else {
        return $temp;
    }
}

function make_descriptor($text, $label = 'Information')
{
    labelStart($label, '', 'normal');
    labelEnd($text, 'normal', '');
}

function safe_text($text, $dir)
{
    if ($dir == 'in') {
        $text = str_replace('\r\n', '<br>', $text);
        $text = str_replace('\n', '<br>', $text);
        // $text=htmlentities($text,ENT_QUOTES);
        $text = addslashes($text);
    }
    else {
        $text = stripslashes($text);
        // $text=html_entity_decode ( $text, ENT_QUOTES);
        $text = str_replace('<br>', '\n', $text);
    }
    return $text;
}
