<?php
  /*
  User related functions
  */

function addUserAction($userID, $action, $refID=0)
{
    //now insert a user action record
    $dt=date("Y-m-d H:i");
    $d=date("Y-m-d");
    $h=date("H");
    $sql="INSERT INTO user_actions (user_id, action_dt, action, date, hour, reference_id) VALUES 
    ('$userID', '$dt', '$action', '$d', '$h', '$refID')";
    $dbInsert=dbinsertquery($sql);  
}

  
function loginForm()
{
    ?>
    <style>
        #login-dp{
            min-width: 250px;
            padding: 14px 14px 0;
            overflow:hidden;
            background-color:rgba(255,255,255,.8);
        }
        #login-dp .help-block{
            font-size:12px    
        }
        #login-dp .bottom{
            background-color:rgba(255,255,255,.8);
            border-top:1px solid #ddd;
            clear:both;
            padding:14px;
        }
        #login-dp .social-buttons{
            margin:12px 0    
        }
        #login-dp .social-buttons a{
            width: 49%;
        }
        #login-dp .form-group {
            margin-bottom: 10px;
        }
        .btn-fb{
            color: #fff;
            background-color:#3b5998;
        }
        .btn-fb:hover{
            color: #fff;
            background-color:#496ebc 
        }
        .btn-tw{
            color: #fff;
            background-color:#55acee;
        }
        .btn-tw:hover{
            color: #fff;
            background-color:#59b5fa;
        }
        @media(max-width:768px){
            #login-dp{
                background-color: inherit;
                color: #fff;
            }
            #login-dp .bottom{
                background-color: inherit;
                border-top:0 none;
            }
        }
    </style>
    <ul class="nav navbar-nav navbar-right">
        <li><p class="navbar-text">Already have an account?</p></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b>Login</b> <span class="caret"></span></a>
            <ul id="login-dp" class="dropdown-menu">
                <li>
                     <div class="row">
                            <div class="col-md-12">
                                Login via
                                <div class="social-buttons">
                                    <a href="#" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
                                    <a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a>
                                </div>
                                or
                                 <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav">
                                        <div class="form-group">
                                             <label class="sr-only" for="inputEmail">Email address</label>
                                             <input type="email" class="form-control" id="inputEmail" placeholder="Email address" required>
                                        </div>
                                        <div class="form-group">
                                             <label class="sr-only" for="inputPassword">Password</label>
                                             <input type="password" class="form-control" id="inputPassword" placeholder="Password" required>
                                             <div class="help-block text-right"><a href="/account/forgot">Forget the password ?</a></div>
                                        </div>
                                        <div class="form-group">
                                             <button type="button" class="btn btn-primary btn-block" onClick="userLogin();">Sign in</button>
                                        </div>
                                        <div class="checkbox">
                                             <label>
                                             <input id='remember-me' type="checkbox" checked> keep me logged-in
                                             </label>
                                        </div>
                                 </form>
                            </div>
                            <div class="bottom text-center">
                                New here ? <a href="/account/register/"><b>Join Us</b></a>
                            </div>
                     </div>
                </li>
            </ul>
        </li>
      </ul>
      <script>
      function userLogin()
      {
          //if success
          $.cookie('pngclassUser', response.token, { expires: 30, path: '/' });
      }
      </script>
    <?php
}