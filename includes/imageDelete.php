<?php
  require_once("bootCore.php");
  
  $imageID=intval($_POST['imageID']);
  
  $sql="SELECT * FROM images WHERE id=$imageID";
  $dbImage=dbselectsingle($sql);
  if($dbImage['numrows']>0)
  {
      $filename=stripslashes($dbImage['data']['filename']);
      $path=stripslashes($dbImage['data']['path']);
      unlink("../uploads/".$path.$filename);
      unlink("../uploads/".$path.'thumb_'.$filename);
      unlink("../uploads/".$path.'display_'.$filename);
      
      $sql="DELETE FROM images WHERE id=$imageID";
      $dbDelete=dbexecutequery($sql);
      $output['status']='success'; 
  } else {
      $output['status']='error';
  }
  
  echo json_encode($output);