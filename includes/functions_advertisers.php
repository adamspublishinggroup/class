<?php

/**
  * Returns an array of ada
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getAllAdvertisers()
{
	$sql = "SELECT * FROM advertisers ORDER BY name DESC";
	$dbAdvertisers = dbselectmulti($sql);

	return $dbAdvertisers['data'];
}


/**
  * Returns an ad
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getSingleAdvertiser($advertiserId)
{
	$sql = "SELECT * FROM advertisers WHERE id = $advertiserId LIMIT 1";
	$dbAdvertiser = dbselectsingle($sql);

	return $dbAdvertiser['data'];
}

function getAdvertiserInfo($advSlug)
{
    $sql = "SELECT * FROM advertisers WHERE slug = '".addslashes($advSlug)."'";
    $dbAdvertiser = dbselectsingle($sql);

    return $dbAdvertiser['data'];
}

function getAdvertiserSlug($advertiserId)
{
	$sql = "SELECT slug FROM advertisers WHERE id = $advertiserId LIMIT 1";
	$dbAdvertiser = dbselectsingle($sql);

	return $dbAdvertiser['data'];
}
