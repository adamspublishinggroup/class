<?php
  /*
  * a set of various functions to display ads in various presenations
  */


/*
 *  classListing is the default "ad listing" that is show on home page and search results
 */
function classListing($ad, $type='default', $saved=array(), $attns=array() )
{
	// check user
	$user = $GLOBALS['user'];
	$userId = $user['id'];

	// data from db
	$adId = $ad['id'];
	$adAddonClasses = getAddons($adId);
	$adUserId = $ad['user_id'];
	$adStart = $ad['start_date'];
	$adEnd = $ad['end_date'];
	$adTitle = $ad['headline'];
	$adText = $ad['ad_text'];
	if($ad['internet_text']!=''){$adText=$ad['internet_text'];}
	$adKeywords = $ad['keywords'];
	$adStatus = $ad['status'];
	$adCats = $ad['cats'];
	$imgChoice = $ad['image_choice'];
	$adAttns = $ad['attn_getter'];
	$adImages = $ad['images'];
	$mainImg = '';
	if( $imgChoice && $imgChoice != 0 ) {
		if( !empty($adImages) && ( $imgChoice == 1 || $imgChoice == 3 ) ) {
			$mainImg = '<div class="classPostImg" style="background-image:url(/uploads/' . $adImages[0]['thumb'] . ');"></div>';
		} elseif( !empty($adAttns) && !empty($attns) && ( $imgChoice == 2 || $imgChoice == 3 ) ) {
			$adAttnGetter = $attns[$adAttns];
			$mainImg = '<div class="classPostImg attnGtr" style="background-image:url(/uploads/attention/' . $adAttnGetter. ');"></div>';
		}
	}
	
	$isSaved = false;
	if( !empty($saved) ) {
		foreach( $saved as $item ) {
			if( $ad['id'] == $item['ad_id'] ) {
				$isSaved = true;
			}
		}
	}

	// default stuff
	$showCats = true;
	$title = '<h5><a href="/detail/?ad=' . $adId . '">' . $adTitle . '</a></h5>';
	$wordCnt = 250;
	$moreLink = '<a class="classPostMore" href="/detail/?ad=' . $adId . '">See&nbsp;Full&nbsp;Ad</a>';
	$adTools = ''; // this is the list of links at the bottom of each ad listing
	if( ADMIN || ( $adUserId == $userId ) ) {
		$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '" rel="nofollow" class="admin-tool-link">Edit</a> | ';
	}
	$adTools .= '<a href="/print/?ad=' . $adId . '" target="_blank" rel="nofollow">Print</a> | ';
	$adTools .= '<a class="postEmail" href="#" rel="nofollow">Email</a> | ';
	if( $isSaved ) {
		$adTools .= '<a class="postAction" href="#" rel="nofollow" data-gate="unsave">Saved <i class="fa fa-check" aria-hidden="true"></i></a> | ';
	} else {
		$adTools .= '<a class="postAction" href="#" rel="nofollow" data-gate="save">Save</a> | ';
	}
	if( $userId && ( $userId > 0 ) ) {
		$adTools .= '<a href="/account/saved-ads/" rel="nofollow">View Saved</a> | ';
	}

	$adTools .= '<a href="/place-an-ad/" rel="nofollow">Place an Ad</a>';
	// deviated stuff
	switch( $type ) {
		case 'category':
			// nothing, category display is identical to default, here for later if needed
		break;
		case 'preview':
			// basic difference is that no links work (email/print/etc), as this is a preview before purchase
			$title = '<h5><a>' . $adTitle . '</a></h5>';
			$moreLink = '<a class="classPostMore">See&nbsp;Full&nbsp;Ad</a>';
			$adTools = '';
			$adTools .= '<a>Print</a> | ';
			$adTools .= '<a>Email</a> | ';
			$adTools .= '<a>Save</a> | ';
			$adTools .= '<a>Place an Ad</a>';
		break;
		case 'advertiser':
			// basic differences are because listings are bigger and we don't want user to escape
			$showCats = false;
			$title = '<h4><a href="detail/?ad=' . $adId . '">' . $adTitle . '</a></h4>';
			$wordCnt = 400;
			$moreLink = '<a class="classPostMore" href="detail/?ad=' . $adId . '">See&nbsp;Full&nbsp;Ad</a>';
		break;
		case 'saved':
			// basic difference is bigger listing (no left nav) and different set of ad tools
			$title = '<h4><a href="/detail/?ad=' . $adId . '">' . $adTitle . '</a></h4>';
			$wordCnt = 300;
			$adTools = '';
			if( ADMIN || ( $adUserId == $userId ) ) {
				$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '" rel="nofollow" class="admin-tool-link">Edit</a> | ';
			}
			$adTools .= '<a href="/print/?ad=' . $adId . '" rel="nofollow">Print</a> | ';
			$adTools .= '<a class="postEmail" href="#" rel="nofollow">Email</a> | ';
			$adTools .= '<a class="postAction" href="#" rel="nofollow" data-gate="remove">Remove</a>';
		break;
		case 'history':
			// basic difference is bigger listing (no left nav) and different set of ad tools
			$title = '<h4><a href="/detail/?ad=' . $adId . '">' . $adTitle . '</a></h4>';
			$wordCnt = 300;
			$adTools = '';
			$adTools .= '<a href="/print/?ad=' . $adId . '" rel="nofollow">Print</a> | ';
			$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '&amp;pickup=true" rel="nofollow">Pick Up</a> | ';
			$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '" rel="nofollow">Edit</a> | ';
			if($ad['published']==0 && ($ad['ad_paid_dt'] || $ad['admin_paid'] || $ad['free_ad'] || $ad['bill_customer']))
			{
				$adTools .= '<a href="/includes/handler/publish.php?ad=' . $adId . '&amp;action=publish" rel="nofollow">Publish</a>';
			} else {
				$adTools .= '<a href="/includes/handler/publish.php?ad=' . $adId . '&amp;action=unpublish" rel="nofollow">Un-publish</a>';
			}
		break;

		default:
			// nothing, everything is set
	}
	// vars set after switch
	$stripAd = strip_tags( $adText, '<p><b><i><u><strike><strong><em><br>' );
	$excerpt = substr( $stripAd, 0, $wordCnt ) . '...';

	if( $adStatus == 1  || $type=='preview' ) { ?>
		<div id="classPost_<?= $adId ?>" class="classPost <?= $adAddonClasses ?>">
			<div class="classPostDates">This ad runs from <strong><?= $adStart ?></strong> to <strong><?= $adEnd ?></strong></div>
			<div class="clearFix"></div>
			<?php if( !empty( $adCats ) && $showCats ) {
				$catsHtml = '<div class="classPostCats">Filed in ';
				foreach( $adCats as $adCat ) {
					$catsHtml .= '<a';
					if( $type!='preview' ) {
						$catsHtml .= ' href="/category/?c=' . $adCat['id'] . '"';
					}
					$catsHtml .= '>' . $adCat['category_name'] . '</a>, ';
				}
				$catsHtml = rtrim($catsHtml,", ").".";
				$catsHtml .= '</div>';
				$catsHtml .= '<div class="clearFix"></div>';
				echo $catsHtml;
			} ?>
			<br>
			<div class="postMainWrap<?= ( $mainImg ? ' withImg' : '' ) ?>">
				<?= $mainImg ?>
				<?= $title; ?>
				<div id="listing_<?= $adId ?>_text" class="classPostText"><?= $excerpt ?> <?= $moreLink ?></div>
			</div>
			<div class="clearFix"></div>
			<div class="classPostToolbar" data-ad="<?= $adId ?>" data-title="<?= $adTitle ?>">
				<?= $adTools ?>
			</div>
		</div>
	<?php }
}



/*
 *  mapListing is the default "map listing" that is show on maps page
 */
function mapListing( $ad, $type='default', $i, $attns=array() )
{
	// check user
	$user = $GLOBALS['user'];
	$userId = $user['id'];

	// data from db
	$adId = $ad['id'];
	$adAddonClasses = getAddons($adId);
	$adUserId = $ad['user_id'];
	$adStart = $ad['start_date'];
	$adEnd = $ad['end_date'];
	$adTitle = $ad['headline'];
	$adText = $ad['ad_text'];
	if($ad['internet_text']!=''){$adText=$ad['internet_text'];}
    $adKeywords = $ad['keywords'];
	$adStatus = $ad['status'];
	$adCats = $ad['cats'];
	$hasImg = flase;
	$adImages = $ad['images'];
	if( !empty( $adImages ) ) { $hasImg = true; }
	$imgChoice = $ad['image_choice'];
	$adAttns = $ad['attn_getter'];
	$adImages = $ad['images'];
	$mainImg = '';
	if( $imgChoice && $imgChoice != 0 ) {
		if( !empty($adImages) && ( $imgChoice == 1 || $imgChoice == 3 ) ) {
			$mainImg = '<div class="classPostImg" style="background-image:url(/uploads/' . $adImages[0]['thumb'] . ');"></div>';
		} elseif( !empty($adAttns) && !empty($attns) && ( $imgChoice == 2 || $imgChoice == 3 ) ) {
			$adAttnGetter = $attns[$adAttns];
			$mainImg = '<div class="classPostImg attnGtr" style="background-image:url(/uploads/attention/' . $adAttnGetter . ');"></div>';
		}
	}

	$address = $ad['address_street'];
	$city = $ad['address_city'];
	$state = $ad['address_state'];
	$zip = $ad['address_zip'];
	$adAddrMap = str_replace( ' ', '+', $address ) . ',+' . str_replace( ' ', '+', $city ) . ',+' . $state . '+' . $zip;
	$adAddrSee = '';
	if( trim($address)>'' && trim($city)>'' && trim($state)>'' && trim($zip)>'' ) {
		$adAddrSee = $address . ', ' . $city . ',&nbsp;' . $state . '&nbsp;' . $zip;
	}
	$adLat = $ad['lat'];
	$adLon = $ad['lon'];

	// default stuff
	$showCats = true;
	$title = '<h5><a href="/detail/?ad=' . $adId . '">' . $adTitle . '</a></h5>';
	$wordCnt = 250;
	$moreLink = '<a class="classPostMore" href="/detail/?ad=' . $adId . '">See&nbsp;Full&nbsp;Ad</a>';
	$adTools = ''; // this is the list of links at the bottom of each ad listing
	if( ADMIN || ( $adUserId == $userId ) ) {
		$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '" rel="nofollow" class="admin-tool-link">Edit</a> | ';
	}
	$adTools .= '<a href="http://maps.google.com/maps?daddr=' . $adAddrMap . '" target="_blank" rel="nofollow">Get Directions</a> | ';
	$adTools .= '<a href="/place-an-ad/">Place an Ad</a>';
	// deviated stuff
	switch( $type ) {
		case 'maptype':
			// nothing, this isn't used for anything yet, just here for future and because the other function has it
		break;

		default:
			// nothing, everything is set
	}
	// vars set after switch
	$stripAd = strip_tags( $adText, '<p><b><i><u><strike><strong><em><br>' );
	$excerpt = substr( $stripAd, 0, $wordCnt ) . '...';

	if( $adStatus == 1 ) {
		?>
		<div id="classPost_<?= $adId ?>" class="classPost <?= $adAddonClasses ?>">
			<?php if( trim($adLat) > '' && trim($adLon) > '' ) { ?>
				<h5 id="mapTag_<?= $i ?>" class="mapTag">#<?= $i ?></h5>
			<?php } else { ?>
				<h5 id="mapTag_<?= $i ?>" class="mapTag mapTagNotShown">(Not shown on map.)</h5>
			<?php } ?>
			<div class="classPostDates">This ad runs from <strong><?= $adStart ?></strong> to <strong><?= $adEnd ?></strong></div>
			<div class="clearFix"></div>
			<?php if( !empty( $adCats ) && $showCats ) {
				$catsHtml = '<div class="classPostCats">Filed in ';
				foreach( $adCats as $adCat ) {
                    $catsHtml .= '<a';
                    if( $type!='preview' ) {
                        $catsHtml .= ' href="/category/?c=' . $adCat['id'] . '"';
                    }
                    $catsHtml .= '>' . $adCat['category_name'] . '</a> ';

                }
				$catsHtml = rtrim($catsHtml,", ").".";
                $catsHtml .= '</div>';
				$catsHtml .= '<div class="clearFix"></div>';
				echo $catsHtml;
			} ?>
			<br>
			<div class="postMainWrap<?= ( $mainImg ? ' withImg' : '' ) ?>">
				<?= $mainImg ?>
				<?= $title; ?>
				<?php if( trim($adAddrSee) > '' ) { ?>
					<div id="listing_<?= $adId ?>_address" class="classPostText classPostAddress" data-lat="<?= $adLat ?>" data-lon="<?= $adLon ?>"><strong><?= $adAddrSee ?></strong></div>
				<?php } ?>
				<div id="listing_<?= $adId ?>_text" class="classPostText"><?= $excerpt ?> <?= $moreLink ?></div>
			</div>
			<div class="clearFix"></div>
			<div class="classPostToolbar" data-ad="<?= $adId ?>" data-title="<?= $adTitle ?>">
				<?= $adTools ?>
			</div>
		</div>
	<?php }
}


/*
 *  classDetail is the default layout that shows a single ad
 */
function classDetail($ad, $type='default', $saved=array(), $attns=array() )
{
	global $config;

	// check user
	$user = $GLOBALS['user'];
	$userId = $user['id'];

	// data from db
	$adId = $ad['id'];
	$adUserId = $ad['user_id'];
	$adStart = $ad['start_date'];
	$adEnd = $ad['end_date'];
	$adTitle = $ad['headline'];
	$adText = $ad['ad_text'];
	if($ad['internet_text']!=''){$adText=$ad['internet_text'];}
    $adKeywords = $ad['keywords'];
	$adStatus = $ad['status'];
	$adCats = $ad['cats'];
	$adImages = $ad['images'];
	$mapIt = false;
	$adLat = $ad['lat'];
	$adLon = $ad['lon'];
	if( trim($adLat) > '' && trim($adLon) > '' ) {
		$mapIt = true;
	}
	$hasImg = false;
	if( !empty($adImages) ) {
		$hasImg = true;
	}
	$imgChoice = $ad['image_choice'];
	$adAttns = $ad['attn_getter'];

	$isSaved = false;
	if( !empty($saved) ) {
		foreach( $saved as $item ) {
			if( $ad['id'] == $item['ad_id'] ) {
				$isSaved = true;
			}
		}
	}

    //get the ad details
    $adDetails = getAdDetails($adId);
	// default stuff
	$showCats = true;
	$adTools = ''; // this is the list of links at the bottom of each ad listing
	if( ADMIN || ( $adUserId == $userId ) ) {
		$adTools .= '<a href="/place-an-ad/place-ad-basics/?ad=' . $adId . '" rel="nofollow" class="admin-tool-link">Edit</a> | ';
	}
	$adTools .= '<a href="/print/?ad=' . $adId . '" target="_blank" rel="nofollow">Print</a> | ';
	$adTools .= '<a class="postEmail" href="#" rel="nofollow">Email</a> | ';
	if( $isSaved ) {
		$adTools .= '<a class="postAction" href="#" rel="nofollow" data-gate="unsave">Saved <i class="fa fa-check" aria-hidden="true"></i></a> | ';
	} else {
		$adTools .= '<a class="postAction" href="#" rel="nofollow" data-gate="save">Save</a> | ';
	}
	$adTools .= '<a class="postAction" href="/place-an-ad/" rel="nofollow" data-gate="report" title="Report as inappropriate.">Report</a> | ';
	$adTools .= '<a href="/place-an-ad/" rel="nofollow">Place an Ad</a>';
	// deviated stuff
	switch( $type ) {
		case 'preview':
			// basic difference is that no links work (email/print/etc), as this is a preview before purchase
			$adTools = '';
			$adTools .= '<a>Print</a> | ';
			$adTools .= '<a>Email</a> | ';
			$adTools .= '<a>Save</a> | ';
			$adTools .= '<a>Place an Ad</a>';
		break;
		case 'advertiser':
			// basic difference, no pesky distracting categories to lure user away
			$showCats = false;
		break;
		case 'saved':
			// possibly used for showing a saved ad after end date
			// needs to be added
		break;
		case 'history':
			// for viewing ad from history after end date
			// needs to be added
		break;
		case 'print':
			// for printing an ad
			$adTools = '';
		break;

		default:
			// nothing, everything is set
	}

	if( $adStatus == 1  || $type=='preview' ) { ?>

		<div class="post-meta">
			<div class="post-meta-dates">This ad runs from <strong><?= $adStart ?></strong> to <strong><?= $adEnd ?></strong></div>
			<?php if( !empty( $adCats ) && $showCats ) {
				$catsHtml = '<div class="post-meta-cats">Filed in ';
				foreach( $adCats as $adCat ) {
					$catsHtml .= '<a';
					if( $type!='preview' ) {
						$catsHtml .= ' href="/category/?c=' . $adCat['id'] . '"';
					}
					$catsHtml .= '>' . $adCat['category_name'] . '</a> ';

				}
				$catsHtml = rtrim($catsHtml,", ").".";
                $catsHtml .= '</div>';
				echo $catsHtml;
			} ?>
		</div>

		<?php if( $hasImg && ( $imgChoice == 1 || $imgChoice == 3 ) ) { ?>
			<div class="post-images">
				<?php if( $type != 'print' ) { ?>
					<div class="slider_for">
						<?php foreach( $adImages as $item ) { ?>
							<div class="slide_img_wrap">
								<a class="slide_img adDetailSlide" href="/uploads/<?= $item['display'] ?>" style="background-image:url(/uploads/<?= $item['display'] ?>);"></a>
							</div>
						<?php } ?>
					</div>
					<?php if( count($adImages) > 1 ) { ?>
						<div class="slider_nav">
						<?php foreach( $adImages as $item ) { ?>
							<div class="thumb_img_wrap">
								<div class="thumb_img" style="background-image:url(/uploads/<?= $item['thumb'] ?>);"></div>
							</div>
						<?php } ?>
						</div>
					<?php } ?>
				<?php } else { ?>
					<div class="slide_img_wrap_print">
						<img src="/uploads/<?= $adImages[0]['display'] ?>" alt="<?= $adTitle ?>" />
					</div>
				<?php } ?>
			</div>
		<?php } ?>

		<div class="post-text">
			<?php 
			if( ( !empty($adAttns) && !empty($attns) && $imgChoice == 2 ) || ( !$hasImg && !empty($adAttns) && !empty($attns) && $imgChoice == 3 ) ) { 
				$adAttnGetter = $attns[$adAttns];
				?>
				<span class="post-text-attn-gtr" style="background-image:url(/uploads/attention/<?= $adAttnGetter ?>);"></span>
			<?php } ?>
			<?= $adText ?>
			<div class="clearFix"></div>
		</div>

		<?php if( !empty($adDetails )) { ?>
			<div class="post-details">
				<h4>Details</h4>
				<?php foreach($adDetails as $detail) { ?>
					<p><span class="detail-label"><?= $detail['label'] ?>:</span><br><span class="detail-value"><?= $detail['formatted_value'] ?></span></p>
				<?php } ?>
			</div>
		<?php } ?>

		<?php if( $mapIt ) { ?>
			<a class="post-map-static" href="https://www.google.com/maps/@<?= $adLat ?>,<?= $adLon ?>,14z" target="_blank" rel="nofollow">
				<img width="600" src="https://maps.googleapis.com/maps/api/staticmap?autoscale=false&zoom=12&size=600x300&maptype=roadmap&key=<?= GOOGLE_MAP_KEY ?>&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0x<?php echo str_replace( '#', '', $config['color_highlight'] )  ?>%7Clabel:%7C<?= $adLat ?>+<?= $adLon ?>" alt="Google Map of <?= $adTitle ?>">
			</a>
		<?php } ?>

		<div class="classPostToolbar" data-ad="<?= $adId ?>" data-title="<?= $adTitle ?>">
			<?= $adTools ?>
		</div>

	<?php }
}
