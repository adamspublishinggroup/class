<!-- email ad request form -->
<div id="emailAdModal" class="popup-modal"></div>
<div id="emailAdForm" class="popup-form">
	<div class="popup-form-close"><i id="changeClose" class="fa fa-times"></i></div>
	<div class="popup-form-logo"></div>
	<div class="popup-form-intro">
		<div class="popup-form-title">Email This Ad:</div>
		<div class="popup-form-subtitle"></div>
	</div>
	<!-- <div class="popup-form-pre-content">Use this form to send an ad to a friend.</div> -->
	<div class="popup-form-content">
		<form id="email_ad_form" name="contact_form" class="form-horizontal">
			<div class="errors">Please fix errors marked in red.</div>
			<input type="hidden" id="adid" name="adid" value="">
			<input type="text" id="sender_name" name="sender_name" value="" size="40" placeholder="Your name" maxlength="255/">
			<input type="email" id="recipient_email" name="recipient_email" value="" size="40" placeholder="Email to" maxlength="255/">
			<textarea id="message" name="message" cols="40" rows="4" class="noGuiEditor" placeholder="Optional Message (Besides the Ad text)"></textarea>
			<div class="g-recaptcha" data-sitekey="<?= RECAPTCHA_PUBLIC ?>" style="margin-bottom:8px;"></div>
			<button type="submit" id="sendEmail">Send Ad</button>
		</form>
	</div>
	<!-- <div class="popup-form-extro"></div> -->
</div>
<div id="emailAdSuccessModal" class="popup-modal"></div>
<div id="emailAdSuccess" class="popup-form">
	<div class="popup-form-close"><i id="changeClose" class="fa fa-times"></i></div>
	<div class="popup-form-logo"></div>
	<div class="popup-form-intro">
		<div class="popup-form-title">Thank You!</div>
		<div class="popup-form-subtitle">Ad sent successfully!</div>
	</div>
	<!-- <div class="popup-form-pre-content"></div> -->
	<!-- <div class="popup-form-content"></div> -->
	<!-- <div class="popup-form-extro"></div> -->
</div>
