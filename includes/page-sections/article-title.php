<div class="page-title-wrap">
	<div class="mobile-hide"><?php
		rail_item('leader');
		if($page!='detail'){
			pageModules('featured');
		}
	?></div>
	<?php require_once( INCLUDE_DIR . 'page-sections/featured-slider.php' ); ?>
	<?php
	$path = $_SERVER['REQUEST_URI'];

	if( !empty( $_GET['slug'] ) && $_GET['slug'] != '' && $_GET['slug'] != 'index.php' ) {
		$article = getArticle($_GET['slug']);
		if( !empty($article) ) { ?>
			<div class="article-title"><h1><?= $article['headline'] ?></h1></div>
		<?php } else { ?>
			<div class="page-title"><h1><?= $page_h1_title ?></h1></div>
		<?php } ?>
	<?php } elseif( stripos( $path, 'articles' ) > 0 ) {
		$pathUrlParts = explode('/articles/',$path);
		$path = $pathUrlParts[1];
		$path = explode('/',$path);
		$path = $path[0];
		if( $path != '' ) {
			$sql = "SELECT * FROM article_categories WHERE slug='$path'";
			$dbCat = dbselectsingle($sql);
			$artCatID = $dbCat['data']['id'];
			$artCatName = $dbCat['data']['category'];
			$artCatSlug = $dbCat['data']['slug'];
			?>
			<div class="page-title"><h1>Articles - <?= $artCatName ?></h1></div>
		<?php } else { ?>
			<div class="page-title"><h1><?= $page_h1_title ?></h1></div><?php
		}
	} ?>
</div>
