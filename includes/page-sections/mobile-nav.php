
<div id="mobileNav">
	<div class="tab">Menu</div>
	<div class="main">
		<div class="group">
			<div class="groupTitle">Menu</div>
			<ul class="groupMenu">
				<?php
				//global $siteMenu;
				$menuItems = menuItems('mobile');

				$menuHtml = '';
				if( !empty( $menuItems ) ) {
					foreach( $menuItems as $menuItem ) {
						// see if this is a parent
						$isParent = false;
						if( !empty( $menuItem['submenus'] ) ) { $isParent = true; }
						// build items
						if( !$isParent ) {
							$menuHtml .= '<li';
							if( $page == $menuItem['page'] ) {
								$menuHtml .= ' class="current"';
							}
							$menuHtml .= '>';
							$menuHtml .= '<a href="' . $menuItem['url'] . '">' . $menuItem['name'] . '</a>';
							$menuHtml .= '</li>';
						} else {
							$menuHtml .= '<li class="subGroup">';
							$menuHtml .= '<span class="subGroupTitle">' . $menuItem['name'] . ' <i class="fa fa-angle-right" aria-hidden="true"></i></span>';
							$menuHtml .= '<ul class="subGroupMenu">';
							foreach( $menuItem['submenus'] as $sub ) {
								$menuHtml .= '<li>';
								$menuHtml .= '<a';
								if( $page == $sub['page'] ) {
									$menuHtml .= ' class="current"';
								}
								$menuHtml .= ' href="' . $sub['url'] . '"';
								$menuHtml .= '>' . $sub['name'].'</a>';
								$menuHtml .= '</li>';
							}
							$menuHtml .= '</ul>';
							$menuHtml .= '</li>';
						}
					}
				} else {
					$menuHtml .= 'Menu is not defined';
				}
				echo $menuHtml;
				?>
			</ul>
		</div>
		<?php if( !empty( $socials ) ) { ?>
			<div class="group">
				<div class="groupTitle">Social</div>
				<ul class="groupMenu">
					<?php foreach( $socials as $social ) { ?>
						<li><a href="<?= $social['link'] ?>" target="_blank"><?= $social['text'] ?></a></li>
					<?php } ?>
				</ul>
			</div>
		<?php } ?>
	</div>
</div>
