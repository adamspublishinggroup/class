<?php
    if(!isset($_COOKIE['pngClassCookie']))
    {
?>
<div id='cookie_bar'>
<p>This site uses cookies to improve your experience. If you have questions, please read our <a href='/static/terms'>Terms of Use</a> and our <a href='/static/privacy'>Privacy Policy</a>
<span class='pull-right' ><i id='cookieClose' class='fa fa-times'></i></span>
</p>
</div>

<script>
$(function() 
{
    $('#cookieClose').click(function(){
        
        $.cookie('pngClassCookie',1,{ expires: 365 });
        $('#cookie_bar').hide();
    })
})
</script>

<?php } ?>