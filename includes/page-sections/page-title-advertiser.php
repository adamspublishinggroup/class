<?php

if( !empty($al) ) {

	$bizId = $al['id'];
	$logo = $al['logo'];
	$name = $al['name'];
	$slogan = $al['slogan'];
	$blerb = $al['about'];
	$website = $al['url'];
	$social_fb = $al['facebook'];
	$social_tw = $al['twitter'];
	$phone = $al['phone'];
	$phReplace = array('.', '-', '(', ')', ' ');
	$phoneRaw = str_replace($phReplace, '', $phone);
	$phoneLink = 'tel:' . $phoneRaw;
	$phoneDot = substr_replace( substr_replace( $phoneRaw, '.', 3, 0), '.', 7, 0);
	$phoneDash = substr_replace( substr_replace( $phoneRaw, '-', 3, 0), '-', 7, 0);
	$phoneFull = substr_replace( substr_replace( substr_replace( $phoneRaw, ') ', 3, 0), '-', 8, 0), '(', 0, 0);
	$lat = $al['lat'];
	$lon = $al['lon'];
	$address = $al['street'];
	$city = $al['city'];
	$state = $al['state'];
	$zip = $al['zip'];
	$adAddrMap = str_replace( ' ', '+', $address ) . ',+' . str_replace( ' ', '+', $city ) . ',+' . $state . '+' . $zip;
	$adAddrSee = $address . ', ' . $city . ',&nbsp;' . $state . '&nbsp;' . $zip;
	$adAddrBox = $address . '<br>' . $city . ',&nbsp;' . $state . '&nbsp;' . $zip;
	?>
	<div class="title-content">
		<div class="page-title page-title-advertiser">
			<div class="pta-logo" style="background-image:url(<?= $logo ?>);">
			</div><div class="pta-info-wrap">
				<h1><?= $name ?></h1>
				<div class="pta-info-slogan"><?= $slogan ?></div>
				<?php
				if( trim($phone) > '' ) {
					echo '<a class="pta-info-phone" href="' . $phoneLink . '">' . $phoneFull . '</a>';
				}
				if( trim($phone) > '' && trim($website) > '' ) {
					echo ' • ';
				}
				if( trim($website) > '' ) {
					echo '<a class="pta-info-social" href="' . $website . '">Website</a>';
				}
				if( ( trim($phone) > '' || trim($website) > '' ) && trim($social_fb) > '' ) {
					echo ' • ';
				}
				if( trim($social_fb) > '' ) {
					echo '<a class="pta-info-social" href="' . $social_fb . '">Facebook</a>';
				}
				if( ( trim($phone) > '' || trim($website) > '' || trim($social_fb) > '' ) && trim($social_tw) > '' ) {
					echo ' • ';
				}
				if( trim($social_tw) > '' ) {
					echo '<a class="pta-info-social" href="' . $social_tw . '">Twitter</a>';
				}
				?>
				<div class="pta-info-blerb"><?= $blerb ?></div>
			</div>
		</div>
		<div class="page-map-wrap">
			<div class="page-map">
				<div id="advertisersMap"></div>
				<a class="page-map-address" href="http://maps.google.com/maps?daddr=<?= $adAddrMap ?>" target="_blank"><?= $adAddrSee ?></a>

				<?php
				$markersJS = '';
				$markersJS .= '{';
				$markersJS .= 'latitude: ' . $lat . ',';
				$markersJS .= 'longitude: ' . $lon . ',';
				$markersJS .= 'html: \'';
				$markersJS .= '<div style="text-align:center;">';
				$markersJS .= '<div style="margin-bottom:3px;"><b>' . $name . '</b></div>';
				$markersJS .= '<a href="http://maps.google.com/maps?daddr=' . $adAddrMap . '" target="_blank"><b>Directions &raquo;</b></a>';
				$markersJS .= '</div>';
				$markersJS .= '\',';
				$markersJS .= '},';
				?>
				<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?&amp;language=en&amp;key=<?= GOOGLE_MAP_KEY ?>"></script>
				<script>
				$("#advertisersMap").gMap({
					controls: false,
					scrollwheel: true,
					maptype: 'ROADMAP',
					markers: [ <?= $markersJS ?> ],
					icon: {
						image: "/img/mapMarker.png",
						iconsize: [32, 42],
						iconanchor: [16, 42]
					},
					latitude: <?= $lat ?>,
					longitude: <?= $lon ?>,
					zoom: 13
				});
				</script>
			</div>
		</div>
	</div>

<?php } else { ?>

	<div class="page-title page-title-advertiser">
		<div>Advertiser not found.</div>
	</div>

<?php } ?>
