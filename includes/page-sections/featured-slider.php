<?php
$featured = getFeaturedAds(20);
if( !empty($featured) ) {
	shuffle( $featured );
	$featured = array_slice( $featured, 0, 20);
	?>
	<div id="featuredCarousel" class="fc-wrapper">
		<div class="fc-container">
			<?php foreach( $featured as $ad ) {
				// data from db
				$adId = $ad['id'];
				$adUserId = $ad['user_id'];
				$adStart = $ad['start_date'];
				$adEnd = $ad['end_date'];
				$adTitle = $ad['headline'];
				$adText = $ad['ad_text'];
				if($ad['internet_text']!=''){$adText=$ad['internet_text'];}
				$adKeywords = $ad['keywords'];
				$adStatus = $ad['status'];
				$adCats = $ad['cats'];
				$adImages = $ad['images'];
				$mainImg = '';
				$wordCnt = 30;
				if( !empty($adImages) ) {
					$mainImg = '<div class="fc-item-img-wrap"><div class="fc-item-img" style="background-image:url(/uploads/' . $adImages[0]['thumb'] . ')"></div></div>';
				} else {
					$wordCnt = 120;
					$mainImg = '';
				}
				$stripAd = strip_tags( $adText, '<p><a><b><i><u><strike><strong><em><br>' );
				$excerpt = substr( $stripAd, 0, $wordCnt ) . '...';
				?>
				<a class="fc-item" href="/detail/?ad=<?= $adId ?>">
					<div class="fc-item-heading"><?= $adTitle ?></div>
					<div class="fc-item-desc"><?= $excerpt ?></div>
					<?= $mainImg ?>
				</a>
			<?php } ?>
		</div>
	</div>
	<?php
}
