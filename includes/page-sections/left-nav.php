<?php $cats = categories(); ?>
<div class="content-nav">
	<div class="search-form">
		<form id="searchForm" method=get action="/">
			<input type="text" id="q" name="q" name="search" placeholder="search" value="" ><button type="submit" form="searchForm" value="Submit"><i class="fa fa-check" aria-hidden="true"></i>
			</button><div id="mobile-filter-trigger" class="mobile-show">Advanced</div>
		</form>
	</div>
	<div id="catsMenu"<?= ( NESTED_CATEGORIES ? '' : ' class="no-nesting"' ) ?>>
		<?php
		function catIterator( $thisCats ) {
			foreach( $thisCats as $cat ) {
				$catId = $cat['category_id'];
				$catName = $cat['category_name'];
				$catCount = $cat['ad_count'];
				$subCats = $cat['subcats'];
				$hasChildren = false;
				if( !empty($subCats) && NESTED_CATEGORIES ) {
					$hasChildren = true;
				} ?>
				<div class="catItem<?= ( $hasChildren ? ' hasChildren' : '' ) ?>">
					<?= ( $hasChildren ? '<div class="subCatTrigger">+</div>' : '' ) ?>
					<div class="catItemLinks">
						<a href="/category/?c=<?= $catId ?>"><?= $catName ?> (<?= $catCount ?>)</a>
					</div>
					<?php if( $hasChildren ) { ?>
						<div class="catSubGroup">
							<?php catIterator( $subCats ); ?>
						</div>
					<?php } ?>
				</div>
			<?php }
		};
		catIterator( $cats );
		?>
		<div class="clearFix"></div>
	</div>
	<div class="mobile-hide"><?php 
    rail_item('left');
    dfp_ads('left');
    pageModules('left') ?>
    </div>
</div>
