<div class="page-title-wrap">
	<div class="mobile-hide"><?php
		rail_item('leader');
		if($page!='detail'){
			pageModules('featured');
		}
	?></div>
	<?php require_once( INCLUDE_DIR . 'page-sections/featured-slider.php' ); ?>
	<?php if( $page_h1_title && trim($page_h1_title) > '' ) { ?>
		<?php
		if( isset( $_GET['q'] ) ) {
			$q = trim(addslashes(urldecode($_GET['q'])));
			$page_h1_title = 'Search - ' . $q;
		}
		?>
		<div class="page-title"><h1><?= $page_h1_title ?></h1></div>
	<?php } ?>
</div>
