<div class="content-nav">
	<div id="placeAdMenu">
		<a class="adMenu<?= ( $currPage == 'basics' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-basics/">
			<i class="fa fa-cube" aria-hidden="true"></i><span class="mobile-hide"> Basics</span>
		</a>
        <a class="adMenu<?= ( $currPage == 'images' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-images/">
			<i class="fa fa-picture-o" aria-hidden="true"></i><span class="mobile-hide"> Images</span>
		</a>
        <a class="adMenu<?= ( $currPage == 'dates' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-dates/">
            <i class="fa fa-calendar" aria-hidden="true"></i><span class="mobile-hide"> Dates</span>
        </a>
        <a class="adMenu<?= ( $currPage == 'addons' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-addons/">
			<i class="fa fa-wrench" aria-hidden="true"></i><span class="mobile-hide"> Addons</span>
		</a>
        <a class="adMenu<?= ( $currPage == 'preview' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-preview/">
			<i class="fa fa-eye" aria-hidden="true"></i><span class="mobile-hide"> Preview</span>
		</a>
        <a class="adMenu<?= ( $currPage == 'summary' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-summary/">
			<i class="fa fa-check-square-o" aria-hidden="true"></i><span class="mobile-hide"> Summary</span>
		</a>
        <a class="adMenu<?= ( $currPage == 'checkout' ? ' current' : '' ) ?>" href="/place-an-ad/place-ad-checkout/">
			<i class="fa fa-shopping-cart" aria-hidden="true"></i><span class="mobile-hide"> Checkout</span>
		</a>
	</div>
</div>
