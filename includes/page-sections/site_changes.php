<?php
$showChangeBar = true;
//get last change date for privacy and terms
//pngClassChanges contains a last acknowledged date (last time the user "accepted" changes)

if(isset($_COOKIE['pngClassChanges']))
{
    $lAccept=date("Y-m-d H:i:s",$_COOKIE['pngClassChanges']);
    $sql="SELECT MAX(last_modified) as lMod FROM static_pages WHERE slug IN ('privacy','terms') AND last_modified>'$lAccept'";
    $dbLast = dbselectsingle($sql);
    if($dbLast['numrows']>0 && $dbLast['data']['lMod']!='') {
        //means we have a modified date that is newer than the last acknowledgement date
        $last = strtotime($dbLast['data']['lMod']);
        //if(ADMIN) $details = ". Had ".date("Y-m-d H:i:s",$lAccept)." in the cookie, last date in static pages is ".date("Y-m-d H:i:s", $last);
        if(ADMIN) $details = ", as of ".date("Y-m-d H:i:s", $last);
    } else {
        $showChangeBar = false;
    }
} else {
    $sql="SELECT MAX(last_modified) as lMod FROM static_pages WHERE slug IN ('privacy','terms')";
    $dbLast = dbselectsingle($sql);
    $last = strtotime($dbLast['data']['lMod']);
    //if(ADMIN)$details = ". No cookie, setting last date to ".date("Y-m-d H:i:s",$last);
    if(ADMIN)$details = ".";
}

if( $showChangeBar ) {
    ?>
    <div id='change_bar'>
        <p>We have updated our <a href='/static/terms'>Terms of Service</a> and/or our <a href='/static/privacy'>Privacy Policy</a><?= $details ?>
            <span class='pull-right' ><i id='changeClose' class='fa fa-times'></i></span>
        </p>
    </div>
    <script>
        $(function() {
            $('#changeClose').click(function(){
                $.cookie('pngClassChanges','<?= $last ?>',{ expires: 365 });
                $('#change_bar').hide();
            });
        });
    </script>
    <?php 
}