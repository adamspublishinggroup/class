<div class="content-nav">
	<div class="search-form">
		<form id="searchForm">
			<input type="text" id="searchInput" name="search" placeholder="search" ><button type="submit" form="searchForm" value="Submit"><i class="fa fa-check" aria-hidden="true"></i></button>
		</form>
	</div>
	<div id="noCatsMenu">
		<?php if( !empty($ad)) {
			$advertiserId = $ad['advertiser_id'];
			if( $advertiserId && $advertiserId != 0 ) {
				$slugArr = getAdvertiserSlug($advertiserId);
				$slug = $slugArr['slug'];
				?><a class="nav-link nav-back" href="/advertisers/<?= $slug ?>">&laquo;&nbsp;Back&nbsp;To&nbsp;Listings</a><?php
			} else {
				?><a class="nav-link nav-back" href="#" onclick="goBack()">&laquo;&nbsp;Back</a><?php
			}
		} else {
			?><a class="nav-link nav-back" href="#" onclick="goBack()">&laquo;&nbsp;Back</a><?php
		}
		?>
		<div class="clearFix"></div>
		<script>function goBack() { window.history.back(); }</script>
	</div>
</div>
