<div class="content-nav">
	<div id="dashboardMenu">
		<a class="adMenu<?= ( $currPage == 'saved-ads' ? ' current' : '' ) ?>" href="/account/saved-ads/">
			<i class="fa fa-floppy-o" aria-hidden="true"></i><span class="mobile-hide"> Saved Ads</span>
		</a><a class="adMenu<?= ( $currPage == 'history' ? ' current' : '' ) ?>" href="/account/history/">
			<i class="fa fa-history" aria-hidden="true"></i><span class="mobile-hide"> Ad History</span>
		</a><a class="adMenu<?= ( $currPage == 'settings' ? ' current' : '' ) ?>" href="/account/settings/">
			<i class="fa fa-cog" aria-hidden="true"></i><span class="mobile-hide"> Settings</span>
		</a>
	</div>
</div>
