<?php

/**
  * Returns an array of categories with number of ads in each.
  *
  * Example return array
  * array {
  *     array('category_id'=>10, 'category_name'=>'Real Estate', 'ad_count'=>3, 'subcats'=>array( //could contain it's own array))
  * }
  *
  * @param int $startCatID defaults to 0. Pass in the "root" category
  *
  * @return array
  */
function categories($startCatID=0)
{
	global $config;
    $date=date("Y-m-d");
	$cats = array();
	$sql = "SELECT * FROM categories WHERE parent_id = $startCatID ORDER BY category_order ASC, category_name ASC";
	$dbCats=dbselectmulti($sql);
	if( $dbCats['numrows'] > 0 ) {
		foreach( $dbCats['data'] as $cat ) {
			$subcats = array();
			//check for sub categories
			$sql = "SELECT * FROM categories
					WHERE parent_id=$cat[id]
					ORDER BY category_order ASC, category_name DESC";
			$dbSubs = dbselectmulti($sql);
			if( $dbSubs['numrows'] > 0 ) {
				$subcats = categories($cat['id']);
			}
			//get ad counter
            $sql = "SELECT count(A.id) as adcount
					FROM ads A, ad_category_xref B
					WHERE A.start_date <= '$date'
          AND A.end_date >= '$date'
					AND A.published = 1
                    AND A.status = 1
					AND A.id = B.ad_id
					AND B.category_id = $cat[id]";

            $dbAdCount = dbselectsingle($sql);
            $adCount = $dbAdCount['data']['adcount'];

            //add in the count of all ads for the subcats
            if(count($subcats)>0)
            {
                foreach($subcats as $sub)
                {
                    $adCount+=$sub['ad_count'];
                }
            }
            if($config['show_empty_categories'] || $adCount > 0)
            {
            $cats[] = array(
                    'category_id'    => $cat['id'],
                    'category_name'    => stripslashes($cat['category_name']),
                    'ad_count'        => $adCount,
                    'subcats'        => $subcats
                );
            }
		}
	}

	return $cats;
}

/**
  * Returns an array of info for the queried category.
  *
  * Example return array
  * array {
  *     array('category_id'=>10, 'category_name'=>'Real Estate', 'ad_count'=>3, 'subcats'=>array( //could contain it's own array))
  * }
  *
  * @param int $catID. Pass in the requested category
  *
  * @return array
  */
function category($catID)
{
	global $config;
    $date = date("Y-m-d");
	$cats = array();
	$sql = "SELECT * FROM categories WHERE id = $catID ORDER BY category_order ASC, category_name DESC";
	$dbCats = dbselectmulti($sql);
	if( $dbCats['numrows'] > 0 ) {
		foreach( $dbCats['data'] as $cat ) {
			$subcats = array();
			//check for sub categories
			$sql = "SELECT * FROM categories WHERE parent_id=$cat[id] ORDER BY category_order ASC, category_name DESC";
			$dbSubs = dbselectmulti($sql);
			if( $dbSubs['numrows'] > 0 ) {
				$subcats=categories($cat['id']);
			}
			//get ad counter
			$sql="SELECT count(A.id) as adcount
					FROM ads A, ad_category_xref B
					WHERE A.start_date <= '$date'
          AND A.end_date >= '$date'
					AND A.published = 1
                    AND A.status = 1
					AND A.id = B.ad_id
					AND B.category_id = $cat[id]";
			$dbAdCount = dbselectsingle($sql);
			$adCount = $dbAdCount['data']['adcount'];

            //add in the count of all ads for the subcats
            if(count($subcats)>0)
            {
                foreach($subcats as $sub)
                {
                    $adCount+=$sub['ad_count'];
                }
            }
            if($config['show_empty_categories'] || $adCount > 0)
            {
            $cats[] = array(
					'category_id'	=> $cat['id'],
					'category_name'	=> stripslashes($cat['category_name']),
					'ad_count'		=> $adCount,
					'subcats'		=> $subcats
				);
		    }
        }
	}

	return $cats;
}


/**
  * Returns an array of ads
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param int $catID defaults Pass in the "root" category
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getOwnAds($userId)
{

    $sql = "SELECT * FROM ads
	WHERE user_id=$userId
	ORDER BY start_date DESC";
	$dbAds = dbselectmulti($sql);

	if( $dbAds['numrows'] > 0 ) {
        return getFullAds($dbAds['data']);
    } else {
        return array();
    }
}



/**
  * Returns an array of ads
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param int $catID defaults Pass in the "root" category
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getSavedAds($userId)
{
    $sql="SELECT A.* FROM ads A, ads_saved B WHERE A.id=B.ad_id AND B.user_id=$userId";
    $dbAds = dbselectmulti($sql);
    return $dbAds['data'];
}


/*
*  return simple list of ad ids that are saved for a user
*/
function savedAds($userId)
{
	$sql = "SELECT * FROM ads_saved WHERE user_id=$userId";
	$dbRows = dbselectmulti($sql);
	if( $dbRows['numrows'] > 0 ) {
		return $dbRows['data'];
	} else {
		return array();
	}
}


/**
  * Returns an array of ads
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param int $catID defaults Pass in the "root" category
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getAdvertisersAds($advertiserID,$source='page')
{
  site_log(0,'advertiser',$advertiserID,$source);

  $date = date("Y-m-d");
  $sql = "SELECT * FROM ads
  WHERE status = 1
  AND published = 1
  AND start_date <= '$date'
  AND end_date >= '$date'
  AND advertiser_id = $advertiserID
  ORDER BY start_date DESC";
  $dbAds = dbselectmulti($sql);
  if( $dbAds['numrows'] > 0 ) {
      return getFullAds($dbAds['data']);
  } else {
      return array();
  }
}


/**
  * Returns an array of display ads
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param int $catID defaults Pass in the "root" category
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getAdvertisersDisplays($advertiserID,$source='page')
{
  site_log(0,'advertiser',$advertiserID,$source);

  $date = date("Y-m-d");
  $sql = "SELECT * FROM advertisers_display
          WHERE start_date <= '$date'
          AND end_date >= '$date'
          AND advertiser_id = $advertiserID
          ORDER BY start_date DESC";
  $dbAds = dbselectmulti($sql);
  
  if( $dbAds['numrows'] > 0 ) {
      return $dbAds['data'];
  } else {
      return array();
  }
}


function getAllAds($sortOrder='date',$catIDs=array())
{
    global $totalAdCount,$config;
    $date = date("Y-m-d");
    if(isset($_GET['c']))
    {
        $catID = intval($_GET['c']);
        //get all sub categories of the passed $catID
        $sql="SELECT id FROM categories WHERE parent_id=$catID";
        $dbCats=dbselectmulti($sql);
        if($dbCats['numrows']>0)
        {
            foreach($dbCats['data'] as $cat)
            {
                $catIDs[]=$cat['id'];
            }
            $catIDs="$catID,".implode(",",$catIDs);
        } else {
            $catIDs="$catID";
        }
    } else {
        $catIDs = "";
    }

    if(isset($_GET['p']))
    {
        $offset = 10*intval($_GET['p'])-10;
    } else {
        $offset = 0;
    }

    if(isset($_GET['q']))
    {
        $q=trim(addslashes(urldecode($_GET['q'])));
        $query = "AND (A.keywords LIKE '%$q%' OR A.headline LIKE '%$q%' OR A.ad_text LIKE '%$q%')";
    } else {
        $query = '';
    }

    switch($sortOrder) {
        case 'rand':
                $order=" ORDER BY A.featured DESC, RAND()";
                break;
        case 'date':
                $order=" ORDER BY A.featured DESC, A.start_date DESC";
                break;
        case 'creation_date':
                $order=" ORDER BY A.featured DESC, A.created_dt DESC";
                break;
    }

    //have to run this query twice, once to get total count, then to get just the necessary ones.
    if($catIDs!='' )
    {
    $sql = "SELECT DISTINCT(A.id) FROM ads A, ad_category_xref B
            WHERE A.status = 1
            AND A.published = 1
            AND A.start_date <= '$date'
            AND A.end_date >= '$date'
            AND A.id = B.ad_id
            AND B.category_id IN($catIDs)
            $query ";
    } else {
    $sql = "SELECT DISTINCT(A.id) FROM ads A
            WHERE A.status = 1
            AND A.published = 1
            AND A.start_date <= '$date'
            AND A.end_date >= '$date'
            $query";
    }
    $dbAdsCount = dbselectmulti($sql);
    $totalAdCount = $dbAdsCount['numrows'];


    if($catIDs!='')
    {
    $sql = "SELECT DISTINCT(A.id), A.* FROM ads A, ad_category_xref B
            WHERE A.status = 1
            AND A.published = 1
            AND A.start_date <= '$date'
            AND A.end_date >= '$date'
            AND A.id = B.ad_id
            AND B.category_id IN($catIDs)
            $query
            $order
            LIMIT 10 OFFSET $offset";
    } else {
    $sql = "SELECT DISTINCT(A.id), A.* FROM ads A
            WHERE A.status = 1
            AND A.published = 1
            AND A.start_date <= '$date'
            AND A.end_date >= '$date'
            $query
            $order
            LIMIT 10 OFFSET $offset";
    }
    $dbAds = dbselectmulti($sql);
    
    if( $dbAds['numrows'] > 0 ) {
        return getFullAds($dbAds['data']);
    } else {
        return array();
    }

}

function getAllMapAds($mapID=1)
{
    global $totalAdCount, $config;
    $date = date("Y-m-d");

    //get all sub categories of the passed $catID
    $sql="SELECT id FROM categories WHERE category_map=$mapID";
    $dbCats=dbselectmulti($sql);
    if($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            $catIDs[]=$cat['id'];
        }
        $catIDs=implode(",",$catIDs);
    }
    return getAllAds('date', $catIDs);
}

/**
  * Returns an ad
  *
  * Example return array
  * array {
  *     db_record w, image field if one is present
  * }
  *
  *
  * @param string $sortOrder defaults 'date'  other options are creation_date, always sort first by featured
  *
  * @return array
  */
function getSingleAd($adId,$log=true,$isPrint=false)
{
    if($log)site_log($adId,'detail');
    $sql = "SELECT * FROM ads WHERE id = $adId";
    $dbAd = dbselectsingle($sql);
    if( $dbAd['numrows'] > 0 ) {
        $ads[] = $dbAd['data'];
        $ad=getFullAds($ads,$isPrint);
    }
    return $ad[0];

}

/*
*   This function encapsulates all the necessary parts and does one big build for the ad text
*
*/
function getFullAds($ads,$isPrint=false)
{
    foreach( $ads as &$ad ) {
        $ad['cats']=getAdCategories($ad['id']);
        $ad['images']=getImages($ad['id']);
        $ad['address']=getAddress($ad['id']);
        $ad['addons']=getAddons($ad['id']);
        if($ad['internet_text']!='' && !$isPrint ){$ad['ad_text']=$ad['internet_text'];}
    }
    return $ads;
}

/**
  * Returns an array of ad details
  *
  * Example return array
  * array {
  *     'label', descriptive label for meta tag
  *     'meta_id', the meta tag id
  *     'meta_type', the meta tag type
  *     'raw_value', the raw value from ad_meta
  *     'formatted', the propertly formatted meta value for the ad
  * }
  *
  *
  * @param int $adID
  *
  * @return array
  */
function getAdDetails($adID)
{
    $adDetails = array();

    //get the categories the ad has been selected for, then the distinct meta_tags for those categories
    $sql="SELECT meta_id, meta_value_short, meta_value_long, meta_name, meta_type, meta_default FROM ad_meta AS A, metadata AS B WHERE A.meta_id=B.id AND A.ad_id=$adID";
    $dbMetaTags=dbselectmulti($sql);

    if($dbMetaTags['numrows']>0)
    {
        /*
        Possible types
               'select'=>'List of options',
               'text'=>'Text (up to 255 characters)',
               'textblock'=>'Large amount of text',
               'number'=>'Number',
               'checkbox'=>'Checkbox',
               'telephone'=>'Telephone number',
               'address'=>'Address block',
               'date'=>'Date'
        */

        foreach($dbMetaTags['data'] as $metaTag)
        {
            switch($metaTag['meta_type'])
            {
                case 'select':
                    $options = explode("|",$metaTag['meta_default']);
                    $formattedValue = $options[$metaTag['meta_value_short']];

                    $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);

                break;

                case 'text':
                     $formattedValue = stripslashes($metaTag['meta_value_short']);

                     $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'textblock':
                    $formattedValue = stripslashes($metaTag['meta_value_long']);
                    $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'number':
                    $formattedValue = stripslashes($metaTag['meta_value_short']);
                    $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'checkbox':
                    if($metaTag['meta_value_short']==0)
                    {
                        $formattedValue = "No";
                    } else {
                        $formattedValue = "Yes";
                    }

                    $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'telephone':
                     $formattedValue = stripslashes($metaTag['meta_value_short']);

                     $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'address':
                     $temp = stripslashes($metaTag['meta_value_long']);
                     $temp=explode("|",$temp);
                     $formattedValue = $temp[0]."<br>".($temp[1]!=''? $temp[1]."<br>" : '').$temp[2]." ".$temp[3]." ".$temp[4];
                     $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

                case 'date':
                     $formattedValue=date("D m/d/Y",strtotime($metaTag['meta_value_short']));
                    $adDetails[]=array('label'=>stripslashes($metaTag['meta_name']),
                    'meta_id'=>$metaTag['meta_id'],
                    'meta_type'=>stripslashes($metaTag['meta_type']),
                    'raw_value'=>$metaTag['meta_value_short'],
                    'formatted_value'=>$formattedValue);
                break;

            }
        }
    }


    return $adDetails;
}

function getAdCategories($adID)
{
    $sql = "SELECT A.id, A.category_name FROM categories A, ad_category_xref B WHERE B.ad_id = $adID AND B.category_id=A.id";
    $dbCats = dbselectmulti($sql);
    return $dbCats['data'];
}

function getAddons($adID)
{
    $sql = "SELECT A.online_class FROM addons A, ad_addons B WHERE B.ad_id = $adID AND B.addon_id=A.id";
    $dbAddons = dbselectmulti($sql);
    $classes = "";
    if($dbAddons['numrows']>0)
    {
        foreach($dbAddons['data'] as $addon)
        {
            $classes.=$addon['online_class']." ";
        }
    }
    
    return $classes;
}

function getImages($adID)
{
	$images = array();
	//attach images
	$sql = "SELECT * FROM images WHERE ad_id = $adID";
	$dbImages = dbselectmulti($sql);
	if( $dbImages['numrows'] > 0 ) {
		foreach( $dbImages['data'] as $i ) {
			$images[]=array(
					'thumb'=>stripslashes($i['path']).'thumb_'.stripslashes($i['filename']),
					'display'=>stripslashes($i['path']).'display_'.stripslashes($i['filename'])
					);
		}
	}
	return $images;
}

function getAddress($adID)
{
    global $config;
    $address = array();
    if($config['meta_mapping_id']!=0)
    {
        $sql="SELECT * FROM ad_meta WHERE ad_id=$adID AND meta_id=".$config['meta_mapping_id'];
        $dbMeta = dbselectsingle($sql);
        $metaTag = $dbMeta['data'];
        $temp = stripslashes($metaTag['meta_value_long']);
        $temp=explode("|",$temp);
        $address['street']=$temp[0];
        $address['street_2']=$temp[1];
        $address['city']=$temp[2];
        $address['state']=$temp[3];
        $address['zip']=$temp[4];
    }
    return $address;
}

function paginator()
{
	global $totalAdCount;
	$currentURL = explode("?",$_SERVER['REQUEST_URI']);
	$currentURL = $currentURL[0];
	$page = intval($_GET['p']);
	if( $page == 0 ) { $page = 1; }

	//get total pages in ads record
	$pages = ceil( $totalAdCount / 10 );
	if( isset( $_GET['q'] ) ) { $q = '&q=' . $_GET['q']; } else { $q = ''; };
	if( isset( $_GET['c'] ) ) { $c = '&c=' . $_GET['c']; } else { $c = ''; };

	if( $page > 1 ) { $prev = $page - 1; } else { $prev = 1; }
	if( $page < $pages ) { $next = $page + 1; } else { $next = $pages; }

	$prevURL = $currentURL . '?p=' . $prev . $q . $c;
	$nextURL = $currentURL . '?p=' . $next . $q . $c;
	$firstURL = $currentURL . '?p=' . '1' . $q . $c;
	$lastURL = $currentURL . '?p=' . $pages . $q . $c;
	?>
	<nav aria-label="Page navigation" style="text-align:center;">
		<ul class="pagination">
			<?php
			if( $pages > 1 ) {
				// display always (this is a first-page link)
				echo '<li><a href="' . $firstURL . '" aria-label="First Page">First</a></li>' . "\n";
			}

			if( $pages > 5 ) {
				//display if there are more than 5 pages total (this is a previous-page link)
				echo '<li><a href="' . $prevURL . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>' . "\n";
			}

			if( $pages > 2 ) {
				// dont bother with individual pages if there are only 2
				if( $pages <= 5 ) {
					// display all pages up to 5 pages
					for( $i = 1; $i <= $pages; $i++ ) {
						$url = $currentURL . '?p=' . $i . $q . $c;
						echo '<li' . ( $i == $page ? ' class="active"' : '' ) . '><a href="' . $url . '">' . $i . '</a></li>' . "\n";
					}
				} else {
					// more than 5 pages gets special handling here
					$pageArr = array();
					$currPage  = $page;
					$forBefore = $page - 4;
					$treBefore = $page - 3;
					$twoBefore = $page - 2;
					$oneBefore = $page - 1;
					$oneAfter  = $page + 1;
					$twoAfter  = $page + 2;
					$treAfter  = $page + 3;
					$forAfter  = $page + 4;

					if( $forBefore >= 1 ) {
						// if false, then we know we are at the beginning
						// if true we could be anywhere in the list, but we'll start at the end
						$pageArr[0] = $forBefore;
						$pageArr[1] = $treBefore;
						$pageArr[2] = $twoBefore;
						$pageArr[3] = $oneBefore;
						$pageArr[4] = $page;
						if( $twoAfter <= $pages ) {
							// we don't care about more than 2 after, since that's only a facor at the beginning of the list
							$pageArr[0] = $twoBefore;
							$pageArr[1] = $oneBefore;
							$pageArr[2] = $page;
							$pageArr[3] = $oneAfter;
							$pageArr[4] = $twoAfter;
						} else if ( $oneAfter <= $pages ) {
							$pageArr[0] = $treBefore;
							$pageArr[1] = $twoBefore;
							$pageArr[2] = $oneBefore;
							$pageArr[3] = $page;
							$pageArr[4] = $oneAfter;
						}
					} elseif( $treBefore >= 1 ) {
						$pageArr[0] = $twoBefore;
						$pageArr[1] = $oneBefore;
						$pageArr[2] = $page;
						$pageArr[3] = $oneAfter;
						$pageArr[4] = $twoAfter;
					} elseif( $twoBefore >= 1 ) {
						$pageArr[0] = $twoBefore;
						$pageArr[1] = $oneBefore;
						$pageArr[2] = $page;
						$pageArr[3] = $oneAfter;
						$pageArr[4] = $twoAfter;
					} elseif( $oneBefore >= 1 ) {
						$pageArr[0] = $oneBefore;
						$pageArr[1] = $page;
						$pageArr[2] = $oneAfter;
						$pageArr[3] = $twoAfter;
						$pageArr[4] = $treAfter;
					} else {
						$pageArr[0] = $page;
						$pageArr[1] = $oneAfter;
						$pageArr[2] = $twoAfter;
						$pageArr[3] = $treAfter;
						$pageArr[4] = $forAfter;
					}

					// build list
					foreach( $pageArr as $item ) {
						$url = $currentURL . '?p=' . $item . $q . $c;
						echo '<li' . ( $item == $page ? ' class="active"' : '' ) . '><a href="' . $url . '">' . $item . '</a></li>' . "\n";
					}
				}
			}

			if( $pages > 5 ) {
				//display if there are more than 5 pages total (this is a next-page link)
				echo '<li><a href="' . $nextURL . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>' . "\n";
			}

			if( $pages > 1 ) {
				// display always (this is a last-page link)
				echo '<li><a href="' . $lastURL . '" aria-label="Last Page">Last</a></li>' . "\n";
			}
			?>
		</ul>
	</nav>
	<?php
}

/*
*  This function returns an array of the most popular $count ads in the past week (the ones with the most detail views
*/
function getPopularAds($count=5)
{
    $date = date("Y-m-d",strtotime("-1 week"));

    $sql = "SELECT COUNT(site_log.id) as pop, ad_id, B.* FROM site_log, ads B
    WHERE source='page'
    AND action_type='detail'
    AND ad_id<>0
    AND action_date>='$date'
    AND ad_id=B.id
    AND B.end_date>='".date("Y-m-d")."' 
    GROUP BY ad_id
    ORDER BY pop DESC
    LIMIT $count";

    $dbAds = dbselectmulti($sql);

    if( $dbAds['numrows'] > 0 ) {
        foreach( $dbAds['data'] as &$ad ) {
            $ad['images']=getImages($ad['id']);
            $ad['cats']=getAdCategories($ad['id']);
            if($ad['internet_text']!=''){$ad['ad_text']=$ad['internet_text'];}
        }
    }

    return $dbAds['data'];
}

/*
*  This function returns an array of the most popular $count ads in the past week (the ones with the most detail views
*/
function getFeaturedAds($count=5)
{
    global $config;
    $date = date("Y-m-d");

    $sql = "SELECT A.* FROM ads A, ad_addons B
    WHERE B.addon_id=$config[featured_scroller_add_on]
    AND A.id=B.ad_id
    AND A.start_date <= '$date'
    AND A.end_date >= '$date'
    ORDER BY RAND()
    LIMIT $count";

    $dbAds = dbselectmulti($sql);

    if( $dbAds['numrows'] > 0 ) {
        foreach( $dbAds['data'] as &$ad ) {
            $ad['images']=getImages($ad['id']);
            $ad['cats']=getAdCategories($ad['id']);
            if($ad['internet_text']!=''){$ad['ad_text']=$ad['internet_text'];}
        }
    }

    return $dbAds['data'];
}
