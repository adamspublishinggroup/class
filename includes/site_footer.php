<footer>
	<div class="footerContainer">
		<div id="footer-posts">
			<?php rail_item('footer') ?>
			<?php dfp_ads('footer') ?>
        </div>
		<?php
            $menuItems = menuItems('footer');
            if(count($menuItems)>0)
            {
                print "<div id='footer-nav'>\n";
                foreach($menuItems as $menuItem)
                {
                    $cUrl = str_replace("/index.php","",$_SERVER['SCRIPT_NAME']);
                    print "<a class='menubutton ".($cUrl == $menuItem['url'] ? "current" : "")."' href='".$menuItem['url']."'>".$menuItem['name']."</a>\n";
                }
                print "</div>\n";
            }
        ?>
       <div id="footer-legal">Copyright &copy; <?= date("Y") ?> <?= SITE_NAME ?> - All Rights Reserved.</div>
	</div>
</footer>
<?php require_once( INCLUDE_DIR . 'page-sections/mobile-nav.php' ); ?>
<?php require_once( INCLUDE_DIR . 'page-sections/email-ad-popup.php' ); ?>
<?php require_once( INCLUDE_DIR . 'page-sections/success-modal.php' ); ?>
<?php require_once( INCLUDE_DIR . 'page-sections/cookie_acknowledgement.php' ); ?>
