<?php if( FACEBOOK_APP_ID != '' ) { ?>
<script>
	window.fbAsyncInit = function() {
		FB.init({
			appId	: '<?php echo FACEBOOK_APP_ID ?>',
			cookie	: true,
			xfbml	: true,
			version	: 'v2.8'
		});
		FB.AppEvents.logPageView();

		
	};

	(function(d, s, id){
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) {return;}
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

	function fbLogin(){

		FB.getLoginStatus(function(response) {
            if (response.status === 'connected') {
                console.log('Logged into FB.');
                //console.log(response.authResponse.accessToken);
            } else {
                /*
                We only want to prompt the user if we are on the /account/login page.
                */
                <?php if($_SERVER['SCRIPT_NAME']=='/account/login/index.php' || $_SERVER['SCRIPT_NAME']=='/account/register/index.php'){ ?>
                
                <?php } ?>
            }
        });
        
        FB.login(function(response){
			// Handle the response object, like in statusChangeCallback() in our demo
			// code.
			// The response object is returned with a status field that lets the
			// app know the current login status of the person.
			// Full docs on the response object can be found in the documentation
			// for FB.getLoginStatus().
			if (response.status === 'connected') {
			// Logged into your app and Facebook.
			var fbToken = response.authResponse.accessToken;
			var fbEmail = '';
			var fbName = '';
			FB.api('/me', { fields: 'name,email,birthday,gender' },
				function(response) {
					registerFBUser(fbToken,response.email,response.name, response.gender);
				}
			);

			} else {
				// The person is not logged into this app or we are unable to tell.
			}
		},{scope: 'public_profile,email'});
	}
    function registerFBUser(token,email,name, gender)
    {
        $.ajax({
            url: "/includes/fbRegisterUser.php",
            type: "POST",
            data: {token: token, email:email, name: name, gender: gender, stage: 'initial' },
            dataType: 'json',
            success: function(response){
                if(response['status']=='success')
                {
                    if(response['action']=='login')
                    {
                        location.href='/account/';
                    } else {
                        location.href='/account/fbregister';
                    }
                }
            },
            error: function (xhr, desc, er) {
                console.log('An error occurred: '+desc);
                return false;
            }
        });

        console.log('got the following: token-'+token+', email-'+email+', name-'+name);
    }
</script>
<?php } ?>

<header>
	<div class="headerContainer">
		<a id="header-logo" href="/"><?= SITE_NAME ?></a>
		<a id="header-cta" href="<?= ORDER_ENTRY_URL ?>">
			<div class="cta-info">Place an Ad<span class="mobile-hide"> With Us</span>
			</div><div class="cta-action"><?= CONTACT_PHONE ?></div>
		</a>
		<div id="header-nav">
			<?php
			global $siteMenu;

			$menuHtml = '';
			if( !empty( $siteMenu ) ) {
				foreach( $siteMenu as $menuItem ) {
					$current = '';
					if( $page == $menuItem['page'] ) { $current = ' current'; }
					$isParent = false;
					if( !empty( $menuItem['submenus'] ) ) { $isParent = true; }
					if( !$isParent ) {
						$menuHtml .= '<a class="menubutton' . $current . '" href="' . $menuItem['url'] . '">' . $menuItem['name'] . '</a>';
					} else {
						$menuHtml .= '<div class="menubutton isParent">' . $menuItem['name'] . '<div class="subMenuContainer">';
						foreach( $menuItem['submenus'] as $sub ) {
							$current = '';
							if( $page == $sub['page'] ) { $current = ' current'; }
							$menuHtml .= '<a class="submenubutton' . $current . '" href="' . $sub['url'] . '">' . $sub['name'] . '</a>';
						}
						$menuHtml .= '</div></div>';
					}
				}
			} else {
				$menuHtml .= 'Menu is not defined';
			}
			echo $menuHtml;
			?>
		</div>
	</div>
</header>
<div style="width:100%;text-align:center;"><?php dfp_ads('leader') ?></div>
