<?php
 
function readClassFile($feed,$date,$display=false,$folderdate='')
{
    //get all categories
    $sql="SELECT id,category_code FROM categories WHERE category_code<>''";
    $dbCats=dbselectmulti($sql);
    $catCodes=array();
    if ($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            $catCodes[$cat['category_code']]=$cat['id'];
        }   
    }
    
    
    $addresses=array();
    $inserted=0;
    $updated=0;
    
    $filenameTemplate = "IDAHOXML-11IN_97_DATE.txt";
    $filename=str_replace("DATE",$date,$filenameTemplate);
    
    $ads=class_feedreader($filename,$date,$folderdate);
    if ($ads=='nofile')
    {
        print "<div class='alert alert-warning'>The file was not on the server.</div>";
    } elseif ($ads=='noads')
    {
        print "<div class='alert alert-warning'>The file was not on the server, but it appears to be empty.</div>";
    } elseif (count($ads)>0)
    {
        $today=date("Y-m-d");
        $importdatetime=date("Y-m-d H:i:s");
        
        //build an array of all existing ads for this feed
        $sql="SELECT system_ad_number FROM classifieds WHERE system_ad_number <> '' AND end_date>='$today'";
        $dbExistingAds=dbselectmulti($sql);
        $existingads=array();
        if($dbExistingAds['numrows']>0)
        {
            foreach($dbExistingAds['data'] as $ea)
            {
                $existingads[]=$ea['ad_number'];
            }    
        }
        if($display){print "The following ad number were existing:<br>";print implode(", ",$existingads)."<br><br>";}
        $deleteads=$existingads;
        
        
        //get all tags
        $sql="SELECT * FROM c2_classtags WHERE tag_field<>''";
        $dbTags=dbselectmulti($sql);
        $i=0;
        if($dbTags['numrows']>0)
        {
            foreach($dbTags['data'] as $itag)
            {
                $iTags[$i]['default']=stripslashes($itag['tag_default']);
                $iTags[$i]['name']=stripslashes($itag['tag_name']);
                $iTags[$i]['type']=$itag['tag_type'];
                $iTags[$i]['field']=stripslashes($itag['tag_field']);
                $adtableinserts.=", ".stripslashes($itag['tag_field']);
                $i++;
            }
        } else {
            $iTags=array();
        }
        //get all charge codes
        $sql="SELECT * FROM c2_classcharges WHERE charge_field<>''";
        $dbCharges=dbselectmulti($sql);
        $i=0;
        if($dbCharges['numrows']>0)
        {
            foreach($dbCharges['data'] as $icharge)
            {
                $iCharges[$i]['default']=stripslashes($icharge['charge_default']);
                $iCharges[$i]['field']=stripslashes($icharge['charge_field']);
                $iCharges[$i]['name']=stripslashes($icharge['charge_name']);
                $adtableinserts.=", ".stripslashes($icharge['charge_field']);
                $i++;
            }
        } else {
            $iCharges=array();
        }
        
            
        $adnumbers="";
        $adinserts=array();
        $graphicinserts=array();
        $s=0;
        foreach ($ads as $ad)
        {
            $adnumber="";
            $class="";
            $start="";
            $stop="";
            $adtext="";
            $adid=0;
            $graphics=array();
            $internetTags=array();
            $chargeCodes=array();
            $taginserts='';
            $adsql='';
            $tagupdates='';
            $chargeinserts='';
            $chargeupdates='';
            $field='';
            $updateError='';
            foreach($ad as $key=>$value)
            {
                
                switch ($key)
                {
                    case "advertiser-id":
                        $advertiserid=$value;
                    break;
                    
                    case "ad-number":
                        $adnumber=$value;
                    break;
                    
                    case "class-code":
                        $class=$value;
                        $ocode=$value;
                        //we need to see if it's in the vdcodes array, and if so, translate it to the proper internal code
                        if (array_key_exists($class,$vdcodes))
                        {
                            $class=$vdcodes[$class];
                            //print "<br />translated a code to $class from $value<br />";
                        } else {
                            //print "Could not find $class in the array<br />\n";
                        }
                    break;
                    
                    case "start-date":
                        $start=date("Y-m-d",strtotime($value));
                    break;
                    
                    case "stop-date":
                       $stop=date("Y-m-d",strtotime($value));
                    break;
                    
                    case "charge-codes":
                       $ctemp=explode(" ",trim($value));
                       foreach($ctemp as $key=>$cname)
                       {
                           $chargeCodes[$cname]=1; //if its here that means its active
                       }
                    break;
                    
                    case 'ad-text':
                       $adtext=strip_tags($value,"<p>,<br>,<b>,<strong>,<i>,<sup>,<sub>,<font>");
                       $adtext=str_replace('�','**d**',$adtext);
                       $adtext=str_replace('�','**f14**',$adtext);
                       $adtext=str_replace('�','**f12**',$adtext);
                       $adtext=str_replace('�','**f34**',$adtext);
                       $adtext=htmlentities($adtext);
                       $adtext=str_replace('**d**','&amp;#176;',$adtext);
                       $adtext=str_replace('**f14**','&amp;#188;',$adtext);
                       $adtext=str_replace('**f12**','&amp;#189;',$adtext);
                       $adtext=str_replace('**f34**','&amp;#190;',$adtext);
                       $adtext=addslashes($adtext);
                       $adtext=str_replace("  "," ",$adtext);
                       $adtext=str_replace("  "," ",$adtext);
                       $adtext=str_replace("  "," ",$adtext);
                    break;
                    
                    case 'logos':
                    foreach ($value as $graphic)
                    {
                        if ($graphic!='')
                        {
                            $graphics[]=str_replace("&amp;","&",$graphic);
                        }
                        
                    }
                   break;
                   
                   case 'tags':
                    foreach ($value as $tagname=>$tagvalue)
                    {
                        $internetTags[$tagname]=$tagvalue;
                    }
                   break;
                }  
            }
            $adnumbers.="'$adnumber',";
            
            
            $adtoken=generate_random_string(16);
            if (trim($adtext)!='' || count($graphics)>0)
            {
                
                //print "Graphics is ".print_r($graphics);
                // need to update
                if (count($graphics)>0)
                {
                    foreach ($graphics as $graphic)
                    {
                        if ($graphic!='')
                        {
                            $graphic=addslashes($graphic);
                            $graphicinserts[]="('$adtoken', '$adid', '$adnumber', '$graphic','$feed[feed_code]','$feed[feed_extra]')";
                            //print "inserted a graphic with $ginsql";
                        }
                    }
                
                }
                $tvalue='';
                if($display){print "S is $s<br>";}
                foreach($iTags as $itag)
                {
                    $tname=$itag['name'];
                    $field=$itag['field'];
                    $default=$itag['default'];
                    if($internetTags[$tname]!='')
                    {
                        $tvalue=$internetTags[$tname];    
                    } else {
                        $tvalue=$default;
                    }
                    if ($tname=='street_address' && $tvalue!='')
                    {
                        if($display){print "Address found: $tvalue<br>";}
                        $addresses[$s]['street']=$tvalue;
                        $addresses[$s]['adtoken']=$adtoken;
                        $addresses[$s]['adnumber']=$adnumber;
                    }
                    if ($tname=='City' && $tvalue!='')
                    {
                        $addresses[$s]['city']=$tvalue;
                    }
                    if ($tname=='State' && $tvalue!='')
                    {
                        $addresses[$s]['state']=$tvalue;
                    }
                    if ($tname=='Zip' && $tvalue!='')
                    {
                        $addresses[$s]['zip']=$tvalue;
                    }
                    if($iTags[$tname]['type']=='number' || $iTags[$tname]['type']=='numberrange')
                    {
                        $tvalue=str_replace("\$",'',$tvalue);
                        $tvalue=floatval($tvalue);
                    }
                            
                    $tvalue=addslashes($tvalue);
                    
                    $taginserts.=", '$tvalue'";
                    $tagupdates.=", $field='$tvalue'";
                    //$taginserts[]=",'$adtoken', '$adid', '$adnumber', '$tname', '$tvalue','$feed[feed_code]','$feed[feed_extra]')";
                    
                }
                $tvalue='';
                foreach($iCharges as $ic)
                {
                  $cname=$ic['name'];
                  $field=$ic['field'];
                  $default=$ic['default'];
                  if($chargeCodes[$cname]!='')
                    {
                        $tvalue=$chargeCodes[$cname];    
                    } else {
                        $tvalue=$default;
                    }
                    $chargeinserts.=", '$tvalue'";
                    $chargeupdates.=", $field='$tvalue'";
                    
                }
                
                //ok, we know that if the start date of the ad is today that means this is the first insertion,
                //otherwise we are updating an existing ad
                if($feed['main_feed']==1)
                {
                    //we only modify the c2_classifieds_v2 main record for the main feed
                    if(in_array($adnumber,$existingads))
                    {
                        if($display){print "Ad number was found in existingads array<br>";}
                        //updating an existing ad
                        //will need to update the old ad to the new adtoken
                        $updatesql="UPDATE c2_classifieds_v2 SET ad_token='$adtoken', class_code='$class', start_date='$start', stop_date='$stop', ad_text='$adtext', update_datetime='$importdatetime', advertiser_id='$advertiserid', display_ad='$feed[display_feed]', $feed[field_name]=1 $tagupdates $chargeupdates WHERE ad_number='$adnumber' AND feed_extra='$feed[feed_extra]' AND type='$feed[feed_code]'";
                        $dbUpdate=dbexecutequery($updatesql);
                        $updateError=$dbUpdate['error'];
                        if($display){print "Updating with $updatesql<br>Error: $updateError<br>";}
                        $updated++;
                    } else {
                        //inserting a new ad
                        if($display){print "Ad number was NOT found in existingads array<br>";}
                        $adsql="('$adtoken', '$feed[feed_code]','$feed[feed_extra]','$adnumber', '$class', '$start', '$stop', '$adtext', '$importdatetime', '$advertiserid', '$feed[display_feed]', 1 $taginserts $chargeinserts)";
                        //inserting a new ad
                        $adinserts[]=$adsql;
                        $inserted++;
                    }
                } else {
                    //we just update the record
                    $updatesql="UPDATE c2_classifieds_v2 SET $feed[field_name]=1 WHERE ad_number='$adnumber'";
                    if($display){print "Updating with $updatesql<br>";}
                    $dbUpdateExisting=dbexecutequery($updatesql);
                    if($dbUpdateExisting['error']=='')
                    {
                        $updateexisting++;
                    } else {
                        $updateError=$dbUpdateExisting['error'];
                    }
                }
                
                if ($display)
                {
                    print "Importing ad: $adnumber<br>
                    Ad Token: $adtoken<br>
                    Start date: $start<br>
                    End date: $stop<br>
                    Advertiser_id: $advertiserid<br>
                    Class code: $ocode - translated to $class<br>
                    Ad Text: $adtext<br>
                    Insert Sql was: $adsql<br>
                    Update Sql was: $updatesql<br>
                    Update Error was: $updateError<br />
                    Graphics: ".implode(",",$graphics)."<br>";
                    if($start==$today)
                    {
                        print "New Ad was inserted<br>";
                    } else {
                        print "Existing Ad was updated<br>";
                    }
                    print "Additional c2_classified fields added: $adtableinserts<br>";
                    print "Charge codes: ".implode(",",$ctemp)."<br />";
                    print "Charge insert sql: $chargeinserts<br>Charge update sql: $chargeupdates<br>";
                    print "Tags: ".implode(",",$internetTags)."<br>";
                    print "Tag insert sql: $taginserts<br>Tag update sql: $tagupdates<br>";
                    if ($class!=$ocode)
                    {
                        print "Translated class code from $ocode to $class<br />";
                    }
                    print "<hr />";
                }
               
                
               
            }
           
          $s++;    
        }
        $adnumbers=substr($adnumber,0,strlen($adnumbers)-1);
        
        if($feed['main_feed']==1)
        {
            //now insert all the new ads for the main feed
            //we process 200 records at a time to not hit the database too hard
            $totalads=0;
            $countads=count($adinserts);
            if ($countads>0)
            {
                $counter=1;
                $running=true;
                $a="";
                if($countads<200){$max=$countads-1;}else{$max=200;}
                foreach($adinserts as $ad)
                {
                    $totalads++;
                    if ($counter<$max)
                    {
                        $a.=$ad.", ";
                        $running=true;
                    } else {
                        $a.=$ad;
                        if ($a!='')
                        {
                            if(substr($a,strlen($a)-1,1)==',')
                            {
                                $a=substr($a,0,strlen($a)-1);
                            }
                            
                            $adinsql="INSERT INTO c2_classifieds_v2 (ad_token, type,feed_extra,ad_number, class_code, start_date, stop_date, ad_text, insert_datetime, advertiser_id, display_ad, $feed[field_name] $adtableinserts) VALUES $a";
                            $dbAd=dbinsertquery($adinsql);
                            if ($dbAd['error']!='')
                            {
                                print "Ad inserting error: $dbAd[error]<br /><br>$adinsql";
                            }
                        }
                        $counter=1;
                        $a="";
                        $running=false;
                    }
                    $counter++;  
                }
            }
            if ($running)
            {
                $a=substr($a,0,strlen($a)-1);
                if(substr($a,strlen($a)-1,1)==',')
                {
                    $a=substr($a,0,strlen($a)-1);
                }
                if ($a!='')
                {
                    $adinsql="INSERT INTO c2_classifieds_v2 (ad_token, type,feed_extra,ad_number, class_code, start_date, stop_date, ad_text, insert_datetime, advertiser_id, display_ad, $feed[field_name] $adtableinserts)
                         VALUES $a";
                    $dbAd=dbinsertquery($adinsql);
                    if ($dbAd['error']!='')
                    {
                        print "Ad inserting error: $dbAd[error]<br />";
                    }
                }
                $counter=1;
                $a="";
                $running=false;
            }
            if ($display){print "Inserted $totalads out of $countads ads<br />";}
            //now insert all the new graphics
            //we process 200 records at a time to not hit the database too hard
            if (count($graphicinserts)>0)
            {
                $running=true;
                $counter=1;
                $a="";
                if(count($graphicinsert)<200){$max=count($graphicinserts);}else{$max=200;}
                foreach($graphicinserts as $graphic)
                {
                    if ($counter<$max)
                    {
                        $a.=$graphic.",";
                        $running=true;
                
                    } else {
                        $a.=$graphic;
                        if ($a!='')
                        {
                            $ginsql="INSERT INTO c2_classifieds_graphics (ad_token, ad_id, ad_number, filename, ad_type, ad_extra) VALUES $a";
                            $dbGraphic=dbinsertquery($ginsql);
                            if ($dbGraphic['error']!='')
                            {
                                print "Graphic inserting error: $dbGraphic[error]<br />";
                            }
                        }
                        $counter=0;
                        $a="";
                        $running=false;
                
                    }
                    $counter++;
                }
            }
            if ($running)
            {
                $a=substr($a,0,strlen($a)-1);
                if ($a!='')
                {
                    $ginsql="INSERT INTO c2_classifieds_graphics (ad_token, ad_id, ad_number, filename, ad_type, ad_extra) VALUES $a";
                    $dbGraphic=dbinsertquery($ginsql);
                    if ($dbGraphic['error']!='')
                    {
                        print "Graphic inserting error: $dbGraphic[error]<br />";
                    }
                }
                $counter=0;
                $a="";
                        
            }
           
            
            
            
            print "<br><br><br>A total of $inserted $type ads have been successfully inserted for feed $feed[feed_name] from $filename.<br>While a total of $updated ads were updated.<br><br>";
            $GLOBALS['info'].="A total of $inserted $type ads have been successfully inserted for feed $feed[feed_name] from $filename.<br>While a total of $updated ads were updated.<br><br>";
        } else {
            //just updated existing ads
            print "<br><br><br>A total of $updateexisting ads have been successfully updated for feed $feed[feed_name] from $filename.<br><br>";
            $GLOBALS['info'].="A total of $updateexisting ads have been successfully updated for feed $feed[feed_name] from $filename.<br><br>";
            
            
        }
        $deletes='';
        if(count($deleteads)>0)
        {
            
            print "The follow ads have been killed.<br>";
            foreach($deleteads as $key=>$da)
            {
                //before we delete, lets look up the ad and find out what class code and start date it has
                //based on the classified category, some ads will not be deleted until XX days after start
                $sql="SELECT A.start_date, B.hold_purge  FROM c2_classifieds_v2 A, c2_classifieds_categories B
                 WHERE A.ad_number='$da' AND A.class_code=B.category_code";
                $dbCheck=dbselectsingle($sql);
                $check=$dbCheck['data'];
                if($check['hold_purge']>0)
                {
                    if(time()>strtotime($check['start_date']."+ $check[hold_purge] days"))
                    {
                        $purge=true;
                    } else {
                        $purge=false;
                    }
                } else {
                    $purge=true;
                }
                if($purge)
                {
                    print "$da<br>";
                    $deletes.="'$da',";
                }
                $purge=false;
            }    
        }
        $deletes=substr($deletes,0,strlen($deletes)-1);
        if($deletes!='')
        {
            print "Deleting ads...<br>With $deletes<br><br>";
            $sql="DELETE FROM c2_classifieds_v2 WHERE ad_number IN ($deletes)";
            $dbDelete=dbexecutequery($sql);
        }
        
        if (count($addresses)>0 && $geocode)
        {
            print "Geocoding now...<br />";
            print "There should be ".count($addresses)." addresses to check!<br />";
            $bad=batch_geocode($addresses,$display);
            if (count($bad)>0)
            {
                $server=$_SERVER['SERVER_NAME'];
                $mail = new htmlMimeMail();
                $message="The following ads failed to geocode properly. You will need to change the address in CMS as well as in Vision Data\n";
                foreach($bad as $item)
                {
                    $message.="<br>$item[adnumber] - Status code: $item[status]\n$item[street], $item[city] $item[state] $item[zip]\n";
                    $message.="<a href='http://$server/app/cms/classifieds.php?action=edit&id=$item[adid]'>Click here to edit in the CMS</a>\n\n";
                }
                print $message;   
                $GLOBALS['info'].=$message;   
                if ($alertgeocode)
                {
                    $day=strtolower(date("l",strtotime($date)));
                    if ($feed['alert_'.$day])
                    {
                        sendTextEmailAlert($message);
                    }    
                }
                
            }
        } else {
            print "<br><br>There were no addresses to geocode.<br>";
        }
    } else {
         print "No ads where found in that file for $feed[feed_name] using $filename<br>";
        $GLOBALS['info'].="No ads where found in that file for $feed[feed_name] using $filename<br>";
    }
    import_mobile_ads($feed);
    
    
    print "<a href='?action=again'>Click here to run for a different date.</a>\n"; 
}

function import_mobile_ads($feed)
{
    $sql="SELECT A.*, B.feed_code, B.feed_extra FROM c2_classified_settings A, c2_classifieds_feeds B WHERE A.feed_id=B.id";
    $dbSettings=dbselectsingle($sql);
    $classSettings=$dbSettings['data'];
    $date=date("Y-m-d H:i:s");
    //only run this for feeds matching the default class setting feed
    if($feed['feed_code']==$classSettings['feed_code'])
    {
        $sql="SELECT * FROM c2_classifieds_mobile WHERE approved=1 AND startdate<='$date' AND enddate>'$date'";
        $dbAds=dbselectmulti($sql);
        $mobileads=0;
        if($dbAds['numrows']>0)
        {
            foreach($dbAds['data'] as $ad)
            {
                $adtoken=generate_random_string(16);
                $adtext="<p><b>".$ad['headline']."</b></p><p>".$ad['adtext']."</p>";
                $sql="INSERT INTO c2_classifieds_v2 (ad_token, type,feed_extra,ad_number, class_code, start_date, stop_date, ad_text, insert_datetime, advertiser_id, display_ad) VALUES ('$adtoken', '$classSettings[feed_code]', '$classSettings[feed_extra]', '$ad[id]', '$ad[category_id]','$ad[startdate]','$ad[enddate]','$adtext','".date("Y-m-d H:i:s")."',0,0)";
                $dbInsert=dbinsertquery($sql);
                if($dbInsert['error']=='')
                {
                    $id=$dbInsert['insertid'];
                    if($ad['photo']!='')
                    {
                        $sql="INSERT INTO c2_classifieds_graphics (ad_token, ad_id, ad_number, filename, ad_type) VALUES ('$adtoken', '$id', '$adid', '$ad[photo]', '$classSettings[feed_code]')";
                        $dbInsert=dbinsertquery($sql);
                    }
                    $mobileads++;
                }
            }
        }
        $message="<br>There were $dbAds[numrows] mobile ads available, of which $mobileads were inserted into the classified system successfully.";
        $GLOBALS['info'].=$message;
        print $message."<br>";
    }
}

function batch_geocode($addresses,$display)
{
    
    $sql="SELECT default_lat, default_lon FROM c2_classified_settings";
    $dbDefault=dbselectsingle($sql);
    if ($dbDefault['numrows']>0)
    {
        $defaultLat=$dbDefault['data']['default_lat'];
        $defaultLon=$dbDefault['data']['default_lon'];
    } else {
        $defaultLat=43.57939;
        $defaultLon=-116.55910;
    }
    
    //print "Starting geocoding<br />\n";
    // Initialize delay in geocode speed
    $badads=array();
    //$key=stripslashes($dbKey['data']['googlemapkey']);
    $key='ABQIAAAAzfHiEH3MngsHcn4mhhpdiBTXoPOVFnYQqph6EnBqc9r_qC_IBBR05RjbJxfr1XHhhhAC61biqjjZjg';
    
    $delay = 0;
    $base_url="http://maps.googleapis.com/maps/api/geocode/json";
    //lets test a hardcoded address:
    if($display){print "Batch geocoder was handed ".count($addresses)." addresses to check...<br>";}
    // Iterate through the rows, geocoding each address
    foreach ($addresses as $adid=>$caddress)
    {
      $geocode_pending = true;
      $delay = 0;
    
      while ($geocode_pending) {
        $address = urlencode($caddress["street"].' '.$caddress['city'].' '.$caddress['state'].' '.$caddress['zip']);
        $adnumber = $caddress["adnumber"];
        $adtoken = $caddress["adtoken"];
        $url = $base_url . "?address=" .$address."&sensor=false";
        if($display){print "Checking $caddress[street] with url of $url<br>";}
        //$csv = file_get_contents($url);
        $temp=get_web_page($url);
        $json=json_decode($temp['content'],true);
        $results=$json['results'][0];
        $status=$json['status'];
        $geometry=$results['geometry']['location'];
        if ($status=="OK") {
          // successful geocode
          $geocode_pending = false;
          $lat=$geometry['lat']; 
          $lon=$geometry['lng']; 
          if ($lat=='' || $lat==0){$lat=$defaultLat;}
          if ($lon=='' || $lon==0){$lon=$defaultLon;}
          $sql="UPDATE c2_classifieds_v2 SET lat='$lat', lon='$lon', street_address='$caddress[street]', city='$caddress[city]', state='$caddress[state]' WHERE ad_number='$adnumber' AND ad_token='$adtoken'";
          $dbUpdate=dbexecutequery($sql);
          if ($dbUpdate['error']!=''){print "Geocode errored with $dbUpdate[error]<br />";}
          if($display){print "Geocoded $adnumber with token $adtoken successfully - $sql<br />";}
          $address['status']='successful';
        } else if ($status=="UNKNOWN_ERROR" || trim($status)=="") {
          // sent geocodes too fast
          $delay += 1000;
        } else if ($status=='INVALID_REQUEST' || $status=='ZERO_RESULTS' || $status=='REQUEST_DENIED') {
          // failure to geocode
          $geocode_pending = false;
          $address['status']=$status;
          $badads[]=$caddress;
        }
        
        usleep($delay);
        //print "$status with lat=$lat and lon=$lng<br>";
      }
      if($i==50){break;}else{$i++;}
    }
   
   return $badads;
}


function class_feedreader($file,$date='',$folderdate='')
{
    $sql="SELECT * FROM c2_classified_settings";
    $dbSettings=dbselectsingle($sql);
    
    $sql="SELECT tag_name FROM c2_classtags";
    $dbTags=dbselectmulti($sql);
    $tags=$dbTags['data'];
    $ads=array();
    $path=$_SERVER['PHP_SELF'];
    $path=explode("/",$path);
    $levels=count($path)-3;
    $path=$path[count($path)-2];
    $i=0;
    $folderDepth='';
    while($i<$levels)
    {
        $folderDepth.='../';
        $i++;
    }
    if($date=='')
    {
        $date=date("mdy"); 
    } else {
        if($_POST)
        {
            $date=date("mdy",strtotime($_POST['date'])); 
        }  else {
            $date=date("mdy",strtotime($date)); 
        } 
    }
    if($dbSettings['data']['vdata_date_folders'])
    {
        $file=$folderDepth."vdata/".$folderdate."/".$file;
    } else {
        $file=$folderDepth."vdata/".$file;
    }    
    $GLOBALS['info'].="Looking for a file in $date - $file<br>";
    if (file_exists($file))
    {
        $contents=file_get_contents($file);
        if ($contents==''){return 'noads';}
        $contents=str_replace("<feed>","",$contents);
        $contents=str_replace("</feed>","",$contents);
        //ok, now break the feed up into <ad> blocks
        $contents=str_replace("<ad>","",$contents);
        $contents=str_replace("<![CDATA[","",$contents);
        $contents=str_replace("]]>","",$contents);
        $contents=explode("</ad>",$contents);
        $i=0;
        foreach($contents as $contentitem)
        {
            //get the easy items first
            $advid=explode("<advertiser-id>",$contentitem);
            $advid=explode("</advertiser-id>",$advid[1]);
            $advid=str_replace("<advertiser-id>","",$advid[0]);
            $ads[$i]['advertiser-id']=str_replace("</advertiser-id>","",$advid);
            
            $numberitem=explode("<ad-number>",$contentitem);
            $numberitem=explode("</ad-number>",$numberitem[1]);
            $numberitem=str_replace("<ad-number>","",$numberitem[0]);
            $ads[$i]['ad-number']=str_replace("</ad-number>","",$numberitem);
            
            $chargeitem=explode("<charge-codes>",$contentitem);
            $chargeitem=explode("</charge-codes>",$chargeitem[1]);
            $chargeitem=str_replace("<charge-codes>","",$chargeitem[0]);
            $ads[$i]['charge-codes']=str_replace("</charge-codes>","",$chargeitem);
            
            $codeitem=explode("<class-code>",$contentitem);
            $codeitem=explode("</class-code>",$codeitem[1]);
            $codeitem=str_replace("<class-code>","",$codeitem[0]);
            $ads[$i]['class-code']=str_replace("</class-code>","",$codeitem);
            
            $startitem=explode("<start-date>",$contentitem);
            $startitem=explode("</start-date>",$startitem[1]);
            $startitem=str_replace("<start-date>","",$startitem[0]);
            $ads[$i]['start-date']=str_replace("</start-date>","",$startitem);
            
            $stopitem=explode("<stop-date>",$contentitem);
            $stopitem=explode("</stop-date>",$stopitem[1]);
            $stopitem=str_replace("<stop-date>","",$stopitem[0]);
            $ads[$i]['stop-date']=str_replace("</stop-date>","",$stopitem);
            
            $textitem=explode("<ad-text>",$contentitem);
            $textitem=explode("</ad-text>",$textitem[1]);
            $textitem=str_replace("<ad-text><![CDATA[","",$textitem[0]);
            $adtext=str_replace("]]></ad-text>","",$textitem);
            $adtext=feedcleaner($adtext);
            $ads[$i]['ad-text']=addslashes($adtext);
            
            if (strpos($contentitem,"<online-text>")>0)
            {
                //means that we have some extra online only text that we'll tack on to the 
                //end of the ad-text segment
                $extratext=explode("<online-text>",$contentitem);
                $extratext=explode("</online-text>",$extratext[1]);
                $extratext=str_replace("<online-text><![CDATA[","",$extratext[0]);
                $extratext=str_replace("<online-text>","",$extratext);
                $extratext=str_replace("]]></online-text>","",$extratext);
                $extraadtext=str_replace("</online-text>","",$extratext);
                $extraadtext=feedcleaner($extraadtext);
                $ads[$i]['ad-text']=addslashes($extraadtext);
            
            }
            
            //now logos
            $item=explode("<logos>",$contentitem);
            $item=explode("</logos>",$item[1]);
            $item=str_replace("<logos>","",$item[0]);
            $logos=str_replace("</logos>","",$item);
            if ($logos!='')
            {
                //ok, found some logos
                $graphics=str_replace("<logo>","",$logos);
                $graphics=explode("</logo>",$graphics);
                foreach($graphics as $graphic)
                {
                     $graphic=str_replace("</logo>","",$graphic);
                     if ($graphic!='')
                     {
                        $ads[$i]['logos'][]=trim($graphic);    
                     }
                     
                }
                
            }
            
            //now tags
            $item=explode("<tags>",$contentitem);
            $item=explode("</tags>",$item[1]);
            $item=str_replace("<tags>","",$item[0]);
            $feedtags=str_replace("</tags>","",$item);
            if ($feedtags!='')
            {
                //ok, found some tags
                //run through the process with each possible tag
                foreach($tags as $tag)
                {
                    $tag=$tag['tag_name'];
                    if (strpos($feedtags,$tag)>0)
                    {
                        //means that tag is here
                        $temp=explode("<$tag>",$feedtags);
                        $temp=explode("</$tag>",$temp[1]);
                        $temp=str_replace("<$tag>","",$temp[0]);
                        $temp=str_replace("</$tag>","",$temp);
                        if ($temp!='')
                        {
                            $ads[$i]['tags'][$tag]=$temp;
                        }
                    }
                }
            }
            $i++;
        }
        return $ads;
    } else {
        return 'nofile';
    }
}


function feedcleaner($text)
{
    $text=str_replace("�","'",$text);
        $text=str_replace("�","1/2",$text);
        $text=str_replace("�","3/4",$text);
        $text=str_replace("�","&#241;",$text);
        $text=str_replace("�","-",$text);
        $text=str_replace("�","&#233;",$text);
        
        $text = str_replace("&nbsp;","&#160;",$text);
        $text = str_replace('&amp;trade;','&#169;',$text);
        $text = str_replace('&trade;','&#169;',$text);
        $text = str_replace('&reg;','&#174;',$text);
        $text = str_replace('&amp;reg;','&#174;',$text);
        $text = str_replace("  "," ",$text);
        $text = str_replace("�", "'", $text);
        $text = str_replace("�", "'", $text);
        
        // Various extended HTML characters - note no ; -- replace function will check with and without ;
        $text = str_replace("&shy", "", $text);
        $text = str_replace("&ndash", "&#8211;", $text);
        $text = str_replace("&mdash", "&#8212;", $text);
        $text = str_replace("&iexcl", "&#161;", $text);
        $text = str_replace("&pound", "&#163;", $text);
        $text = str_replace("&curren", "&#164;", $text);
        $text = str_replace("&yen", "&#165;", $text);
        $text = str_replace("&sect", "&#167;", $text);
        $text = str_replace("&copy", "&#169;", $text);
        $text = str_replace("&reg", "&#174;", $text);
        $text = str_replace("&deg", "&#176;", $text);
        $text = str_replace("&acute", "&#180;", $text);
        $text = str_replace("&frac14", "&#188;", $text);
        $text = str_replace("&frac12", "&#189;", $text);
        $text = str_replace("&frac34", "&#190;", $text);
        $text = str_replace("&iquest", "&#191;", $text);
        $text = str_replace("&sbquo", ",", $text);
        $text = str_replace("&Agrave", "&#192;", $text);
        $text = str_replace("&Aacute", "&#193;", $text);
        $text = str_replace("&Acirc", "&#194;", $text);
        $text = str_replace("&Atilde", "&#195;", $text);
        $text = str_replace("&Auml", "&#196;", $text);
        $text = str_replace("&Aring", "&#197;", $text);
        $text = str_replace("&AElig", "&#198;", $text);
        $text = str_replace("&Ccedil", "&#199;", $text);
        $text = str_replace("&Egrave", "&#200;", $text);
        $text = str_replace("&Eacute", "&#201;", $text);
        $text = str_replace("&Ecirc", "&#202;", $text);
        $text = str_replace("&Euml", "&#203;", $text);
        $text = str_replace("&Igrave", "&#204;", $text);
        $text = str_replace("&Iacute", "&#205;", $text);
        $text = str_replace("&Icirc", "&#206;", $text);
        $text = str_replace("&Iuml", "&#207;", $text);
        $text = str_replace("&ETH", "&#208;", $text);
        $text = str_replace("&Ntilde", "&#209;", $text);
        $text = str_replace("&Ograve", "&#210;", $text);
        $text = str_replace("&Oacute", "&#211;", $text);
        $text = str_replace("&Ocirc", "&#212;", $text);
        $text = str_replace("&Otilde", "&#213;", $text);
        $text = str_replace("&Ouml", "&#214;", $text);
        $text = str_replace("&times", "&#215;", $text);
        $text = str_replace("&Oslash", "&#216;", $text);
        $text = str_replace("&Ugrave", "&#217;", $text);
        $text = str_replace("&Uacute", "&#218;", $text);
        $text = str_replace("&Ucirc", "&#219;", $text);
        $text = str_replace("&Uuml", "&#220;", $text);
        $text = str_replace("&Yacute", "&#221;", $text);
        $text = str_replace("&Yacute", "&#221;", $text);
        $text = str_replace("&THORN", "&#222;", $text);
        $text = str_replace("&szlig", "&#223;", $text);
        
        //lowercase letters
        $text = str_replace("&agrave", "&#224;", $text);
        $text = str_replace("&aacute", "&#225;", $text);
        $text = str_replace("&acirc", "&#226;", $text);
        $text = str_replace("&atilde", "&#227;", $text);
        $text = str_replace("&auml", "&#228;", $text);
        $text = str_replace("&aring", "&#229;", $text);
        $text = str_replace("&aelig", "&#230;", $text);
        $text = str_replace("&ccedil", "&#231;", $text);
        $text = str_replace("&egrave", "&#232;", $text);
        $text = str_replace("&eacute", "&#233;", $text);
        $text = str_replace("&ecirc", "&#234;", $text);
        $text = str_replace("&euml", "&#235;", $text);
        $text = str_replace("&igrave", "&#236;", $text);
        $text = str_replace("&iacute", "&#237;", $text);
        $text = str_replace("&icirc", "&#238;", $text);
        $text = str_replace("&iuml", "&#239;", $text);
        $text = str_replace("&eth", "&#240;", $text);
        $text = str_replace("&ntilde", "&#241;", $text);
        $text = str_replace("&ograve", "&#242;", $text);
        $text = str_replace("&oacute", "&#243;", $text);
        $text = str_replace("&ocirc", "&#244;", $text);
        $text = str_replace("&otilde", "&#245;", $text);
        $text = str_replace("&ouml", "&#246;", $text);
        $text = str_replace("&divide", "&#247;", $text);
        $text = str_replace("&oslash", "&#248;", $text);
        $text = str_replace("&ugrave", "&#249;", $text);
        $text = str_replace("&uacute", "&#250;", $text);
        $text = str_replace("&ucirc", "&#251;", $text);
        $text = str_replace("&uuml", "&#252;", $text);
        $text = str_replace("&yacute", "&#253;", $text);
        $text = str_replace("&thorn", "&#254;", $text);
        $text = str_replace("&yuml", "&#255;", $text);
        
        $text = str_replace("&deg", "deg.", $text);
        $text = str_replace("&hellip", "...", $text);
        $text = str_replace("&lsquo", "&#39;", $text);
        $text = str_replace("&rsquo", "&#39;", $text);
        $text = str_replace("&ldquo", "&quot;", $text);
        $text = str_replace("&rdquo", "&quot;", $text);
        $text = str_replace(" & ", " &amp; ", $text);
        $text = str_replace("'", "&#39;", $text);
        $text = str_replace("&lt;p&rt;", "<p>", $text);
        $text = str_replace("&lt;/p&rt;", "</p>", $text);
        $text = str_replace("\"", "&quot;", $text);
        $text = str_replace("&amp;quot;", "&quot;", $text);
        return $text;
}



function get_web_page( $url )
{
    $options = array(
        CURLOPT_RETURNTRANSFER => true,     // return web page
        CURLOPT_HEADER         => false,    // don't return headers
        CURLOPT_FOLLOWLOCATION => true,     // follow redirects
        CURLOPT_ENCODING       => "",       // handle all encodings
        CURLOPT_USERAGENT      => "spider", // who am i
        CURLOPT_AUTOREFERER    => true,     // set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
        CURLOPT_TIMEOUT        => 120,      // timeout on response
        CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
    );

    $ch      = curl_init( $url );
    curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //     // return web page
    //    CURLOPT_HEADER         => false,    // don't return headers
    //    CURLOPT_FOLLOWLOCATION => true,     // follow redirects
    //    CURLOPT_ENCODING       => "",       // handle all encodings
    //    CURLOPT_USERAGENT      => "spider", // who am i
    //    CURLOPT_AUTOREFERER    => true,     // set referer on redirect
    //    CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
    //    CURLOPT_TIMEOUT        => 120,      // timeout on response
    //    CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects)
   
   //curl_setopt_array( $ch, $options );
    $content = curl_exec( $ch );
    $err     = curl_errno( $ch );
    $errmsg  = curl_error( $ch );
    $header  = curl_getinfo( $ch );
    curl_close( $ch );

    $header['errno']   = $err;
    $header['errmsg']  = $errmsg;
    $header['content'] = $content;
    return $header;
}
  
?>
