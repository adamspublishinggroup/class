<?php if( !empty( $_SESSION['loggedin'] ) && $_SESSION['loggedin'] == true && !empty( $_SESSION['email'] ) ) { ?>

	<?php
	$user = $GLOBALS['user'];
	$userID=$user['id'];
	$myads = getOwnAds($userID);
	if( !empty($myads) ) { ?>
		<div class="posts-wrap">
			<?php foreach( $myads as $ad ) {
				classListing( $ad, 'history' );
			} ?>
		</div>
	<?php } else { ?>
		<div class="posts-wrap no-posts">
			<p>You have no ads to show in your history.</p>
		</div>
	<?php } ?>

<?php } else { ?>

	<h3 style="text-align:center;">YOU ARE NOT LOGGED IN</h3>
	<p style="text-align:center;">You will be redirected to the login screen in <span id="counter" style="font-weight:700;">5</span> second(s).</p>
	<script type="text/javascript">
		function countdown() {
			var i = document.getElementById('counter');
			if( parseInt(i.innerHTML) <= 1 ) {
				location.href = '/account/login/';
			}
			i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
	</script>

<?php } ?>
