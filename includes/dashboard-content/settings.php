<?php
if( !empty( $_SESSION['loggedin'] ) && $_SESSION['loggedin'] == true && !empty( $_SESSION['email'] ) ) { 
    
    // save user profile changes
    if( $_POST['submit'] == 'Save Profile' ) {
        $userID = intval( $_POST['user_id'] );
        $userName = addslashes( $_POST['username'] );
        $firstName = addslashes( $_POST['first'] );
        $lastName = addslashes( $_POST['last'] );
        $email = addslashes( $_POST['email'] );
        $phone = addslashes( $_POST['phone'] );
        $street = addslashes( $_POST['street'] );
        $city = addslashes( $_POST['city'] );
        $state = addslashes( $_POST['state'] );
        $zip = addslashes( $_POST['zip'] );
        $gender = addslashes( $_POST['gender'] );
        $birthdate = addslashes( $_POST['birthdate'] );
        $bio = addslashes( $_POST['bio'] );
        if( $_POST['veteran'] ) {
            $veteran = 1;
        } else {
            $veteran = 0;
        }
        // basic info changes
        $sql = "UPDATE users SET username='$userName', first='$firstName', last='$lastName', phone='$phone', email='$email', street='$street', city='$city', state='$state', zip='$zip', gender='$gender', birthdate='$birthdate', bio='$bio', veteran='$veteran' WHERE id=$userID";  
        $dbUpdate = dbexecutequery($sql);
        // user avatar changes
        if( isset($_FILES) && $_FILES['mugshot']['tmp_name'] != '' ) {
            $uploadOK = 1;
            // Allow certain file formats
            $imageFileType = $_FILES['mugshot']['type'];
            if( $imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg" && $imageFileType != "image/gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
                $uploadOK = 0;
            }
            if( $uploadOK ) {
                $target_dir = "../../uploads/avatars/";
                $target_file = $target_dir . basename($_FILES["mugshot"]["name"]);
                if (move_uploaded_file($_FILES["mugshot"]["tmp_name"], $target_file)) {
                    $sql = "UPDATE users SET image='".addslashes($_FILES['mugshot']['name'])."' WHERE id=$userID";
                    $dbUpdate = dbexecutequery($sql);
                    $error = $dbUpdate['error'];
                } else {
                    $error = 'File upload error.';
                }
            }
        }
        addUserAction( $userID, 'user updated account' );
    }

    // save user password changes
    if( $_POST['submit'] == 'Change Password' ) {
        $userID = $_SESSION['user_id'];
        addUserAction( $userID,'user changed password' );
        $password = password_hash( $password, PASSWORD_DEFAULT );
        $sql = "UPDATE users SET password='$password' WHERE id=$userID";
        $dbUpdate = dbexecutequery($sql);
    }


    // get user info for form population
    global $config;
    $id = $_SESSION['user_id'];
    $genders = array('female'=>'Female','male'=>'Male');
    $sql = "SELECT * FROM users WHERE id=$id";
    $dbUser = dbselectsingle($sql);
    $user = $dbUser['data'];
    if( $user['state'] == '' ) {
        $user['state'] = $config['default_state'];
    }
    ?>
    <div class="col-xs-12">
        <form id="passwordForm" name="passwordForm" method=post class="form-horizontal">
            <h4>Change password</h4>
            <div class="form-group">
                <label for="user_password" class="col-sm-3 control-label">Password</label>
                <div class="col-sm-9">
                    <input type="password" id="user_password" name="user_password" />
                </div>
            </div>
            <div class="form-group">
                <label for="confirm_password" class="col-sm-3 control-label">Confirm Password</label>
                <div class="col-sm-9">
                    <input type="password" id="confirm_password" name="confirm_password" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <input type="submit" id="submit" name="submit" class="btn-dark" value="Change Password">
                </div>
            </div>
        </form>
    </div>
    <div class="clearFix"><br></div>
    <div class="col-xs-12">
        <form id="settingsForm" name="settingsForm" method=post enctype="multipart/form-data" class="form-horizontal">
            <h4>Change information</h4>
            <?php
            make_text('username',stripslashes($user['username']),'Username');
            make_text('first',stripslashes($user['first']),'First Name');
            make_text('last',stripslashes($user['last']),'Last Name');
            make_text('email',stripslashes($user['email']),'Email');
            make_phone('phone',$user['phone'],'Phone');
            make_text('street',$user['street'],'Street');
            make_text('city',$user['city'],'City');
            make_state('state',$user['state'],'State');
            make_text('zip',$user['zip'],'Zip');
            make_select('gender',$genders[$user['gender']],$genders,'Gender');
            make_file('mugshot','Bio Pic','','/uploads/avatars/'.$user['image']);
            make_date('birthdate',$user['birthdate'],'Birthdate');
            make_textarea('bio',stripslashes($user['bio']),'Bio');
            make_checkbox('veteran',$user['veteran'],'Veteran','Check this box if you are a veteran. You may qualify for free ads.');
            make_hidden('user_id',$id);
            make_submit('submit','Save Profile');
            ?>
        </form>
    </div>
    <script>
        $(document).ready(function() {
            var $validatorSettings = $("#settingsForm").validate({
                rules: {
                    email: {
                        required: true,
                        email: true,
                        remote: "/includes/checkEmail.php?self=true"
                    },
                    username: {
                        required: true,
                        minlength: 3,
                        remote: "/includes/checkUsername.php?self=true"
                    },
                    confirm_password: {
                        equalTo: "#user_password"
                    },
                    check_age: {
                        required: true
                    }
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
            var $validatorPassword = $("#passwordForm").validate({
                rules: {
                    confirm_password: {
                        equalTo: "#user_password"
                    }                
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });
        });
    </script>

<?php } else { ?>

	<h3 style="text-align:center;">YOU ARE NOT LOGGED IN</h3>
	<p style="text-align:center;">You will be redirected to the login screen in <span id="counter" style="font-weight:700;">3</span> second(s).</p>
	<script type="text/javascript">
		function countdown() {
			var i = document.getElementById('counter');
			if( parseInt(i.innerHTML) <= 1 ) {
				location.href = '/account/login/';
			}
			i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },3000);
	</script>

<?php } ?>
