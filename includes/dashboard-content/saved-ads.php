<?php if( !empty( $_SESSION['loggedin'] ) && $_SESSION['loggedin'] == true && !empty( $_SESSION['email'] ) ) { ?>

	<?php
	$userId = $GLOBALS['user']['id'];
	$myads = getSavedAds($userId);
	if( !empty($myads) ) { ?>
		<div class="posts-wrap">
			<?php foreach( $myads as $ad ) {
				classListing( $ad, 'saved' );
			} ?>
		</div>
	<?php } else { ?>
		<div class="posts-wrap no-posts">
			<p>You have no saved ads.</p>
		</div>
	<?php } ?>

<?php } else { ?>

	<h3 style="text-align:center;">YOU ARE NOT LOGGED IN</h3>
	<p style="text-align:center;">You will be redirected to the login screen in <span id="counter" style="font-weight:700;">5</span> second(s).</p>
	<script type="text/javascript">
		function countdown() {
			var i = document.getElementById('counter');
			if( parseInt(i.innerHTML) <= 1 ) {
				location.href = '/account/login/';
			}
			i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
	</script>

<?php } ?>
