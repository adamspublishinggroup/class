<?php
  require_once("bootCore.php");
  error_reporting(E_ERROR);
  require_once("../vendor/autoload.php");
  $imageID=intval($_POST['imageID']);


  $sql="SELECT * FROM images WHERE id=$imageID";
  $dbImage=dbselectsingle($sql);

  $filename=$dbImage['data']['filename'];
  $path = $dbImage['data']['path'];


  $imageDirectory = "../uploads/".$path;

  $json['filename']=$filename;
  $json['path']=$path;

  $canvasX = intval($_POST['canvasX']);
  $canvasY = intval($_POST['canvasY']);
  $canvasWidth = intval($_POST['canvasWidth']);
  $canvasHeight = intval($_POST['canvasHeight']);
  $naturalWidth = intval($_POST['naturalWidth']);
  $naturalHeight = intval($_POST['naturalHeight']);
  $rotate = $_POST['rotation'];
  $scaleX = $_POST['scaleX'];
  $scaleY = $_POST['scaleY'];
  $cropX = intval($_POST['cropX']);
  $cropY = intval($_POST['cropY']);
  $cropWidth = intval($_POST['cropWidth']);
  $cropHeight = intval($_POST['cropHeight']);

  $json['messages'][]="Filename is $filename<br>";
  //crop box data
  //{"left":121.25,"top":43.5,"width":199,"height":257}
  //$cropX = 121;
  //$cropY = 43;
  //$cropWidth = 199;
  //$cropHeight = 275;


  //canvas data
  //{"left":115.86574999999996,"top":-47.33300000000003,
  //"width":405.95500000000004,"height":405.95500000000004,"naturalWidth":300,"naturalHeight":300}
  //$canvasX = 115;
  //$canvasY = -47;


  //offset the crop x/y with the canvas placement
  $cropX = $cropX - $canvasX;
  $cropY = $cropY - $canvasY;

  //Image Data
  //{"rotate":-90,"scaleX":-1,"scaleY":1,"naturalWidth":300,"naturalHeight":300,"aspectRatio":1,
  //"width":405.95500000000004,"height":405.95500000000004,"left":0,"top":0}
  //$naturalWidth = 300;
  //$naturalHeight = 300;
  //$canvasWidth = ceil(405.95500000000004);
  //$canvasHeight = ceil(405.95500000000004);
  //$rotate = -90;
  //$scaleX = -1; //flip horizontal
  //$scaleY = 1;

  use Intervention\Image\ImageManager;
  $json['messages'][]= "Loaded Libary<br>";
  // create an image manager instance with favored driver
  $manager = new ImageManager(array('driver' => 'gd'));

  // load the image
  $image = $manager->make($imageDirectory.$filename);

  $json['messages'][]= "Loaded Image<br>";

  //first, we resize the image if needed
  if($canvasWidth != $naturalWidth)
  {
    $image->resize($canvasWidth, $canvasHeight);
    $json['messages'][]= "Resized Image<br>";
  }
  //next we flip it if needed
  if($scaleX==-1) {$image->flip('h'); print "Flipped Horizontally<br>";};
  if($scaleY==-1) {$image->flip('v'); print "Flipped Vertically<br>";};

  //rotate if needed
  if($rotate != 0)
  {
      $image->rotate(-$rotate);
      $json['messages'][]= "Rotated Image<br>";
  }

  // crop image
  $image->crop($cropWidth, $cropHeight, $cropX, $cropY);
  $json['messages'][]= "Cropped Image<br>";

  //output final image
  $image->save($imageDirectory.$filename);
  $json['messages'][]= "Saved Image<br>";

  //first, we resize the image to display size (width) and constrain the aspect ratio
  $image->resize(1000, null, function ($constraint) {
      $constraint->aspectRatio();
	  $constraint->upsize();
  });
  $image->save($imageDirectory.'display_'.$filename);
  //now resize for thumb (height) and constrain the aspect ratio
  $image->resize(null, 200, function ($constraint) {
      $constraint->aspectRatio();
  });
  $image->save($imageDirectory.'thumb_'.$filename);




  //make the thumbnail and the display image


  $json['status']='success';
  echo json_encode($json);
