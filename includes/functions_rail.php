<?php
/*
* master coordinator for page modules
*/
function pageModules($pos='left')
{
    global $config;

    //go through the config to pull out the various modules that are set to "left" position
    $modules=array();

    if($pos==$config['show_advertisers_rail'])
    {
        $order = $config['advertisers_position'];
        $modules[$order]='advertisers';
    }
    if($pos==$config['show_single_advertiser_rail'])
    {
        $order = $config['advertiser_position'];
        $modules[$order]='advertiser';
    }
    if($pos==$config['show_most_popular'])
    {
        $order = $config['most_popular_position'];
        $modules[$order]='popular';
    }
    if($pos==$config['show_featured'])
    {
        $order = $config['featured_position'];
        $modules[$order]='featured';
    }
    if($pos==$config['show_maps'])
    {
        $order = $config['maps_position'];
        $modules[$order]='maps';
    }
    //sort the modules
    ksort($modules);
    if(!empty($modules))
    {
        foreach($modules as $order=>$module)
        {
            switch ($module)
            {
                case 'advertisers':
                  multiAdvertiserWidget();
                break;

                case 'advertiser':
                   advertiserWidget();
                break;

                case 'popular':
                    popularWidget();
                break;

                case 'featured':
                   featuredWidget();
                break;

                case 'maps':
                   mapWidget();
                break;


            }
        }
    }
}


function multiAdvertiserWidget()
{
    $advertisers = getAllAdvertisers();
    if( !empty($advertisers) ) {
        shuffle( $advertisers );
        $advertisers = array_slice( $advertisers, 0, 4);
        ?>
        <div class="rail-post-cust">
            <div class="rail-post-advertisers">
                <div class="rail-advertisers-info">
                    <div class="rail-advertisers-title">Advertisers</div>
                    <div class="rail-advertisers-slogan">Check the Advertisers page for great local deals!</div>
                    <a class="rail-advertisers-pagelink" href="/advertisers/"><h6>See All Advertisers</h6></a>
                </div>
                <div class="rail-advertisers-listing">
                    <?php
                    foreach( $advertisers as $advertiser ) {
                        $status = $advertiser['active'];
                        $feat = false;
                        $bizId = $advertiser['id'];
                        $slug = $advertiser['slug'];
                        $logo = $advertiser['logo'];
                        $name = $advertiser['name'];
                        $slogan = $advertiser['slogan'];
                        $website = $advertiser['url'];
                        $phone = $advertiser['phone'];
                        $phReplace = array('.', '-', '(', ')', ' ');
                        $phoneRaw = str_replace($phReplace, '', $phone);
                        $phoneLink = 'tel:' . $phoneRaw;
                        $phoneDot = substr_replace( substr_replace( $phoneRaw, '.', 3, 0), '.', 7, 0);
                        $phoneDash = substr_replace( substr_replace( $phoneRaw, '-', 3, 0), '-', 7, 0);
                        $phoneFull = substr_replace( substr_replace( substr_replace( $phoneRaw, ') ', 3, 0), '-', 8, 0), '(', 0, 0);
                        $hasLogo = false;
                        if( trim($logo) > '' ) {
                            $hasLogo = true;
                        }
                        if( $status ) {
                            ?>
                            <a class="rail-advertiser<?= ($feat ? ' is-featured' : '') ?><?= ( $hasLogo ? ' with-logo' : '' ) ?>" href="/advertisers/<?= $slug ?>">
                                <?php if( $hasLogo ) { ?>
                                    <div class="rail-advertiser-logo" style="background-image:url(<?= $logo ?>);"></div>
                                <?php } ?>
                                <div class="rail-advertiser-name"><h6><?= $name ?></h6></div>
                                <div class="rail-advertiser-slogan"><?= $slogan ?></div>
                                <?php if( trim($phone) > '' ) { ?>
                                    <div class="rail-advertiser-phone"><?= $phoneFull ?></div>
                                <?php } ?>
                            </a>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="rail-advertisers-actions">
                    <a class="rail-advertisers-action-pagelink" href="/advertisers/"><h6>See All Advertisers</h6></a>
                </div>
            </div>
        </div>
        <?php
    }
}

function advertiserWidget($advertiserId=0)
{
    if($advertiserId==0)
    {
        $sql="SELECT id FROM advertisers ORDER BY RAND() LIMIT 1";
        $dbAdv = dbselectsingle($sql);
        $advertiserId = $dbAdv['data']['id'];
    }
    $advertiser = getSingleAdvertiser($advertiserId);
    if( !empty($advertiser) ) {
        $bizId = $advertiser['id'];
        $slug = $advertiser['slug'];
        $logo = $advertiser['logo'];
        $name = $advertiser['name'];
        $slogan = $advertiser['banner'];
        $website = $advertiser['url'];
        $phone = $advertiser['phone'];
        $phReplace = array('.', '-', '(', ')', ' ');
        $phoneRaw = str_replace($phReplace, '', $phone);
        $phoneLink = 'tel:' . $phoneRaw;
        $phoneDot = substr_replace( substr_replace( $phoneRaw, '.', 3, 0), '.', 7, 0);
        $phoneDash = substr_replace( substr_replace( $phoneRaw, '-', 3, 0), '-', 7, 0);
        $phoneFull = substr_replace( substr_replace( substr_replace( $phoneRaw, ') ', 3, 0), '-', 8, 0), '(', 0, 0);
        ?>
        <div class="rail-post-cust">
            <div class="rail-post-advertiser">
                <div class="rail-biz-info">
                    <div class="rail-biz-info-logo" style="background-image:url(<?= $logo ?>);"></div>
                    <div class="rail-biz-info-name"><?= $name ?></div>
                    <div class="rail-biz-info-slogan"><?= $slogan ?></div>
                    <a class="rail-biz-info-pagelink" href="/advertisers/<?= $slug ?>"><h6>See&nbsp;All&nbsp;Listings</h6></a>
                </div>
                <div class="rail-biz-listings">
                    <?php
                    $advertiserList = getAdvertisersAds($advertiserId,'widget');
                    if( !empty($advertiserList) ) {
                        foreach( $advertiserList as $ad ) {
                            $adId = $ad['id'];
                            $adTitle = $ad['headline'];
                            $adText = $ad['ad_text'];
                            $adStatus = $ad['status'];
                            $adImages = $ad['image'];
                            $hasImg = false;
                            if( trim($adImages) > '' ) {
                                $hasImg = true;
                            }
                            $stripAd = strip_tags( $adText, '<p><a><b><i><u><strike><strong><em><br>' );
                            $adExcerpt = substr( $stripAd, 0, 90 ) . '...';
                            if( $adStatus ) {
                                ?>
                                <a class="rail-biz-listing<?= ( $hasImg ? ' with-image' : '' ) ?>" href="/advertiser-detail/?ad=<?= $adId ?>">
                                    <?php if( $hasImg ) { ?>
                                        <div class="rail-biz-listing-image" style="background-image:url(/uploads/<?= $adImages ?>);"></div>
                                    <?php } ?>
                                    <h6 class="rail-biz-listing-title"><?= $adTitle ?></h6>
                                    <div class="rail-biz-listing-excerpt"><?= $adExcerpt ?></div>
                                </a>
                                <?php
                            }
                        }
                    } else {
                        ?>
                        <div class="posts-wrap no-posts">There are currently no ads running for this advertiser.</div>
                        <?php
                    }
                    ?>
                </div>
                <div class="rail-biz-actions">
                    <a class="rail-biz-action-pagelink" href="/advertisers/<?= $slug ?>"><h6>See&nbsp;All&nbsp;Listings</h6></a>
                    <?php if( trim($website) > '' ) { ?>
                        <a class="rail-biz-action-weblink" href="<?= $website ?>" target="_blank"><h6>Visit Website</h6></a>
                    <?php } ?>
                    <?php if( trim($phone) > '' ) { ?>
                        <a class="rail-biz-action-phone" href="<?= $phoneLink ?>"><h6><?= $phoneFull ?></h6></a>
                    <?php } ?>
                </div>
            </div>
        </div>

    <?php
    }
}

function popularWidget()
{
   $popularList = getPopularAds();
   if( !empty($popularList) ) { ?>
    <div class="rail-posts-popular">
        <div class="rail-pop-info">
            <div class="rail-pop-title">Popular Ads</div>
        </div>
        <div class="rail-pop-listings">
            <?php
            foreach( $popularList as $ad ) {
                $adId = $ad['id'];
                $adTitle = $ad['headline'];
                $adStatus = $ad['status'];
                if( $adStatus ) {
                    ?>
                    <a class="rail-pop-listing" href="/detail/?ad=<?= $adId ?>">
                        <div class="rail-pop-listing-title"><?= $adTitle ?></div>
                    </a>
                    <?php
                }
            }
            ?>
        </div>
    </div>
<?php }
}

function mapWidget()
{
    $sql = "SELECT * FROM maps WHERE active = 1 ORDER BY map_name DESC";
    $dbMaps = dbselectmulti($sql);

    if( !empty($dbMaps) ) { ?>
        <div class="rail-maps-widget">
            <div class="rail-map-info">
                <div class="rail-map-title">Maps</div>
                <div class="rail-map-graphic"></div>
            </div>
            <div class="rail-map-listings">
                <?php
                if( $dbMaps['numrows'] > 0 ) {
                    foreach( $dbMaps['data'] as $map ) {
                        if( $map['active'] ) {
                            ?>
                            <a class="rail-map-listing" href="/maps/?map_id=<?= $map['id'] ?>">
                                <h6 class="rail-map-listing-title"><?= $map['map_name'] ?></h6>
                            </a>
                            <?php
                        }
                    }
                }
                ?>
            </div>
        </div>
    <?php }
}

function featuredWidget()
{
   print "<h4>Featured Widget</h4>";
}
