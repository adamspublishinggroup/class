<?php
require_once("../bootCore.php");

$adID = empty($_POST['adID']) ? 0 : intval($_POST['adID']);

$adID=intval($_SESSION['ad_id']);

//lookup the ad
$sql="SELECT * FROM ads WHERE id=$adID";
$dbAd = dbselectsingle($sql);
$ad = $dbAd['data'];
$status=$ad['status'];
global $config;
$featured = 0;
if($status==2)
{
    //only do this part for ads at status = 2 (new, un-edited ads)
    //if($_POST['print'] || $_POST['print']=='on'){$print=1;}else{$print=0;};
    $print = 1; //always include print
    
    
    
    $startDate=date("Y-m-d",strtotime($_POST['startDate']));
    $endDate=date("Y-m-d",strtotime($_POST['endDate']));

    //calculate number of days online and in print
    //we know publications days of week are in PRINT_DOW
    $daysOnline = dateDifference($startDate,$endDate);
    
    //print days a little more complicated, we need to loop from start to end, checking each date to see if it is in the "DOW" allowed
    $cDate = strtotime($startDate);
    $daysPrint = 0;

    //convert PRINT_DOW to an array
    $printDow = explode(",",PRINT_DOW);
    while($cDate<=strtotime($endDate))
    {
        $dow = date("w",$cDate);
        if(in_array($dow,$printDow))
        {
            $daysPrint++;
            $printDays[]=date("Y-m-d",$cDate);
        }
        $cDate += 3600*24; //add a day                     
    }

    $sql="UPDATE ads SET print=$print, start_date='$startDate', end_date='$endDate', days_online=$daysOnline,  
    days_print=$daysPrint, print_days = '".implode(",",$printDays)."' WHERE id='$adID'";
    $dbUpdate=dbexecutequery($sql);
}

 
 if($status==3)
 {
     //ok, at this point we will set the status back to 1 and redirect back to the account history page
     $sql="UPDATE ads SET status=1, published=1 WHERE id=$adID";
     $dbUpdate=dbexecutequery($sql);
     redirect("/account/history");
 } else {
     redirect("/place-an-ad/place-ad-addons/");
 }
 
