<?php
  require_once('../bootCore.php');
  
  /*
  This feature marks an image as "for print"
  */
  
  $image_id=intval($_POST['image_id']);
  
  $ad_id= intval($_POST['ad_id']);
  
  $placement = addslashes($_POST['placement']);
  
  
  $sql="UPDATE images SET placement='$placement' WHERE ad_id=$ad_id AND id=$image_id";
  $dbUpdate=dbexecutequery($sql);
  
  $hasTop = 0;
  $hasBottom = 0;
  //look for top/bottom
  $sql="SELECT * FROM images WHERE ad_id=$ad_id";
  $dbImages=dbselectmulti($sql);
  foreach($dbImages['data'] as $image)
  {
      if($image['placement']=='top'){$hasTop=1;}
      if($image['placement']=='bottom'){$hasBottom=1;}
  }
  
  echo json_encode(array('status'=>'success', 'hasTop'=>$hasTop, 'hasBottom'=>$hasBottom));