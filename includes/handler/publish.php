<?php
  require_once("../bootCore.php");
  
  $adID=intval($_GET['ad']);
  
  //this will allow a user to toggle their ad published/not-published
  //but we need to make sure the logged in user is the owner of this ad
  
  $sql="SELECT * FROM ads WHERE id=$adID";
  $dbAd = dbselectsingle($sql);
  
  $ad = $dbAd['data'];
  
  if($ad['user_id']==$user['id'])
  {
      //good to go, otherwise, just return to history
      $action = $_GET['action'];
      if($action == 'publish' && ($ad['ad_paid_dt'] || $ad['admin_paid'] || $ad['free_ad'] || $ad['bill_customer'])){
          $sql="UPDATE ads SET publish = 1 WHERE id = $adID";
      } else {
          $sql="UPDATE ads SET publish = 0 WHERE id = $adID";
      }
      $dbUpdate = dbexecutequery($sql);
  }
  redirect("/account/history");