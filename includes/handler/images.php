<?php
require_once("../bootCore.php");
require_once("../../vendor/autoload.php");
use Intervention\Image\ImageManager;
// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'gd'));

$adID = empty($_POST['adID']) ? 0 : intval($_POST['adID']);

//build a path to store the images
//store based on current date
$year = date ("Y");
$month = date ("m");

if(!file_exists("../../uploads/$year"))
{
   mkdir("../../uploads/$year", 0755);
}

if(!file_exists("../../uploads/$year/$month"))
{
   mkdir("../../uploads/$year/$month", 0755);
}
$path = "$year/$month/";
$imageDirectory = "../../uploads/".$path;


$output['ping']='starting';
if(isset($_FILES))
{
    $output['has_files']=1;
    $output['file_count']=count($_FILES);
    //get max id of image for this ad
    $sql="SELECT MAX(id) as photoID from images WHERE ad_id=$adID";
    $dbMax = dbselectsingle($sql);
    $max = $dbMax['data']['photoID'];
    $max++;
    
    //how many files are there for this ad
    $sql="SELECT id FROM images WHERE ad_id=$adID";
    $dbCount = dbselectmulti($sql);
    $count = $dbCount['numrows'];
    $isFirst = 0;
    if( $count == 0 ) {
      $isFirst = 1;
    }
    $output['starting_loop']=1;
    foreach($_FILES as $file) {
        $output['file']=$file['name'];
        //$name=str_replace(array(" ","?","*","'","(",")","{","}",",","&","#","@","!","^","=","\\","/","~","|","\""),"",$adID."-".$file['name']);
        $type = $file['type'];
        switch ($type)
        {
            case 'image/jpeg':
            $ext = 'jpg';
            break;
            
            case 'image/jpg':
            $ext = 'jpg';
            break;
            
            case 'image/png':
            $ext = 'png';
            break;
            
            case 'image/png':
            $ext = 'gif';
            break;
            
        }
        $name = "image_".$adID."_".$max.".".$ext;
        
        switch($file['error']) {
            case 0: // file found
                $output['init']='file is processing';
                if(processFile($file,$imageDirectory,$name) == true) {
                   $success = true;
                   //create the record
                   if($isFirst){$placement = 'top';}else{$placement='';}
                   $sql="INSERT INTO images (ad_id, filename, path, for_print, placement) VALUES ('$adID', '$name', '$path', '$isFirst', '$placement')";
                   $dbInsert=dbinsertquery($sql);
                   if($dbInsert['error']=='')
                   {

                      // load the image
                      $image = $manager->make($imageDirectory.$name);

                      //first, we resize the image to display size (width) and constrain the aspect ratio
                      $image->resize(1000, null, function ($constraint) {
                          $constraint->aspectRatio();
                          $constraint->upsize();
                      });
                      $image->save($imageDirectory.'display_'.$name);
                      //now resize for thumb (height) and constrain the aspect ratio
                      $image->resize(null, 200, function ($constraint) {
                          $constraint->aspectRatio();
                      });
                      $image->save($imageDirectory.'thumb_'.$name);


                      $imageID = $dbInsert['insertid'];
                      $output['status']='success';
                      $output['image_id']=$imageID;
                      $output['count']=$count;
                      $output['filename']=$path.'thumb_'.$name;
                   } else {
                       $output['status']='error';
                       $output['sql']=$sql;
                       $output['sql_error']=$dbInsert['error'];
                   }
                } else {
                   $output['message']='Unable to process the file';
                   $output['status']='error';
                   $success = false;
                }
                
            break;

            default:
                $output['message']='Error with the file itself';
                $output['status']='error';
                $output['error']='Error with the file itself: '.$file['error'];
            break;
        }
        $max++;
    }
    $output['finished_loop']=1;
} else {
    $output['message']='No files uploaded';
    $output['status']='error';
}

echo json_encode($output);
