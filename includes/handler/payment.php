<?php
  /* handles payment */
require_once( "../bootCore.php" );
require ( VENDOR_DIR.'/autoload.php' ); 

use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;

$adID=intval($_SESSION['ad_id']);
$successful = false;
global $config;

//handling things
//http://developer.authorize.net/api/reference/features/acceptjs.html#Obtaining_a_Public_Client_Key

$dt = date("Y-m-d H:i");             
if($_GET['action']=='republishedited')
{
    $adID=intval($_GET['ad_id']);
    $sql="UPDATE ads SET status=1, published = 1 WHERE id=$adID";
    $dbUpdate=dbexecutequery($sql);
    
    redirect("/admin/searchAds.php"); 
}    
if($adID == 0 || $adID == '')
{
    //unable to process a non-id ad
    die('no ad passed');
}

if($_POST['submit']=='Re-publish my ad')
{
    $sql="UPDATE ads SET status=1, published = 1 WHERE id=$adID";
    $dbUpdate=dbexecutequery($sql);
    
    redirect("/account/history/"); 
}
if($_POST['admin_free'])
{
    //admin has approved the ad
    $sql="UPDATE ads SET published = 1, status=1, admin_paid=1, payment_type='admin' WHERE id=$adID";
    $dbUpdate = dbexecutequery($sql);
    redirect("/place-an-ad/place-ad-complete/");
} elseif($_POST['admin_cash']) {
    //admin has approved the ad
    $sql="UPDATE ads SET published = 1, status=1, ad_paid_dt='$dt', ad_paid_amount=ad_total, payment_type='cash'  WHERE id=$adID";
    $dbUpdate = dbexecutequery($sql);
    redirect("/place-an-ad/place-ad-complete/");
} elseif($_POST['admin_check']) {
    //admin has approved the ad
    $sql="UPDATE ads SET published = 1, status=1, ad_paid_dt='$dt', ad_paid_amount=ad_total, payment_type='check', payment_notes='".addslashes($_POST['check_number'])."'  WHERE id=$adID";
    $dbUpdate = dbexecutequery($sql);
    redirect("/place-an-ad/place-ad-complete/");
} else {
    $proceed = false;
    
    $pay = new Payment();
    
    //now we do all the work
    
    if($_SESSION['loggedin'])
    {
        //get the user id
        $token = addslashes($_SESSION['token']);
        $sql="SELECT * FROM users WHERE token='$token'";
        $dbUser = dbselectsingle($sql);
        if($dbUser['numrows']>0)
        {
            $user = $dbUser['data'];
            $userID = $user['id'];
            $email = stripslashes($user['email']);
            $firstName=stripslashes($user['first']);
            $lastName=stripslashes($user['last']);
            $proceed = true;
        } else {
            $error = "Unable to find your user account.";
        }  
    } else {
        /*
        * if user is not logged in, we need to create the new user account first
        */ 
        //create the user
        $userName = addslashes($_POST['username']);
        $firstName = addslashes($_POST['firstname']);
        $lastName = addslashes($_POST['lastname']);
        $email = addslashes($_POST['email']);
        $password = addslashes($_POST['user_password']);
        $street = stripslashes($user['street']);
        $city = stripslashes($user['city']);
        $state = stripslashes($user['state']);
        $zip = stripslashes($user['zip']);
        $phone = stripslashes($user['phone']);
        
        $password=password_hash($password,PASSWORD_DEFAULT);
        $token = generate_random_string(32);
        $createdDT = date("Y-m-d H:i");
        $sql="INSERT INTO users (username, first, last, email, password, token, verified, level, created_dt, street, city, state, zip, phone) VALUES 
        ('$userName', '$firstName', '$lastName', '$email', '$password', '$token', 1, 1, '$createdDT', '$street', '$city', '$state', '$zip', '$phone')";
        $dbInsert=dbinsertquery($sql);
        $userID=$dbInsert['insertid'];
        if($dbInsert['error']=='')
        {
            addUserAction($userID,'register');
            
            $emailData = array('name'=>"$firstName $lastName",
              'first'=>$firstName,
              'last'=>$lastName,
              'token'=>$token,
              'email'=>$email);
              
            $emailContent = email_generator(3,$emailData);
          
            $subject = $emailContent['subject'];
            $message = $emailContent['body'];
          
          
            //send confirmation email to user
            send_email($email,$subject,$message);
            
            
            //admin also gets user signup email
            $emailData = array('name'=>"$firstName $lastName",
              'first'=>$firstName,
              'last'=>$lastName,
              'email'=>$email);
              
            $emailContent = email_generator(9,$emailData);
          
            $subject = $emailContent['subject'];
            $message = $emailContent['body'];
            //send new user email to admin
            send_email(CONTACT_EMAIL,$subject,$message);
            
            
            $createdUser = true;
            $proceed = true;
        } else {
            $error = "Unable to create new user account.";
        } 
    }
    
    
    if($proceed)
    {
        $needsApproval = false;
        if($config['require_approval'])
        {
            //if the user is "trusted" then set to 1, otherwise it goes into pending
            if($user['trusted'])
            {
                $finalStatus = 1;
            } else {
                $needsApproval = true;
                $finalStatus = 6;
            }
        } else {
            $finalStatus = 1;
        }
        
        $sql="SELECT * FROM ads WHERE id=$adID";
        $dbAd = dbselectsingle($sql);
        $ad = $dbAd['data'];
            
        if($_POST['bill_me'])
        {
            //update ad order to show that a bill should be sent to the customer (email and/or mail)
            //then set the ad to publish 
            //see if there is a bill_me_fee, if so, increment the total amount due by that amount
            $billFee = $config['bill_me_fee'];
            
            $sql="UPDATE ads SET bill_customer = 1, published = 1, status = $finalStatus, ad_total=ad_total+$billFee WHERE id=$adID";
            $dbUpdate=dbexecutequery($sql); 
            $successful = true; 
        } elseif($_POST['bill_advertiser'])
        {
            //update ad order to show that a bill should be sent to the customer (email and/or mail)
            //then set the ad to publish 
            $billFee = $config['bill_me_fee'];
            $sql="UPDATE ads SET bill_advertiser = 1, published = 1, status = $finalStatus, ad_total=ad_total+$billFee WHERE id=$adID";
            $dbUpdate=dbexecutequery($sql); 
            $successful = true; 
        } else {
            if($config['billing_interface']=='Stripe') {
            
                if(MODE=='testing')
                {
                    $card['card'] = '4242424242424242';
                    $card['expiremonth'] = '12';
                    $card['expireyear'] = '2017';
                    $card['cvv'] = '123';
                    $card['firstName'] = 'Joe';
                    $card['lastName'] = 'Hansen';
                    $card['email'] = 'jhansen@pioneernewsgroup.com';
                    $email = 'jhansen@pioneernewsgroup.com';
                    $proceed=true;
                } else {    
                    $card['firstName'] = $firstName;
                    $card['lastName'] = $lastName;
                    $card['email'] = $email;
                    $card['expiremonth'] = $_POST['expireMonth'];
                    $card['expireyear'] = $_POST['expireYear'];
                    $card['cvv'] = $_POST['cvv'];
                        
                }
            
                $name = $_POST['cardHolderName'];
                $last4 = substr($_POST['cardNumber'],strlen($_POST['cardNumber'])-4);
                $dt = date("Y-m-d H:i:s");
                $adTotal = $ad['ad_total'];
                $check = $pay->setcard($card);
                if($check === true){
                    $amount['amount'] = $adTotal;
                    $amount['currency'] = "USD";
                    $amount['description'] = "Charge for ad number: $adID";
                    $response = $pay->makepayment($amount);
                    if($response['status'] === true )
                    {
                        $transaction_id=$response['transaction_id'];
                        $sql="UPDATE ads SET published = 1, status = $finalStatus, ad_paid_amount='$adTotal', ad_paid_dt = '$dt', card_last_4 = '$last4', transaction_id='$transaction_id' WHERE id=$adID";
                        $dbUpdate=dbexecutequery($sql);
                        $successful = true;
                    } else {
                        $error = $response;
                    }
                } else {
                    $error = $check;
                }
                
            } elseif($config['billing_interface']=='Authorize')
            {
                
            }
        }
        if($successful)
        {
            
            
            
            $emailContent = email_generator(1,$emailData);
            
            $subject = $emailContent['subject'];
            $message = $emailContent['body'];
            //send confirmation email to user
            send_email($email,$subject,$message);
            
            $adDisplay = "<p><b>Headline: </b>".$ad['headline']."</p>";
            $adDisplay = "<p><b>Print Text: </b>".$ad['ad_text']."</p>";
            $adDisplay = "<p><b>Online Text: </b>".$ad['online_text']."</p>";
            $adDisplay = "<p><b>Start Date: </b>".date("m/d/Y",strtotime($ad['start_date']))."</p>";
            $adDisplay = "<p><b>End Date: </b>".date("m/d/Y",strtotime($ad['end_date']))."</p>";
            $sql="SELECT * FROM images WHERE ad_id=$adID";
            $dbImages = dbselectmulti($sql);
            if($dbImages['numrows']>0)
            {
                $adDisplay.="<p><b>The following image(s) were attached to the ad:</b></p>";
                foreach($dbImages['data'] as $image)
                {
                    $adDisplay.="<p><img src='".SITE_URL."/uploads/".$image['path'].$image['filename']."' width=200 /></p>\n";
                }
            }
            //send the admins an email when a new ad has been successfully created
            if($config['send_new_ad_alert'] && $needsApproval)
            {
                $emailData = array('ad'=>$adDisplay);
                $emailContent = email_generator(7,$emailData);
                
                $subject = $emailContent['subject'];
                $message = $emailContent['body'];
                //send confirmation email to user
                send_email(CONTACT_EMAIL,$subject,$message);
                    
            } elseif($config['send_new_ad_alert'])
            {
                $emailData = array('ad'=>$adDisplay);
                $emailData = array('id');
                $emailContent = email_generator(8,$emailData);
                
                $subject = $emailContent['subject'];
                $message = $emailContent['body'];
                //send confirmation email to user
                send_email(CONTACT_EMAIL,$subject,$message);
                
            }
            
            //are we posting the ad to facebook?
            if(MODE=='testing')
            {
                //we weould be post this to facebook now
                print "We would be posting to facebook now...<br>And then redirecting to the place-ad-complete page.";
            } else {
                if(!$needsApproval)
                {
                    //see if we have a facebook or twitter addon
                    $sql="SELECT * FROM ad_addons WHERE ad_id=$adID";
                    $dbAddons=dbselectmulti($sql);
                    if($dbAddons['numrows']>0)
                    {
                        foreach($dbAddons['data'] as $addon)
                        {
                            if($addon['id']==$config['facebook_add_on'])
                            {
                                $link = SITE_URL."/detail/?ad=$adID";
                                $message="<b>".$ad['headline']."</b> ".$ad['ad_text']."&nbsp;&nbsp;&nbsp;<a href='$link'>Read the full ad</a>";
                                postToFacebook($link,$message);
                            }elseif($addon['id']==$config['twitter_add_on'])
                            {
                                $link = SITE_URL."/detail/?ad=$adID";
                                $message="<b>".$ad['headline']."</b>&nbsp;&nbsp;&nbsp;<a href='$link'>Read the full ad</a>";
                                postToTwitter($link,$message);
                            }
                        }
                    }
                }
            }
            redirect("/place-an-ad/place-ad-complete/");
        } else {
            $sql="UPDATE ads SET last_error = '$error' WHERE id=$adID";
            $dbUpdate=dbexecutequery($sql);
            redirect("/place-an-ad/place-ad-checkout/");
        } 
    } else {
        $sql="UPDATE ads SET last_error = '$error' WHERE id=$adID";
        $dbUpdate=dbexecutequery($sql);
        redirect("/place-an-ad/place-ad-checkout/");
    } 
    
} 

class Payment
{
    private $pay;
    private $card;
    function setcard($value){
 
        global $config;
        try{
            
            $card = [
                'firstName'    => $value['firstName'],
                'lastName'     => $value['lastName'],
                'number' => $value['card'],
                'expiryMonth' => $value['expiremonth'],
                'expiryYear' => $value['expireyear'],
                'cvv' => $value['cvv'],
                'email'    => $value['email'],
              
            ];
            $ccard = new CreditCard($card);
            $ccard->validate();
            $this->card = $card;
            return true;
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
 
    }
 
    function makepayment($value){
        global $config;
        try{
           // Setup payment Gateway
            $pay = Omnipay::create('Stripe');
            $pay->setApiKey(STRIPE_KEY);
            // Send purchase request
            $response = $pay->purchase(
                [
                    'amount' => $value['amount'],
                    'currency' => $value['currency'],
                    'description' => $value['description'],
                    'card' => $this->card
                ]
            )->send();
            
            // Process response
            if ($response->isSuccessful()) {
 
                $transaction_id = $response->getTransactionReference();
                return array('status'=>true,'transaction_id'=>$transaction_id);
 
            } elseif ($response->isRedirect()) {
 
                // Redirect to offsite payment gateway
                return $response->getMessage();
 
            } else {
               // Payment failed
               return $response->getMessage();
            }
        }
        catch(\Exception $ex){
            return $ex->getMessage();
        }
    }
}