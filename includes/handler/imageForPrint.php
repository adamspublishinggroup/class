<?php
  require_once('../bootCore.php');
  
  /*
  This feature marks an image as "for print"
  */
  
  $image_id=intval($_POST['image_id']);
  
  $ad_id= intval($_POST['ad_id']);
  
  //allow for toggling, let's see what it was before
  $sql="SELECT for_print FROM images WHERE ad_id=$ad_id AND id=$image_id";
  $dbCheck = dbselectsingle($sql);
  
  if($dbCheck['data']['for_print']==1)
  {
      $for_print = 0;
  } else {
      $for_print = 1;
  }
  
  $sql="UPDATE images SET for_print=$for_print WHERE ad_id=$ad_id AND id=$image_id";
  $dbUpdate=dbexecutequery($sql);
  
  echo json_encode(array('status'=>'success','for_print'=>$for_print));