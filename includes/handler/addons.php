<?php
require_once("../bootCore.php");

$adID = empty($_POST['adID']) ? 0 : intval($_POST['adID']);

$adID=intval($_SESSION['ad_id']);

//lookup the ad
$sql="SELECT * FROM ads WHERE id=$adID";
$dbAd = dbselectsingle($sql);
$ad = $dbAd['data'];
$status=$ad['status'];
global $config;
$featured = 0;
if($status==2)
{
    //only do this part for ads at status = 2 (new, un-edited ads)
    //if($_POST['print'] || $_POST['print']=='on'){$print=1;}else{$print=0;};
    $print = 1; //always include print
    
    
    //handle the "addons"
    //clear all current addons for the ad
    $sql="DELETE FROM ad_addons WHERE ad_id=$adID";
    $dbDelete = dbexecutequery($sql);
    $addons = array();
    foreach($_POST as $key=>$value)
    {
        if(stripos($key,"addon")>0)
        {
            $addonID = str_replace("_addon_","",$key);
            $addons[]="($adID,$addonID)";
            if($addonID == $config['featured_ad_on'])
            {
                $featured = 1;
            }    
        }
    }
    if(!empty($addons))
    {
        $sql = "INSERT INTO ad_addons (ad_id, addon_id) VALUES ".implode(",",$addons);
        $dbInsert = dbinsertquery($sql);
    }
    
    
    $sql="UPDATE ads SET print=$print, featured = $featured WHERE id='$adID'";
    $dbUpdate=dbexecutequery($sql);
}
$sql="SELECT DISTINCT(A.id), A.meta_name, A.meta_type, A.required FROM 
 metadata AS A, ad_category_xref B, meta_category_xref C WHERE B.ad_id=$adID AND B.category_id=C.category_id AND A.id=C.meta_id";
 $dbMetaTags=dbselectmulti($sql);
 
 if($dbMetaTags['numrows']>0)
 {
     
     foreach($dbMetaTags['data'] as $metaTag)
     {
         switch($metaTag['meta_type'])
         {
             case 'select': $updates[]=save_meta_select($metaTag,$adID); break;
             case 'text':  $updates[]=save_meta_text($metaTag,$adID); break;
             case 'textblock':  $updates[]=save_meta_textblock($metaTag,$adID); break;
             case 'number':  $updates[]=save_meta_number($metaTag,$adID); break;
             case 'checkbox':  $updates[]=save_meta_checkbox($metaTag,$adID); break;
             case 'telephone':  $updates[]=save_meta_telephone($metaTag,$adID); break;
             case 'address':  $updates[]=save_meta_address($metaTag,$adID); break;
             case 'date':  $updates[]=save_meta_date($metaTag,$adID); break;
             
         }
     }
     
     if(count($updates)>0)
     {
         //clear all existing ad meta
         $sql="DELETE FROM ad_meta WHERE ad_id=$adID";
         $dbDelete=dbexecutequery($sql);
         
         //add new meta data
         $sql="INSERT INTO ad_meta (ad_id, meta_id, meta_value_short, meta_value_long) VALUES ".implode(",",$updates);
         $dbInsert=dbinsertquery($sql);
     }
 } 
 
 
 if($status==3)
 {
     //ok, at this point we will set the status back to 1 and redirect back to the account history page
     $sql="UPDATE ads SET status=1, published=1 WHERE id=$adID";
     $dbUpdate=dbexecutequery($sql);
     redirect("/account/history");
 } else {
     redirect("/place-an-ad/place-ad-preview/");
 }
 
 function save_meta_select($metaTag,$adID)
 {
     $val = addslashes($_POST['meta_'.$metaTag['id']]);
     return "($adID,$metaTag[id],'$val','')"; 
 } 
 
 function save_meta_text($metaTag,$adID)
 {
     $val = addslashes($_POST['meta_'.$metaTag['id']]);
     return "($adID,$metaTag[id],'$val','')";
 }  
 
 
 function save_meta_textblock($metaTag,$adID)
 {
     $val = addslashes($_POST['meta_'.$metaTag['id']]);
     return "($adID,$metaTag[id],'','$val')";
 }
 
 function save_meta_number($metaTag,$adID)
 {
     $val = addslashes($_POST['meta_'.$metaTag['id']]);
     return "($adID,$metaTag[id],'$val','')";
 }
 
 function save_meta_checkbox($metaTag,$adID)
 {
     if($_POST['meta_'.$metaTag['id']])
     {
         $val=1;
     } else {
         $val=0;
     }
     return "($adID,$metaTag[id],'$val','')";
 }
 
 function save_meta_telephone($metaTag,$adID)
 {
     $val = addslashes($_POST['meta_'.$metaTag['id']]);
     return "($adID,$metaTag[id],'$val','')";
 }
 
 function save_meta_address($metaTag,$adID)
 {
     $street=$_POST['meta_'.$metaTag['id'].'_street']; 
     $street2=$_POST['meta_'.$metaTag['id'].'_street2']; 
     $city=$_POST['meta_'.$metaTag['id'].'_city']; 
     $state=$_POST['meta_'.$metaTag['id'].'_state']; 
     $zip=$_POST['meta_'.$metaTag['id'].'_zip'];
     $val=addslashes($street."|".$street2."|".$city."|".$state."|".$zip);
     
     //update the ad record with the lat/ln
     $geo=geocode($street.",".$city." ".$state." ".$zip);
     $lat=$geo['lat'];
     $lon=$geo['lon'];
     
     $sql="UPDATE ads SET lat='$lat', lon='$lon' WHERE id=$adID";
     $dbUpdate=dbexecutequery($sql);
     
     return "($adID,$metaTag[id],'','$val')";
 }
 
 function save_meta_date($metaTag,$adID)
 {
     $val = date("Y-m-d",strtotime($_POST['meta_'.$metaTag['id']]));
     return "($adID,$metaTag[id],'$val','')";
 }

 
