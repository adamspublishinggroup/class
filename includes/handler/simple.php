<?php
 require_once("../bootCore.php");
 require_once("../../vendor/autoload.php");
 use Intervention\Image\ImageManager;
 // create an image manager instance with favored driver
 $manager = new ImageManager(array('driver' => 'gd'));

 //dim variables
 $userToken='';
 
 $headline = addslashes($_POST['ad_headline']);
 $text = addslashes($_POST['ad_text']);
 $iText = addslashes($_POST['internet_text']);
 if ($config['include_headline_adcount']) {
    $wordCount = count(explode(" ",strip_tags($text))) + count(explode(" ",strip_tags($headline)));
 } else {
    $wordCount = count(explode(" ",strip_tags($text))); 
 }
 $keywords = addslashes($_POST['keywords']);
 $adGeo = addslashes($_POST['ad_geo']);
 $cats = $_POST['ad_categories'];
 $adID = intval($_POST['ad_id']);
 $packageID = intval($_POST['package_id']);
 $userToken =$_SESSION['token'];
 $startDate = $_POST['start_date'];
 $numWeeks = $_POST['end_date']-1;
 
 //calc end date from start date + num weeks
 $endDate = date("Y-m-d",strtotime($startDate."+$numWeeks weeks"));
 
 //need to figure out the print dates now from start to end.
 $printDays[]=$startDate;
 $daysPrint = 1;
 for($i=1;$i<=$numWeeks;$i++)
 {
     $printDays[]=date("Y-m-d",strtotime($startDate."+$i weeks"));
     $daysPrint++;
 }
 $printDates = implode(",",$printDays);
 
 
 $boldCount = 0;
 $italicCount = 0;
 $underlineCount = 0;
 
 $wordCountCop = true;
 $allowedWordLength = 15;
 $textLength = strlen($text);
 if( ( $wordCount * $allowedWordLength ) / $textLength >= 1 ) {
    $wordCountCop = false;
 }
 
 if($_POST['heading']=='none')
 {
     $featureHeadline = 0;
 } else {
     $featureHeadline = 1;
 }
 
 $createdDT = date("Y-m-d H:i");
 
 if ($config['rich_ad_text']) { 
     $re = '/<b>(.|\n)*?<\/b>/i';
     preg_match_all($re, $text, $bolds, PREG_SET_ORDER, 0);
     $boldCount = 0;
     if(count($bolds)>0)
     {
        foreach($bolds as $b)
        {
            $test=trim(strip_tags($b[0]));
            //print "b is $test<br>";
            $boldCount+=count(explode(" ",$test));
        } 
     }
     
     $re = '/<u>(.|\n)*?<\/u>/i';
     preg_match_all($re, $text, $underlines, PREG_SET_ORDER, 0);
     $underlineCount = 0;
     if(count($underlines)>0)
     {
        foreach($underlines as $u)
        {
            $test=trim(strip_tags($u[0]));
            //print "u is $test<br>";
            $underlineCount+=count(explode(" ",$test));
        } 
     }
     
     $re = '/<i>(.|\n)*?<\/i>/i';
     preg_match_all($re, $text, $italics, PREG_SET_ORDER, 0);
     $italicCount = 0;
     if(count($italics)>0)
     {
        foreach($italics as $i)
        {
            $test=trim(strip_tags($i[0]));
            //print "i is $test<br>";
            $italicCount+=count(explode(" ",$test));
        } 
     }
     
 } elseif($_POST['feature_headline']) {
     $boldCount = count(explode(" ",strip_tags($headline)));
 }
 
 if($adID==0)
 {
     //creating a new ad
     //if the userToken is blank, that means we have a user who is not logged in.
     if($_SESSION['admin'] || $_POST['admin_edit'])
     {
         
         $userID = $_POST['user_id'];
         $wordCountCop = false;
         $admin =1;
     } else {
         $admin =0;
         if($userToken=='')
         {
             //in this case we need to create a temporary user account, which will be updated at the end of the ad setup process.
             $userToken = generate_random_string(32); 
             $sql="INSERT INTO users (first, last, username, token, verified, level, temporary, created_dt) 
             VALUES ('TEMP','ACCOUNT','".uniqid ("PNG")."', '$userToken', 0, 1, 1, '$createdDT')";
             $dbUser=dbinsertquery($sql);
             $userID=$dbUser['insertid'];
             $_SESSION['token']=$userToken;
             
             addUserAction($userID, 'created temp account'); 
         } else {
             //look up user ID from token
             $sql="SELECT * FROM users WHERE token='$userToken'";
             $dbUser=dbselectsingle($sql);
             $user=$dbUser['data'];
             $userID = $user['id'];
         }
     }
     if( $wordCountCop ) {
        $_SESSION['error']="No cheating. Looks like you're using some mighty long words there.<br>Please refrain from stringing words together.";
        redirect("/place-an-ad/place-ad-basics/"); 
     } else {
        $sql="INSERT INTO ads (user_id, headline, feature_headline, ad_text, internet_text, keywords, status, created_dt, word_count, bold_count, underline_count, italic_count, published, admin_placed, package_id, ad_geo, start_date, end_date, print_days, days_print) VALUES 
            ('$userID', '$headline', '$featureHeadline','$text', '$iText', '$keywords', 2, '$createdDT', '$wordCount', '$boldCount','$underlineCount','$italicCount', 0,$admin, $packageID, '$adGeo', '$startDate', '$endDate', '$printDates', '$daysPrint')";
         $dbAd = dbinsertquery($sql);
         if($dbAd['error']=='')
         {
             $adID=$dbAd['insertid'];
             //create the ad_category_xref records
             if(count($cats)>0)
             {
                 $catInserts=array();
                 foreach($cats as $cat)
                 {
                     $catInserts[]="($adID,$cat)";
                 }
                 $sql="INSERT INTO ad_category_xref (ad_id, category_id) VALUES ".implode(",",$catInserts);
                 $dbInsert=dbinsertquery($sql);
             }
             
             
             $_SESSION['ad_id']=$adID;
             addUserAction($userID, "created new ad",$adID);
             redirect("/place-an-ad/place-ad-images/");
         } else {
             $_SESSION['error']="There was a problem creating the new ad.";
             redirect("/place-an-ad/place-ad-basics/"); 
         }
     }
     
 } else {
     $sql="UPDATE ads SET start_date='$startDate', end_date='$endDate', headline = '$headline', feature_headline = '$featureHeadline', ad_text = '$text', internet_text='$iText', updated_dt='$createdDT', keywords = '$keywords', word_count='$wordCount', bold_count='$boldCount', italic_count='$italicCount', underline_count='$underlineCount', ad_geo='$adGeo', 
     days_print='$daysPrint', print_days='$printDates'  WHERE id=$adID";
     $dbUpdate=dbexecutequery($sql); 
     addUserAction($userID, "updated existing ad",$adID); 
 }
 
 
 //handle any images uploaded
 if(count($_FILES)>0)
 {
     
    //build a path to store the images
    //store based on current date
    $year = date ("Y");
    $month = date ("m");

    if(!file_exists("../../uploads/$year"))
    {
       mkdir("../../uploads/$year", 0755);
    }

    if(!file_exists("../../uploads/$year/$month"))
    {
       mkdir("../../uploads/$year/$month", 0755);
    }
    $path = "$year/$month/";
    $imageDirectory = "../../uploads/".$path;


    $max = 1;     
     foreach($_FILES as $file)
     {
         $output['file']=$file['name'];
        //$name=str_replace(array(" ","?","*","'","(",")","{","}",",","&","#","@","!","^","=","\\","/","~","|","\""),"",$adID."-".$file['name']);
        $type = $file['type'];
        switch ($type)
        {
            case 'image/jpeg':
            $ext = 'jpg';
            break;
            
            case 'image/jpg':
            $ext = 'jpg';
            break;
            
            case 'image/png':
            $ext = 'png';
            break;
            
            case 'image/png':
            $ext = 'gif';
            break;
            
        }
        $name = "image_".$adID."_".$max.".".$ext;
        
        switch($file['error']) {
            case 0: // file found
                $output['init']='file is processing';
                if(processFile($file,$imageDirectory,$name) == true) {
                   $success = true;
                   //create the record
                   if($isFirst){$placement = 'top';}else{$placement='';}
                   //we only every want one image for the simple ad system, so let's delete any others
                   $sql="DELETE FROM images WHERE ad_id=$adID";
                   $dbDelete=dbexecutequery($sql);
                   
                   $sql="INSERT INTO images (ad_id, filename, path, for_print, placement) VALUES ('$adID', '$name', '$path', '1', '$placement')";
                   $dbInsert=dbinsertquery($sql);
                   if($dbInsert['error']=='')
                   {

                      // load the image
                      $image = $manager->make($imageDirectory.$name);

                      //first, we resize the image to display size (width) and constrain the aspect ratio
                      $image->resize(1000, null, function ($constraint) {
                          $constraint->aspectRatio();
                          $constraint->upsize();
                      });
                      $image->save($imageDirectory.'display_'.$name);
                      //now resize for thumb (height) and constrain the aspect ratio
                      $image->resize(null, 200, function ($constraint) {
                          $constraint->aspectRatio();
                      });
                      $image->save($imageDirectory.'thumb_'.$name);


                      $imageID = $dbInsert['insertid'];
                      $output['status']='success';
                      $output['image_id']=$imageID;
                      $output['count']=$count;
                      $output['filename']=$path.'thumb_'.$name;
                   } else {
                       $output['status']='error';
                       $output['sql']=$sql;
                       $output['sql_error']=$dbInsert['error'];
                   }
                } else {
                   $output['message']='Unable to process the file';
                   $output['status']='error';
                   $success = false;
                }
                
            break;

            default:
                $output['message']='Error with the file itself';
                $output['status']='error';
                $output['error']='Error with the file itself: '.$file['error'];
            break;
        }
        $max++;
    }
 }
 
 //get all "free ads" categories
 $freeCats[] = array();
 $sql="SELECT id FROM categories WHERE free_ads = 1";
 $dbFree = dbselectmulti($sql);
 if($dbFree['numrows']>0)
 {
     foreach($dbFree['data'] as $free)
     {
         $freeCats[]=$free['id'];
     }
 }
 //update existing cats
 //delete first
 $sql="DELETE FROM ad_category_xref WHERE ad_id=$adID";
 $dbDelete=dbexecutequery($sql);
 //create the ad_category_xref records
 if(count($cats)>0)
 {
     $catInserts=array();
     foreach($cats as $cat)
     {
         if(in_array($cat,$freeCats))
         {
             $catInserts = array();
             $catInserts[]="($adID,$cat)";
             break;
         }
         $catInserts[]="($adID,$cat)";
     }
     $sql="INSERT INTO ad_category_xref (ad_id, category_id) VALUES ".implode(",",$catInserts);
     $dbInsert=dbinsertquery($sql);
 }
 
 redirect("/place-an-ad/place-ad-summary/");
 