<?php

function articleBrief($article)
{
	$adText = $article['content'];
	$wordCnt = 250;
	$stripAd = strip_tags( $adText, '<p><b><i><u><strike><strong><em><br>' );
	$excerpt = substr( $stripAd, 0, $wordCnt ) . '...';
	$mainImg = false;
	if( !empty($article['images']) ) {
		$mainImg = '<div class="classAdImg" style="background-image:url(' . $article['images'][0]['thumb'] . ');"></div>';
	}
	$echo = '';
	$echo .= '<div class="classPost">';
	$echo .= '<div class="adMainWrap' . ( $mainImg ? ' withImg' : '' ) . '">';
	$echo .= $mainImg;
	$echo .= '<h5><a href="' . $article['url'] . '">' . stripslashes($article['headline']) . '</a></h5>';
	$echo .= '<div class="classPostText">';
	$echo .= $excerpt;
	$echo .= ' <a class="classPostMore" href="' . $article['url'] . '">Read&nbsp;More</a>';
	$echo .= '</div>';
	$echo .= '</div>';
	$echo .= '</div>';
	echo $echo;
}

function articleFull($article)
{
	$imgs = $article['images'];
	$echo = '';
	$echo .= '<div class="post-meta">';
	$echo .= '<div class="post-meta-cats">Article category: ' . $article['category'] . '</div>';
	$echo .= '</div>';
	if( !empty( $imgs ) ) {
		$echo .= '<div class="post-images">';
		$echo .= '<div class="slider_for">';
		foreach( $imgs as $item ) {
			$echo .= '<div class="slide_img_wrap">';
			$echo .= '<a class="slide_img adDetailSlide" href="' . $item['display'] . '" style="background-image:url(' . $item['display'] . ');"></a>';
			$echo .= '</div>';
		}
		$echo .= '</div>';
		if( count( $imgs ) > 1 ) {
			$echo .= '<div class="slider_nav">';
			foreach( $imgs as $item ) {
				$echo .= '<div class="thumb_img_wrap">';
				$echo .= '<div class="thumb_img" style="background-image:url(' . $item['thumb'] . ');"></div>';
				$echo .= '</div>';
			}
			$echo .= '</div>';
		}
		$echo .= '</div>';
	}
	$echo .= '<div class="post-text">' . stripslashes($article['content']) . '</div>';
	echo $echo;
}

function getArticle($slug)
{
    $slug = addslashes($slug);
    $sql="SELECT A.*, B.category FROM articles A, article_categories B WHERE A.slug='$slug' AND A.category_id=B.id";
    $dbArticle = dbselectsingle($sql);
    if($dbArticle['numrows']>0)
    {
        $article = $dbArticle['data'];
        $article['images']=getArticleImages($article['id']);
        return $article;
    } else {
        return array();
    }
}

function getArticles($categoryID=0)
{
    if(isset($_GET['p']))
    {
        $offset = 10*intval($_GET['p'])-10;
    } else {
        $offset = 0;
    }

    if($categoryID!=0)
    {
        $sql="SELECT A.*, B.category, B.slug as catSlug FROM articles A, article_categories B WHERE A.category_id=B.id AND A.category_id=$categoryID AND A. published=1 ORDER BY publish_dt LIMIT 10 OFFSET $offset";
    } else {
        $sql="SELECT A.*, B.category, B.slug as catSlug FROM articles A, article_categories B WHERE A.category_id=B.id AND published=1 ORDER BY publish_dt LIMIT 10 OFFSET $offset";
    }
    $dbArticles=dbselectmulti($sql);
    if($dbArticles['numrows']>0)
    {
        foreach($dbArticles['data'] as &$article)
        {
            $article['url']=stripslashes("/articles/".$article['catSlug']."/".$article['slug']);
            $article['images']=getArticleImages($article['id']);
        }
    }
    return $dbArticles['data'];
}

function articlePaginator()
{
	global $totalArticleCount;
	$currentURL = explode("?",$_SERVER['REQUEST_URI']);
	$currentURL = $currentURL[0];
	$page = intval($_GET['p']);
	if( $page == 0 ) { $page = 1; }

	//get total pages in ads record
	$pages = ceil( $totalArticleCount / 10 );

	if( $page > 1 ) { $prev = $page - 1; } else { $prev = 1; }
	if( $page < $pages ) { $next = $page + 1; } else { $next = $pages; }

	$prevURL = $currentURL . '?p=' . $prev;
	$nextURL = $currentURL . '?p=' . $next;
	$firstURL = $currentURL . '?p=' . '1';
	$lastURL = $currentURL . '?p=' . $pages;
	?>
	<nav aria-label="Page navigation" style="text-align:center;">
		<ul class="pagination">
			<?php
			if( $pages > 1 ) {
				// display always (this is a first-page link)
				echo '<li><a href="' . $firstURL . '" aria-label="First Page">First</a></li>' . "\n";
			}

			if( $pages > 5 ) {
				//display if there are more than 5 pages total (this is a previous-page link)
				echo '<li><a href="' . $prevURL . '" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>' . "\n";
			}

			if( $pages > 2 ) {
				// dont bother with individual pages if there are only 2
				if( $pages <= 5 ) {
					// display all pages up to 5 pages
					for( $i = 1; $i <= $pages; $i++ ) {
						$url = $currentURL . '?p=' . $i;
						echo '<li' . ( $i == $page ? ' class="active"' : '' ) . '><a href="' . $url . '">' . $i . '</a></li>' . "\n";
					}
				} else {
					// more than 5 pages gets special handling here
					$pageArr = array();
					$currPage  = $page;
					$forBefore = $page - 4;
					$treBefore = $page - 3;
					$twoBefore = $page - 2;
					$oneBefore = $page - 1;
					$oneAfter  = $page + 1;
					$twoAfter  = $page + 2;
					$treAfter  = $page + 3;
					$forAfter  = $page + 4;

					if( $forBefore >= 1 ) {
						// if false, then we know we are at the beginning
						// if true we could be anywhere in the list, but we'll start at the end
						$pageArr[0] = $forBefore;
						$pageArr[1] = $treBefore;
						$pageArr[2] = $twoBefore;
						$pageArr[3] = $oneBefore;
						$pageArr[4] = $page;
						if( $twoAfter <= $pages ) {
							// we don't care about more than 2 after, since that's only a facor at the beginning of the list
							$pageArr[0] = $twoBefore;
							$pageArr[1] = $oneBefore;
							$pageArr[2] = $page;
							$pageArr[3] = $oneAfter;
							$pageArr[4] = $twoAfter;
						} else if ( $oneAfter <= $pages ) {
							$pageArr[0] = $treBefore;
							$pageArr[1] = $twoBefore;
							$pageArr[2] = $oneBefore;
							$pageArr[3] = $page;
							$pageArr[4] = $oneAfter;
						}
					} elseif( $treBefore >= 1 ) {
						$pageArr[0] = $twoBefore;
						$pageArr[1] = $oneBefore;
						$pageArr[2] = $page;
						$pageArr[3] = $oneAfter;
						$pageArr[4] = $twoAfter;
					} elseif( $twoBefore >= 1 ) {
						$pageArr[0] = $twoBefore;
						$pageArr[1] = $oneBefore;
						$pageArr[2] = $page;
						$pageArr[3] = $oneAfter;
						$pageArr[4] = $twoAfter;
					} elseif( $oneBefore >= 1 ) {
						$pageArr[0] = $oneBefore;
						$pageArr[1] = $page;
						$pageArr[2] = $oneAfter;
						$pageArr[3] = $twoAfter;
						$pageArr[4] = $treAfter;
					} else {
						$pageArr[0] = $page;
						$pageArr[1] = $oneAfter;
						$pageArr[2] = $twoAfter;
						$pageArr[3] = $treAfter;
						$pageArr[4] = $forAfter;
					}

					// build list
					foreach( $pageArr as $item ) {
						$url = $currentURL . '?p=' . $item;
						echo '<li' . ( $item == $page ? ' class="active"' : '' ) . '><a href="' . $url . '">' . $item . '</a></li>' . "\n";
					}
				}
			}

			if( $pages > 5 ) {
				//display if there are more than 5 pages total (this is a next-page link)
				echo '<li><a href="' . $nextURL . '" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>' . "\n";
			}

			if( $pages > 1 ) {
				// display always (this is a last-page link)
				echo '<li><a href="' . $lastURL . '" aria-label="Last Page">Last</a></li>' . "\n";
			}
			?>
		</ul>
	</nav>
	<?php
}

function getArticleImages($articleID)
{
    $images = array();
    $sql = "SELECT * FROM article_images WHERE article_id = $articleID";
    $dbImages = dbselectmulti($sql);
    if( $dbImages['numrows'] > 0 ) {
        $i = 0;
        foreach( $dbImages['data'] as $img ) {
            $images[$i]['display']="/uploads/articles/display_".stripslashes($img['filename']);
            $images[$i]['thumb']="/uploads/articles/thumb_".stripslashes($img['filename']);
            $i++;
        }
    }
    return $images;
}
