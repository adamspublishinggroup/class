<?php
    // get packages
    $sql="SELECT * FROM packages ORDER BY weight ASC";
    $dbPackages=dbselectmulti($sql);

    //get the information to display here from the database
    $sql="SELECT * FROM ad_blurb WHERE id=1";
    $dbBlurb = dbselectsingle($sql);
    $blurb = $dbBlurb['data'];
    $headline = stripslashes($blurb['headline']);
    $content = stripslashes($blurb['content']);

    //see if this user has any "Open" ads
    $sql="SELECT * FROM ads WHERE user_id=$user[id] AND published=0 AND status=2 ORDER BY created_dt DESC";
    $dbAds = dbselectmulti($sql);
?>

<h3><?= $headline ?></h3>

<?php if( !empty($dbPackages) && $dbPackages['numrows'] > 0 ) { ?>
    <div id="packages">
        <?php 
        $packageHTML = '';
        foreach( $dbPackages['data'] as $package ) {
            if( $package['active'] == 1 ) {
                $packageHTML .= '<div id="package_' . $package['id'] . '" class="package '.($package['default']? 'selectedPackage' : 'unSelected').'" data-id="' . $package['id'] . '">';
                if($package['default']){$defaultPackageID=$package['id'];}
                if($package['package_graphic']!='')
                {
                    $packageHTML .= '<div class="package-graphic-wrap">';
                    $packageHTML .= '<div class="package-graphic" style="background-image:url(\'/img/' . $package['package_graphic'] . '\');"></div>';
                    $packageHTML .= '</div>';
                }
                $packageHTML .= '<div class="package-name">' . $package['package_name'] . '</div>';
                $packageHTML .= '<div class="package-desc-wrap">';
                $packageHTML .= '<div class="package-desc-trigger">(See Description)</div>';
                $packageHTML .= '<div class="package-description">' . $package['package_description'] . '</div>';
                $packageHTML .= '</div>';
                $packageHTML .= '<ul class="package-list">';
                $packageHTML .= '<li class="package-duration">Ad runs for <b>' . ceil($package['run_length']/7) . '</b> weeks.</li>';
                $packageHTML .= '<li class="package-word-count">Up to <b>' . $package['words_included'] . '</b> words in ad.</li>';
                if($package['max_live']>0)
                {
                $packageHTML .= '<li class="package-max-ads">Up to <b>' . $package['max_live'] . '</b> ads running.</li>';
                }
                $packageHTML .= '</ul>';
                $packageHTML .= '</div>';
            }
        }
        echo $packageHTML;

        ?>
    </div>
<?php } ?>

<div class='blurbContent'><?= $content ?></div>

<?php if($config['require_new_ad_acknowledgement']) { ?>
    <div class="checkboxes">
        <label for='new_acknowledge'>
            <input type="checkbox" id='new_acknowledge' name='new_acknowledge' onclick='checkAck()' /> You have read and agree to the ad rules.
        </label>
    </div>
<?php } ?>

<a id='startAd' class='formButton pull-right <?= ($config['require_new_ad_acknowledgement'] ? "disabled" : "" )?>' href="#">Click to get started</a><br>

<?php if( !empty($dbAds) && $dbAds['numrows'] > 0 ) { ?>
<div class="clearFix"></div>
<br>
<div class="started-ads-table">
    <h4>You have some ads that you've started, do you want to finish one of them?</h4>
    <table class="table table-bordered">
        <tr><th>Headline</th><th>Created</th></tr>
        <?php foreach( $dbAds['data'] as $ad ) { ?>
            <tr id="row_<?= $ad['id'] ?>">
                <td>
                    <i class="fa fa-trash" onClick="removeAd(<?= $ad['id'] ?>);"></i>&nbsp;&nbsp;&nbsp;
                    <?php if( trim( stripslashes( $ad['headline'] ) ) != '' ) { ?>
                        <a href="/place-an-ad/place-ad-basics/?ad=<?= $ad['id'] ?>"><?= stripslashes($ad['headline']) ?></a>
                    <?php } else { ?>
                        <a href="/place-an-ad/place-ad-basics/?ad=<?= $ad['id'] ?>">(blank)</a>
                    <?php } ?>
                </td>
                <td><?= date( 'm/d/Y H:i', strtotime( $ad['created_dt'] ) ) ?></td>
            </tr>
        <?php } ?>
    </table>
</div>
<?php } ?>

<script>
    var newAdUrl = '/place-an-ad/place-ad-basics/?new&package=<?= $defaultPackageID; ?>';
</script>
