<?php if( $ad ) { ?>

	<div class="post-detail-wrap <?= getAddons($ad['id']) ?>">
		<?php require_once( INCLUDE_DIR . 'page-sections/detail-title.php' ); ?>
		<?php
		// get generic list of attention-getters
		$attns = array();
		$sql = "SELECT * FROM attention_getters";
		$dbRecord = dbselectmulti($sql);
		if( $dbRecord['numrows'] >= 1 ) {
			foreach( $dbRecord['data'] as $row ) {
				$attns[$row['id']] = $row['image'];
			}
		}
		// get list of saved ads
		$savedAds = array();
		$user = $GLOBALS['user'];
		$userId = $user['id'];
		if( $userId && $userId > 0 ) {
			$savedAds = savedAds( $userId );
		}
		// get ad
		classDetail( $ad, 'default', $savedAds, $attns );
		?>
	</div>

<?php } else { ?>

	<div class="no-post">Sorry, this ad doesn't appear to exist. Please try again.</div>

<?php } 
pageModules('featured');