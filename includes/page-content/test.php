Test functions...
<style>
.test-functions {background-color: #aaa;padding:20px;}
.test-functions pre {padding:12px;max-height: 120px;}
.test-functions .shown pre {max-height: 20000px;}
.preShowMore {cursor: pointer;}

</style>
<script>
(function ($) {
$(function () {
	$('.preShowMore').click(function(e){
		e.preventDefault();
		var p = $(this).parent();
		if( p.hasClass('shown') ){
			p.removeClass('shown');
		}else{
			p.addClass('shown');
			$('.content-wrap').css('height','auto');
		}
	});
});
})(jQuery);
</script>
<div class="test-functions">
	<div>
		<strong>session</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php print_r($_SESSION); ?></pre>
	</div><br><div>
		<strong>getAllAds()</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $allAds = getAllAds(); print_r($allAds); ?></pre>
	</div><br><div>
		<strong>getCatDetails(2)...</strong> <strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $catDetails = getCatDetails(2); print_r($catDetails); ?></pre>
	</div><br><div>
		<strong>categories()</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $cats = categories(); print_r($cats); ?></pre>
	</div><br><div>
		<strong>adsInCat(2)</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $ads = adsInCat(2); print_r($ads); ?></pre>
	</div><br><div>
		<strong>getOwnAds(8)</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $myads = getOwnAds(8); print_r($myads); ?></pre>
	</div><br><div>
		<strong>getAdDetails(1)</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $adDetails = getAdDetails(1); print_r($adDetails); ?></pre>
	</div><br><div>
		<strong>getAllAdvertisers()</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $adverts = getAllAdvertisers(); print_r($adverts); ?></pre>
	</div><br><div>
		<strong>getSingleAdvertiser(1)</strong> &nbsp;&nbsp;&nbsp;<strong class="preShowMore">Show More &raquo;</strong>
		<pre><?php $advert = getSingleAdvertiser(1); print_r($advert); ?></pre>
	</div><br>
</div>
