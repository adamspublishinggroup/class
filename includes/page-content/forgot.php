<?php if($_POST) {
	$emailCheck=addslashes($_POST['inputEmail']);

	$sql="SELECT * FROM users WHERE email='$emailCheck'";
	$dbCheck=dbselectsingle($sql);

	if($dbCheck['numrows']>0)
	{
		//create a NEW token, update account, send reset email
		$token=$dbCheck['data']['token'];
		$email=stripslashes($dbCheck['data']['email']);
		$id=$dbCheck['data']['id'];
		addUserAction($userID,'recover');
        
        
        $emailData = array('token'=>$token);
        $emailContent = email_generator(4,$emailData);
          
        $subject = $emailContent['subject'];
        $message = $emailContent['body'];
         
        //send reset email to user
		if(send_email($email,$subject,$message))
        {
			print "<div class='alert alert-success' role='alert'><b>Success</b> You'll be receiving a password recovery email shortly.</div>\n";
		} else {
			print "<div class='alert alert-danger' role='alert'><b>Error</b> That email address was not found in our system.</div>\n";
		}
	 } else {
		print "<div class='alert alert-danger' role='alert'><b>Error</b> That email address was not found in our system.</div>\n";
	}
}
?>

<p>Enter your email address and we'll send you a reset link so you can create a new password.</p>
<form method=post class='form'>
	<input type="email" id="inputEmail" name="inputEmail" placeholder="Email address" required value='<?php echo $_POST['inputEmail'] ?>'>
	<button type="submit">Reset Password</button>
</form>
