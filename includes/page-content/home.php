<?php $ads = getAllAds(); ?>
<?php if( !empty($ads) ) { ?>

	<div class="posts-wrap">
		<?php
		// get generic list of attention-getters
		$attns = array();
		$sql = "SELECT * FROM attention_getters";
		$dbRecord = dbselectmulti($sql);
		if( $dbRecord['numrows'] >= 1 ) {
			foreach( $dbRecord['data'] as $row ) {
				$attns[$row['id']] = $row['image'];
			}
		}
		// get list of saved ads
		$savedAds = array();
		$user = $GLOBALS['user'];
		$userId = $user['id'];
		if( $userId && $userId > 0 ) {
			$savedAds = savedAds( $userId );
		}
		// get each ad
		foreach( $ads as $ad ) {
			classListing( $ad, 'home', $savedAds, $attns );
		}
        paginator();
		?>
	</div>
<?php } else { ?>
	<div class="posts-wrap no-posts">
		<p>There are currently no ads running.</p>
	</div>
<?php } ?>
