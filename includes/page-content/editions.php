<?php
  /*
  Display all past editions in a 3 column array
  */
  $sql="SELECT * FROM editions ORDER BY edition_date DESC";
  $dbEditions = dbselectmulti($sql);
  
  if($dbEditions['numrows']>0)
  {
      foreach($dbEditions['data'] as $edition)
      {
        print "<div class='row' style='border-bottom: thin solid black;padding:10px 0;margin-bottom:10px;'>\n";
        if($config['enable_pageflip'])
        {
            print "<a href='/static/editions/viewer/?edition=$edition[id]'>\n";
            print "<b>Pub Date: ".date("m/d/Y",strtotime($edition['edition_date']))."</b><br>";
            print "<img src='/uploads/editions/".stripslashes($edition['edition_thumb'])."' class='img-responsive' border=0 height=200px />";
            print "</a>\n";
        } else {
           print "<div class='col-xs-12 col-md-5'><a href='/uploads/editions/".stripslashes($edition['edition_filename'])."'>\n";
           print "<b>Pub Date: ".date("m/d/Y",strtotime($edition['edition_date']))."</b><br>";
           print "<img src='/uploads/editions/".stripslashes($edition['edition_thumb'])."' class='img-responsive' border=0 height=200px />";
           print "</a></div><div class='col-xs-12 col-md-7'>\n";
           if($edition['embed']!='')
           {
               ?>
               <div data-configid="<?= $edition['embed'] ?>" style="width:325px; height:350px;" class="issuuembed"></div>
                <?php
           } 
        }
        print "</div></div>\n";    
      }
  } else {
      print "<div class='alert alert-info' role='alert'>There are no published editions ready for viewing at this time.</div>";
  }
  
?>
 <script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>