<?php
$locations = getLocations();

if( !empty( $locations ) ) {
	// set some empty variables
	$latArr = array();
	$lonArr = array();
	$markersJS = '';

	$i = 1;
	foreach( $locations as $location ) {
		// some vars used in the maps js
		$name = $location['location_name'];
		$address = $location['street'];
		$city = $location['city'];
		$state = $location['state'];
		$zip = $location['zip'];
		$locAddrMap = str_replace( ' ', '+', $address ) . ',+' . str_replace( ' ', '+', $city ) . ',+' . $state . '+' . $zip;
		// add to map if lat/lon exist
		if( trim($location['lat']) > '' && trim($location['lon']) > '' ) {
			// building lat/lon array for average
			$latArr[] = $location['lat'];
			$lonArr[] = $location['lon'];
			// building map markers html
			$markersJS .= '{';
			$markersJS .= 'latitude: ' . $location['lat'] . ',';
			$markersJS .= 'longitude: ' . $location['lon'] . ',';
			$markersJS .= 'html: \'';
			$markersJS .= '#' . $i;
			$markersJS .= ' - ';
			$markersJS .= '<b>' . $name . '</b>';
			$markersJS .= '<br>';
			$markersJS .= '<a href="http://maps.google.com/maps?daddr=' . $locAddrMap . '" target="_blank"><b>Directions &raquo;</b></a>';
			$markersJS .= '\',';
			$markersJS .= '},';

			$i++;
		}
	}

	// calc the lat/lon averages
	$latAvg = array_sum($latArr) / count($latArr);
	$lonAvg = array_sum($lonArr) / count($lonArr);

	?>
	<div id="classMap" class="map-wrap">
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?&amp;language=en&amp;key=<?= GOOGLE_MAP_KEY ?>"></script>
		<div id="map_extended"></div>
		<script>
		$("#map_extended").gMap({
			controls: false,
			scrollwheel: true,
			maptype: 'ROADMAP',
			markers: [ <?= $markersJS ?> ],
			icon: {
				image: "/img/mapX.png",
				iconsize: [24, 24],
				iconanchor: [12, 12]
			},
			latitude: <?= $latAvg ?>,
			longitude: <?= $lonAvg ?>,
			zoom: 12
		});
		</script>
	</div>

	<div class="puLocations-wrap">
		<ul class="puLocations">
			<?php
			$i = 1;
			foreach( $locations as $location ) {
				// some vars
				$name = $location['location_name'];
				$address = $location['street'];
				$city = $location['city'];
				$state = $location['state'];
				$zip = $location['zip'];
				$locLat = $location['lat'];
				$locLon = $location['lon'];
				$locAddrMap = str_replace( ' ', '+', $address ) . ',+' . str_replace( ' ', '+', $city ) . ',+' . $state . '+' . $zip;
				$locAddrSee = '';
				if( trim($address)>'' && trim($city)>'' && trim($state)>'' && trim($zip)>'' ) {
					$locAddrSee = $address . ', ' . $city . ',&nbsp;' . $state . '&nbsp;' . $zip;
				}
				?>
				<li class="puLocation">
					<a  class="locLink" href="http://maps.google.com/maps?daddr=<?= $locAddrMap ?>" target="_blank">
						<span class="locName"><?= $name ?></span>
						<span class="locPoint">
							<?php if( trim($locLat) > '' && trim($locLon) > '' ) { ?>
								(Map&nbsp;point&nbsp;#<?= $i ?>)
								<?php $i++; ?>
							<?php } else { ?>
								(Not&nbsp;shown&nbsp;on&nbsp;map.)
							<?php } ?>
						</span><br>
						<span class="locAddr"><?= $locAddrSee ?></span>
					</a>
				</li>
				<?php
			}
			?>
		</ul>
	</div>

<?php } else { ?>
	<div class="posts-wrap no-posts">
		<p>There are currently no locations to show.</p>
	</div>
<?php }
