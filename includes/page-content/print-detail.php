<?php if( $ad ) { ?>

	<?php site_log( $ad['id'], 'print' ); ?>

	<div class="post-detail-wrap">
		<?php
		// get generic list of attention-getters
		$attns = array();
		$sql = "SELECT * FROM attention_getters";
		$dbRecord = dbselectmulti($sql);
		if( $dbRecord['numrows'] >= 1 ) {
			foreach( $dbRecord['data'] as $row ) {
				$attns[$row['id']] = $row['image'];
			}
		}
		// set savedAds to an empty array
		$savedAds=array();
		// get ad
		classDetail( $ad, 'print', $savedAds, $attns );
		?>
	</div>

<?php } else { ?>

	<div class="no-post">Sorry, this ad doesn't appear to exist. Please try again.</div>

<?php } ?>
