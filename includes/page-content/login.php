<?php if( !empty( $_SESSION['loggedin'] ) && $_SESSION['loggedin'] == true && !empty( $_SESSION['email'] ) ) { ?>

	<h3 style="text-align:center;">Success</h3>
	<p style="text-align:center;">You have already logged in!<br>We are now redirecting you to the member area in <span id="counter" style="font-weight:700;">5</span> second(s).</p>
	<script type="text/javascript">
		function countdown() {
			var i = document.getElementById('counter');
			if( parseInt(i.innerHTML) <= 1 ) {
				location.href = '/account/';
			}
			i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
	</script>

<?php } elseif( $_POST ) {

	if( isset($_GET['r']) ) {
		$redirect="?r=".$_GET['r'];
	}
	$error = false;
	$email = addslashes( $_POST['inputEmail'] );
	$password = addslashes( $_POST['inputPassword'] );
	if( $_POST['remember-me'] ) {
		$remember = true;
	} else {
		$remember = false;
	}

	//look up user by email
	$sql = "SELECT * FROM users WHERE email='$email'";
	$dbCheck = dbselectsingle($sql);
	if( $dbCheck['numrows'] > 0 ) {
		$user = $dbCheck['data'];
		//ok, we have a person, lets check the password
		if( password_verify( $password, $user['password'] ) ) {
			$error = false;
			if( $remember ) {
				setcookie( "pngclassUser", $user['token'], time()+3600*24*30, '/' );
				?>
				<script>
					$.cookie( 'pngclassUser', '<?php echo $user['token'] ?>', { expires: 30, path: '/' } );
				</script>
				<?php
			}
			$_SESSION['loggedin'] = true;
			$_SESSION['token'] = $user['token'];
			$_SESSION['email'] = stripslashes( $user['email'] );
			addUserAction($user['id'],'login');
            if( $redirect != '' ) {
				$redir = urldecode( str_replace( '?r=', '', $redirect ) );
				redirect( $redir );
			} else {
				?>
				<!-- <h3 style="text-align:center;">Success</h3>
				<p style="text-align:center;">We are now redirecting you to the member area in <span id="counter" style="font-weight:700;">5</span> second(s).</p> -->
				<script type="text/javascript">
					// function countdown() {
					// 	var i = document.getElementById('counter');
					// 	if( parseInt(i.innerHTML) <= 1 ) {
					// 		location.href = '/account/';
					// 	}
					// 	i.innerHTML = parseInt(i.innerHTML)-1;
					// }
					// setInterval(function(){ countdown(); },1000);

					// just go there immediately
					location.href = '/account/';
				</script>
				<?php
			}
		} else {
			$error = true;
			?>
			<h3 style="text-align:center;">Error</h3>
			<p>Sorry, your account could not be found. Please <a href="/account/login/<?= $redirect ?>">click here to try again</a>.</p>
			<?php
		}
	} else {
		$error = true;
		?>
		<h3 style="text-align:center;">Error</h3>
		<p>Sorry, your account could not be found. Please <a href="/account/login/<?= $redirect ?>">click here to try again</a>.</p>
		<?php
	}

} else {

	?>
	<p style="text-align:center;">Thanks for visiting! Please either login below, or <a href="/account/register/">click here to register</a>.</p>
	<br>
	<div class="login-box">
		<h5 style="text-align:center;">Login Via</h5>
		<div class="social-buttons" style="text-align:center;">
			<!--
			<div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
			-->
			<a href="#" onClick="fbLogin();" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
			<!--<a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a> -->
		</div>
		<h5 style="text-align:center;">OR</h5>
		<form class="form" role="form" method="post" accept-charset="UTF-8" id="login-nav">
			<input type="email" id="inputEmail" name="inputEmail" placeholder="Email address" required>
			<input type="password" id="inputPassword" name="inputPassword" placeholder="Password" required>
			<div class="checkboxes">
				<label>
					<input id="remember-me" name="remember-me" type="checkbox" checked> keep me logged-in
				</label>
			</div>
			<button type="submit">Sign in</button>
			<a href="/account/forgot/">Forgot password?</a>
		</form>
	</div>

<?php } ?>
