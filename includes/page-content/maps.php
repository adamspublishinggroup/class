<?php
$m = $_GET['map_id'];
$c = $_GET['city'];
$zoom = 10;
if(isset($c) && $c != 'all') {
	$zoom = 12;
}

$mapList = getAllMapAds( $m );

if( !empty( $mapList ) ) {
	// set some empty variables
	$cityArr = array();
	$latArr = array();
	$lonArr = array();
	$markersJS = '';

	$i = 1;
	foreach( $mapList as $ad ) {
		$adStatus = $ad['status'];
		if( $adStatus == 1 ) {
			// som vars used in the maps js
			$address = $ad['address']['street'];
			$city = $ad['address']['city'];
			$state = $ad['address']['state'];
			$zip = $ad['address']['zip'];
			if( trim($city) > '' ) {
				$cityArr[] = $city;
			}
			$adAddrMap = str_replace( ' ', '+', $address ) . ',+' . str_replace( ' ', '+', $city ) . ',+' . $state . '+' . $zip;
			// add to map if lat/lon exist
			if( trim($ad['lat']) > '' && trim($ad['lon']) > '' && ( ( !isset($c) || ( isset($c) && $c == 'all' ) ) || ( isset($c) && $c == $city ) ) ) {
				// building lat/lon array for average
				$latArr[] = $ad['lat'];
				$lonArr[] = $ad['lon'];
				// building map markers html
				$markersJS .= '{';
				$markersJS .= 'latitude: ' . $ad['lat'] . ',';
				$markersJS .= 'longitude: ' . $ad['lon'] . ',';
				$markersJS .= 'html: \'';
				$markersJS .= '#' . $i;
				$markersJS .= ' - ';
				$markersJS .= '<b>' . $ad['headline'] . '</b>';
				$markersJS .= '<br>';
				$markersJS .= '<a href="http://maps.google.com/maps?daddr=' . $adAddrMap . '" target="_blank"><b>Directions &raquo;</b></a>';
				$markersJS .= '\',';
				$markersJS .= '},';
			}
		}
		if( ( !isset($c) || ( isset($c) && $c == 'all' ) ) || ( isset($c) && $c == $city ) ) {
			$i++;
		}
	}

	// grab all cities for filter
	$cityArr = array_unique($cityArr);
	sort($cityArr);

	// calc the lat/lon averages
	$latAvg = array_sum($latArr) / count($latArr);
	$lonAvg = array_sum($lonArr) / count($lonArr);

	?>
	<div id="classMap" class="map-wrap">
		<div class="selectCity">
			<div class="selectCity-dropdown">
				<form style="text-align:right;">
					<input type="hidden" name="map_id" value="<?= $m ?>">
					<select name="city" style="max-width:240px;">
						<option disabled="disabled"<?= ( !isset($c) ? ' selected="selected"' : '' ) ?>>Select City...</option>
						<?php foreach( $cityArr as $sity ) { ?>
							<option value="<?= $sity ?>"<?= ( (isset($c) && $c == $sity) ? ' selected="selected"' : '' ) ?>><?= $sity ?></option>
						<?php } ?>
						<option value="all"<?= ( (isset($c) && $c == 'all') ? ' selected="selected"' : '' ) ?>>Show All</option>
					</select>
					<input type="submit" value="Filter">
				</form>
			</div>
		</div>
		<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?&amp;language=en&amp;key=<?= GOOGLE_MAP_KEY ?>"></script>
		<div id="map_extended"></div>
		<script>
		$("#map_extended").gMap({
			controls: false,
			scrollwheel: true,
			maptype: 'ROADMAP',
			markers: [ <?= $markersJS ?> ],
			icon: {
				image: "/img/mapX.png",
				iconsize: [24, 24],
				iconanchor: [12, 12]
			},
			latitude: <?= $latAvg ?>,
			longitude: <?= $lonAvg ?>,
			zoom: <?= $zoom ?>
		});
		</script>
	</div>

	<div class="posts-wrap">

		<?php
		// get generic list of attention-getters
		$attns = array();
		$sql = "SELECT * FROM attention_getters";
		$dbRecord = dbselectmulti($sql);
		if( $dbRecord['numrows'] >= 1 ) {
			foreach( $dbRecord['data'] as $row ) {
				$attns[$row['id']] = $row['image'];
			}
		}
		// get ads
		$i = 1;
		foreach( $mapList as $ad ) {
			$zity = $ad['address']['city'];
			if( ( ( !isset($c) || ( isset($c) && $c == 'all' ) ) || ( isset($c) && $c == $zity ) ) ) {
				mapListing( $ad, $type='default', $i, $attns );
				$i++;
			}
		}
		?>
	</div>

<?php } else { ?>
	<div class="posts-wrap no-posts">
		<p>There are currently no ads running.</p>
	</div>
<?php }
