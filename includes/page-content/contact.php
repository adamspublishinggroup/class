<div class="page-form">
	<form id="contact_form">
		<div class="errors">Please fix errors marked in red.</div>
		<input type="email" id="contact_email" placeholder="Email" aria-describedby="emailErrorStatus" required>
		<input type="text" id="contact_name" placeholder="Name" aria-describedby="nameErrorStatus" required>
		<textarea rows="4" id="contact_message" placeholder="Message..." ></textarea>
		<div class="errors">Please fix errors marked in red.</div>
		<button class='formButton' onclick="sendContact(event);">Send</button>
	</form>
</div>
<div class="clearFix"></div>

<?php
echo staticPage('contact');
