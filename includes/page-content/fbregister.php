<div id="registerConfirmation" style="display:none;">
    <div class="alert alert-success" role="alert">
        <strong>Registration successful!</strong>
        <br>
        <p>Please enjoy the site.</p>
        <br><br><a class='btn btn-primary' href='/'>Start browsing now</a>
    </div>
</div>

<div id="registerFormDiv" style="display:block;">
    <form action="" method="post" id="registerForm">
        <fieldset>
            <h4>Thanks for signing up with Facebook. We just need two final pieces of information to create your account.</h4>
            <div class="errors">Please fix errors marked in red.</div>

            <input id="username" name="username" placeholder="Username" type="text">
            <div class="form-group1 checkboxes">
                <div class="col-sm-12">
                  <div class="checkbox1">
                    <label>
                      <input type="checkbox" name='check_age'  /> You certify that you are at least 13 years or older.
                    </label>
                  </div>
                </div>
            </div>
            
            
            <br>
            <button type="submit">Complete Registration</button>

        </fieldset>
    </form>
</div>
<script>
$(document).ready(function() {
        var $validator = $("#registerForm").validate({
            rules: {
                username: {
                    required: true,
                    minlength: 3,
                    remote: "/includes/checkUsername.php"
                },
                check_age: {
                    required: true
                }
            },
            submitHandler: function(form) {
                uname=$('#username').val();
                $.ajax({
                    url: "/includes/fbRegisterUser.php",
                    type: "POST",
                    data: {stage: 'complete', userName: uname },
                    dataType: 'json',
                    success: function(response){
                        if(response['status']=='success')
                        {
                            $('#registerFormDiv').hide();
                            $('#registerConfirmation').show();
                        }
                    },
                    error: function (xhr, desc, er) {
                        console.log('An error occurred: '+desc);
                        return false;
                    }
                });
            }
        });
    });
</script>
