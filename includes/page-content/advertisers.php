<?php
$advertisers = getAllAdvertisers();
if( !empty($advertisers) ) { ?>
	<div class="posts-wrap">
		<?php foreach( $advertisers as $advertiser ) {
			$status = $advertiser['active'];
			$feat = false;
			$bizId = $advertiser['id'];
			$slug = $advertiser['slug'];
			$logo = $advertiser['logo'];
			$name = $advertiser['name'];
			$slogan = $advertiser['slogan'];
			$blerb = $advertiser['about'];
			$website = $advertiser['url'];
			$phone = $advertiser['phone'];
			$phReplace = array('.', '-', '(', ')', ' ');
			$phoneRaw = str_replace($phReplace, '', $phone);
			$phoneLink = 'tel:' . $phoneRaw;
			$phoneDot = substr_replace( substr_replace( $phoneRaw, '.', 3, 0), '.', 7, 0);
			$phoneDash = substr_replace( substr_replace( $phoneRaw, '-', 3, 0), '-', 7, 0);
			$phoneFull = substr_replace( substr_replace( substr_replace( $phoneRaw, ') ', 3, 0), '-', 8, 0), '(', 0, 0);
			$stripAd = strip_tags( $blerb, '<p><a><b><i><u><strike><strong><em><br>' );
			$excerpt = substr( $stripAd, 0, 200 ) . '...';
			$hasPhone = false;
			if( trim($phone) > '' ) {
				$hasPhone = true;
			}
			$hasLogo = false;
			if( trim($logo) > '' ) {
				$hasLogo = true;
			}
			if( $status ) { ?>
				<div class="classAdvertiser<?= ($feat ? ' is-featured' : '') ?>">
					<div class="advertiserMainWrap<?= ( $hasLogo ? ' withImg' : '' ) ?>">
						<?php if( $hasLogo ) { ?>
							<div class="advertiserImg" style="background-image:url(<?= $logo ?>);"></div>
						<?php } ?>
						<div class="advertiserTitleLinks">
							<h5><a name="biz_<?= $bizId ?>" href="/advertisers/<?= $slug ?>"><?= $name ?></a></h5>
							<?php if( $hasPhone ) { ?>
								 • 
								<h5><a href="<?= $phoneLink ?>"><?= $phoneFull ?></a></h5>
							<?php } ?>
						</div>
						<div class="advertiserSlogan"><?= $slogan ?></div>
						<div class="advertiserExcerpt"><?= $excerpt ?> <a class="advertiserMore" href="/advertisers/<?= $slug ?>">See&nbsp;Advertiser's&nbsp;Page</a></div>
					</div>
					<div class="clearFix"></div>
				</div>
			<?php }
		} ?>
	</div>
<?php } else { ?>
	<div class="posts-wrap no-posts">
		<p>There are currently no advertisers in your area to display.</p>
	</div>
<?php } ?>
