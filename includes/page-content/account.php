<?php if( !empty( $_SESSION['loggedin'] ) && $_SESSION['loggedin'] == true && !empty( $_SESSION['email'] ) ) { ?>

	<h3 style="text-align:center;">Logged in as: <?= $_SESSION['email'] ?></h3>
	<p style="text-align:center;">Not <?= $_SESSION['email'] ?>? <a href="/account/logout/">Logout here</a>.
	<br>
	<div class="dashboard-main">
		<a class="dashboard-main-btn" href="/account/saved-ads/"><i class="fa fa-floppy-o" aria-hidden="true"></i>Saved Ads</a>
		<a class="dashboard-main-btn" href="/account/history/"><i class="fa fa-history" aria-hidden="true"></i>Ad History</a>
		<a class="dashboard-main-btn" href="/account/settings/"><i class="fa fa-cog" aria-hidden="true"></i>User Settings</a>
	</div>

<?php } else { ?>

	<h3 style="text-align:center;">YOU ARE NOT LOGGED IN</h3>
	<p>You will be redirected to the login screen in <span id="counter" style="font-weight:700;">5</span> second(s).</p>
	<script type="text/javascript">
		function countdown() {
			var i = document.getElementById('counter');
			if( parseInt(i.innerHTML) <= 1 ) {
				location.href = '/account/login/';
			}
			i.innerHTML = parseInt(i.innerHTML)-1;
		}
		setInterval(function(){ countdown(); },1000);
	</script>

<?php } ?>
