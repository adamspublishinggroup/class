<?php
if($_POST)
{
	//add a user action
	$id=intval($_POST['user_id']);
	$password = password_hash(addslashes($_POST['user_password']),PASSWORD_DEFAULT);

	$sql="UPDATE users SET password = '$password' WHERE id=$id";
	$dbUpdate=dbexecutequery($sql);
	if($dbUpdate['error']!=''){print $sql."<br>";}

	$sql="SELECT * FROM users WHERE id=$id";
	$dbUser=dbselectsingle($sql);
	$user=$dbUser['data'];

	addUserAction($id,'passwordreset');
	print "<div class='alert alert-success' role=alert'><b>Password Reset</b> Your password has been reset successfully and you have been logged in. You will be redirected to the home page.</div>";
    
	?>
	<script>
		$.cookie('pngclassUser', '<?= $user['token']?>', { expires: 30, path: '/' });
	</script>
	<?php
	setcookie("pngclassUser", $user['token'], time()+3600*24*30, '/');
	$_SESSION['loggedin']=true;
	$_SESSION['token']=$user['token'];
	$_SESSION['email']=stripslashes($user['email']);
    redirect("/",3);
} else {
	$t=addslashes($_GET['t']);

	$sql="SELECT * FROM users WHERE token = '$t' AND verified=1";
	$dbCheck = dbselectsingle($sql);

	if($dbCheck['numrows']>0)
	{
		$user=$dbCheck['data'];
		$id=$dbCheck['data']['id'];
		?>
		<form class="form-horizontal" action="" method="post" id="resetForm">
			<input id="user_password" name="user_password" placeholder="Password" type="password">
			<input id="confirm_password" name="confirm_password" placeholder="Confirm Password" type="password">
			<input type="hidden" name="user_id" value="<?= $id ?>" />
			<button type="submit">Reset Password</button>
		</form>
		<script>
		$(document).ready(function() {
			var $validator = $("#resetForm").validate({
				rules: {
					confirm_password: {
					equalTo: "#user_password"
					}
				},
			});
			$('#user_password').pwstrength({
				ui: { showVerdictsInsideProgressBar: true }
			});
		});
		</script>
		<?php
	} else {
		print "<div class='alert alert-warning' role=alert'><b>Invalid Attempt</b> There was a problem activating the account. Please contact us at ".SEND_ERRORS_TO." to activate your account.</div>";
	}
}
