<div id="registerConfirmation" style="display:none;">
	<div class="alert alert-success" role="alert">
		<strong>Registration successful!</strong>
		<br>
		<p>You'll be receiving an email with a confirmation link. Once you receive that, click the included confirmation link and you'll be able to access all the features of our site.</p>
	</div>
</div>

<div id="registerFormDiv" style="display:block;">
	<h5 style="text-align:center;">You can register via </h5>
    <div class="social-buttons" style="text-align:center;">
        <!--
        <div class="fb-login-button" data-max-rows="1" data-size="small" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div>
        -->
        <a href="#" onClick="fbLogin();" class="btn btn-fb"><i class="fa fa-facebook"></i> Facebook</a>
        <!--<a href="#" class="btn btn-tw"><i class="fa fa-twitter"></i> Twitter</a> -->
    </div>
    <h5 style="text-align:center;">or complete the registration form</h5>
        
    <form action="" method="post" id="registerForm">
		<fieldset>

			<div class="errors">Please fix errors marked in red.</div>

			<input id="firstname" name="firstname" placeholder="First Name" type="text">
			<input id="lastname" name="lastname" placeholder="Last Name" type="text">
			<input id="username" name="username" placeholder="Username" type="text">
			<input id="user_password" name="user_password" placeholder="Password" type="password">
			<input id="confirm_password" name="confirm_password" placeholder="Confirm Password" type="password">
            <input id="email" name="email" placeholder="E-Mail Address" type="email">
            <input id="street" name="street" placeholder="Street Address" type="text">
			<input id="city" name="city" placeholder="City" type="text">
            <select id='state' name='state' style='margin-bottom:8px;'>
                <?php
                    $states=array(
                        'AL'=>'ALABAMA',
                        'AK'=>'ALASKA',
                        'AZ'=>'ARIZONA',
                        'AR'=>'ARKANSAS',
                        'CA'=>'CALIFORNIA',
                        'CO'=>'COLORADO',
                        'CT'=>'CONNECTICUT',
                        'DE'=>'DELAWARE',
                        'DC'=>'DISTRICT OF COLUMBIA',
                        'FL'=>'FLORIDA',
                        'GA'=>'GEORGIA',
                        'GU'=>'GUAM',
                        'HI'=>'HAWAII',
                        'ID'=>'IDAHO',
                        'IL'=>'ILLINOIS',
                        'IN'=>'INDIANA',
                        'IA'=>'IOWA',
                        'KS'=>'KANSAS',
                        'KY'=>'KENTUCKY',
                        'LA'=>'LOUISIANA',
                        'ME'=>'MAINE',
                        'MD'=>'MARYLAND',
                        'MA'=>'MASSACHUSETTS',
                        'MI'=>'MICHIGAN',
                        'MN'=>'MINNESOTA',
                        'MS'=>'MISSISSIPPI',
                        'MO'=>'MISSOURI',
                        'MT'=>'MONTANA',
                        'NE'=>'NEBRASKA',
                        'NV'=>'NEVADA',
                        'NH'=>'NEW HAMPSHIRE',
                        'NJ'=>'NEW JERSEY',
                        'NM'=>'NEW MEXICO',
                        'NY'=>'NEW YORK',
                        'NC'=>'NORTH CAROLINA',
                        'ND'=>'NORTH DAKOTA',
                        'OH'=>'OHIO',
                        'OK'=>'OKLAHOMA',
                        'OR'=>'OREGON',
                        'PW'=>'PALAU',
                        'PA'=>'PENNSYLVANIA',
                        'PR'=>'PUERTO RICO',
                        'RI'=>'RHODE ISLAND',
                        'SC'=>'SOUTH CAROLINA',
                        'SD'=>'SOUTH DAKOTA',
                        'TN'=>'TENNESSEE',
                        'TX'=>'TEXAS',
                        'UT'=>'UTAH',
                        'VT'=>'VERMONT',
                        'VA'=>'VIRGINIA',
                        'VI'=>'VIRGIN ISLANDS',
                        'WA'=>'WASHINGTON',
                        'WV'=>'WEST VIRGINIA',
                        'WI'=>'WISCONSIN',
                        'WY'=>'WYOMING'
                        );
                        foreach($states as $abbr=>$state)
                        {
                            print "<option value='$abbr' ".($abbr == 'OR' ? 'selected' : '').">$state</option>\n";
                        }
                ?>
            </select>
            <input id="zip" name="zip" placeholder="Zip" type="text">
            <input id="phone" name="phone" placeholder="(000) 000-0000" type="tel">
           
            
            <div class="form-group1 checkboxes">
                <div class="col-sm-12">
                  <div class="checkbox1">
                    <label>
                      <input type="checkbox" name='check_age'  /> You certify that you are at least 13 years or older.
                    </label>
                  </div>
                </div>
            </div>
            <p>If you'd like to include a message to the admins (example you want to inquire about discounts for special cases, being billed for your ads, or advertising opportunities on the site), just enter it below</p>
            <textarea id='regmessage' name='regmessage' rows=5 cols=30></textarea>
            <br>
			<button type="submit">Register Now</button>

		</fieldset>
	</form>
</div>
<script>
	$(document).ready(function() {
		var $validator = $("#registerForm").validate({
			rules: {
				email: {
					required: true,
					email: true,
					remote: "/includes/checkEmail.php"
				},
				firstname: {
					required: true,
				},
				lastname: {
                    required: true,
                },
                street: {
                    required: true,
                },
                city: {
                    required: true,
                },
                state: {
                    required: true,
                },
                zip: {
                    required: true,
                },
                phone: {
                    required: true,
					phoneUS: true,
				},
				username: {
					required: true,
					minlength: 3,
					remote: "/includes/checkUsername.php"
				},
                confirm_password: {
                    equalTo: "#user_password"
                },
				check_age: {
					required: true
				}
			},
			submitHandler: function(form) {
				fname=$('#firstname').val();
				lname=$('#lastname').val();
                uname=$('#username').val();
                street=$('#street').val();
                city=$('#city').val();
                state=$('#state').val();
                zip=$('#zip').val();
				phone=$('#phone').val();
				email=$('#email').val();
                pass=$('#user_password').val();
				message=$('#regmessage').val();

				$.ajax({
					url: "/includes/registerUser.php",
					type: "POST",
					data: {firstName: fname, lastName: lname, email: email, userName: uname, pass: pass, message: message, street: street, city: city, state: state, zip: zip, phone: phone },
					dataType: 'json',
					success: function(response){
						if(response['status']=='success')
						{
							$('#registerFormDiv').hide();
							$('#registerConfirmation').show();
						}
					},
					error: function (xhr, desc, er) {
						console.log('An error occurred: '+desc);
						return false;
					}
				});
			}
		});
        $('#zip').mask('99999');
        $('#phone').mask('(999) 999-9999');
		$('#user_password').pwstrength({
			ui: { showVerdictsInsideProgressBar: true }
		});
	});

</script>
