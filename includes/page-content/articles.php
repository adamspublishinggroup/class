<?php
$path = $_SERVER['REQUEST_URI'];

if(!empty($_GET['slug']) && $_GET['slug']!='' && $_GET['slug']!='index.php') {
	$article=getArticle($_GET['slug']);
	if( !empty($article) ) { ?>
		<div class="post-detail-wrap">
			<?php articleFull( $article ); ?>
		</div>
	<?php } else { ?>
		<div class="no-article">Sorry, this article doesn't appear to exist. Please try again.</div>
	<?php }
} elseif( stripos($path,"articles" ) > 0 ) {
	$pathUrlParts = explode("/articles/",$path);
	$path = $pathUrlParts[1];
	$path = explode("/",$path);
	$path = $path[0];
	if( $path!='' ) {
		$sql="SELECT * FROM article_categories WHERE slug='$path'";
		$dbCat = dbselectsingle($sql);
		$artCatID=$dbCat['data']['id'];
		$articles=getArticles($artCatID);
	} else {
		$articles=getArticles(0);
	}
	if( !empty($articles) ) { ?>
		<div class="posts-wrap">
			<?php
			// show summary for each article
			foreach( $articles as $article ) {
				articleBrief($article);
			}
			articlePaginator();
			?>
		</div>
	<?php } else { ?>
		<div class="articles-wrap no-article">
			<p>There are currently no articles in this category.</p>
		</div>
	<?php }
}

pageModules('featured');
