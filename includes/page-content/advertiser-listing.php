<?php 
if( !empty($ads) || !empty($displayAds) ) {

	if( !empty($ads) && !empty($displayAds) ) {

		?>
		<div id="alvertwiser-switch" class="<?= ($dispAdsFirst ? 'showDisp' : 'showList') ?>">
			<ul class="alvertwiser-switch-nav">
				<li><a href="#" class="<?= ($dispAdsFirst ? '' : 'active') ?>" data-tab="showList">Listings</a></li>
				<li><a href="#" class="<?= ($dispAdsFirst ? 'active' : '') ?>" id="showDispTrigger" data-tab="showDisp">Specials</a></li>
			</ul>
			<div id="alvertwiser_listing">
				<?php htmlNormalAds($ads); ?>
			</div>
			<div id="alvertwiser_display">
				<?php htmlDisplayAds($displayAds); ?>
			</div>
		</div>
		<?php

	} else {

		if( !empty($ads) ) { htmlNormalAds($ads); }
		if( !empty($displayAds) ) { htmlDisplayAds($displayAds); }

	}

} else {

	?>
	<div class="posts-wrap no-posts">
		<p>There are currently no ads running for this advertiser.</p>
	</div>
	<?php

}


function htmlNormalAds($ads)
{
	$listingsHtml = '';
	$listingsHtml .= '<div class="posts-wrap">';

		// get generic list of attention-getters
		$attns = array();
		$sql = "SELECT * FROM attention_getters";
		$dbRecord = dbselectmulti($sql);
		if( $dbRecord['numrows'] >= 1 ) {
			foreach( $dbRecord['data'] as $row ) {
				$attns[$row['id']] = $row['image'];
			}
		}

		// get list of saved ads
		$savedAds = array();
		$user = $GLOBALS['user'];
		$userId = $user['id'];
		if( $userId && $userId > 0 ) {
			$savedAds = savedAds( $userId );
		}

		// get each ad
		foreach( $ads as $ad ) {
			$listingsHtml .= classListing( $ad, 'advertiser', $savedAds, $attns );
		}

	$listingsHtml .= '</div>';

	echo $listingsHtml;
}

function htmlDisplayAds($displayAds)
{
	$displaysHtml = '';
	$displaysHtml .= '<div class="displays-wrap grid">';
		foreach( $displayAds as $display ) {
			$displaysHtml .= '<a class="displays-item grid-item adDetailSlide" href="' . $display['filepath'] . 'display_' . $display['filename'] . '">';
			$displaysHtml .= '<img src="' . $display['filepath'] . 'thumb_' . $display['filename'] . '" alt="' . $display['alt_text'] . '">';
			$displaysHtml .= '</a>';
		}
	$displaysHtml .= '</div>';

	echo $displaysHtml;
}
