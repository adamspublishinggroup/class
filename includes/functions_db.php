<?php

if(defined('ABS_PATH'))
{
    require_once(ABS_PATH."/includes/class.db.php");
} else {
    require_once('/includes/classes/class.db.php');
}                                               
//open the database connection
$database = new DB();

// executes a database query
function dbexecutequery($query = '') {
	global $database, $databaseCalls;
    $result['query']=$query;
            
    if ($query != "") {
		if ($results = $database->query( $query )) {
            $result['numrows']= $database->num_rows;
            $result['data']='';
            $error=dberror();
            if ($error!='')
            {
                $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
            }
            $result['error']=$error;
	    } else {
            $error=dberror();
            $result['numrows']= 0;
            $result['data']='';
            if ($error!='')
            {
                $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
            }
            $result['error']=$error;
        }
    } else {
        $result['numrows']= 0; 
        $result['data']='';
        $result['error']='A blank query was submitted.';
    }
    $databaseCalls[]=$result;
    
    return $result; 
}

//executes an INSERT query
function dbinsertquery($query = '') {
    global $database, $databaseCalls;
    $result['query']=$query;
    if ($query != "") {
        if ($results = $database->query( $query )) {
            $result['insertid']= $database->lastid();
            $result['numrows']= $database->lastid(); //because I was super stupid long long ago
            $result['data']='';
            $error=dberror();
            if ($error!='')
            {
                $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
            }
            $result['error']=$error;
        } else {
            $error=dberror();
            $result['numrows']=0;
            $result['insertid']=0;
            $result['data']='';
            if ($error!='')
            {
                $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
            }
            $result['error']=$error;
        }
    } else {
        $result['numrows']=0;
        $result['insertid']=0;
        $result['data']='';
        $error=dberror();
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    }
    $databaseCalls[]=$result;
    return $result;
}
// grabs an array of rows from the query results
function dbselectmulti($query='',$log=false){
    global $database, $databaseCalls;
    $result = array();
    $result['query']=$query;
    if ($results = $database->get_results( $query )){
        $result['numrows']= $database->num_rows($query);
        foreach($results as $row)
        {
            $result['data'][] = $row;
        }
        $error=dberror();
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    } else {
        $result['numrows']=0;
        $result['data']='';
        $result['error']=dberror();
    }
    $databaseCalls[]=$result;
    return $result;
}
function dbselectsingle($query='',$log=false){
    global $database, $databaseCalls;
    $result = array();
    $result['query']=$query;
    if ($q = $database->get_row( $query )) {
        $result['numrows']= $database->num_rows($query);
        $result['data']= $q;
        $error=dberror();
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    } else {
        $error=dberror();
        $result['numrows']=0;
        $result['data']='';
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    }
    $databaseCalls[]=$result;
    return $result;
}

function dbgetfields($table='',$log=false){
    global $databas, $databaseCalls;
    $result=array();
    $query="SELECT * FROM $table";
    $result['query']=$query;
    $fields = $database->list_fields( $query );
    if ($fields) {
        $i=0; 
        foreach($fields as $field)
        {
           $result['fields'][] = $field;
           $i++; 
        }
        
        $result['numrows']=$i;
        $error=dberror();
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    } else {
        $error=dberror();
        $result['fields']='';
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    }
    $databaseCalls[]=$result;
    return $result;
}

function dbfieldexists($table,$field)
{
    global $database;
    $fields=dbgetfields($table);
    foreach($fields['fields'] as $checkfield)
    {
        if ($checkfield['Field']==$field)
        {
            return true;
        }
    }
    return false;
}

function dbgettables($db=''){
    global $database, $databaseCalls;
    $result=array();
    $query="SHOW TABLES FROM $db";
    $result['query']=$query;
    $tables=$database->query($query);
    if ($tables) {
        $i=0;
        foreach($tables as $row)
        {
            $result['tables'][] = $row["Tables_in_$db"];
            $i++;
        }
        
        $result['numrows']=$i;
        $error=dberror();
        
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    } else {
        $error=dberror();
        $result['tables']='';
        $result['numrows']='0';
        if ($error!='')
        {
            $error="An error occurred while processing. The sql was:<br>$query<br>The error was:<br>$error<br>";
        }
        $result['error']=$error;
    }
    $databaseCalls[]=$result;
    return $result;
}
    
// closes the connection to the database
function dbclose(){
	global $database;
    if ($database) {
		return ($database->disconnect()) ? true : false;
	} else {
		// no connection
		return false;
	}
}
	
// gets error information
function dberror() {
    global $database;
    if ($database->error){
       return $database->error;
    }
    return "";
}

function cleanInput($input) {
 
    $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
);
 
    $output = preg_replace($search, '', $input);
    return $output;
}