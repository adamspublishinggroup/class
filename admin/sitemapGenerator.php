<?php
  include "../includes/bootCore.php";
  
  //sitemap.xml generator
  global $config;
  $modDate = date("Y-m-d");
  $xml = <<<XML
<?xml version="1.0" encoding="utf-8"?><!--generated fresh on $modDate by PNGClass -->
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
XML;

   //ok we will do all the menu pages first
   
   $menuItems = menuItems();
   
   foreach($menuItems as $menuItem)
   {
        $xml.="
        <url>
            <loc>".SITE_URL.$menuItem['url']."</loc>
            <lastmod>$modDate</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1.0</priority>
        </url>
";
        if(count($menuItem['submenus'])>0)
        {
            foreach($menuItem['submenus'] as $sub)
            {
             $xml.="
        <url>
            <loc>".SITE_URL.$sub['url']."</loc>
            <lastmod>$modDate</lastmod>
            <changefreq>monthly</changefreq>
            <priority>1.0</priority>
        </url>
";   
            }
        }  
   }
   
   //now include all categories
   //ok we will do all the menu pages first
   $sql="SELECT id FROM categories ORDER BY category_name";
   $dbCategories = dbselectmulti($sql);
   if($dbCategories['numrows']>0)
   {
       foreach($dbCategories['data'] as $cat)
       {
            $xml.="
            <url>
                <loc>".SITE_URL."/category/?c=".$cat['id']."</loc>
                <lastmod>$modDate</lastmod>
                <changefreq>monthly</changefreq>
                <priority>0.7</priority>
            </url>
    ";
             
       }
   }
   //now all of the ads
   $sql="SELECT id FROM ads WHERE end_date>='$modDate' AND published = 1";
   $dbAds = dbselectmulti($sql);
   if($dbAds['numrows']>0)
   {
       foreach($dbAds['data'] as $ad)
       {
            $xml.="
            <url>
                <loc>".SITE_URL."/detail/?ad=".$ad['id']."</loc>
                <lastmod>$modDate</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.6</priority>
            </url>
    ";
            
       }
   }
   
   //now all of the articles
   $sql="SELECT A.*, B.category, B.slug as catSlug FROM articles A, article_categories B WHERE A.category_id=B.id AND A.category_id=$categoryID AND A. published=1 ORDER BY publish_dt";
   $dbArticles = dbselectmulti($sql);
   if($dbArticles['numrows']>0)
   {
       foreach($dbArticles['data'] as $article)
       {
           $url = "/articles/".$article['catSlug']."/".$article['slug']; 
           $xml.="
            <url>
                <loc>".SITE_URL.$url."</loc>
                <lastmod>$modDate</lastmod>
                <changefreq>daily</changefreq>
                <priority>0.5</priority>
            </url>
    ";
            
       }
   }
   
   //now we need to output the xml file
   $xml = preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $xml);
   $xmlFile = file_put_contents("../sitemap.xml",$xml);
   
   
   print "Sitemap generated";