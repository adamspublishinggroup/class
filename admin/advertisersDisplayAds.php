<?php
include "bootstrap.php";
require_once("../vendor/autoload.php");
use Intervention\Image\ImageManager;


function page()
{
	?>
	<div class="page-header">
		<h1>Advertiser's Display Ads</h1>
	</div>
	<?php

	if ($_POST) {
		$action = $_POST['submit'];
	} else {
		$action = $_GET['action'];
	}

	switch ($action) {
		case "add": edit_display(); break;
		case "edit": edit_display(); break;
		case "Save Display Ad": save_display(); break;
		case "delete": delete_display(); break;
		case "Filter": list_display(); break;
		default: list_display(); break;
	}
}

function edit_display()
{
	$id = intval($_GET['id']);
	if ($id != 0) {
		$sql = "SELECT * FROM advertisers_display WHERE id=$id";
		$dbAdvDisplay = dbselectsingle($sql);
		$record = $dbAdvDisplay['data'];
	}

	print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
	$sql = "SELECT * FROM advertisers ORDER BY name";
	$dbAdvs = dbselectmulti($sql);
	$advs[0] = "Select advertiser";
	if ($dbAdvs['numrows'] > 0) {
		foreach($dbAdvs['data'] as $adv) {
			$advs[$adv['id']] = stripslashes($adv['name']);
		}
	}
	make_select('advertiser_id', $advs[$record['advertiser_id']], $advs, 'Advertiser', 'Which advertiser does this display ad belong to?');
	make_date('start_date', $record['start_date'], 'Start Date', '');
	make_date('end_date', $record['end_date'], 'End Date', '');
	if ($record['filename'] != '') {
		$file = 'Current file: ' . $record['filepath'] . $record['filename'];
		$file.= '<br><img class="" src="' . $record['filepath'] . 'thumb_' . $record['filename'] . '">';
	}
	make_file('filename', 'Filename', 'JPEG, 1000px wide x 1000px high -ish. ' . $file, $record['filepath'] . $record['filename']);
	make_text('alt_text', stripslashes($record['alt_text']) , 'Title/Alt-Text');
	make_hidden('advDisplayAd_id', $id);
	make_submit('submit', 'Save Display Ad');
	print "</form>\n";
}

function save_display()
{
	$advertiser = addslashes($_POST['advertiser_id']);
	$startDate = addslashes($_POST['start_date']);
	$endDate = addslashes($_POST['end_date']);
	$altText = addslashes($_POST['alt_text']);
	$advDisplayAdID = $_POST['advDisplayAd_id'];

	// verify/make directory
	$YYYY = date('Y');
	$MM = date('m');
	$target_dir = '../uploads/' . $YYYY . '/' . $MM . '/';
	if (!file_exists($target_dir)) {
		if (!mkdir($target_dir, 0755, true)) {
			print 'Failed to create directory for uploaded image.';
		}
	}

	// insert or update all fields except file upload
	$uploadOK = false;
	if ($advDisplayAdID == 0) {
		$sql = "INSERT INTO advertisers_display (advertiser_id, start_date, end_date, alt_text) VALUES ('$advertiser', '$startDate', '$endDate', '$altText')";
		$dbInsert = dbinsertquery($sql);
		$advDisplayAdID = $dbInsert['insertid'];
		if ($dbInsert['error'] == '') {
			$uploadOK = true;
		}
	} else {
		$sql = "UPDATE advertisers_display SET advertiser_id='$advertiser', start_date='$startDate', end_date='$endDate', alt_text='$altText' WHERE id=$advDisplayAdID";
		$dbUpdate = dbexecutequery($sql);
		if ($dbUpdate['error'] == '') {
			$uploadOK = true;
		}
	}

	// update record with file upload
	if (count($_FILES) > 0 && $uploadOK) {
		// create an image manager instance with favored driver
		$manager = new ImageManager(array('driver' => 'gd'));

		foreach($_FILES as $field => $file) {
			if ($file['name'] != '') {

				// Allow certain file formats
				$imgType = $file['type'];
				if ($imgType != "image/jpg" && $imgType != "image/png" && $imgType != "image/jpeg" && $imgType != "image/gif") {
					echo "Sorry, only JPG, JPEG, PNG and GIF files are allowed. You uploaded a $imgType file.";
					$uploadOK = false;
				}
				
				if ($uploadOK) {
					// build vars and names
					$filePath = '/uploads/' . $YYYY . '/' . $MM . '/';
					$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
					if (trim($altText) > '') {
						$fileName = slugify($altText);
						$fileName = substr($fileName, 0, 20);
					} else {
						$fileName = date('Ymd');
					}
					$fileName = $advDisplayAdID . '-' . $fileName . '.' . $ext;
					$displayName = 'display_' . $fileName;
					$thumbName = 'thumb_' . $fileName;
					$target_file = $target_dir . $fileName;

					// upload, resize and upload
					if (move_uploaded_file($file["tmp_name"], $target_file)) {
						// update record with new file path and name
						$sql = "UPDATE advertisers_display SET filepath='$filePath', $field='$fileName' WHERE id=$advDisplayAdID";
						$dbUpdate = dbinsertquery($sql);
						$error = $dbUpdate['error'];
						// load and resize the image
						$image = $manager->make($target_file);
						$image->resize(1000, null, function ($constraint) {
							$constraint->aspectRatio();
							$constraint->upsize();
						});
						$image->save($target_dir.$displayName);
						$image->resize(null, 200, function ($constraint) {
							$constraint->aspectRatio();
						});
						$image->save($target_dir.$thumbName);
					} else {
						$error = 'File upload error.';
					}
				}
			}
		}
	}

	redirect("?action=list");
}

function delete_display()
{
	$id = intval($_GET['id']);
	if ($id != 0) {
		$sql = "SELECT * FROM advertisers_display WHERE id=$id";
		$dbAdvDisplay = dbselectsingle($sql);
		if ($dbAdvDisplay['numrows'] > 0) {
			$record = $dbAdvDisplay['data'];
			$filePath = $record['filepath'];
			$fileName = $record['filename'];
			if(unlink('..'.$filePath.$fileName)){
				unlink('..'.$filePath.'thumb_'.$fileName);
				unlink('..'.$filePath.'display_'.$fileName);
				$sql="DELETE FROM advertisers_display WHERE id=$id";
				$dbDelete=dbexecutequery($sql);
			}
		}
	}

	redirect("?action=list");
}

function list_display()
{
	$filtered = false;
	$advertiser = addslashes($_POST['advertiser_id']);

	if ($advertiser != 0) {
		$filtered = true;
	}

	print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
	$sql = "SELECT id, name FROM advertisers ORDER BY name";
	$dbAdvs = dbselectmulti($sql);
	$advs[0] = "Select advertiser";
	if ($dbAdvs['numrows'] > 0) {
		foreach($dbAdvs['data'] as $adv) {
			$advs[$adv['id']] = stripslashes($adv['name']);
		}
	}
	make_select('advertiser_id', $advs[$advertiser], $advs, 'Advertiser', '');
	make_submit('submit', 'Filter');
	print "</form>\n";

	if ($filtered) {
		$sql = "SELECT * FROM advertisers_display WHERE advertiser_id='$advertiser' ORDER BY start_date";
	} else {
		$sql = "SELECT * FROM advertisers_display ORDER BY start_date";
	}
	$dbAdvDisplay = dbselectmulti($sql);
	if ($dbAdvDisplay['error'] == '') {
		tableStart("<a href='?action=add'>Add new display ad</a>", "ID,Advertiser,Ad Image,Start Date,End Date,Alt Text");
		foreach($dbAdvDisplay['data'] as $adv) {
			// TODO - join the following sql statement into the above sql statement
			$advId = $adv['advertiser_id'];
			$sql = "SELECT name FROM advertisers WHERE id='$advId'";
			$dbAdvName = dbselectsingle($sql);
			print "<tr>";
			print "<td>" . $adv['id'] . "</td>";
			print "<td>" . $dbAdvName['data']['name'] . "</td>";
			print "<td><div class='config-file-preview' style='background-image:url(".$adv['filepath']."thumb_".$adv['filename'].");'></div></td>";
			print "<td>" . $adv['start_date'] . "</td>";
			print "<td>" . $adv['end_date'] . "</td>";
			print "<td>" . $adv['alt_text'] . "</td>";
			print "<td>
				<div class='btn-group'>
					<a href='?action=edit&id=$adv[id]' class='btn'>Edit</a>
					<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						<span class='caret'></span>
						<span class='sr-only'>Toggle Dropdown</span>
					</button>
					<ul class='dropdown-menu'>
						<li><a href='?action=delete&id=$adv[id]' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
					</ul>
				</div>
				</td>";
			print "</tr>";
		}
		tableEnd($dbAdvDisplay);
	} else {
		echo '<div class="alert alert-warning"><b>No Data</b> The display ads could not be retrieved. Error: ' . $dbAdvDisplay['error'] . '</div>';
	}
}

function slugify($text)
{
	$text = preg_replace('~[^\pL\d]+~u', '-', $text); // replace non letter or digits by -
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text); // transliterate
	$text = preg_replace('~[^-\w]+~', '', $text); // remove unwanted characters
	$text = trim($text, '-'); // trim
	$text = preg_replace('~-+~', '-', $text); // remove duplicate -
	$text = strtolower($text); // lowercase
	if (empty($text)) { return 'n-a'; } // if no text is left
	return $text;
}