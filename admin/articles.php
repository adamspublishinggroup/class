<?php
include "bootstrap.php";
require_once("../vendor/autoload.php");
use Intervention\Image\ImageManager;
// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'gd'));
                
    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Articles</h1>
    </div>
   <?php
   switch($action)
   {
       case "images": list_images(); break;
       case "add": record(); break;
       case "addimage": image_record(); break;
       case "edit": record(); break;
       case "editimage": image_record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       case "deleteimage": delete_image(); break;
       default: list_records(); break;
   }  
}

function image_record()
{
    $articleID=intval($_GET['article_id']);
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql="SELECT * FROM article_images WHERE id=$id";
        $dbImage=dbselectsingle($sql);
        $record = $dbImage['data'];
    }
    print "<form method=post enctype='multipart/form-data' action='articleImageHandler.php'>\n";
    make_file('image','Image','','../uploads/articles/'.$record['filename']);
    make_hidden('id',$id);
    make_hidden('article_id',$articleID);
    make_submit('submit','Save Image');
    print "</form>\n";
}

function delete_image()
{
    $articleID=intval($_GET['article_id']);
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql="SELECT * FROM article_images WHERE id=$id";
        $dbImage=dbselectsingle($sql);
        $record = $dbImage['data'];
        
        if(unlink("../uploads/articles/".$record['filename']))
        {
            unlink("../uploads/articles/display_".$record['filename']);
            unlink("../uploads/articles/thumb_".$record['filename']);
            $sql="DELETE FROM article_images WHERE id=$id";
            $dbDelete = dbexecutequery($sql);
        }
    }
    redirect("?action=images&article_id=$articleID"); 
}

function list_images()
{
    $articleID=intval($_GET['article_id']);
    $sql="SELECT * FROM article_images WHERE article_id=$articleID";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=list'>Return to articles</a>,<a href='?action=addimage&article_id=$articleID'>Add new image</a>","Image");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['filename']);
            print "<tr>\n";
            print "<td><img src='../uploads/articles/thumb_$name' class='img-thumbnail' height=100 /></td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=editimage&article_id=$articleID&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=deleteimage&article_id=$articleID&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}

function record($record=array())
{
    $sql="SELECT * FROM article_categories ORDER BY category";
    $dbCats = dbselectmulti($sql);
    $cats[0]="Select category";
    if($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            $cats[$cat['id']]=stripslashes($cat['category']);
        }
    }
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM articles WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
        $record['published']=1;
    }
    print "<form method=post class='form-horizontal'>\n";
    make_select('category_id',$cats[$record['category_id']],$cats,'Category','Which category should this be placed in?');
    make_text('headline',stripslashes($record['headline']),'Headline','');
    make_textarea('content',stripslashes($record['content']),'Article Content','');
    make_checkbox('published',$record['published'],'Publish','Check to publish this article');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM articles WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    $sql="SELECT * FROM article_images WHERE article_id=$id";
    $dbImages=dbselectmulti($sql);
    if($dbImages['numrows']>0)
    {
        $baseDir="../uploads/articles/";
        foreach($dbImages['data'] as $image)
        {
            if(unlink($baseDir.$image['filename']))
            {
                unlink($baseDir."thumb_".$image['filename']);
                $sql="DELETE FROM article_images WHERE id=".$image['id'];
                $dbDelete=dbexecutequery($sql);
            }    
        }
    }
    redirect("?action=list");
}

function save_record()
{
    
    $id=intval($_POST['id']);
    
    $category_id=intval($_POST['category_id']);
    $headline=addslashes($_POST['headline']);
    $slug = strtolower(str_replace(array("*",".",",","?","\"","\\","/","#","!","@","\$","%","^","(",")","'","-","+","_","=","|","{","[","]","}","<",">"),"",$headline));
    $slug=str_replace(" ","-",$slug);
    
    $content=addslashes($_POST['content']); 
    if($_POST['published']){$published=1;}else{$published=0;}
    $dt=date("Y-m-d H:i");
    
    if($id==0)
    {
        $sql="INSERT INTO articles (category_id, headline, content, publish_dt, published, slug) VALUES 
        ($category_id, '$headline', '$content', '$dt', '$published', '$slug')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE articles SET category_id='$category_id', headline='$headline', content='$content', publish_dt='$dt', published='$published' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    $sql="SELECT * FROM article_categories ORDER BY category";
    $dbCats = dbselectmulti($sql);
    $cats[0]="Select category";
    if($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            $cats[$cat['id']]=stripslashes($cat['category']);
        }
    }
    
    $sql="SELECT * FROM articles ORDER BY publish_dt";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new article</a>","Category,Headline");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $cat=$cats[$record['category_id']];
            $name=stripslashes($record['headline']);
            print "<tr>\n";
            print "<td>$cat</td>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=images&article_id=$id'> Images</a></li>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}
