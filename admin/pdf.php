<?php
error_reporting(E_ALL);
include('../vendor/tcpdf/config/tcpdf_config.php');
include('../vendor/tcpdf/tcpdf.php');


// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor("Joe");
$pdf->SetTitle('Test Bill');
$pdf->SetSubject('Test Billing Document');
$pdf->SetKeywords('bill,test');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('dejavusans', '', 14, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));



$invoice = <<<INVOICE

<style>
   
    
    
    .invoice-box table td{
        padding:5px;
        vertical-align:top;
    }
    
    .invoice-box table tr td:nth-child(2){
        text-align:right;
    }
    
    .invoice-box table tr.top table td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.top table td.title{
        font-size:45px;
        line-height:45px;
        color:#333;
    }
    
    .invoice-box table tr.information table td{
        padding-bottom:40px;
    }
    
    .invoice-box table tr.heading td{
        background:#eee;
        border-bottom:1px solid #ddd;
        font-weight:bold;
    }
    
    .invoice-box table tr.details td{
        padding-bottom:20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom:1px solid #eee;
    }
    
    .invoice-box table tr.item.last td{
        border-bottom:none;
    }
    
    .invoice-box table tr.total td:nth-child(2){
        border-top:2px solid #eee;
        font-weight:bold;
    }
    
    </style>
<div style="width:800px;
        margin:auto;
        padding:30px;
        border:1px solid #eee;
        box-shadow:0 0 10px rgba(0, 0, 0, .15);
        font-size:16px;
        line-height:24px;
        font-family:'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color:#555;">
<table cellpadding="0" cellspacing="0" style='width:100%;
        line-height:inherit;
        text-align:left;'>
<tr class="top">
    <td colspan="2">
        <table>
        <tr>
            <td style="font-size:45px;line-height:45px;color:#333;">
                <img src="/img/flat-logo.jpg" width=250>
            </td>
            <td>
                Invoice #: 123<br>
                Created: January 1, 2015<br>
                Due: February 1, 2015
            </td>
        </tr>
        </table>
        </td>
</tr>
<tr class="information">
    <td colspan="2" style='padding:5px;
        vertical-align:top;'>
        <table>
            <tr>
                <td style='padding:5px;
        vertical-align:top;'>
                    Sparksuite, Inc.<br>
                    12345 Sunny Road<br>
                    Sunnyville, TX 12345
                </td>
                <td style='padding:5px;
        vertical-align:top;'>
                    Acme Corp.<br>
                    John Doe<br>
                    info@lagrandenickel.com
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr class="heading">
    <td>
        Payment Method
    </td>
    <td>
        Check #
    </td>
</tr>
<tr class="details">
    <td>
        Check
    </td>
    <td style=" text-align:right;">
        1000
    </td>
</tr>
<tr class="heading">
    <td>
        Item
    </td>
    <td style=" text-align:right;">
        Price
    </td>
</tr>
<tr class="item">
    <td>
        Website design
    </td>
    <td style=" text-align:right;">
        $300.00
    </td>
</tr>
<tr class="item">
    <td>
        Hosting (3 months)
    </td>
    <td style=" text-align:right;">
        $75.00
    </td>
</tr>
<tr class="item last">
    <td>
        Domain name (1 year)
    </td>
    <td style=" text-align:right;">
        $10.00
    </td>
</tr>
<tr class="total">
    <td></td>
    <td style=" text-align:right;">
        Total: $385.00
    </td>
</tr>
</table>
</div>
INVOICE;


// Set some content to print
$html = <<<EOD
<h1>Welcome to <a href="http://www.tcpdf.org" style="text-decoration:none;background-color:#CC0000;color:black;">&nbsp;<span style="color:black;">TC</span><span style="color:white;">PDF</span>&nbsp;</a>!</h1>
<i>This is the first example of TCPDF library.</i> <Br><br>
<table>
<tr><th>Col 1</th><th style='width:300px;'>Col 2</th></tr>
<tr><td>Data 1</td><td>300</td></tr>
<tr><td>Data 1</td><td>300</td></tr>
<tr><td>Data 1</td><td>300</td></tr>
</table>
<p>This text is printed using the <i>writeHTMLCell()</i> method but you can also use: <i>Multicell(), writeHTML(), Write(), Cell() and Text()</i>.</p>
<p>Please check the source code documentation and other examples for further information.</p>
<p style="color:#CCCC00;">TO IMPROVE AND EXPAND TCPDF I NEED YOUR SUPPORT, PLEASE <a href="http://sourceforge.net/donate/index.php?group_id=128076">MAKE A DONATION!</a></p>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTML($invoice, true, false, true, false, '');
$pdf->lastPage();

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$uploadDir = "/home/pioneerw3b/public_html/sites/class/uploads/bills";
$pdf->Output($uploadDir.'/sample_001.pdf', 'F');
