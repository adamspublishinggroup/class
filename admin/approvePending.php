<?php
include "bootstrap.php";
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Approve Pending Ads</h1>
    </div>
   <?php
   switch($action)
   {
       case "approve": approve_ad(); break;
       default: list_records(); break;
   }  
}

function approve_ad()
{
    $adID = intval($_GET['ad']);
    
    $sql="UPDATE ads SET status=2 WHERE id=$adID";
    $dbUpdate=dbexecutequery($sql);
    
    
    
    //see if we have a facebook or twitter addon
    $sql="SELECT * FROM ad_addons WHERE ad_id=$adID";
    $dbAddons=dbselectmulti($sql);
    if($dbAddons['numrows']>0)
    {
        foreach($dbAddons['data'] as $addon)
        {
            if($addon['id']==$config['facebook_add_on'])
            {
                $link = SITE_URL."/detail/?ad=$adID";
                $message="<b>".$ad['headline']."</b> ".$ad['ad_text']."&nbsp;&nbsp;&nbsp;<a href='$link'>Read the full ad</a>";
                postToFacebook($link,$message);
            }elseif($addon['id']==$config['twitter_add_on'])
            {
                $link = SITE_URL."/detail/?ad=$adID";
                $message="<b>".$ad['headline']."</b>&nbsp;&nbsp;&nbsp;<a href='$link'>Read the full ad</a>";
                postToTwitter($link,$message);
            }
        }
    }
    
    
    
    
    print "<div class='alert alert-success' role='alert'>The ad has been approved <a href='?action=list' class='pull-right btn btn-success'>View pending list</a></div>\n";
}

function list_records()
{
    $sql="SELECT * FROM ads WHERE status=6";
    $dbAds = dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new add-on</a>","ID,Start,End,Published,Status,Headline,Admin,Advertiser");
    if ($dbAds['numrows']>0)
    {
        foreach($dbAds['data'] as $ad)
        {
            $id=$ad['id'];
            
            print "<td>".$ad['id']."</td>";
            print "<td>".date("m/d/Y",strtotime($ad['start_date']))."</td>";
            print "<td>".date("m/d/Y",strtotime($ad['end_date']))."</td>";
            print "<td>".($ad['published']==1 ? "Published" : "Unpublished")."</td>";
            print "<td>".$status."</td>";
            print "<td>".stripslashes($ad['headline'])."</td>";
            print "<td>".($ad['admin_placed']==1 ? "<i class='fa fa-check'></i>" : "")."</td>";
            print "<td>".($ad['advertiser_id']>0 ? "<i class='fa fa-check'></i>" : "")."</td>";
            
            print "<td>
        <div class='btn-group'>
          <a href='?action=view&id=$id' class='btn'>View</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=approve&id=$id' ><i class='fa fa-check'></i> Approve</a></li>
            <li><a href='searchAds.php?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbAds);
}
