<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Add Ons <small>Additional upcharges for ads</small></h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM addons WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
        $record['active']=1;
        $record['word_charge']=0;
        $record['flat_charge']=0;
        $record['third_party']=0;
        $record['percentage_charge']=0;
    }
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_text('addon_short',stripslashes($record['addon_short']),'Short label for addon (bills, summary, etc)');
    make_text('addon',stripslashes($record['addon']),'Full label for addon (shown during order entry)');
    make_number('word_charge',stripslashes($record['word_charge']),'Word Charge','Override per-word charge (Only one of these should be active at any time)');
    make_number('flat_charge',stripslashes($record['flat_charge']),'Flat Charge','Flat amount to charge if this feature is selected.');
    make_number('percentage_charge',stripslashes($record['percentage_charge']),'Percentage Charge','Extra percentage of the ad charge to add on. (Example 50% would be Ad cost + 50%).');
    make_checkbox('charge_per_print',$record['charge_per_print'],'Charge each print','If checked, this addon will be charged once for each time the ad is published during the dates selected, otherwise it will be charged only once per order.');
    make_text('online_class',stripslashes($record['online_class']),'Class for online display','Enter the css class or classes to be applied to an ad for this addon.');
    make_text('print_label',stripslashes($record['print_label']),'Print label','Enter the label that will be shown in the print dump to indicate this addon.');
    make_checkbox('third_party',$record['third_party'],'Third Party','If checked, revenue (charges) for this addon will be tracked for a third party.');
    make_file('preview_image','Preview Image','Include an image to show what this would look like. (Image should be no more than 150px wide)','../uploads/addons/'.$record['preview_image']);
    make_checkbox('active',$record['active'],'Active','If checked, this addon will be available.');
    make_hidden('id',$record['id']);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM addons WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    //@TODO need to clean up ad records when metadata tags are deleted!
    
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $addon=addslashes($_POST['addon']);
    $addon_short=addslashes($_POST['addon_short']);
    $word_charge=addslashes($_POST['word_charge']);
    $flat_charge=addslashes($_POST['flat_charge']);
    $online_class=addslashes($_POST['online_class']);
    $print_label=addslashes($_POST['print_label']);
    $percentage_charge=intval($_POST['percentage_charge']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($_POST['third_party']){$third_party=1;}else{$third_party=0;}
    if($_POST['charge_per_print']){$per_print=1;}else{$per_print=0;}
    
    if($id==0)
    {
        $sql="INSERT INTO addons (addon, addon_short, word_charge, flat_charge, percentage_charge, active, online_class, print_label, 
        third_party, charge_per_print) VALUES ('$addon', '$addon_short', '$word_charge', '$flat_charge', '$percentage_charge', '$active', '$online_class', '$print_label', '$third_party', '$per_print')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE addons SET addon='$addon', addon_short='$addon_short', word_charge='$word_charge', flat_charge='$flat_charge', active='$active', percentage_charge='$percentage_charge', online_class='$online_class', print_label='$print_label', 
        third_party='$third_party', charge_per_print='$per_print' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if(count($_FILES)>0)
    {
        $file=$_FILES['preview_image'];
        $imageDirectory='../uploads/addons/';
        $ext = end(explode(".",$_FILES['preview_image']['name']));
        $name='addon_'.$id.".".$ext;
        
        if(processFile($file,$imageDirectory,$name) == true)
        {
           $success = true;
           //create the record
           $sql="UPDATE addons SET preview_image='$name' WHERE id=$id";
           $dbUpdate=dbexecutequery($sql);
           if($dbUpdate['error']=='')
           {
              move_uploaded_file($file,$imageDirectory.$name);
           }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    global $types;
    $sql="SELECT * FROM addons";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new add-on</a>","Add On,Active");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['addon_short']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>".($record['active'] ? "Yes" : "No")."</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}