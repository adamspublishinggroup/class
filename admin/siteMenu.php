<?php
include "bootstrap.php";   
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Site Menu</h1>
    </div>
   <?php
   switch($action)
   {
       case "add":    edit_record(); break;
       case "edit":   edit_record(); break;
       case "sort":   sort_records(); break;
       case "Save":   save_record(); break;
       case "delete": delete_record(); break;
       default:       list_records(); break;
   }  
}

function sort_records()
{
    ?>
    <style>
        body.dragging, body.dragging * {
          cursor: move !important;
        }

        .dragged {
          position: absolute;
          opacity: 0.5;
          z-index: 2000;
        }

        ol.menuSorter li.placeholder {
          position: relative;
          /** More li styles **/
        }
        ol.menuSorter li.placeholder:before {
          position: absolute;
          /** Define arrowhead **/
        }
    </style>
    <?php
    $parentID = intval($_GET['parent_id']);
    $sql="SELECT id, name FROM site_menu WHERE parent_id=$parentID ORDER BY sort_order ASC";
    $dbMenuItems = dbselectmulti($sql);
    if($dbMenuItems['numrows']>0)
    {
        print "<div id='sorter'>
        <ol class='menuSorter'>";
        foreach($dbMenuItems['data'] as $menu)
        {
            print "<li data-id='$menu[id]'>".stripslashes($menu['name'])."</li>\n";       
        }
        print "</ol>
        </div>";    
    }
    ?>
    <script>
    $(function  () {
      var group = $("ol.menuSorter").sortable({
          group: 'menuSorter',
          onDrop: function ($item, container, _super) {
            var sortdata = group.sortable("serialize").get();

            var jsonString = JSON.stringify(sortdata, null, ' ');
            $.ajax({
                url: 'sortmenu.php',
                type: "POST",
                dataType: "json",
                data: {sorting: sortdata},
                success: function(response){
                }      
            })
            _super($item, container);
          }
      });
    });
    </script>
    <br>
    <a href='?action=list&parent_id=<?= $parentID ?>' class='btn btn-primary'>Return to editor</a>
    <?php    
}

function edit_record()
{
    $sql="SELECT * FROM maps ORDER BY map_name DESC";
    $dbMaps = dbselectmulti($sql);
    $maps[0]="Not associated to a map";
    if($dbMaps['numrows']>0)
    {
        foreach($dbMaps['data'] as $map)
        {
            $maps[$map['id']]=stripslashes($map['map_name']);
        }
    } 
    
    $sql="SELECT * FROM article_categories ORDER BY category DESC";
    $dbArticleCategories = dbselectmulti($sql);
    $articleCategories[0]="Not associated to an article category";
    if($dbArticleCategories['numrows']>0)
    {
        foreach($dbArticleCategories['data'] as $artCat)
        {
            $articleCategories[$artCat['id']]=stripslashes($artCat['category']);
        }
    } 
    
    $sql="SELECT * FROM static_pages ORDER BY headline DESC";
    $dbPages = dbselectmulti($sql);
    $statics[0]="Not associated to a static page";
    if($dbPages['numrows']>0)
    {
        foreach($dbPages['data'] as $page)
        {
            $statics[$page['id']]=stripslashes($page['headline']);
        }
    } 
    $id=intval($_GET['id']);
    $parentID=intval($_GET['parent_id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM site_menu WHERE id=$id";
        $dbMenu = dbselectsingle($sql);
        $menu = $dbMenu['data'];
    } else {
        $menu['active'] = 1;
    } 
    print "<form method=post class='form-horizontal'>\n";
    make_checkbox('active',$menu['active'],'Active','Check if this menu is active');
    make_text('name',stripslashes($menu['name']),'Menu Name');
    make_text('url',stripslashes($menu['url']),'Target URL');
    make_select('map_id',$maps[$menu['map_id']],$maps,'Associate to a map','Select a map to associate to this menu');
    make_select('static_id',$statics[$menu['static_id']],$statics,'Associate to a page','Select a static page to associate to this menu');
    make_select('article_category_id',$articleCategories[$menu['article_category_id']],$articleCategories,'Associate to an article category','Select an article category to associate to this menu');
    make_checkbox('show_top',$menu['show_top'],'Top?','Check to include on main top menu');
    make_checkbox('show_mobile',$menu['show_mobile'],'Mobile?','Check to include on mobile menu');
    make_checkbox('show_footer',$menu['show_footer'],'Footer?','Check to include in the footer menu');
    make_hidden('id',$id);
    make_hidden('parent_id',$parentID);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $parentID=intval($_GET['parent_id']);
    $sql="DELETE FROM site_menu WHERE id=$id OR parent_id=$id";
    $dbDelete = dbexecutequery($sql);  
    redirect("?action=list&parent_id=$parentID");
}

function save_record()
{
    
    $id=intval($_POST['id']);
    $parent_id=intval($_POST['parent_id']);
    $name=addslashes($_POST['name']);
    $url=addslashes($_POST['url']);
    $map_id=intval($_POST['map_id']);
    $static_id=addslashes($_POST['static_id']);
    $article_category_id=addslashes($_POST['article_category_id']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($_POST['show_mobile']){$mobile=1;}else{$mobile=0;}
    if($_POST['show_footer']){$footer=1;}else{$footer=0;}
    if($_POST['show_top']){$top=1;}else{$top=0;}
    if($id==0)
    {
        $sql="INSERT INTO site_menu (name, url, active, map_id, static_id, article_category_id, sort_order, show_mobile, show_footer, parent_id, show_top) VALUES 
        ('$name', '$url', '$active', '$map_id', '$static_id', '$article_category_id', 1, '$mobile', '$footer', '$parent_id', '$top')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        $sql="UPDATE site_menu SET name='$name', url='$url', active='$active', map_id='$map_id', static_id='$static_id', article_category_id='$article_category_id', show_mobile='$mobile', show_footer='$footer', parent_id='$parent_id', show_top='$top' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        category($category);
    } else {
        redirect("?action=list&parent_id=$parent_id");
    }
}

function list_records()
{
    $parentID=intval($_GET['parent_id']);
    $sql="SELECT * FROM site_menu WHERE parent_id=$parentID ORDER BY sort_order";
    $dbMenus=dbselectmulti($sql);
    tableStart("<a href='?action=add&parent_id=$parentID'>Add new item</a><br><a href='?action=list&parent_id=0'>Return to top</a><a href='?action=sort&parent_id=$parentID'>Sort this level</a>","Active,Name,Main Menu, Mobile Menu, Footer Menu");
    if ($dbMenus['numrows']>0)
    {
        foreach($dbMenus['data'] as $menu)
        {
            $id=$menu['id'];
            $name=stripslashes($menu['name']);
            if($menu['active']){$active='True';}else{$active='No';}
            print "<tr>\n";
            print "<td>$active</td>\n";
            print "<td>$name</td>\n";
            print "<td>".($menu['show_top'] ? "<i class='fa fa-check'></i>": "")."</td>\n";
            print "<td>".($menu['show_mobile'] ? "<i class='fa fa-check'></i>": "")."</td>\n";
            print "<td>".($menu['show_footer'] ? "<i class='fa fa-check'></i>": "")."</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id&parent_id=$parentID' class='btn'>Edit</a>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>";
         print "   <li><a href='?action=list&parent_id=$id'>Sub-menu</a></li>
         <li><a href='?action=delete&id=$id&parent_id=$parentID' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbCategories);
}