<?php
include "bootstrap.php";
function page($action)
{
    switch($action)
    {
        case 'delete':
            delete_ad();
            break;
        case 'togglePub':
            toggle_pub();
            break;
        case 'notes':
            notes();
            break;
        case 'editbasics':
            edit_basics();
            break;
        case 'Save Ad':
            save_basics();
            break;
        case 'Save Notes':
            save_notes();
            break;
        case 'Search':
            search_ads();
            break;
            
        default:
            search_ads();
            break;
    }
    
}

function notes()
{
    $id=intval($_GET['id']);
    $sql="SELECT notes FROM ads WHERE id=$id";
    $dbAd = dbselectsingle($sql);
    $ad = $dbAd['data'];
    
    print "<form method=post>\n";
    make_textarea('notes',stripslashes($ad['notes']),'Notes','Enter any relevant ad notes');
    make_submit('submit','Save Notes');
    make_hidden('id',$id);
    print "</form>\n";
}

function save_notes()
{
    $id=intval($_POST['id']);
    $notes = addslashes($_POST['notes']);
    $sql="UPDATE ads SET notes = '$notes' WHERE id = $id";
    $dbUpdate=dbexecutequery($sql);
    
    redirect("?action=list");   
}


function search_ads() {
    global $adStatuses;
    if($_POST)
    {
        $userID = intval($_POST['user_id']);
        $advertiserID = intval($_POST['advertiser_id']);
        $catID = intval($_POST['category_id']);
        $startDate = addslashes($_POST['startDate']);
        $endDate = addslashes($_POST['endDate']);
        $adText = addslashes($_POST['adText']);
    } else {
        $startDate = date("m/d/Y",strtotime("-1 month"));
        $endDate = date("m/d/Y",strtotime("+1 month"));
        $userID = 0;
        $advertiserID = 0;
        $catID = 0;
        $adText = '';
    }
    ?>
    <div class="page-header" style='margin-bottom:10px;'>
      <h1>Search for ads</h1>
    </div>
<form method=post class='form-horizontal panel'>
   <div class='col-xs-12 col-md-6'>
    <?php 
    //pull in a list of all users, allow admin to set the user to be attached to the ad
    $sql="SELECT id, first, last, username FROM users ORDER BY last DESC";
    $dbUsers = dbselectmulti($sql);
    $users = array();
    $users[0]="Please select";
    if($dbUsers['numrows']>0)
    {
        foreach($dbUsers['data'] as $user)
        {
            $users[$user['id']]=stripslashes($user['username']." - ".$user['first'].' '.$user['last']);    
        }
    }
    make_select('user_id',$users[$userID],$users,'From User');
    make_text('user_last',$_POST['user_last'],'Last name like');
    make_text('user_phone',$_POST['user_phone'],'User Phone');
    //pull in a list of all advertisers,
    $sql="SELECT id, name FROM advertisers ORDER BY last DESC";
    $dbAdvertisers = dbselectmulti($sql);
    $advertisers = array();
    $advertisers[0]="Please select";
    if($dbAdvertisers['numrows']>0)
    {
        foreach($dbAdvertisers['data'] as $advertiser)
        {
            $advertisers[$advertiser['id']]=stripslashes($advertiser['name']);    
        }
    }
    make_select('advetiser_id',$advertisers[$advertiserID],$advertisers,'From advertiser');
    
    $categories = categories(0);
    $selCats[0] = "All categories";
    if(count($categories)>0)
    {
        foreach($categories as $cat)
        {
            $selCats[$cat['category_id']]=$cat['category_name'];
            if(count($cat['subcats'])>0)
            {
                foreach($cat['subcats'] as $sub)
                {
                    $selCats[$sub['category_id']]="--".$sub['category_name'];
                } 
            }        
        }
    }
    make_select('category_id',$selCats[$catID],$selCats,'Show ads in category');
    
    ?>
    </div>
    
    <div class='col-xs-12 col-sm-6'>  
    <?php
    make_text('adText',$adText,'Ads containing');
    make_checkbox('unpublished',$_POST['unpublished'],'Unpublished','Include unpublished ads');
    ?>
    
    <div class="form-group">
        <label for="startDate" class="col-sm-2 control-label">Find ads in this date range</label>
        <div class="col-sm-10">
            <div class="row">
                <label for="startDate" class="col-sm-2 control-label">Start Date</label>
                <div class="col-sm-2">
                    <div class="input-group date" id="startDate" style='width: 150px;'>
                        <input type="text" name="startDate" value='<?= $startDate ?>' />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <label for="endDate" class="col-sm-2 control-label">End Date</label>
                <div class="col-sm-2">
                    <div class="input-group date" id="endDate"  style="width:150px;">
                        <input type="text" name="endDate" value='<?= $endDate ?>' />
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#startDate').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('#endDate').datetimepicker({
            format: 'MM/DD/YYYY',
            useCurrent: false //Important! See issue #1075
        });
        $("#startDate").on("dp.change", function (e) {
            $('#endDate').data("DateTimePicker").minDate(e.date);
        });
        $("#endDate").on("dp.change", function (e) {
            $('#startDate').data("DateTimePicker").maxDate(e.date);
        });
    });
</script> 
<?php
    make_submit('submit','Search');
    print "</form>\n";

    $searchDateStart = date("Y-m-d",strtotime($startDate));        
    $searchDateEnd = date("Y-m-d",strtotime($endDate));
    $userids='';
        
    if($_POST['user_last'] || $_POST['user_phone'])
    {
        $ids=array();
        //need to find possible users
        if($_POST['user_last'])
        {
            $last = addslashes($_POST['user_last']);
            $sql="SELECT id FROM users WHERE last LIKE '%$last%'";
            $dbUsers = dbselectmulti($sql);
            if($dbUsers['numrows']>0)
            {
                foreach($dbUsers['data'] as $user)
                {
                    $ids[]=$user['id'];
                }
                
            }
        }
        if($_POST['user_phone'])
        {
            $phone = addslashes($_POST['user_phone']);
            //lets try to remove the area code
            if(strlen($phone)>8)
            {
                if(strpos($phone,") ")>0)
                {
                    $phone = end(explode(") ",$phone));
                }
                if(strpos($phone,")")>0)
                {
                    $phone = end(explode(")",$phone));
                }
                if(strpos($phone," ")>0)
                {
                    $phone = end(explode(" ",$phone));
                }
                //if it's still more than 8 characters, means we have something like 208-465-8171
                if(strlen($phone)>8)
                {
                    if(substr($phone,3,1)=='-')
                    {
                        $phone=substr($phone,4);
                    }
                }
            }
            $sql="SELECT id FROM users WHERE phone LIKE '%$phone%'";
            $dbUsers = dbselectmulti($sql);
            if($dbUsers['numrows']>0)
            {
                foreach($dbUsers['data'] as $user)
                {
                    $ids[]=$user['id'];
                }
                
            }
            $userids = implode(",",$ids);
        }
    }
    
    $sql="SELECT DISTINCT(A.id), A.headline, A.published, A.status, A.start_date, A.end_date FROM ads A, ad_category_xref B WHERE A.id=B.ad_id ".
    ($_POST['unpublished'] ? "AND published = 0 " : "").
    ($userids!='' ? "AND user_id IN($userids) " : "").
    ($userID!=0 ? "AND user_id = $userID " : "").
    ($advertiserID!=0 ? "AND advertiser_id = $userID " : "").
    ($catID!=0 ? "AND B.category_id = $catID " : "").        
    ($adText!='' ? "AND (headline LIKE '%$adText%' OR ad_text LIKE '%$adText%') " : "").        
    "AND (start_date >='$searchDateStart' AND start_date<='$searchDateEnd')";
    $dbAds = dbselectmulti($sql);
    if($dbAds['numrows']>0)
    {
        tableStart("<a href='/place-an-ad/place-ad-basics/?new'>Create new ad</a>","ID,Start,End,Published,Status,Headline,Admin,Advertiser");
        foreach($dbAds['data'] as $ad)
        {
            $status = $adStatuses[$ad['status']];
            
            
            print "<tr>";
            print "<td>".$ad['id']."</td>";
            print "<td>".date("m/d/Y",strtotime($ad['start_date']))."</td>";
            print "<td>".date("m/d/Y",strtotime($ad['end_date']))."</td>";
            print "<td>".($ad['published']==1 ? "Published" : "Unpublished")."</td>";
            print "<td>".$status."</td>";
            print "<td>".stripslashes($ad['headline'])."</td>";
            print "<td>".($ad['admin_placed']==1 ? "<i class='fa fa-check'></i>" : "")."</td>";
            print "<td>".($ad['advertiser_id']>0 ? "<i class='fa fa-check'></i>" : "")."</td>";
             print "<td>
            <div class='btn-group'>
              <a href='?action=editbasics&id=$ad[id]' class='btn'>Edit Basics</a>
              <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                <span class='caret'></span>
                <span class='sr-only'>Toggle Dropdown</span>
              </button>
              <ul class='dropdown-menu'>
                <li><a href='/place-an-ad/place-ad-basics/?ad=$ad[id]'>Edit in Ad builder</a></li>
                <li><a href='?action=notesid=$ad[id]'>Add/View notes</a></li>
                <li><a href='refundAd.php?id=$ad[id]'>Refund</a></li>
                <li><a href='/place-an-ad/place-ad-basics/?pickup&ad=$ad[id]'>Re-run this ad</a></li>
                <li><a href='?action=togglePub&id=$ad[id]'>Toggle Publish Status</a></li>
                <li><a href='?action=delete&id=$ad[id]' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
              </ul>
            </div>
            </td>";
            print "</tr>";
        }
        tableEnd($dbAds);
    } else {
        print "<div class='alert alert-warning' role='alert'><b>Error</b> No ads found matching that criteria.</div>";
    }       
}

function delete_ad()
{
    $id=intval($_GET['id']);
    $sql="UPDATE ads SET status=9, published=0 WHERE id=$id"; //"delete" the ad by changing status
    $dbDelete = dbexecutequery($sql);
    redirect("?action=list");
}

function toggle_pub()
{
    $id=intval($_GET['id']);
    $sql="SELECT published FROM ads WHERE id=$id";
    $dbAd = dbselectsingle($sql);
    $published = $dbAd['data']['published'];
    if($published==1){$published=0;}else{$published=1;}
    $sql="UPDATE ads SET published=$published WHERE id=$id";
    $dbUpdate=dbexecutequery($sql);
    redirect("searchAds.php");
}

/*
*   Allow an admin to alter the ad without going through all the editing steps and re-billing/unpublishing the ad
*/
function edit_basics()
{
   $id=intval($_GET['id']);
   $sql="SELECT * FROM ads WHERE id = $id";
   $dbAd = dbselectsingle($sql);
   $ad = $dbAd['data'];
    //pull in all ad categories
    $cats = array();
    if($id!=0)
    {
        $sql="SELECT category_id FROM ad_category_xref WHERE ad_id='$id'";
        $dbCats=dbselectmulti($sql);
        if($dbCats['numrows']>0)
        {
            foreach($dbCats['data'] as $cat)
            {
                $cats[]=$cat['category_id'];
            }
        }
    }
    
    $categories = categories(0); 
    
    //pull in a list of all users, allow admin to set the user to be attached to the ad
    $sql = "SELECT id, first, last, username FROM users ORDER BY last DESC";
    $dbUsers = dbselectmulti($sql);
    $users[0] = "Select a user for this ad";
    if( $dbUsers['numrows'] > 0 ) {
        foreach( $dbUsers['data'] as $user ) {
            $users[$user['id']]=stripslashes($user['username']." - ".$user['first'].' '.$user['last'])."</option>\n";
        }
    }
    print "<form method=post class='form-horizontal'>\n";
    make_select('user_id',$users[$ad['user_id']],$users,'User','Who is this ad for?');
    make_text('headline',stripslashes($ad['headline']),'Headline');
    make_textarea('ad_text',stripslashes($ad['ad_text']),'Ad Text','',50,20,false);
    make_textarea('internet_text',stripslashes($ad['internet_text']),'Internet Text','',50,20,false);
    ?>
    <div class='form=group'>
        <label for="ad_categories" class="col-sm-2 control-label">Categories</label>
        <div class='col-sm-10'>
        <select id="ad_categories" name="ad_categories[]" multiple="multiple" class='form-control'>
            <?php
            $selected = '';
            if( count( $categories ) > 0 ) {
                foreach( $categories as $cat ) {
                    if(in_array($cat['category_id'],$cats)){$selected='selected';}else{$selected='';}
                    print "<option value='$cat[category_id]' $selected>$cat[category_name]</option>\n";
                    if( count( $cat['subcats'] ) > 0 ) {
                        foreach( $cat['subcats'] as $subcat ) {
                            if(in_array($subcat['category_id'],$cats)){$checked='selected';}else{$checked='';}
                            print "<option value='$subcat[category_id]' $checked>&nbsp;&nbsp;&nbsp;&nbsp;$subcat[category_name]</option>\n";
                        }
                    }
                }
            }
            ?>
        </select>
        </div>
    </div>
    <br>
    <script type="text/javascript">
        $("#ad_categories").select2();
        $('#ad_text').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']]
            ],
            callbacks: {
                onPaste: function (e) {
                    var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
                    e.preventDefault();
                    var stripedHtml = bufferText.replace(/<[^>]+>/g, '');
                    // Firefox fix
                    setTimeout(function () {
                        document.execCommand('insertText', false, stripedHtml);
                    }, 10);
                }
            }
        });
        $('#internet_text').summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']]
            ]
        });
       
     </script>
    <?php
    make_hidden('ad_id',$id);
    make_submit('submit','Save Ad');
    print "</form>\n";
}

function save_basics()
{
    $adID = intval($_POST['ad_id']);
    $headline = addslashes($_POST['headline']);
    $ad_text = addslashes($_POST['ad_text']);
    $internet_text = addslashes($_POST['internet_text']);
    $user_id = intval($_POST['user_id']);
    $cats = $_POST['ad_categories'];
 
    $dt = date("Y-m-d H:i");
    $sql="UPDATE ads SET user_id='$user_id', headline = '$headline', ad_text='$ad_text', internet_text='$internet_text', updated_dt ='$dt' WHERE id=$adID";
    $dbUpdate=dbexecutequery($sql);
    
    //save categories
     //get all "free ads" categories
     $freeCats[] = array();
     $sql="SELECT id FROM categories WHERE free_ads = 1";
     $dbFree = dbselectmulti($sql);
     if($dbFree['numrows']>0)
     {
         foreach($dbFree['data'] as $free)
         {
             $freeCats[]=$free['id'];
         }
     }
     //update existing cats
     //delete first
     $sql="DELETE FROM ad_category_xref WHERE ad_id=$adID";
     $dbDelete=dbexecutequery($sql);
     //create the ad_category_xref records
     if(count($cats)>0)
     {
         $catInserts=array();
         foreach($cats as $cat)
         {
             if(in_array($cat,$freeCats))
             {
                 $catInserts = array();
                 $catInserts[]="($adID,$cat)";
                 break;
             }
             $catInserts[]="($adID,$cat)";
         }
         $sql="INSERT INTO ad_category_xref (ad_id, category_id) VALUES ".implode(",",$catInserts);
         $dbInsert=dbinsertquery($sql);
     }
     
     redirect("?action=list");
}