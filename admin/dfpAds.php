<?php
include "bootstrap.php";

        
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>DFP Ad Placements</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $positions = array('left'=>"Left rail",'right-top'=>"Right rail top",'right-bottom'=>"Right rail bottom",'leader'=>"Leaderboard", 'footer'=>"Footer position");
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM dfp_ads WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    
    print "<div class='row'>
    <div class='col-md-9'>";
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_text('slot_name',stripslashes($record['slot_name']),'Slot name','As defined in DFP');
    make_select('location',$positions[$record['location']],$positions,'Placement','Select a placement');
    make_text('slot_size',stripslashes($record['slot_size']),'Sizes','MUST BE IN FORMAT [300,250], or [[300,600],[300,250] (sizes are examples)');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    print "</div>
    <div class='col-md-3'>
    <div class='well'>
    <h4>There are maximum ad sizes for each position</h4>
    <p>Left rail position: Max size 160x600</p>
    <p>Right rail positions: 300px x 600px</p>
    <p>Leaderboard: 728px x 90px</p>
    <p>Footer: 728px x 90px</p>
    </div>
    </div>";
}

function delete_record()
{
    $id=intval($_GET['id']);
    
    $sql="DELETE FROM dfp_ads WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $name=addslashes($_POST['slot_name']);
    $sizes=addslashes($_POST['slot_size']);
    $location=addslashes($_POST['location']);
    
    if($id==0)
    {
        $sql="INSERT INTO dfp_ads (slot_name, slot_size, location ) VALUES 
        ('$name', '$sizes', '$location')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE dfp_ads SET slot_name='$name', slot_size='$sizes', location='$location' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    $target_dir = "../uploads/rail_items/";
    $target_file = $target_dir . basename($_FILES["filename"]["name"]);
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    $positions = array('left'=>"Left rail",'right-top'=>"Right rail top",'right-bottom'=>"Right rail bottom",'leader'=>"Leaderboard", 'footer'=>"Footer position");
    $sql="SELECT * FROM dfp_ads";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new item</a>","Name,Position,Sizes");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['slot_name']);
            $sizes= stripslashes($record['slot_size']);
            $position=$positions[stripslashes($record['location'])];
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>$position</td>\n";
            print "<td>$sizes</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}