<?php
include "bootstrap.php";
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Admins</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM admins WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
       
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('name',stripslashes($record['name']),'Name');
    make_text('email',stripslashes($record['email']),'Email');
    make_password('passthing','','Password','Leave blank to leave as set');
    make_hidden('id',$record['id']);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM admins WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $admin=$_POST;
    
    $id=intval($_POST['id']);
    $name=addslashes($_POST['name']);
    $email=addslashes($_POST['email']);
    $password=password_hash($_POST['passthing'], PASSWORD_DEFAULT);
    $token = generate_random_string(32);
    
    if($id==0)
    {
        $sql="INSERT INTO admins (name, email, password, token) VALUES ('$name', '$email', '$password', '$token')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        if($_POST['passthing']!='')
        {
            $pupdate=", password='".$password."'";
        }
        $sql="UPDATE admins SET name='$name', email='$email' $pupdate WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($admin);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    $sql="SELECT * FROM admins";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new admin</a>","Name,Email");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['name']);
            $email=stripslashes($record['email']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>$email</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}