<?php
include "bootstrap.php";

        
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Packages</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM packages WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
        $record['active']=1;
        $record['weight']=99;
        $record['commercial']=0;
    }
    
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    print "<div class='col-xs-12 col-md-7'>";
    make_checkbox('active',$record['active'],'Active','Check if this package is active');
    
    make_text('package_name',stripslashes($record['package_name']),'Name','');
    make_textarea('package_description',stripslashes($record['package_description']),'Description','Explain what this package contains');
    make_file('package_graphic','Graphic','Graphic to go with the description','../uploads/packages/'.stripslashes($record['package_graphic']));
    make_number('package_price',stripslashes($record['package_price']),'Package Price','Base package price');
    make_number('per_word_price',stripslashes($record['per_word_price']),'Per word price','Price for additional words');
    
    make_number('words_included',intval($record['words_included']),'Words Included','Number of words included in the base package');
    make_number('run_length',intval($record['run_length']),'Run length','Number of days this package will run for');
    make_number('max_live',intval($record['max_live']),'Max live','Number of ads of this package type that can be concurrently published before user must step up to higher package');
    make_number('photos_included',intval($record['photos_included']),'Photos Included','Number of photos included in the base package');
    make_number('per_photo_price',stripslashes($record['per_photo_price']),'Per photo price','Price for additional photos');
    make_number('bold_word_cost',stripslashes($record['bold_word_cost']),'Bold Word Price','Price per bold word');
    make_number('italic_word_cost',stripslashes($record['italic_word_cost']),'Italic Word Price','Price per italic word');
    make_number('underline_word_cost',stripslashes($record['underline_word_cost']),'Italic Word Price','Price per underline word');
    make_checkbox('commercial',$record['commercial'],'Commercial','Check if this is a commercial package (retail business)');
    make_number('weight',intval($record['weight']),'Sort Order','Order left to right with 1 on left');
    print "</div>";
    print "<div class='col-xs-12 col-md-5'>\n";
    print "<h4>Select Addons included in this package</h4>\n";
    $sql="SELECT * FROM addons ORDER BY addon_short";
    $dbAddons=dbselectmulti($sql);
    
    //get any addons already selected for this package
    $sql="SELECT * FROM package_addons WHERE package_id=$id";
    $dbPackageAddons = dbselectmulti($sql);
    $paddons = array();
    if($dbPackageAddons['numrows']>0)
    {
        foreach($dbPackageAddons['data'] as $pad)
        {
            $paddons[]=$pad['addon_id'];
        }   
    }
    if($dbAddons['numrows']>0)
    {
        foreach($dbAddons['data'] as $addon)
        {
            if(in_array($addon['id'],$paddons)){$checked = 1;}else{$checked=0;}
            make_checkbox('addon_'.$addon['id'],$checked,$addon['addon_short'],$addon['addon']);
        }
    }
    print "</div>";
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
}

function delete_record()
{
    $id=intval($_GET['id']);
    
    $sql="DELETE FROM packages WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    $sql="DELETE FROM package_addons WHERE package_id=$id";
    $dbDelete=dbexecutequery($sql);
    
    
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    $id=intval($_POST['id']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($_POST['commercial']){$commercial=1;}else{$commercial=0;}
    $package_name=addslashes($_POST['package_name']);
    $package_price=addslashes($_POST['package_price']);
    $per_word_price=addslashes($_POST['per_word_price']);
    $package_description=addslashes($_POST['package_description']);
    $run_length=intval($_POST['run_length']);
    $words_included=intval($_POST['words_included']);
    $photos_included=intval($_POST['photos_included']);
    $per_photo_price=intval($_POST['per_photo_price']);
    $max_live=intval($_POST['max_live']);
    $weight=intval($_POST['weight']);
    $bold_word_cost=addslashes($_POST['bold_word_cost']);
    $italic_word_cost=addslashes($_POST['italic_word_cost']);
    $underline_word_cost=addslashes($_POST['underline_word_cost']);
    
    if($id==0)
    {
        $sql="INSERT INTO packages (package_name, package_description, package_price, per_word_price, run_length, words_included, max_live, 
        active, weight, commercial, photos_included, per_photo_price, bold_word_cost, italic_word_cost, underline_word_cost) VALUES 
        ('$package_name', '$package_description', '$package_price', '$per_word_price', '$run_length', '$words_included', '$max_live', 
        '$active', '$weight', '$commercial', '$photos_included', '$per_photo_price', '$bold_word_cost', '$italic_word_cost', 
        '$underline_word_cost')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
        $record['id']=$id;
    } else {
        $sql="UPDATE packages SET package_name='$package_name', package_description='$package_description', package_price='$package_price', 
        run_length='$run_length', words_included='$words_included', max_live='$max_live', active='$active', bold_word_cost='$bold_word_cost', 
        italic_word_cost='$italic_word_cost', underline_word_cost='$underline_word_cost', per_word_price='$per_word_price', weight='$weight', 
        commercial='$commercial', photos_included='$photos_included', per_photo_price='$per_photo_price' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    //clear addons
    $sql="DELETE FROM package_addons WHERE package_id=$id";
    $dbDelete=dbexecutequery($sql);
    $addonInserts = array();
    foreach($_POST as $key=>$value)
    {
        if(substr($key,0,6)=='addon_')
        {
            $addonID = str_replace("addon_","",$key);
            $addonInserts[]="($id,$addonID)";
        }
    }
    if(count($addonInserts)>0)
    {
        $sql="INSERT INTO package_addons (package_id, addon_id) VALUES ".implode(",",$addonInserts);
        $dbInsert=dbinsertquery($sql);
    }
    
    $target_dir = "../uploads/packages/";
    $target_file = $target_dir . basename($_FILES["package_graphic"]["name"]);
    
    if(isset($_FILES) && $_FILES['package_graphic']['name']!='')
    {
        $uploadOK=1;
        // Allow certain file formats
        $imageFileType =$_FILES['package_graphic']['type'];
        if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
        && $imageFileType != "image/gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
            $uploadOK = 0;
        }
        if($uploadOK)
        {
            if (move_uploaded_file($_FILES["package_graphic"]["tmp_name"], $target_file)) {
                $sql="UPDATE packages SET package_graphic='".addslashes($_FILES['package_graphic']['name'])."' WHERE id=$id";
                $dbUpdate=dbexecutequery($sql);
                $error=$dbUpdate['error'];
                
            } else {
                $error='File upload error.';
            }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    $sql="SELECT * FROM packages";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new package</a>","Package");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['package_name']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}