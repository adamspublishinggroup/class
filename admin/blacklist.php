<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Email blacklist <small>Email addresses that have been opt-ed out of correspondence</small></h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM blacklist WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('email',stripslashes($record['email']),'Email','Email address to opt-out');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM blacklist WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $email=addslashes($_POST['email']);
    $dt = date("Y-m-d H:i:s");
    if($id==0)
    {
        $sql="INSERT INTO blacklist (email, opt_out_dt) VALUES ('$email', '$dt')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE blacklist SET site_property='$email' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    global $types;
    $sql="SELECT * FROM blacklist";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new blacklisted email</a>","Email");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $email=stripslashes($record['email']);
            print "<tr>\n";
            print "<td>$email</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}