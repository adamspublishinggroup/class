<?php
include "bootstrap.php";
function page()
{
   ?>
    <div class="page-header">
      <h1>Users</h1>
    </div>
   <?php
   if($_POST)
   {
       $action=$_POST['submit'];
   } else {
       $action = $_GET['action'];
   }
   switch($action)
   {
       case "add": edit_user(); break;
       case "edit": edit_user(); break;
       case "password": password(); break;
       case "Save Password": save_password(); break;
       case "Save User": save_user(); break;
       case "delete": delete_user(); break;
       default: list_users(); break;
   }
     
}

function password()
{
    $id=intval($_GET['id']);
    print "<form class='form-horizontal' method=post>";
    make_password('password','','Enter new password','',true);
    make_hidden('id',$id);
    make_submit('submit','Save Password');
    print "</form>\n";
}

function delete_user()
{
    $id=intval($_GET['id']);
    $sql="SELECT * FROM users WHERE id=$id";
    $dbUser=dbselectsingle($sql);
    $user=$dbUser['data'];
    
    if($user['image']!='')
    {
        unlink("../uploads/avatars/".stripslashes($user['image']));
    }
    
    $sql="DELETE FROM users WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    redirect("?action=list");
}

function save_password()
{
    $id=intval($_POST['id']);
    $password = $_POST['password'];
    
    $password=password_hash($password,PASSWORD_DEFAULT);
    
    $sql="UPDATE users SET password='$password' WHERE id=$id";
    $dbUpdate=dbexecutequery($sql);
    
    redirect("?action=list");      
}


function edit_user()
{
    global $config;
    
    $genders = array('female'=>'Female','male'=>'Male');
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql="SELECT * FROM users WHERE id=$id";
        $dbUser=dbselectsingle($sql);
        $user=$dbUser['data'];
    } else {
        $user['state']=$config['default_state'];
        $user['verified']=1;
    }
    
    //get advertisers
    $advertisers[0]="Not associated with an advertiser";
    $sql="SELECT id, name FROM advertisers ORDER BY name";
    $dbAdvertisers=dbselectmulti($sql);
    if($dbAdvertisers['numrows']>0)
    {
        foreach($dbAdvertisers['data'] as $advertiser)
        {
            $advertisers[$advertiser['id']]=stripslashes($advertiser['name']);
        }
    }
    
    print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
    make_text('first',stripslashes($user['first']),'First Name','This is required');
    make_text('last',stripslashes($user['last']),'Last Name','This is required');
    make_text('username',stripslashes($user['username']),'Username');
    make_text('email',stripslashes($user['email']),'Email');
    make_phone('phone',$user['phone'],'Phone');
    make_text('street',$user['street'],'Street');
    make_text('city',$user['city'],'City');
    make_state('state',$user['state'],'State');
    make_text('zip',$user['zip'],'Zip');
    make_select('gender',$genders[$user['gender']],$genders,'Gender');
    make_file('mugshot','Bio Pic','','/uploads/avatars/'.$user['image']);
    make_date('birthdate',date('Y-m-d',strtotime($user['birthdate'])),'Birthdate');
    make_textarea('bio',stripslashes($user['bio']),'Bio');
    make_select('advertiser_id',$advertisers[$user['advertiser_id']],$advertisers,'Assign to advertiser','Which business is this user associated with?');
    make_number('email_flags',$user['email_flags'],'# of bad emails sent','This is the number of reported flags against this user');
    make_checkbox('age_check',$user['13_check'],'Age check','User has verified they are 13+');
    make_checkbox('bill_me',$user['bill_me'],'User is bill me','User can be billed for ads');
    make_checkbox('trusted',$user['trusted'],'Trusted','If checked, users ads will be automatically approved if the site is set to require new ad approval.');
    make_checkbox('veteran',$user['veteran'],'Veteran','If checked, user is a veteran and will receive the discount specified in the site settings');
    make_checkbox('senior',$user['senior'],'Senior','If checked, user is a senior and will receive the discount specified in the site settings');
    if($id==0)
    {
        make_checkbox('send',1,'Send to user','If checked, will create account and will send verification email to user and prompt them to create a password.');
    }
    if($user['verify']==0 || $id == 0) {
    make_checkbox('verify',$user['verified'],'Verify User','Verify user. If this is a new account, will skip the verification process');
    }
    make_hidden('user_id',$id);
    make_submit('submit','Save User');
    print "</form>\n";
    ?>
    <script>
    var firstname=$('#first').val();
    var lastname=$('#last').val();
    var $username=$('#username');
    console.log('init');
    $('#last').on('blur',function(){
        if ($username.val()=='')
        {
            var b='';
            var finitial=firstname.substr(0,1);
            var lname=lastname;
            b=finitial+lname;
            b=b.toLowerCase();
            b=b.replace(" ","");
            $username.val(b);    
        } 
    });
     
    </script>
    <?php
}


function save_user()
{
    $userName=addslashes($_POST['username']);
    $firstName=addslashes($_POST['first']);
    $lastName=addslashes($_POST['last']);
    $email=addslashes($_POST['email']);
    $phone=addslashes($_POST['phone']);
    $street=addslashes($_POST['street']);
    $city=addslashes($_POST['city']);
    $state=addslashes($_POST['state']);
    $zip=addslashes($_POST['zip']);
    $gender=addslashes($_POST['gender']);
    $birthdate=addslashes($_POST['birthdate']);
    if($birthdate = '') {$birthdate = "1970-01-01";}
    $bio=addslashes($_POST['bio']);
    $advertiser_id=intval($_POST['advertiser_id']);
    $emailFlags=intval($_POST['email_flags']);
    if($_POST['send']){$send=1;}else{$send=0;}
    if($_POST['verify']){$verify=1;}else{$verify=0;}
    if($_POST['age_check']){$ageCheck=1;}else{$ageCheck=0;}
    if($_POST['bill_me']){$billMe=1;}else{$billMe=0;}
    if($_POST['veteran']){$veteran=1;}else{$veteran=0;}
    if($_POST['trusted']){$trusted=1;}else{$trusted=0;}
    if($_POST['senior']){$senior=1;}else{$senior=0;}
    $userID=$_POST['user_id'];
    
    if($userID==0)
    {
          $passwordClear=generate_random_string(8);
          $createdDT = date("Y-m-d H:i");
          $password=password_hash($passwordClear,PASSWORD_DEFAULT);
          $token = generate_random_string(32);
          $sql="INSERT INTO users (username, first, last, email, phone, password, token, verified, level, created_dt, temporary, 
          advertiser_id, street, city, state, zip, gender, birthdate, bio, bill_me, 13_check, veteran, email_flags, trusted, senior) VALUES 
          ('$userName', '$firstName', '$lastName', '$email', '$phone', '$password', '$token', $verify, 0,  '$createdDT', 0, 
          $advertiser_id, '$street', '$city', '$state', '$zip', '$gender', '$birthdate', '$bio', '$billMe', '$ageCheck', '$veteran', '$emailFlags', '$trusted', '$senior')";
          $dbInsert=dbinsertquery($sql);
          $userID=$dbInsert['insertid'];
          
          if($dbInsert['error']=='')
          {
              addUserAction($userID,'admin created');
              
              $message="An account has been created for you for ".SITE_NAME.".\r\n";
              if(!$verify)
              {
              $message.="In order to ensure that we protect your privacy, please confirm this account by clicking the activation link below.\r\n
              
              <a href='".SITE_URL."/account/confirm/?t=".$token."'>Click here to activate your account.</a>\r\n
              
              If the link does not work, copy and paste the following into your web browsers address bar:\r\n
              
              ".SITE_URL."/account/confirm/?t=".$token." \r\n ";
              } 
              
              $message.="Your account has been set up with the following credentials:\r\n
              Username: $userName\r\n
              Password: $passwordClear\r\n
              Thank you, \r\n
              ".SITE_NAME;
              
              if($send && $email!='')
              {
                  send_email($email,$subject,$message);
              }
          } 
    } else {
        
        $sql="UPDATE users SET username='$userName', first='$firstName', last='$lastName', phone='$phone', 
         email='$email', password='$password', verified='$verify', level='$level', advertiser_id='$advertiser_id', 
         street='$street', city='$city', state='$state', zip='$zip', gender='$gender', birthdate='$birthdate', 
         bio='$bio', 13_check='$ageCheck', veteran='$veteran', email_flags='$emailFlags', bill_me='$billMe', trusted='$trusted', senior = '$senior' WHERE id=$userID";  
        $dbUpdate=dbexecutequery($sql);
        
        
    }
    
    if(isset($_FILES) && $_FILES['mugshot']['tmp_name']!='')
    {
        $uploadOK=1;
        // Allow certain file formats
        $imageFileType =$_FILES['mugshot']['type'];
        if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
        && $imageFileType != "image/gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
            $uploadOK = 0;
        }
        if($uploadOK)
        {
            $target_dir = "../uploads/avatars/";
            $target_file = $target_dir . basename($_FILES["mugshot"]["name"]);
    
            if (move_uploaded_file($_FILES["mugshot"]["tmp_name"], $target_file)) {
                $sql="UPDATE users SET image='".addslashes($_FILES['mugshot']['name'])."' WHERE id=$userID";
                $dbUpdate=dbexecutequery($sql);
                $error=$dbUpdate['error'];
                
            } else {
                $error='File upload error.';
            }
        }
    }
    redirect("?action=list");
}

function list_users()
{
    $sql="SELECT id, name FROM advertisers ORDER BY name";
    $dbAdvertisers = dbselectmulti($sql);
    $advertisers[0]="No associated advertiser";
    if($dbAdvertisers['numrows']>0)
    {
        foreach($dbAdvertisers['data'] as $advertiser)
        {
            $advertisers[$advertiser['id']]=stripslashes($advertiser['name']);
        }
    }
    $sql="SELECT id, username, first, last, email, advertiser_id FROM users ORDER BY last";
   $dbUsers=dbselectmulti($sql);
   if($dbUsers['numrows']>0)
   {
       tableStart("<a href='?action=add'>Add new user</a>","ID,Username,Name,Email,Phone,Advertiser");
       foreach($dbUsers['data'] as $user)
       {
           print "<tr>";
           print "<td>".$user['id']."</td>";
           print "<td>".$user['username']."</td>";
           print "<td>".$user['first'].' '.$user['last']."</td>";
           print "<td>".$user['email']."</td>";
           print "<td>".$user['phone']."</td>";
           print "<td>".$advertisers[$user['advertiser_id']]."</td>";
          print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$user[id]' class='btn'>Edit</a>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=password&id=$user[id]'><i class='fa fa-lock'></i> Set Password</a></li>
            <li><a href='?action=delete&id=$user[id]' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
           print "</tr>";    
       }
       tableEnd($dbUsers);
   } else {
       print "<div class='alert alert-warning'><b>No Users</b> No users have been created yet.</div>";
   } 
}