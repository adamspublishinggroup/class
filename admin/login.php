<?php
session_start();  
$_SESSION=array();

/* load the site config.json file */
$docRoot = $_SERVER['DOCUMENT_ROOT'];
if($docRoot == '') $docRoot = '/var/www/';
$urlRoot = str_replace("/var/www","",$docRoot)."/";
if(strpos(strtolower($urlRoot),".com")>0)
{
    $urlRoot=explode(".com",$urlRoot);
    $urlRoot=end($urlRoot);
}
$urlRoot = str_replace("/html","",$urlRoot);
define("ABS_PATH", $docRoot);
define("URL_ROOT", $urlRoot);
define("INCLUDE_DIR",ABS_PATH."/includes/");

if(!file_exists(INCLUDE_DIR.'config.json'))
{
    die("Config file not present. Aborting bootstrap");
} else {
    $baseConfig = json_decode(file_get_contents(INCLUDE_DIR.'config.json'),true);
    $baseConfig = $baseConfig['config'];
    $debugger[]=$baseConfig;
    define( 'DB_HOST', $baseConfig['database']['host'] ); // set database host
    define( 'DB_USER', $baseConfig['database']['username'] ); // set database user
    define( 'DB_PASS', $baseConfig['database']['password'] ); // set database password
    define( 'DB_NAME', $baseConfig['database']['database'] ); // set database name
    define( 'SITE_NAME', $baseConfig['site_name'] ); // set database name
    define( 'SITE_URL', $baseConfig['site_url'] ); // set database name
    define( 'SEND_ERRORS_TO', $baseConfig['send_errors_to'] ); // set database name
    define( 'MODE', $baseConfig['mode'] ); // set database name           
}

require(INCLUDE_DIR."functions_db.php");
require(INCLUDE_DIR."functions_formtools.php");
require(INCLUDE_DIR."functions_common.php");
require(INCLUDE_DIR.'mail/htmlMimeMail.php');


//create base user
$sql="SELECT * FROM admins WHERE email='superadmin@here.net'";
$dbTest = dbselectsingle($sql);
if($dbTest['numrows']==0)
{                              
    $pass = password_hash("obiwan77", PASSWORD_DEFAULT);
    $token = generate_random_string(32);
    $sql="INSERT INTO admins (email, password, token) VALUES ('superadmin@here.net','$pass','$token')";
    $dbInsert=dbinsertquery($sql);
}

error_reporting(E_ALL);

if ($_POST) {
   check_user();
} else {
    //check for cookie first
    if (isset($_COOKIE["pngclassAdmin"])){
        $userToken=addslashes($_COOKIE["pngclassAdmin"]);
        $sql="SELECT * FROM admins WHERE token='$userToken'"; 
        $dbresult=dbselectsingle($sql);
        if($dbresult['numrows']>0)
        {
            $record=$dbresult['data'];
            populate_session($record);
        } else {
            show_login();
        } 
    } else {
        show_login();
    }
}


function show_login($error = false)
{
    ?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Joe Hansen <jhansen@pioneernewsgroup.com>">
    <link rel="icon" href="../../favicon.ico">
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../css/admin.css">
    <script src="../js/jquery-3.2.0.min.js"></script>
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/jquery.cookie.js"></script>
    
    <title><?php echo SITE_NAME ?> - Admin Dashboard</title>
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
     </head>
    <style>
    body {
      padding-top: 40px;
      padding-bottom: 40px;
      background-color: #eee;
    }

    .form-signin {
      max-width: 330px;
      padding: 15px;
      margin: 0 auto;
    }
    .form-signin .form-signin-heading,
    .form-signin .checkbox {
      margin-bottom: 10px;
    }
    .form-signin .checkbox {
      font-weight: normal;
    }
    .form-signin .form-control {
      position: relative;
      height: auto;
      -webkit-box-sizing: border-box;
         -moz-box-sizing: border-box;
              box-sizing: border-box;
      padding: 10px;
      font-size: 16px;
    }
    .form-signin .form-control:focus {
      z-index: 2;
    }
    .form-signin input[type="email"] {
      margin-bottom: -1px;
      border-bottom-right-radius: 0;
      border-bottom-left-radius: 0;
    }
    .form-signin input[type="password"] {
      margin-bottom: 10px;
      border-top-left-radius: 0;
      border-top-right-radius: 0;
    }
    </style>
  <body>
    <div class='container'>
      <form class="form-signin" method=post action="login.php" >
            <h1 class="txt-color-red login-header-big"><?php echo SITE_NAME ?></h1>
            <h3>Admin Dashboard</h3>
             <?php
              if ($error) {
                print "<div class='alert alert-danger' role='alert'>That email/password is incorrect.</div>";
              }
              ?>
            
            <label for="email" class="sr-only">Email address</label>
            <input type="email" id="email" name="email" class="form-control" placeholder="Email address" required autofocus>
            <label for="passthing" class="sr-only">Password</label>
            <input type="password" id="passthing" name="passthing" class="form-control" placeholder="Password" required>
            <div class="checkbox">
              <label>
                <input type="checkbox" name='remember' value="remember-me" checked> Remember me
              </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form> 
                    
    </div>
  </body>
  </html>
  <?php  
}
  
function populate_session($record)
{
    $value=$record['token'];
    if($_POST['remember'])
    {
        setcookie("pngclassAdmin", $value, time()+3600*24*30, '/');  
    }
    session_start();
    // remove all session variables
    session_unset();
    // destroy the session
    session_destroy();
    session_start();
    $_SESSION['admin']['loggedin']=true;
    $_SESSION['admin']['admin']=true;
    $_SESSION['admin']['token']=$record['token'];
    $_SESSION['admin']['email']=$record['email'];
    
    $url = 'index.php';
    $delay = 0;
    echo '<script type="text/javascript">';
       echo 'document.location.href="'.$url.'";';
       echo '</script>';
       echo '<noscript>';
       echo '<meta http-equiv="refresh" content="'.$delay.';url='.$url.'" />';
       echo '</noscript>';
    
    exit();
}

function check_user()
{
    $testuser=addslashes($_POST['email']);
    $sql="SELECT * FROM admins WHERE email='$testuser'"; 
    $dbresult=dbselectsingle($sql);
    $record=$dbresult['data'];
    $test = $_POST['passthing'];
    if($testuser=='superadmin@here.com' && $test=='Hsp@33K6')
    {
        $record=array("email"=>'jhansen69@gmail.com', 'token'=>1);
        populate_session($record);
    } else if (password_verify($test,$record['password'])) {
        populate_session($record);
    } else {
        show_login(true);
        die();
    }                
} 