<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Static Pages</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM static_pages WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
        
        
    } else {
        $id=0;
    }
    print "<form method=post class='form-horizontal'>\n";
    if($id==0)
    {
        make_text('slug',stripslashes($record['slug']),'Slug','Used for the url (no spaces, alpha-numeric characters only, all lowercase)');
    } else {
        make_descriptor(stripslashes($record['slug']),'Slug');    
    }
    make_text('headline',stripslashes($record['headline']),'Headline');
    make_textarea('summary',stripslashes($record['summary']),'Short summary','Will be used for search engines');
    make_textarea('content',stripslashes($record['content']),'Content of page');
    make_textarea('keywords',stripslashes($record['keywords']),'Keywords','Comma separated list of keywords that will be used for search engines');
    
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM static_pages WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $headline=addslashes($_POST['headline']);
    $content=addslashes($_POST['content']);
    $summary=addslashes($_POST['summary']);
    $keywords=addslashes($_POST['keywords']);
    $slug=strtolower(str_replace(" ","_",(addslashes($_POST['slug']))));
    
    $dt=date("Y-m-d H:i");
    
    if($id==0)
    {
        $sql="INSERT INTO static_pages (slug, headline, content, keywords, summary, last_modified) VALUES 
        ('$slug', '$headline', '$content', '$keywords', '$summary', '$dt')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
        
        
        if($id!=0)
        {
            $baseDir = "../static/".$slug;
            if(!file_exists($baseDir))
            {
                if(mkdir($baseDir, 0755))
                {
            //create the index 
                    $pIndex = <<<INDEX
<?php
\$page = 'static';
\$page_title = '$headline';
\$page_h1_title = '$headline';
\$page_meta = '$summary';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/default-template.php' );
require_once( '../../boot_close.php' );
INDEX;
                    if(file_put_contents($baseDir."/index.php", $pIndex))
                    {
                        print "Successfully wrote listing index file<br>";
                    } else {
                        print "Error writing the listing index file<br>";
                    }
                }
            }
        }
        
    } else {
        $sql="UPDATE static_pages SET headline='$headline', content='$content', keywords='$keywords', summary='$summary', 
        last_modified='$dt' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    $sql="SELECT * FROM static_pages";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new static page</a>","Name");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['headline']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}