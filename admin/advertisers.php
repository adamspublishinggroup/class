<?php
include "bootstrap.php";

function page()
{
	?>
	<div class="page-header">
		<h1>Advertisers</h1>
	</div>
	<?php
	if ($_POST) {
		$action = $_POST['submit'];
	} else {
		$action = $_GET['action'];
	}

	switch ($action) {
	case "add": edit_advertiser(); break;
	case "edit": edit_advertiser(); break;
	case "Save Advertiser": save_advertiser(); break;
	case "delete": delete_advertiser(); break;
	default: list_advertisers(); break;
	}
}

function edit_advertiser()
{
	$id = intval($_GET['id']);
	if ($id != 0) {
		$sql = "SELECT * FROM advertisers WHERE id=$id";
		$dbAdvertiser = dbselectsingle($sql);
		$record = $dbAdvertiser['data'];
	}

	print "<form method=post enctype='multipart/form-data' class='form-horizontal'>\n";
	make_text('name', stripslashes($record['name']) , 'Business Name');
	make_text('slogan', stripslashes($record['slogan']) , 'Business Slogan', 'Added to the rail under the business name.');
	if ($id == 0) {
		make_text('slug', stripslashes($record['slug']) , 'Slug', 'Used for their url (no spaces, alpha-numeric characters only, all lowercase)');
	} else {
		make_descriptor(stripslashes($record['slug']) , 'Slug');
	}
	make_text('keywords', stripslashes($record['keywords']) , 'Keywords', 'Added to the page header for SEO');
	make_text('meta_desc', stripslashes($record['meta_desc']) , 'Page Description', 'Added to the page header for SEO');
	make_text('url', stripslashes($record['url']) , 'Website');
	make_text('facebook', stripslashes($record['facebook']) , 'Facebook');
	make_text('twitter', stripslashes($record['twitter']) , 'Twitter');
	make_text('email', stripslashes($record['email']) , 'Email', 'Contact email (will be shown on advertiser page)');
	make_text('street', stripslashes($record['street']) , 'Street');
	make_text('city', stripslashes($record['city']) , 'City');
	make_state('state', stripslashes($record['state']) , 'State');
	make_text('zip', stripslashes($record['zip']) , 'Zip');
	make_phone('phone', stripslashes($record['phone']) , 'Phone');
	make_textarea('about', stripslashes($record['about']) , 'About Business');
	make_checkbox('disp_ads_first', $record['disp_ads_first'], 'Display Ad Priority', 'Check if you\'d like to give Display Ads priority over regular Listings');
	make_checkbox('non_profit', $record['non_profit'], 'Non-Profit', 'Check if is a non-profit account and should receive the non-profit discount specified in the site settings');
	make_checkbox('active', $record['active'], 'Active', 'Check if is an active account');
	if ($record['logo'] != '') {
		$file = 'Current file: /uploads/' . $record['logo'];
		$file.= '<div class="config-file-preview" style="background-image:url(' . $record['logo'] . ');"></div>';
	}
	make_file('logo', 'Logo', 'JPEG, 300px wide x 300px high -ish. ' . $file, $record['logo']);
	make_hidden('advertiser_id', $id);
	make_submit('submit', 'Save Advertiser');
	print "</form>\n";
}

function save_advertiser()
{
	// get post vars
	$name = addslashes($_POST['name']);
	$slogan = addslashes($_POST['slogan']);
	$url = addslashes($_POST['url']);
	$facebook = addslashes($_POST['facebook']);
	$twitter = addslashes($_POST['twitter']);
	$email = addslashes($_POST['email']);
	$street = addslashes($_POST['street']);
	$city = addslashes($_POST['city']);
	$state = addslashes($_POST['state']);
	$zip = addslashes($_POST['zip']);
	$about = addslashes($_POST['about']);
	$phone = addslashes($_POST['phone']);
	$keywords = addslashes($_POST['keywords']);
	$meta_desc = addslashes($_POST['meta_desc']);
	if ($_POST['non_profit']) { $non_profit = 1; } else { $non_profit = 0; }
	if ($_POST['slug'] != '') { $slug = $_POST['slug']; } else { $slug = $name; }
	if ($_POST['disp_ads_first']) { $dispAdsFirst = 1; } else { $dispAdsFirst = 0; }
	if ($_POST['active']) { $active = 1; } else { $active = 0; }
	$advertiserID = $_POST['advertiser_id'];

	// create additional vars
	$slug = slugify($slug);
	$geo = geocode($street . "," . $city . " " . $state . " " . $zip);
	$lat = $geo['lat'];
	$lon = $geo['lon'];
	$target_dir = '../uploads/alvertwisers/';

	// insert or update all fields except file upload
	$uploadOK = false;
	if ($advertiserID == 0) {
		$sql = "INSERT INTO advertisers (name, url, email, facebook, twitter, street, city, state, zip, phone, about, 
					lat, lon, active, slogan, slug, keywords, meta_desc, non_profit, disp_ads_first) VALUES 
					('$name', '$url', '$email', '$facebook', '$twitter', '$street', '$city', '$state', '$zip', '$phone', '$about', 
					'$lat', '$lon', '$active', '$slogan', '$slug', '$keywords', '$meta_desc', '$non_profit', '$dispAdsFirst')";
		$dbInsert = dbinsertquery($sql);
		$advertiserID = $dbInsert['insertid'];
		if ($dbInsert['error'] == '') {
			$uploadOK = true;
		}
	} else {
		$sql = "UPDATE advertisers SET name='$name', url='$url', email='$email', facebook='$facebook', twitter='$twitter', street='$street', 
				city='$city', state='$state', zip='$zip', phone='$phone', about='$about', lat='$lat', lon='$lon', active='$active', 
				slogan='$slogan', keywords='$keywords', meta_desc='$meta_desc', non_profit='$non_profit', disp_ads_first='$dispAdsFirst' WHERE id=$advertiserID";
		$dbUpdate = dbexecutequery($sql);
		if ($dbUpdate['error'] == '') {
			$uploadOK = true;
		}
	}

	// update record with file upload
	if (count($_FILES) > 0 && $uploadOK) {
		foreach($_FILES as $field => $file) {
			if ($file['name'] != '') {

				// Allow certain file formats
				$imgType = $file['type'];
				if ($imgType != "image/jpg" && $imgType != "image/png" && $imgType != "image/jpeg" && $imgType != "image/gif") {
					echo "Sorry, only JPG, JPEG, PNG and GIF files are allowed. You uploaded a $imgType file.";
					$uploadOK = false;
				}

				if ($uploadOK) {
					// build vars and names
					$filePath = '/uploads/alvertwisers/';
					$ext = pathinfo($file['name'], PATHINFO_EXTENSION);
					if (trim($name) > '') {
						$fileName = slugify($name);
						$fileName = substr($fileName, 0, 20);
					} else {
						$fileName = date('Ymd');
					}
					$fileName = $advertiserID . '-' . $fileName . '.' . $ext;
					$target_file = $target_dir . $fileName;
					$dbName = $filePath . $fileName;

					// upload
					if (move_uploaded_file($file["tmp_name"], $target_file)) {
						$sql = "UPDATE advertisers SET $field='$dbName' WHERE id=$advertiserID";
						$dbUpdate = dbinsertquery($sql);
						$error = $dbUpdate['error'];
					} else {
						$error = 'File upload error.';
					}
				}
			}
		}
	}

	if ($advertiserID != 0) {
		$baseDir = "../advertisers/" . $slug;
		if (!file_exists($baseDir)) {
			if (mkdir($baseDir, 0755)) {
				print "Made directory called $baseDir<br />";

				// create the index

				$pIndex = <<<INDEX
<?php
\$page = 'advertiser-listing';
require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/advertiser-listings-template.php' );
require_once( '../../boot_close.php' );
INDEX;
				if (file_put_contents($baseDir . "/index.php", $pIndex)) {
					print "Successfully wrote listing index file<br />";
				} else {
					print "Error writing the listing index file<br />";
				}

				// check for detail directory, or create

				if (!file_exists($baseDir . "/detail/")) {
					mkdir($baseDir . "/detail/", 0755);
					$pDetail = <<<DETAIL
<?php
\$page = 'advertiser-detail';
require_once( '../../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/advertiser-detail-template.php' );
require_once( '../../../boot_close.php' );
DETAIL;
					file_put_contents($baseDir . "/detail/index.php", $pDetail);
				}
			} else {
				print "Failed to create directory $baseDir/detail";
			}
		} else {
			print "Directory existed";
		}
	}

	redirect("?action=list");
}

function delete_advertiser()
{
	$id = intval($_GET['id']);
	if ($id != 0) {
		$sql = "SELECT * FROM advertisers WHERE id=$id";
		$dbAdvertiser = dbselectsingle($sql);
		if ($dbAdvertiser['numrows'] > 0) {
			$record = $dbAdvertiser['data'];
			$fileName = $record['logo'];
			if(unlink('..'.$fileName)){
				$sql="DELETE FROM advertisers WHERE id=$id";
				$dbDelete=dbexecutequery($sql);
			}
		}
	}

	redirect("?action=list");
}

function list_advertisers()
{
	$sql = "SELECT id, name, logo FROM advertisers ORDER BY name";
	$dbAdvertisers = dbselectmulti($sql);
	if ($dbAdvertisers['numrows'] > 0) {
		tableStart("<a href='?action=add'>Add new advertiser</a>", "ID,Name,Logo");
		foreach($dbAdvertisers['data'] as $adv) {
			print "<tr>";
			print "<td>" . $adv['id'] . "</td>";
			print "<td>" . $adv['name'] . "</td>";
			print "<td><div class='config-file-preview' style='background-image:url(" . $adv['logo'] . ");'></div></td>";
			print "<td>
				<div class='btn-group'>
					<a href='?action=edit&id=$adv[id]' class='btn'>Edit</a>
					<button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
						<span class='caret'></span>
						<span class='sr-only'>Toggle Dropdown</span>
					</button>
					<ul class='dropdown-menu'>
						<li><a href='?action=delete&id=$adv[id]' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
					</ul>
				</div>
				</td>";
			print "</tr>";
		}

		tableEnd($dbAdvertisers);
	} else {
		print "<div class='alert alert-warning'><b>No Advertiser</b> No advertisers have been created yet. <a href='?action=new'>Create one now</a></div>";
	}
}

function slugify($text)
{
	$text = preg_replace('~[^\pL\d]+~u', '-', $text); // replace non letter or digits by -
	$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text); // transliterate
	$text = preg_replace('~[^-\w]+~', '', $text); // remove unwanted characters
	$text = trim($text, '-'); // trim
	$text = preg_replace('~-+~', '-', $text); // remove duplicate -
	$text = strtolower($text); // lowercase
	if (empty($text)) { return 'n-a'; } // if no text is left
	return $text;
}