<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Promotional Codes</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $categories = categories(0);
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM promos WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
        $cats = explode(",",$record['exclude_categories']);
    } else {
        $cats=array();
        $id=0;
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('promo_name',stripslashes($record['promo_name']),'Name');
    make_text('promo_code',stripslashes($record['promo_code']),'Code','Must be alphanumeric only, one word');
    make_number('free_days_print',stripslashes($record['free_days_print']),'Free Days (Print)','Blank or zero to not apply, otherwise this will be the number of free print publication dates granted.');
    make_checkbox('free_online',$record['free_online'],'Free Online','If checked, online-only ad will be free.');
    make_checkbox('free_ad',$record['free_ad'],'Free Ad','If checked, the ad will be totally free.');
    make_checkbox('free_bold',$record['free_bold'],'Free Bold','If checked, bolding will not be charged.');
    make_number('free_pictures',stripslashes($record['free_pictures']),'Free Pictures','Blank or zero to not apply, otherwise this will be the number of free pictures granted.');
    make_number('free_words',stripslashes($record['free_words']),'Free Words','Blank or zero to not apply, otherwise this will be the number of free words granted beyond the normal base.');
    make_number('discount',stripslashes($record['discount']),'Overall Discount','Blank or zero to not apply, otherwise will be a total percentage discount applied to the order. (Enter in whole numbers, like 50 for 1/2 off)');
    ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Exclude Categories</label>
        <div class="col-sm-10">
            <small>Select one or more categories that this promo code DOES NOT apply to</small><br>
            <select id="categories" name="categories[]" multiple="multiple" style='width:80%;'>
                <?php
                if( count( $categories ) > 0 ) {
                    foreach( $categories as $cat ) {
                        if(in_array($cat['category_id'],$cats)){$checked='selected';}else{$checked='';}
                        print "<option value='$cat[category_id]' $checked>$cat[category_name]</option>\n";
                        if( count( $cat['subcats'] ) > 0 ) {
                            foreach( $cat['subcats'] as $subcat ) {
                                if(in_array($subcat['category_id'],$cats)){$checked='selected';}else{$checked='';}
                                print "<option value='$subcat[category_id]' $checked>&nbsp;&nbsp;&nbsp;&nbsp;$subcat[category_name]</option>\n";
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
    </div>
    
    <?php
    make_checkbox('active',$record['active'],'Active','If checked, users will be able to apply this promo code.');
    make_hidden('id',$record['id']);
    make_submit('submit','Save');
    print "</form>\n";
    ?>
    <script type="text/javascript">
        $("#categories").select2();
    </script>
    <?php
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM promos WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    //@TODO need to clean up ad records when metadata tags are deleted!
    
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $promo_name=addslashes($_POST['promo_name']);
    $promo_code=addslashes($_POST['promo_code']);
    $free_days_print=intval($_POST['free_days_print']);
    $free_pictures=intval($_POST['free_pictures']);
    $free_words=intval($_POST['free_words']);
    $discount=intval($_POST['discount']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($_POST['free_ad']){$free_ad=1;}else{$free_ad=0;}
    if($_POST['free_online']){$free_online=1;}else{$free_online=0;}
    if($_POST['free_bold']){$free_bold=1;}else{$free_bold=0;}
    $cats = implode(",",$_POST['categories']);
 
    if($id==0)
    {
        $sql="INSERT INTO promos (promo_name, promo_code, active, free_days_print, free_online, free_bold, free_words, free_ad,  
        free_pictures, discount, exlude_categories) VALUES ('$promo_name', '$promo_code', '$active', '$free_days_print', 
        '$free_online', '$free_bold', '$free_words', '$free_ad', '$free_pictures', '$discount', '$cats')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE promos SET promo_name='$promo_name', promo_code='$promo_code', active='$active', 
        free_days_print='$free_days_print', free_online='$free_online', free_ad='$free_ad', 
        free_pictures='$free_pictures', discount='$discount', exlude_categories='$cats', free_bold='$free_bold', 
        free_words='$free_words' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    global $types;
    $sql="SELECT * FROM promos";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new promo</a>","Name,Code,Active");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['promo_name']);
            $code=stripslashes($record['promo_code']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>$code</td>\n";
            print "<td>".($record['active'] ? "Yes" : "No")."</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}