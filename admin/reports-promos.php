<?php
include "bootstrap.php";
function page()
{
   global $outputTypes;
    if($_POST)
    {
        $startDate = $_POST['startDate'];
        $endDate = $_POST['endDate'];
    } else {
        //set start date to first day of the month previous to right now (full month for report)
        $lastMonth = date("Y-m-d",strtotime("-1 month"));
        $startDate = date("m",strtotime($lastMonth))."/1/".date("Y",strtotime($lastMonth));
        $endDate = date("m",strtotime($lastMonth))."/".date("t",strtotime($lastMonth))."/".date("Y",strtotime($lastMonth));
    }
    ?>
    <div class="page-header">
      <h1>Promo Codes Report</h1>
    </div>
    <div class='col-md-9'>
        <form method=post class='form-horizontal'>      
        <div class="form-group">
            <label for="startDate" class="col-sm-2 control-label">Find ads in this range</label>
            <label for="startDate" class="col-sm-1 control-label">Start Date</label>
            <div class="col-sm-2">
                <div class="input-group date" id="startDate" style='width: 150px;'>
                    <input type="text" name="startDate" value='<?= $startDate ?>' />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <label for="endDate" class="col-sm-1 control-label">End Date</label>
            <div class="col-sm-2">
                <div class="input-group date" id="endDate"  style="width:150px;">
                    <input type="text" name="endDate" value='<?= $endDate ?>' />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
    <?php
        //get a list of all promos
        $sql="SELECT id, promo_name FROM promos ORDER BY promo_name";
        $dbPromos = dbselectmulti($sql);
        $promos[0]="Show all promos";
        if($dbPromos['numrows']>0)
        {
            foreach($dbPromos['data'] as $promo)
            {
                $promos[$promo['id']]=stripslashes($promo['promo_name']);
            }
        }
        make_select('promo_id',$promos[$_POST['promo_id']],$promos,'Promo','Select a promo to find ads for');
        make_select('report_output',$outputTypes[$_POST['report_output']],$outputTypes,'Output','How do you want the results?');
        make_submit('submit','Run Report');
    ?>
      </form>
  </div>
  <div class='col-md-3'>
  <h4>Usage</h4>
  <p>This report is built to show all ads using a promo code in the given date range, or by the specified promo code.</p>
  </div>
<script type="text/javascript">
    $(function () {
        $('#startDate').datetimepicker({
            format: 'MM/DD/YYYY'
        });
        $('#endDate').datetimepicker({
            format: 'MM/DD/YYYY',
            useCurrent: false //Important! See issue #1075
        });
        $("#startDate").on("dp.change", function (e) {
            $('#endDate').data("DateTimePicker").minDate(e.date);
        });
        $("#endDate").on("dp.change", function (e) {
            $('#startDate').data("DateTimePicker").maxDate(e.date);
        });
    });
</script> 
   <?php
   if($_POST)
   {
       generateReport($_POST['report_output']); 
   } 
   
}

function generateReport($type)
{
    global $adStatuses;
    include("../includes/class.report.php");
    $report = new Report();
    
    $startDate = date("Y-m-d",strtotime($_POST['startDate']));
    $endDate = date("Y-m-d",strtotime($_POST['endDate']));
    
    $promoID = intval($_POST['promo_id']);
    
    //what do we want in the report?
    
    //lets go with user first+last name, email, ad start date, ad end date, ad headline, ad total cost, ad status, ad published, ad paid (admin or bill me?)
    $header = array('Promo','Ad ID', 'Placed By', 'Start Date', 'End Date', 'Headline', 'Cost', 'Status', 'Published', 'Paid');
    
    $report->setHeaders($header);
    
    $sql="SELECT A.*, B.first, B.last, B.email, C.promo_name FROM ads A, users B, promos C WHERE A.user_id = B.id AND A.promo_code=C.promo_code AND C.id".($promoID==0 ? ">0" : "=$promoID")." AND start_date<='$endDate' AND end_date>='$startDate' ORDER BY C.promo_name, start_date ASC";
    $dbAds = dbselectmulti($sql);
    
    $total = 0;
    $count = 0;
    if($dbAds['numrows']>0)
    {
        foreach($dbAds['data'] as $ad)
        {
            $id = $ad['id'];
            $code = $ad['promo_code'];
            $placed = $ad['first'].' '.$ad['last'].' - '.$ad['email'];
            $start = date("m/d/Y",strtotime($ad['start_date']));
            $end = date("m/d/Y",strtotime($ad['end_date']));
            $headline = stripslashes($ad['headline']);
            $cost = money_format('%(#4n', $ad['ad_total']);
            $total+=$ad['ad_total'];
            $count++;
            $status = $adStatuses[$ad['status']];
            $published = ($ad['published'] ? 'Yes' : 'No');
            if($ad['admin_paid'])
            {
                $paid = "Admin free";
            } elseif($ad['free_ad'])
            {
                $paid = "Free ad";
            } elseif($ad['bill_me'] > 0)
            {
                if($ad['ad_paid_dt']!='')
                {
                    $paid = "Billed and paid";
                } elseif($ad['billed'])
                {
                    $paid = "Bill sent";
                } else {
                    $paid = "Need to bill customer";
                }
            } elseif($ad['bill_advertiser'] > 0)
            {
                if($ad['ad_paid_dt']!='')
                {
                    $paid = "Billed and paid";
                } elseif($ad['billed'])
                {
                    $paid = "Bill sent";
                } else {
                    $paid = "Need to bill advertiser";
                }
            }elseif($ad['ad_paid_dt'])
            {
                $paid = "Ad paid";
            } else {
                $paid = "Not paid";
            }
            
            $record = array($code, $id, $placed, $start, $end, $headline, $cost, $status, $published, $paid);
            $report->addRecord($record);
        }
        $data[] = array('Total Ads'=>$count);
        $data[] = array('Total Revenue'=>money_format('%(#4n', $total));
        $report->addSummary($data);
    }
    $message = $report->output($type);
    print $message;
}