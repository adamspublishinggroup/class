<?php
include "bootstrap.php";
function page()
{
    if($_POST)
    {
        $print_dow = implode(",",$_POST['print_dow']);
        if($_POST['allow_online_only']){$allow_online_only = 1;}else{$allow_online_only=0;}
        if($_POST['nested_categories']){$nested_categories = 1;}else{$nested_categories=0;}
        if($_POST['show_empty_categories']){$show_empty_categories = 1;}else{$show_empty_categories=0;}
        if($_POST['allow_edit_charged_ad']){$allow_edit_charged_ad = 1;}else{$allow_edit_charged_ad=0;}
        
        if($_POST['rich_ad_text']){$rich_ad_text = 1;}else{$rich_ad_text=0;}
        if($_POST['allow_bill_me']){$allow_bill_me = 1;}else{$allow_bill_me=0;}
        if($_POST['include_headline_adcount']){$include_headline_adcount = 1;}else{$include_headline_adcount=0;}
        if($_POST['veteran_free']){$veteran_free = 1;}else{$veteran_free=0;}
        if($_POST['require_new_ad_acknowledgement']){$require_new_ad_acknowledgement = 1;}else{$require_new_ad_acknowledgement=0;}
        if($_POST['require_approval']){$require_approval = 1;}else{$require_approval=0;}
        if($_POST['allow_trusted_ads']){$allow_trusted_ads = 1;}else{$allow_trusted_ads=0;}
        if($_POST['print_allow_styles']){$print_allow_styles = 1;}else{$print_allow_styles=0;}
        if($_POST['online_text']){$online_text = 1;}else{$online_text=0;}
        if($_POST['ad_geographic_field']){$ad_geographic_field = 1;}else{$ad_geographic_field=0;}
        if($_POST['require_headline']){$require_headline = 1;}else{$require_headline=0;}
        if($_POST['require_body']){$require_body = 1;}else{$require_body=0;}
        if($_POST['require_geo']){$require_geo = 1;}else{$require_geo=0;}
        if($_POST['require_tags']){$require_tags = 1;}else{$require_tags=0;}
        if($_POST['require_category']){$require_category = 1;}else{$require_category=0;}
        if($_POST['enable_pageflip']){$enable_pageflip = 1;}else{$enable_pageflip=0;}
        
        if($_POST['show_attention_getters']){$show_attention_getters = 1;}else{$show_attention_getters=0;}
        if($_POST['show_top_bottom_placement']){$show_top_bottom_placement = 1;}else{$show_top_bottom_placement=0;}

        $max_print_photos = intval($_POST['max_print_photos']);
        $max_online_photos = intval($_POST['max_online_photos']);
        
        $veteran_free_count = intval($_POST['veteran_free_count']);
        $default_state = addslashes($_POST['default_state']);
        
        $show_advertisers_rail = addslashes($_POST['show_advertisers_rail']);
        $show_single_advertiser_rail = addslashes($_POST['show_single_advertiser_rail']);
        $show_most_popular = addslashes($_POST['show_most_popular']) ;
        $show_featured = addslashes($_POST['show_featured']);
        $show_maps = addslashes($_POST['show_maps']);
         
        $site_name = addslashes($_POST['site_name']);
        $site_url = addslashes($_POST['site_url']);
        $send_errors_to = addslashes($_POST['send_errors_to']);
        $mail_from_address = addslashes($_POST['mail_from_address']);
        $mail_from_subject = addslashes($_POST['mail_from_subject']);
        $mail_from_name = addslashes($_POST['mail_from_name']);
        $contact_email = addslashes($_POST['contact_email']);
        $contact_phone = addslashes($_POST['contact_phone']);
        $email_abuse_limit = intval($_POST['email_abuse_limit']);
        $stripe_test_public = addslashes($_POST['stripe_test_public']);
        $stripe_test_secret = addslashes($_POST['stripe_test_secret']);
        $stripe_live_public = addslashes($_POST['stripe_live_public']);
        $stripe_live_secret = addslashes($_POST['stripe_live_secret']);
        $facebook_app_id = addslashes($_POST['facebook_app_id']);
        $google_map_key = addslashes($_POST['google_map_key']);
        $google_analytics = addslashes($_POST['google_analytics']);
        $ad_order_mode = addslashes($_POST['ad_order_mode']);
        
        $recaptcha_site = addslashes($_POST['recaptcha_site']);
        $recaptcha_secret = addslashes($_POST['recaptcha_secret']);
        
        $google_dfp_key = addslashes($_POST['google_dfp_key']);
        $timezone = addslashes($_POST['timezone']);
        $advertisers_position = intval($_POST['advertisers_position']);
        $advertiser_position = intval($_POST['advertiser_position']);
        $most_popular_position = intval($_POST['most_popular_position']);
        $featured_position = intval($_POST['featured_position']);
        $maps_position = intval($_POST['maps_position']);
        $meta_mapping_id = intval($_POST['meta_mapping_id']);
        $offsite_ad_order = addslashes($_POST['offsite_ad_order']);
        $facebook_secret = addslashes($_POST['facebook_secret']);
        $facebook_token = addslashes($_POST['facebook_token']);
        $bill_me_fee = addslashes($_POST['bill_me_fee']);
        $bill_days_out = addslashes($_POST['bill_days_out']);
        
        $font_header = addslashes($_POST['font_header']);
        $font_body = addslashes($_POST['font_body']);
        $theme_aura = addslashes($_POST['theme_aura']);
        $color_background = addslashes($_POST['color_background']);
        $color_text = addslashes($_POST['color_text']);
        $color_highlight = addslashes($_POST['color_highlight']);
        $billing_street = addslashes($_POST['billing_street']);
        $billing_city = addslashes($_POST['billing_city']);
        $billing_state = addslashes($_POST['billing_state']);
        $billing_zip = addslashes($_POST['billing_zip']);
        $billing_interface = addslashes($_POST['billing_interface']);
        $authorize_net_secret = addslashes($_POST['authorize_net_secret']);
        $authorize_net_key = addslashes($_POST['authorize_net_key']);
        $authorize_net_login_id = addslashes($_POST['authorize_net_login_id']);
        $authorize_net_client_key = addslashes($_POST['authorize_net_client_key']);
        
        $facebook_add_on = addslashes($_POST['facebook_add_on']);
        $twitter_add_on = addslashes($_POST['twitter_add_on']);
        $featured_add_on = addslashes($_POST['featured_add_on']);
        $featured_scroller_add_on = addslashes($_POST['featured_scroller_add_on']);
        $preview_disclaimer = addslashes($_POST['preview_disclaimer']);
        $checkout_message = addslashes($_POST['checkout_message']);
        
        $print_column_width = addslashes($_POST['print_column_width']);
        $preview_font = addslashes($_POST['preview_font']);
        $preview_font_size = addslashes($_POST['preview_font_size']);
        $preview_line_height = addslashes($_POST['preview_line_height']);
        $preview_letter_spacing = addslashes($_POST['preview_letter_spacing']);
        $print_uppercase_first = intval($_POST['print_uppercase_first']);
        $print_bold_first = intval($_POST['print_bold_first']);
        $senior_discount = intval($_POST['senior_discount']);
        $non_profit_discount = intval($_POST['non_profit_discount']);
        $send_new_ad_alert = intval($_POST['send_new_ad_alert']);
        $default_run_length = intval($_POST['default_run_length']);
        $display_run_length_mode = addslashes($_POST['display_run_length_mode']);
        $date_selection_mode = addslashes($_POST['date_selection_mode']);
        $include_headline_message = addslashes($_POST['include_headline_message']);
        $categories_message = addslashes($_POST['categories_message']);
        $max_images = intval($_POST['max_images']);

        $sql="UPDATE config SET 
                        site_name='$site_name', 
                        site_url='$site_url', 
                        send_errors_to='$send_errors_to', 
                        mail_from_address='$mail_from_address', 
                        facebook_secret='$facebook_secret', 
                        facebook_token='$facebook_token', 
                        bill_days_out='$bill_days_out', 
                        require_new_ad_acknowledgement='$require_new_ad_acknowledgement', 
                        mail_from_subject='$mail_from_subject', 
                        mail_from_name='$mail_from_name', 
                        contact_email='$contact_email', 
                        contact_phone='$contact_phone', 
                        email_abuse_limit='$email_abuse_limit', 
                        font_header='$font_header', 
                        font_body='$font_body',
                        theme_aura='$theme_aura', 
                        color_background='$color_background', 
                        authorize_net_login_id = '$authorize_net_login_id', 
                        authorize_net_key='$authorize_net_key', 
                        authorize_net_secret='$authorize_net_secret', 
                        billing_interface='$billing_interface', 
                        authorize_net_client_key='$authorize_net_client_key', 
                        require_approval='$require_approval', 
                        allow_trusted_ads='$allow_trusted_ads', 
                        color_text='$color_text', 
                        color_highlight='$color_highlight', 
                        stripe_test_public='$stripe_test_public', 
                        stripe_test_secret='$stripe_test_secret', 
                        stripe_live_public='$stripe_live_public', 
                        stripe_live_secret='$stripe_live_secret', 
                        facebook_app_id='$facebook_app_id', 
                        google_map_key='$google_map_key', 
                        google_analytics='$google_analytics', 
                        recaptcha_site='$recaptcha_site', 
                        recaptcha_secret='$recaptcha_secret', 
                        twitter_add_on='$twitter_add_on', 
                        print_column_width = '$print_column_width', 
                        preview_font='$preview_font',
                        preview_font_size='$preview_font_size',
                        preview_line_height='$preview_line_height',
                        preview_letter_spacing='$preview_letter_spacing',
                        print_allow_styles='$print_allow_styles',
                        default_run_length = '$default_run_length', 
                        allow_online_only='$allow_online_only', 
                        nested_categories='$nested_categories', 
                        print_dow='$print_dow', 
                        facebook_add_on='$facebook_add_on', 
                        preview_disclaimer='$preview_disclaimer', 
                        print_uppercase_first='$print_uppercase_first', 
                        google_dfp_key='$google_dfp_key', 
                        show_advertisers_rail='$show_advertisers_rail', 
                        show_single_advertiser_rail='$show_single_advertiser_rail', 
                        bill_me_fee='$bill_me_fee', 
                        allow_bill_me='$allow_bill_me', 
                        show_most_popular='$show_most_popular', 
                        show_featured='$show_featured', 
                        show_maps='$show_maps', 
                        advertisers_position='$advertisers_position', 
                        advertiser_position='$advertiser_position', 
                        most_popular_position='$most_popular_position', 
                        show_empty_categories='$show_empty_categories', 
                        offsite_ad_order='$offsite_ad_order', 
                        featured_scroller_add_on='$featured_scroller_add_on', 
                        featured_position='$featured_position', 
                        maps_position='$maps_position', 
                        timezone='$timezone', 
                        billing_street='$billing_street', 
                        print_bold_first='$print_bold_first', 
                        non_profit_discount='$non_profit_discount', 
                        billing_city='$billing_city', 
                        billing_state='$billing_state', 
                        billing_zip='$billing_zip', 
                        featured_add_on='$featured_add_on', 
                        senior_discount='$senior_discount', 
                        send_new_ad_alert='$send_new_ad_alert', 
                        rich_ad_text='$rich_ad_text', 
                        include_headline_adcount='$include_headline_adcount', 
                        meta_mapping_id='$meta_mapping_id', 
                        default_state='$default_state', 
                        veteran_free='$veteran_free', 
                        veteran_free_count='$veteran_free_count',
                        display_run_length_mode = '$display_run_length_mode',
                        include_headline_message = '$include_headline_message',
                        online_text = '$online_text',
                        ad_geographic_field = '$ad_geographic_field',
                        categories_message = '$categories_message',
                        max_images = '$max_images',
                        require_headline = '$require_headline',
                        require_body = '$require_body',
                        require_geo = '$require_geo',
                        require_tags = '$require_tags',
                        require_category = '$require_category',
                        date_selection_mode = '$date_selection_mode',
                        show_attention_getters = '$show_attention_getters',
                        show_top_bottom_placement = '$show_top_bottom_placement',
                        max_print_photos = '$max_print_photos',
                        max_online_photos = '$max_online_photos',
                        ad_order_mode = '$ad_order_mode',
                        checkout_message = '$checkout_message',
                        allow_edit_charged_ad = '$allow_edit_charged_ad',
                        enable_pageflip = '$enable_pageflip'
                    WHERE id=1";
        $dbUpdate = dbexecutequery($sql);
        $target_dir = "../img/";
        
        if(count($_FILES)>0)
        {
            foreach($_FILES as $field=>$file)
            {
                if($file['name']!='')
                {
                    $target_file = $target_dir . basename($file["name"]);
                    $uploadOK=1;
                    // Allow certain file formats
                    $imageFileType =$file['type'];
                    if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
                    && $imageFileType != "image/gif" ) {
                        echo "Sorry, only JPG, JPEG and PNG files are allowed. You uploaded a $imageFileType file. ";
                        $uploadOK = 0;
                    }
                    $name = addslashes($file['name']);
                    if($uploadOK)
                    {
                        if (move_uploaded_file($file["tmp_name"], $target_file)) {
                            $sql="UPDATE config SET $field='$name' WHERE id=1";
                            $dbUpdate=dbexecutequery($sql);
                            $error=$dbUpdate['error'];
                            
                        } else {
                            $error='File upload error.';
                        }
                    }
                }
            }
        }
        
        $sql="SELECT * FROM config WHERE id=1";
        $dbConfig = dbselectsingle($sql);
        $config = $dbConfig['data'];
        
        themeGenerator();
    }
    
    $sql="SELECT * FROM config WHERE id=1";
    $dbConfig = dbselectsingle($sql);
    $config = $dbConfig['data'];
    $interfaces = array('Authorize.net'=>'Authorize.net','Stripe'=>'Stripe');
         
    $timezones = array('America/New_York'=>'Eastern',
                        'America/Chicago'=>'Central',
                        'America/Denver'=>'Mountain',
                        'America/Phoenix'=>'Mountain no DST',
                        'America/Los_Angeles'=>'Pacific',
                        'America/Anchorage'=>'Alaska',
                        'America/Adak'=>'Hawaii',
                        'Pacific/Honolulu'=>'Hawaii no DST'
    );
    

    $sql="SELECT * FROM fonts";
    $dbfonts = dbselectmulti($sql);
    $fonts = array();
    foreach( $dbfonts['data'] as $font ) {
        $fonts[$font['id']] = $font['friendly_name'];
    }

    $themeAuras = array(
        'std_light' => 'Light Theme',
        'std_dark' => 'Dark Theme',
    );
    
    $sql="SELECT * FROM metadata ORDER BY meta_name";
    $dbMeta = dbselectmulti($sql);
    $metas[0]="Select meta tag for mapping";
    if($dbMeta['numrows']>0)
    {
        foreach($dbMeta['data'] as $meta)
        {
            $metas[$meta['id']]=stripslashes($meta['meta_name']);
        }
    }
    
    $sql="SELECT * FROM addons ORDER BY addon";
    $dbAddons = dbselectmulti($sql);
    $addons[0]="Select add-on or leave blank";
    if($dbAddons['numrows']>0)
    {
        foreach($dbAddons['data'] as $addon)
        {
            $addons[$addon['id']]=stripslashes($addon['addon']);
        }
    }
    
    $orderModes['simple']='Simple one page';
    $orderModes['normal']='Multi-step';
    ?>
    <div class="page-header">
      <h1>Site Configuration</h1>
    </div>
    <form method=post class='form-horizontal' enctype="multipart/form-data">
    <div>

      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#basics" aria-controls="basics" role="tab" data-toggle="tab">Basics</a></li>
        <li role="presentation"><a href="#theme" aria-controls="theme" role="tab" data-toggle="tab">Theme</a></li>
        <li role="presentation"><a href="#payments" aria-controls="payments" role="tab" data-toggle="tab">Payments</a></li>
        <li role="presentation"><a href="#third" aria-controls="third" role="tab" data-toggle="tab">3rd Party</a></li>
        <li role="presentation"><a href="#blocks" aria-controls="blocks" role="tab" data-toggle="tab">Page Blocks</a></li>
        <li role="presentation"><a href="#preview" aria-controls="preview" role="tab" data-toggle="tab">Previews</a></li>
        <li role="presentation"><a href="#system" aria-controls="system" role="tab" data-toggle="tab">System</a></li>
        <li role="presentation"><a href="#ads" aria-controls="ads" role="tab" data-toggle="tab">Ads</a></li>
        <li role="presentation"><a href="#required" aria-controls="required" role="tab" data-toggle="tab">Required Fields</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="basics">
        <?php
          make_text('site_name',stripslashes($config['site_name']),'Site Name');        
          make_text('site_url',stripslashes($config['site_url']),'Site URL');        
          make_select('ad_order_mode',$orderModes[$config['ad_order_mode']],$orderModes,'Ad Order Mode');
          make_text('offsite_ad_order',stripslashes($config['offsite_ad_order']),'Order enty','If you use a different order entry system, enter it\'s url here.');        
          make_text('send_errors_to',stripslashes($config['send_errors_to']),'Send Errors to','Email address to send errors to');        
          make_text('mail_from_address',stripslashes($config['mail_from_address']),'Send Email From','Email address to send site email from');        
          make_text('mail_from_name',stripslashes($config['mail_from_name']),'Send Email Name','Friendly name in the from address for email.');        
          make_text('mail_from_subject',stripslashes($config['mail_from_subject']),'Send Email Subject','Default mail subject if none set.');        
          make_text('contact_email',stripslashes($config['contact_email']),'Contact Email','Email address for contact/support.');        
          make_text('contact_phone',stripslashes($config['contact_phone']),'Contact Phone','Phone for contact/support.');        
          make_number('email_abuse_limit',stripslashes($config['email_abuse_limit']),'Email abuse limit','Number of abuse flags before user can not send email');
          make_checkbox('nested_categories',$config['nested_categories'],'Allow nested catgories','Allow creation and display of sub-categories');
          make_checkbox('allow_online_only',$config['allow_online_only'],'Allow online-only ads','Allow creation of ads for online only');
          make_checkbox('show_empty_categories',$config['show_empty_categories'],'Empty Categories','Show empty categories in the category menu');
          make_select('timezone',$timezones[$config['timezone']],$timezones,'Timezone','Select the timezone where this site operates');
          make_state('default_state',$config['default_state'],'Default State','Default state for address fields');
        ?>
          <div class="form-group">
            <label for="print_dow" class="col-sm-2 control-label">Printing Days of Week</label>
            <div class="col-sm-10">
              <small>Select the days of week that you publish in print</small>
              <?php $dow = explode(",",$config['print_dow']); ?>
              <select id='print_dow' name='print_dow[]' multiple class="form-control">
                  <option value=0 <?= (in_array(0,$dow) ? 'selected' : '') ?>>Sunday</option>
                  <option value=1 <?= (in_array(1,$dow) ? 'selected' : '') ?>>Monday</option>
                  <option value=2 <?= (in_array(2,$dow) ? 'selected' : '') ?>>Tuesday</option>
                  <option value=3 <?= (in_array(3,$dow) ? 'selected' : '') ?>>Wednesday</option>
                  <option value=4 <?= (in_array(4,$dow) ? 'selected' : '') ?>>Thursday</option>
                  <option value=5 <?= (in_array(5,$dow) ? 'selected' : '') ?>>Friday</option> 
                  <option value=6 <?= (in_array(6,$dow) ? 'selected' : '') ?>>Saturday</option> 
              </select>
            </div>
          </div>  
        </div>
        
        <div role="tabpanel" class="tab-pane" id="theme">
            <?php
              make_select('font_header',$fonts[$config['font_header']],$fonts,'Header Font','Select the header font');
              make_select('font_body',$fonts[$config['font_body']],$fonts,'Body Font','Select the body font');
              make_select('theme_aura',$themeAuras[$config['theme_aura']],$themeAuras,'Theme','Select the main theme');
              make_color('color_background',$config['color_background'],'Background Color','Color of the page background. Leave blank for default.');
              make_color('color_text',$config['color_text'],'Text Color','Color of the text. Leave blank for default.');
              make_color('color_highlight',$config['color_highlight'],'Highlight Color','Color used for emphasis, borders, etc.');
              if($config['site_logo_main']!=''){
                    $file = 'Current file: ' . $config['site_logo_main'];
                    $file .= '<div class="config-file-preview" style="background-image:url(/img/' . $config['site_logo_main'] . ');"></div>';
                }
              make_file('site_logo_main', 'Main logo', 'Transparent PNG, 1200px wide max x 160px high max. ' . $file, '/img/' . $config['site_logo_main']);
              if($config['site_logo_mobile']!=''){
                    $file = 'Current file: ' . $config['site_logo_mobile'];
                    $file .= '<div class="config-file-preview" style="background-image:url(/img/' . $config['site_logo_mobile'] . ');"></div>';
                }
              make_file('site_logo_mobile', 'Mobile logo', 'Transparent PNG, 400px wide max x 120px high max. ' . $file, '/img/' . $config['site_logo_mobile']);
              if($config['header_image']!=''){
                    $file = 'Current file: ' . $config['header_image'];
                    $file .= '<div class="config-file-preview" style="background-image:url(/img/' . $config['header_image'] . ');"></div>';
                }
              make_file('header_image', 'Header Background', 'JPEG, 1200px wide x 160px high. ' . $file, '/img/' . $config['header_image']);
              print "<div class='form-group'>\n";
              print "<label class='col-sm-2' style='text-align:right;font-weight:700;'>Generate Style Sheet</label>\n";
              print "<div class='col-sm-10'>After saving configuration, click <a href='/sass/style.php/app.scss' target='_blank'>here</a> to generate the stylesheet.</div>\n";
              print "</div>\n";
            ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="payments">
         <fieldset>
         <legend>Billing Options</legend>
         <?php
          make_select('billing_interface',$interfaces[$config['billing_interface']],$interfaces,'Billing Interface','Which provider will you use for billing ads?');
          make_text('billing_street',stripslashes($config['billing_street']),'Billing Street (used on invoices)');
          make_text('billing_city',stripslashes($config['billing_city']),'Billing City (used on invoices)');
          make_state('billing_state',stripslashes($config['billing_state']),'Billing State (used on invoices)');
          make_text('billing_zip',stripslashes($config['billing_zip']),'Billing Zip (used on invoices)');
          make_checkbox('allow_bill_me',$config['allow_bill_me'],'Allow bill me','Allow user the option to be billed');
          make_number('bill_me_fee',stripslashes($config['bill_me_fee']),'Additional fee for "bill me" ads.');
          make_number('bill_days_out',stripslashes($config['bill_days_out']),'How many days from invoice date are bills due?');
        ?>
        </fieldset>
         
        <fieldset>
         <legend>Authorize.net Card Services <small>Enter details here if you will be using this service</small></legend>
         <?php
          make_text('authorize_net_login_id',stripslashes($config['authorize_net_login_id']),'API Login ID');
          make_text('authorize_net_key',stripslashes($config['authorize_net_key']),'Transaction Key');
          make_text('authorize_net_secret',stripslashes($config['authorize_net_secret']),'Secret Key');
          make_text('authorize_net_client_key',stripslashes($config['authorize_net_client_key']),'Client Key');
        ?>
        </fieldset>
        <fieldset>
         <legend>Stripe Credit Card Services <small>Enter details here if you will be using this service</small></legend>
         <?php
          make_text('stripe_test_public',stripslashes($config['stripe_test_public']),'Stripe Test Public Key');
          make_text('stripe_test_secret',stripslashes($config['stripe_test_secret']),'Stripe Test Private Key');
          make_text('stripe_live_public',stripslashes($config['stripe_live_public']),'Stripe Live Public Key');
          make_text('stripe_live_secret',stripslashes($config['stripe_live_secret']),'Stripe Live Private Key');
        ?>
        </fieldset>
        
        </div>
        <div role="tabpanel" class="tab-pane" id="third">
         <?php
          make_text('facebook_app_id',stripslashes($config['facebook_app_id']),'Facebook App ID');        
          make_text('facebook_secret',stripslashes($config['facebook_secret']),'Facebook Secret');        
          make_text('facebook_token',stripslashes($config['facebook_token']),'Facebook Token');        
          make_text('google_map_key',stripslashes($config['google_map_key']),'Google Map Key');        
          make_text('google_analytics',stripslashes($config['google_analytics']),'Google Analytics Key');        
          make_text('recaptcha_site',stripslashes($config['recaptcha_site']),'Google Recaptcha Public Site Key');        
          make_text('recaptcha_secret',stripslashes($config['recaptcha_secret']),'Google Recaptcha Private Key');        
          make_text('google_dfp_key',stripslashes($config['google_dfp_key']),'Google DFP ID');                  
         ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="blocks">
         <?php
          $placements = array('none'=>"Not displayed", 'left'=>"Left rail", 'right-top'=>"Right rail top", 'right-middle'=>"Right rail middle", 
           'right-bottom'=>"Right rail bottom");
          make_select('show_advertisers_rail',$placements[$config['show_advertisers_rail']],$placements,'All Advertisers','Show the all advertisers promo in rail');
          make_number('advertisers_position',$config['advertisers_position'],'Advertisers Widget Position','Order of the item in the rail (1 at top)');
          make_select('show_single_advertiser_rail',$placements[$config['show_single_advertiser_rail']],$placements,'Single Advertisers','Show the single random advertiser promo in rail');
          make_number('advertiser_position',$config['advertiser_position'],'Advertiser Widget Position','Order of the item in the rail (1 at top)');
          make_select('show_most_popular',$placements[$config['show_most_popular']],$placements,'Most Popular','Show the most popular widget in rail');
          make_number('most_popular_position',$config['most_popular_position'],'Popular Widget Position','Order of the item in the rail (1 at top)');
          
          make_select('show_maps',$placements[$config['show_maps']],$placements,'Maps','Show the maps widget');
          make_number('maps_position',$config['maps_position'],'Maps Widget Position','Order of the item in the rail (1 at top)');
          
          $placements = array('none'=>"Not displayed", 'featured'=>"Display in templated regions");
          make_select('show_featured',$placements[$config['show_featured']],$placements,'Featured Ads','Show the featured ads widget');
          make_number('featured_position',$config['featured_position'],'Featured Widget Position','Order of the item in the rail (1 at top)');
          
         ?>
        </div>
        
        <div role="tabpanel" class="tab-pane" id="preview">
         <?php
          make_textarea('preview_disclaimer',stripslashes($config['preview_disclaimer']),'Preview Disclaimer');
          make_text('print_column_width',stripslashes($config['print_column_width']),'Print Ad Column Width','Enter the size of the print ad column width in pixels. Multiply inches by 100 to get pixels. Example: 175px (which is 1.75 inches).');
          
          make_select('preview_font',$fonts[$config['preview_font']],$fonts,'Preview Font Family','Select the font family to use for the print preview (example: Roboto Condensed).');
          make_text('preview_font_size',stripslashes($config['preview_font_size']),'Preview Font Size','Enter the font size for the print preview in pixels (example: 16px).');
          make_text('preview_line_height',stripslashes($config['preview_line_height']),'Preview Line Height','Enter the line-height for the print preview in pixels (example: 19px).');
          make_text('preview_letter_spacing',stripslashes($config['preview_letter_spacing']),'Preview Letter Spacing','Enter the spacing (if any) between letters. This is typically very low, such as 0.025em.');
          make_checkbox('print_allow_styles',$config['print_allow_styles'],'Allow styles in print.','Show styles in the print preview (bold, italic, etc).');
          make_number('print_uppercase_first',$config['preview_uppercase_first'],'Uppercase First','Uppercase the first X words in the ad preview (ad text, not headline). Set to 0 to not uppercase any.');
          make_number('print_bold_first',$config['preview_bold_first'],'Bold First','Bold the first X words in the ad preview (ad text, not headline). Set to 0 to not uppercase any.');
         ?>
        </div>
        
        
        
        <div role="tabpanel" class="tab-pane" id="system">
         <?php
          make_checkbox('allow_edit_charged_ad',$config['allow_edit_charged_ad'],'Allow Edit of Charged','Allow user to make edits to an ad that has already been charged.');
          make_checkbox('require_new_ad_acknowledgement',$config['require_new_ad_acknowledgement'],'New ad rules checkbox','Require users to acknowledge ad rules before placing their ad.');
          make_checkbox('rich_ad_text',$config['rich_ad_text'],'Rich ad text','Check to allow the rich text editor for ad text');
          make_checkbox('online_text',$config['online_text'],'Allow online text','Check to enable ad to have additional online content');
         make_checkbox('ad_geographic_field',$config['ad_geographic_field'],'Include ad geo info','Include a field to collect a city/state below the ad copy when creating an ad.');
         make_text('include_headline_message',$config['include_headline_message'],'Include headline message','Message displayed with the include headline checkbox');
         make_text('categories_message',$config['categories_message'],'Categories message','Message displayed above the categories selection area');
         make_text('checkout_message',$config['checkout_message'],'Checkout message','Message displayed on the checkout page.');
         make_select('meta_mapping_id',$metas[$config['meta_mapping_id']],$metas,'Mapping Meta','Which meta tag should be used for the listing address?');
         make_checkbox('show_attention_getters',$config['show_attention_getters'],'Attention Getters','Show a the attention getters option in images tab?');
         make_checkbox('show_top_bottom_placement',$config['show_top_bottom_placement'],'Image placement option','Show a top/bottom image placement option when uploading images?');
         make_checkbox('enable_pageflip',$config['enable_pageflip'],'Enable Pageflip Viewer','If selected, will use page flip PDF viewer, otherwise present a list of editions to click on.');
         ?>
         <fieldset>
         <legend>Add Ons</legend>
         <?php
          make_select('facebook_add_on',$addons[$config['facebook_add_on']],$addons,'Facebook','Which add on will be used for pushing the ad to Facebook?');
          make_select('twitter_add_on',$addons[$config['twitter_add_on']],$addons,'Twitter','Which add on will be used for pushing the ad to Twitter?');
          make_select('featured_add_on',$addons[$config['featured_add_on']],$addons,'Featured Ads','Which add on will be used for \'Featured\' ads?');
          make_select('featured_scroller_add_on',$addons[$config['featured_scroller_add_on']],$addons,'Featured Scroller Ads','Which add on will be used for \'Featured\' ads in the scroller (can be same as featured ads)?');
        ?>
        </fieldset>
         </div>
        
        <div role="tabpanel" class="tab-pane" id="ads">
         <?php
         $dateModes = array('range'=>'Range of dates with start and stop dates', 'weeks'=>'Select run dates limited by publication day of week, stop day is extended to 6 days after last pub date.');
         $lengthDisplayModes = array('days'=>'Display as a number of days','weeks'=>'Display as a number of weeks (rounded up)');

         make_select('date_selection_mode',$dateModes[$config['date_selection_mode']],$dateModes,'Ad run dates selection mode');
         make_select('display_run_length_mode',$lengthDisplayModes[$config['display_run_length_mode']],$lengthDisplayModes,'How to display the ad run length');
         make_number('default_run_length',$config['default_run_length'],'Default ad run length','Default number of days to run ad');
         make_number('max_images',$config['max_images'],'Max ad images','Maximum number of images to attach to an ad');
         make_number('max_print_photos',$config['max_print_photos'],'Max print images','Maximum number of images allowed for print ad');
         make_number('max_online_photos',$config['max_online_photos'],'Max online images','Maximum number of images allowed for online ad');
         make_checkbox('send_new_ad_alert',$config['send_new_ad_alert'],'Send new ad alerts','Send an email to the admin when a new ad is placed by a user.');
         make_checkbox('include_headline_adcount',$config['include_headline_adcount'],'Count headline','Include the number of words in the headline in the ad text count.');
         make_checkbox('veteran_free',$config['veteran_free'],'Free ads for veterans','Check if veterans will receive free ads (amount specified in next field)');
         make_number('veteran_free_count',$config['veteran_free_count'],'# Free ads','Number of free ads allowed each month');
         make_number('senior_discount',$config['senior_discount'],'Senior Discount','Percentage discount for users marked as seniors.');
         make_number('non_profit_discount',$config['non_profit_discount'],'Non-profit Discount','Percentage discount for advertisers marked as non-profits.');?>
         
        </div>
        
        <div role="tabpanel" class="tab-pane" id="required">
         <?php
         make_checkbox('require_headline',$config['require_headline'],'Require Headline','If checked, the headline field is required during ad creation.');
         make_checkbox('require_body',$config['require_body'],'Require Body','If checked, the print ad text field is required during ad creation.');
         make_checkbox('require_geo',$config['require_geo'],'Require Geo','If checked (and if included in ad creation), the geographic field is required during ad creation.');
         make_checkbox('require_tags',$config['require_tags'],'Require Tags','If checked, the tags field is required during ad creation.');
         make_checkbox('require_category',$config['require_category'],'Require Category','If checked, the category field is required during ad creation.');
         ?>
        </div>
        
        
      </div>

    </div>
   
   
   
   <?php 
        make_submit('submit','Save Config');
   print "
   </form>";
}