<?php
include "bootstrap.php";

        
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>E-Editions <small>PDFs of your publication)</small></h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM editions WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_date('edition_date',$record['edition_date'],'Publication Date','');
    make_text('edition_filename',stripslashes($record['edition_filename']),'PDF Filename','Must match exactly with the file you uploaded.');
    make_text('embed',stripslashes($record['embed']),'Embed Code','Issuu embed code. Just the part where it says data-configid');
    make_file('edition_thumb','Thumbnail','Thumbnail for the users','../uploads/editions/'.stripslashes($record['edition_thumb']));
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="SELECT * FROM editions WHERE id=$id";
    $dbImage=dbselectsingle($sql);
    $image=$dbImage['data'];
    $thumb=stripslashes($image['edition_thumb']);
    $edition=stripslashes($image['edition_filename']);
    unlink("../uploads/editions/".$thumb);
    unlink("../uploads/editions/".$edition);
    
    $sql="DELETE FROM editions WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $date=addslashes($_POST['edition_date']);
    $filename=addslashes($_POST['edition_filename']);
    $embed=addslashes($_POST['embed']);
    
    if($id==0)
    {
        $sql="INSERT INTO editions (edition_date, edition_filename, embed ) VALUES ('$date', '$filename', '$embed' )";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE editions SET edition_date='$date', edition_filename='$filename', embed='$embed' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    $target_dir = "../uploads/editions/";
    $target_file = $target_dir . basename($_FILES["edition_thumb"]["name"]);
    
    if(isset($_FILES) && count($_FILES)>0)
    {
        if($_FILES['edition_thumb']['name']!=''){
            $uploadOK=1;
            // Allow certain file formats
            $imageFileType =$_FILES['edition_thumb']['type'];
            if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
            && $imageFileType != "image/gif" ) {
                echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
                $uploadOK = 0;
            }
            if($uploadOK)
            {
                if (move_uploaded_file($_FILES["edition_thumb"]["tmp_name"], $target_file)) {
                    $sql="UPDATE editions SET edition_thumb='".addslashes($_FILES['edition_thumb']['name'])."' WHERE id=$id";
                    $dbUpdate=dbexecutequery($sql);
                    $error=$dbUpdate['error'];
                } else {
                    $error='File upload error.';
                }
            }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database or with the image.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    $sql="SELECT * FROM editions";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new edition</a>","Date,Filename,Thumbnail");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $date=stripslashes($record['edition_date']);
            $filename=stripslashes($record['edition_filename']);
            $thumb=stripslashes($record['edition_thumb']);
            print "<tr>\n";
            print "<td>$date</td>\n";
            print "<td>$filename</td>\n";
            print "<td><div class='config-file-preview' style='background-image:url(/uploads/editions/" . $thumb . ");'></div></td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}