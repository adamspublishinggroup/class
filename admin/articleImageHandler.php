<?php
include("../includes/bootCore.php");
require_once("../vendor/autoload.php");
use Intervention\Image\ImageManager;
// create an image manager instance with favored driver
$manager = new ImageManager(array('driver' => 'gd'));
    
$articleID=intval($_POST['article_id']);
$id=intval($_POST['id']);
if($id==0)
{
    $sql="INSERT INTO article_images (article_id) VALUES ('$articleID')";
    $dbImage=dbinsertquery($sql);
    $id = $dbImage['insertid'];
    $error=$dbImage['error'];
}
if(isset($_FILES) && $_FILES['image']['tmp_name']!='' && $id!=0)
{
    $uploadOK=1;
    // Allow certain file formats
    $imageFileType =$_FILES['image']['type'];
    if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
    && $imageFileType != "image/gif" ) {
        $error="Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
        $uploadOK = 0;
    }
    if($uploadOK)
    {
        $target_dir = "../uploads/articles/";
        $file_name=basename($_FILES["image"]["name"]);
        $file_name = strtolower(str_replace(array(" ","*",",","?","\"","\\","/","#","!","@","\$","%","^","(",")","'","-","+","_","=","|","{","[","]","}","<",">"),"",$file_name));
        
        $target_file = $target_dir . $file_name;

        if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
            $sql="UPDATE article_images SET filename='".addslashes($file_name)."' WHERE id=$id";
            $dbUpdate=dbexecutequery($sql);
            $error=$dbUpdate['error'];
            
            // load the image
            $image = $manager->make($target_file);

            //first, we resize the image to display size (width) and constrain the aspect ratio
            $image->resize(1000, null, function ($constraint) {
                 $constraint->aspectRatio();
                  $constraint->upsize();
              });
            $image->save($target_dir.'display_'.$file_name);
              
            //now resize for thumb (height) and constrain the aspect ratio
            $image->resize(null, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $image->save($target_dir.'thumb_'.$file_name);

            
        } else {
            $error='File upload error.';
        }
    }
}
redirect("articles.php?action=images&article_id=$articleID");
