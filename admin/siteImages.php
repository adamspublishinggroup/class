<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Site Images</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM site_images WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_text('slug',stripslashes($record['slug']),'Slug','Identifier for image (like "logo")');
    make_textarea('caption',stripslashes($record['caption']),'Caption for image (if needed)');
    make_file('graphic','Select a file'."../img/".stripslashes($record['filename']));
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="SELECT * FROM site_images WHERE id=$id";
    $dbImage=dbselectsingle($sql);
    $image=$dbImage['data'];
    $filename=stripslashes($image['filename']);
    unlink("../img/".$filename);
    
    $sql="DELETE FROM site_images WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $caption=addslashes($_POST['caption']);
    $slug=addslashes($_POST['slug']);
    $dt=date("Y-m-d H:i");
    
    if($id==0)
    {
        $sql="INSERT INTO site_images (caption, slug, uploaded_dt) VALUES 
        ('$caption', '$slug', '$dt')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE site_images SET caption='$caption', slug='$slug', uploaded_dt='$dt' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    $target_dir = "../img/";
    $target_file = $target_dir . basename($_FILES["graphic"]["name"]);
    
    if(isset($_FILES))
    {
        $uploadOK=1;
        // Allow certain file formats
        $imageFileType =$_FILES['graphic']['type'];
        if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
        && $imageFileType != "image/gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
            $uploadOK = 0;
        }
        if($uploadOK)
        {
            if (move_uploaded_file($_FILES["graphic"]["tmp_name"], $target_file)) {
                $sql="UPDATE site_images SET filename='".addslashes($_FILES['graphic']['name'])."' WHERE id=$id";
                $dbUpdate=dbexecutequery($sql);
                $error=$dbUpdate['error'];
                
            } else {
                $error='File upload error.';
            }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database or with the image.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    global $types;
    $sql="SELECT * FROM site_images";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new image</a>","Name,Slug");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $slug=stripslashes($record['slug']);
            $filename=stripslashes($record['filename']);
            print "<tr>\n";
            print "<td><img src='../img/$filename' class='img img-thumbnail' height=100 style='max-height: 100px;'></td>\n";
            print "<td>$slug</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}