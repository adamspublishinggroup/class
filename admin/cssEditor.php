<?php
include "bootstrap.php";

    
function page($action='list')
{
   if($action=='Save Changes')
   {
       $contents = $_POST['content'];
       $filename = $_POST['filename'];
       
       file_put_contents("../sass/".$filename,$contents);
       ?>
       <div class='alert alert-success' role='alert'>Your file changes have been saved.</div>
       <?php
   }
    ?>
    <div class="page-header">
      <h1>CSS Editor <small>Edit css files</small></h1>
    </div>
   
   <div class='row'>
   <div class='col-xs-12 col-md-3 col-md-push-9'>
   <!--
   <h4>Select a file to edit</h4>
   <ul class='list-unstyled'>
   <li><li><a href='?action=edit&file=_150-ad-extras.scss'>Addons Styling</a></li></li>
   <?php
     /*
     $dir    = '../sass';
     $files = scandir($dir, 0);
     foreach($files as $file)
     {
         if(strpos($file,".scss")>0)
         {
         print "<li><a href='?action=edit&file=$file'>$file</a></li>";
         }
     }
     */
   ?>
   </ul>
   -->
   </div>
   <div class='col-xs-12 col-md-9 col-md-pull-3'>
   <form class='form' method=post>
   <textarea style='width:100%;height:100%;min-height:700px;' name='content'><?php
       $file="_150-ad-extras.scss";
       $contents = file_get_contents("../sass/".$file);
       echo $contents;
   ?></textarea>
   <input type=hidden name='filename' value='<?= $file ?>' />
   <input type=submit name='submit' value='Save Changes' class='btn btn-dark' />
   </form>
   </div>
   
   <?php
   
}
?>
