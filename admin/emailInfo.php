<?php
include "bootstrap.php";


function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Email Information <small>Appears on email correspondence</small></h1>
    </div>
   <?php
   switch($action)
   {
       case "Save": save_record(); break;
       default: record(); break;
   }
}

function record($record=array())
{
    $sql = "SELECT * FROM email_info WHERE id=1";
    $dbRecord = dbselectsingle($sql);
    $record = $dbRecord['data'];
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_textarea('header',stripslashes($record['header']),'Header','Block of text that would appear at the top of all emails.');
    make_textarea('footer',stripslashes($record['footer']),'Footer','Block of text that would appear at the bottom of all emails.');
    make_textarea('address',stripslashes($record['address']),'Address','Address that will be added to the bottom of emails. This address is legally manditory and must be included. It is the physical address that represents a place people can send physical mail to resolve conflicts over your emailing practices.');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";

}

function save_record()
{
    $record=$_POST;

    $header=addslashes($_POST['header']);
    $footer=addslashes($_POST['footer']);
    $address=addslashes($_POST['address']);
    $sql="UPDATE email_info SET header='$header', footer='$footer', address='$address' WHERE id=1";
    $dbUpdate=dbexecutequery($sql);
    $error=$dbUpdate['error'];

    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.</div>";
        record($record);
    } else {
        redirect("?action=list");
    }

}
