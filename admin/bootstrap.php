<?php
ini_set('max_execution_time', 300); //300 seconds = 5 minutes
require("../includes/bootCore.php");
require_once("../vendor/autoload.php");

$script=$_SERVER['SCRIPT_NAME'];
if(!$_SESSION['admin']['loggedin']){redirect('login.php');}

if( isset( $_COOKIE['pngclassAdmin'] ) ) {
    $adminToken = $_COOKIE['pngclassAdmin'];
    $sql="SELECT * FROM admins WHERE token='$adminToken'"; 
    $dbresult=dbselectsingle($sql);
    $record = $dbresult['data'];
    $_SESSION['admin']['loggedin']=true;
    $_SESSION['admin']['admin']=true;
    $_SESSION['admin']['token']=$record['token'];
    $_SESSION['admin']['email']=$record['email'];
     
} 

$sql="SELECT * FROM config LIMIT 1";
$dbConfig=dbselectsingle($sql);
$config = $dbConfig['data'];

$adminMenu[]=array('name'=>'Dashboard', 'url'=>'index.php' );
    $adsMenu[]=array('name'=>'Search Ads', 'url'=>'searchAds.php');
    $adsMenu[]=array('name'=>'Create new ad', 'url'=>'/place-an-ad/place-ad-basics/?new' );
    $adsMenu[]=array('name'=>'Bill Me Ads', 'url'=>'billMe.php');
    if( $config['require_approval'] ) {
        $adsMenu[]=array('name'=>'Approve Pending Ads', 'url'=>'approvePending.php');
    }
$adminMenu[]=array('name'=>'Ads', 'url'=>'#', 'submenus'=>$adsMenu );
$adminMenu[]=array('name'=>'Users', 'url'=>'users.php' );
    $advertisersMenu[]=array('name'=>'Advertisers', 'url'=>'advertisers.php');
    $advertisersMenu[]=array('name'=>'Display Ads', 'url'=>'advertisersDisplayAds.php');
$adminMenu[]=array('name'=>'Advertisers', 'url'=>'#', 'submenus'=>$advertisersMenu );
$adminMenu[]=array('name'=>'Post to Facebook', 'url'=>'facebook.php' );
$adminMenu[]=array('name'=>'Editions', 'url'=>'editions.php' );
$adminMenu[]=array('name'=>'Export', 'url'=>'export.php' );
    $reportsMenu[]=array('name'=>'Monthly Ads', 'url'=>'/admin/reports-month.php');
    $reportsMenu[]=array('name'=>'Billing Ads', 'url'=>'/admin/reports-billing.php');
    $reportsMenu[]=array('name'=>'Promos', 'url'=>'/admin/reports-promos.php');
    $reportsMenu[]=array('name'=>'Packages', 'url'=>'/admin/reports-packages.php');
    $reportsMenu[]=array('name'=>'Advertisers', 'url'=>'/admin/reports-advertisers.php');
    $reportsMenu[]=array('name'=>'Abandoned Ads', 'url'=>'/admin/reports-abandoned.php');
$adminMenu[]=array('name'=>'Reports', 'url'=>'#', 'submenus'=>$reportsMenu );
    $adSettingMenu[]=array('name'=>'Categories', 'url'=>'categories.php' );
    $adSettingMenu[]=array('name'=>'Packages', 'url'=>'packages.php' );
    $adSettingMenu[]=array('name'=>'Metadata', 'url'=>'metatags.php' );
    $adSettingMenu[]=array('name'=>'Add Ons', 'url'=>'addOns.php' );
    $adSettingMenu[]=array('name'=>'Attention Getters', 'url'=>'attnGetters.php' );
    $adSettingMenu[]=array('name'=>'Ad Placement Info', 'url'=>'adblurb.php' );
    $adSettingMenu[]=array('name'=>'Promo Codes', 'url'=>'promos.php' );
    $adSettingMenu[]=array('name'=>'Maps', 'url'=>'maps.php' );
$adminMenu[]=array('name'=>'Ad Settings', 'url'=>'#', 'submenus'=>$adSettingMenu );
    $articleMenu[]=array('name'=>'Articles', 'url'=>'articles.php' );
    $articleMenu[]=array('name'=>'Categories', 'url'=>'articleCategories.php' );
$adminMenu[]=array('name'=>'Articles', 'url'=>'#', 'submenus'=>$articleMenu);
    $settingMenu[]=array('name'=>'Admins', 'url'=>'admins.php' );
    $settingMenu[]=array('name'=>'Email Information', 'url'=>'emailInfo.php' );
    $settingMenu[]=array('name'=>'Email Blacklist', 'url'=>'blacklist.php' );
    $settingMenu[]=array('name'=>'Email Templates', 'url'=>'emailTemplates.php' );
    $settingMenu[]=array('name'=>'Promotional Blocks', 'url'=>'railItems.php' );
    $settingMenu[]=array('name'=>'Configuration', 'url'=>'config.php' );
    $settingMenu[]=array('name'=>'Site Menu', 'url'=>'siteMenu.php' );
    $settingMenu[]=array('name'=>'DFP Ads', 'url'=>'dfpAds.php' );
    $settingMenu[]=array('name'=>'Static Pages', 'url'=>'static.php' );
    $settingMenu[]=array('name'=>'Locations', 'url'=>'locations.php' );
    $settingMenu[]=array('name'=>'Importer', 'url'=>'importer.php' );
    $settingMenu[]=array('name'=>'CSS Editor', 'url'=>'cssEditor.php' );
    $settingMenu[]=array('name'=>'Sitemap Generator', 'url'=>'sitemapGenerator.php', 'blank'=>1 );
$adminMenu[]=array('name'=>'Site Settings', 'url'=>'#', 'submenus'=>$settingMenu);


$outputTypes = array ('screen'=>'To screen','csv'=>'To CSV/Excel','email'=>'Send to my email','pdf'=>'To PDF');

$adStatuses = array(
1=> 'Live',
2=> 'Editing (unpaid)',
3=> 'Editing (paid)',
5=> 'Unknown',
9=> 'Killed'
);
           
?><!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="Joe Hansen <jhansen@pioneernewsgroup.com>">
    <link rel="icon" href="/favicon.ico">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="/css/datatables.min.css">
    <!--
    <link rel="stylesheet" href="/css/datatables.css">
    <link rel="stylesheet" href="/css/dataTables.TableTools.css">
    <link rel="stylesheet" href="/css/bootstrap.datatables.min.css">
    -->
    <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" href="/css/colorpicker.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/jquery.cleditor.css">
    <link rel="stylesheet" href="/css/jquery.tagsinput.min.css">
    <link rel="stylesheet" href="/css/select2.min.css">
    <link rel="stylesheet" href="/css/admin.css">
    <link rel="stylesheet" href="/css/summernote.css">
    <script src="/js/jquery-3.2.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/summernote.min.js"></script>
    <script src="/js/jquery.cleditor.min.js"></script>
    <script src="/js/jquery.cleditor.table.min.js"></script>
    <script src="/js/jquery.cleditor.icon.min.js"></script>
    <script src="/js/jquery.cleditor_init.js"></script>
    <script src="/js/jquery.colorpicker.js"></script>
    <script src="/js/jquery.tagsinput.min.js"></script>
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/moment.min.js"></script>
    <script src="/js/datatables.min.js"></script>
    
    <!--
    <script src="/js/jquery.dataTables.min.js"></script>
    <script src="/js/jquery.dataTables.TableTools.js"></script>
    <script src="/js/dataTables.bootstrap.min.js"></script>
    -->
    <script src="/js/bootstrap-datetimepicker.min.js"></script>
    <script src="/js/bootbox.min.js"></script>
    <script src="/js/jquery.validate.min.js"></script>
    <script src="/js/additional-methods.min.js"></script>
    <script src="/js/pwstrength-bootstrap.min.js"></script>
    <script src="/js/jquery.maskedinput.min.js"></script>
    <script src="/js/select2.full.min.js"></script>
    <script src="/js/jquery.sortable.js"></script>
                                                      
    
    <!-- jquery plot -->
    <link rel="stylesheet" href="/css/jquery.jqplot.min.css">
    <script src="/js/jquery.jqplot.min.js"></script>
    <script src="/js/jqplot-plugins/jqplot.dateAxisRenderer.js"></script>
    <script src="/js/jqplot-plugins/jqplot.barRenderer.js"></script>
    <script src="/js/jqplot-plugins/jqplot.pieRenderer.js"></script>
    <script src="/js/jqplot-plugins/jqplot.categoryAxisRenderer.js"></script>
    <script src="/js/jqplot-plugins/jqplot.pointLabels.js"></script>
    
    <title><?php echo SITE_NAME ?> - Admin Dashboard</title>
     <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
    .page-header h1 {
        margin: 0;
        padding:0;
    }
    </style>
     </head>

  <body>
  <header class="hidden-print">
      <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">APGWest Class Admin</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav">
             <?php
                if(count($adminMenu)>0)
                {
                    foreach($adminMenu as $key=>$menuItem)
                    {
                        $menuId = $key;
                        $menuName = $menuItem['name'];
                        $menuURL = $menuItem['url'];
                        $submenus = $menuItem['submenus'];
                        if(isset($submenus) && count($submenus>0))
                        {
                            if(isset($menuItem['blank']) && $menuItem['blank'])
                            {
                                $t = "target='_blank'";
                            } else {
                                $t = "";
                            }
                            print "
        <li class='dropdown'>
           <a href='#' class='dropdown-toggle' data-toggle='dropdown' role='button' aria-haspopup='true' aria-expanded='false'>$menuName <span class='caret'></span></a>
        ";
                            print "
        <ul class='dropdown-menu' id='menu_$menuId'>";
                            foreach($submenus as $sub)
                            {
                                if(isset($sub['blank']) && $sub['blank'])
                                {
                                    $t = "target='_blank'";
                                } else {
                                    $t = "";
                                }
                                $subMenuName = stripslashes($sub['name']);
                                $subMenuURL = stripslashes($sub['url']);
                                print "<li><a href='$subMenuURL' $t>$subMenuName</a></li>\n";
                            }
                            print "        </ul>
                            </li>\n";
                        } else {
                            print "<li><a href='$menuURL'>$menuName</a></li>";
                        }
                    }
                }
            ?>
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container-fluid -->
    </nav>
    </header>
  
  
      
     
          
  <div class='container-fluid'>
      <div class='row'>
           <div class='col-xs-12' style='padding: 0 20px;'>
             <?php 
            if($_POST)
            {
               $action=$_POST['submit'];
            } else {
               $action=$_GET['action'];
            } 
            page($action); 
            ?>
          </div>
      </div>
  </div>
    <footer class="footer hidden-print">
      <div class="container-fluid" style='text-align:center;margin:10px;'>
        <p class="text-muted">Copyright <?= date("Y") ?> Pioneer News Group <span class='pull-right'><a class='btn btn-primary' href='/admin/logout.php'>Log out</a></span></p>
      </div>
    </footer>

    
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("active");
    });
    
    var keepAliveFunction = function() {
        $.ajax({
          url: "../includes/keepAlive.php",
          type: "POST",
          data: {},
          dataType: 'json',
          success: function(response){
             console.log(response.response);
          }
        });
    };

    keepAliveFunction();
    setInterval(keepAliveFunction, 10000);
    </script>
  </body>
</html>