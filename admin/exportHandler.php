<?php
  /* export handler */
include("../includes/bootCore.php");

global $config;

$featuredAddon = $config['featured_ad_on'];

    
if($_POST)
{
   $dateFilter = "";
   $printFilter = "";
   $paidFilter = "";
   $catFilter = "";
   $addonFilter = "";
   $order = "";
   $startDate=date("Y-m-d",strtotime($_POST['startDate']));    
   $endDate=date("Y-m-d",strtotime($_POST['endDate']));    
   $printDate = $_POST['print_date'];
   
   $cat = addslashes($_POST['category']);
   $addon = addslashes($_POST['addon']);
   $order = addslashes($_POST['order']);
   if($_POST['individual']){$individual = true;}else{$individual=false;}
   if($_POST['paid']){
       $paidFilter = true;
   }
   if($_POST['print']){
       $printFilter = "AND print=1";
   }
   $settings = array ('individual'=>$individual);
   
   
   //now fetch the liners
   if($print_date!=0)
   {
       $dateFilter = "AND FIND_IN_SET('$printDate',print_days)";
   } else {
       $dateFilter = "AND start_date<='$endDate' AND end_date>='$startDate'";
   }
   
   switch($order)
   {
       case "featurealpha";
        $orderFilter = "A.featured DESC, A.headline";
       break;
       
       case "featuredatedesc";
        $orderFilter = "A.featured DESC, A.start_date DESC";
       break;
       
       case "featuredateasc";
        $orderFilter = "A.featured DESC, A.start_date ASC";
       break;
       
       case "alpha";
        $orderFilter = "A.headline";
       break;
       
       case "datedesc";
        $orderFilter = "A.start_date DESC";
       break;
       
       case "dateasc";
        $orderFilter = "A.start_date ASC";
       break;
       
   }
   
   
   $sql = "SELECT id, category_name FROM categories WHERE active=1 ".($_POST['category']!=0 ? "AND id=".intval($_POST['category']) : "");
   $dbCats = dbselectmulti($sql);
   if($dbCats['numrows']>0) {
       foreach($dbCats['data'] as $cat)
       {
            //we are going to do this for each category
            if($addon == $config['featured_ad_on'])
            {
                $sql = "SELECT A.*, B.category_id FROM ads A 
    LEFT OUTER JOIN ad_category_xref B ON A.id = B.ad_id AND B.category_id=$cat[id]
    WHERE A.published = 1 AND A.featured=1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            } elseif($addon!=0)
            {
                $sql = "SELECT A.*, B.addon_id, C.category_id FROM ads A 
    LEFT OUTER JOIN ad_addons B ON A.id = B.ad_id AND B.addon_id=$addon 
    LEFT OUTER JOIN ad_category_xref C ON A.id = C.ad_id AND C.category_id=$cat[id]
    WHERE A.published = 1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            } else {
                $sql = "SELECT A.*, B.category_id FROM ads A 
    LEFT OUTER JOIN ad_category_xref B ON A.id = B.ad_id AND B.category_id=$cat[id]
    WHERE A.published = 1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            }
            
            $dbAds = dbselectmulti($sql); 
            $catAds[$cat['id']] = $dbAds['data'];   
       }
       switch($_POST['output_type'])
       {
           case 'indesign':
            exportIndesign($catAds,$settings);
           break;
           
           case 'quark':
            exportQuark($catAds,$settings);
           break;
           
           case 'csv':
            exportCSV($catAds,$settings);
           break;
           
           case 'text':
            exportText($catAds,$settings);
           break;
           
       }
   }
}  
function exportQuark($catAds,$settings)
{
    print "<div class='alert alert-info' role='alert'>This feature is coming.</div>\n";
}

function exportCSV($catAds,$settings)
{
    print "<div class='alert alert-info' role='alert'>This feature is coming.</div>\n";
}

function exportText($catAds,$settings)
{
    print "<div class='alert alert-info' role='alert'>This feature is coming.</div>\n";
}

function exportIndesign($catAds,$settings)
{
   include(INCLUDE_DIR."class.export.php");
   
   if($settings['individiual']==false)
   {
        $liners = new Export('indesign');
   }
   //loop through each category and process each ad
   foreach($catAds as $category=>$catAd)
   {
       $filename = strtolower(str_replace(array(" ",",",".","!","?","*","/","\\","'","\"","#","\$","%","@","(",")","[","]","<",">","+","="),"",$category));
       
       if($settings['individiual']==true)
       {
            $liners = new Export('indesign');
       }
       
       foreach($catAd as $ad)
       {
           //reset variables
           $liner = array();
           $adImage = '';
           $adImageName = '';
           
           if($ad['feature_headline'])
           {
               $liner['headline']=$ad['headline'];
           } 
           
           $liner['featured']=$ad['featured'];
           
           //look for images
           $sql="SELECT * FROM images WHERE ad_id=$ad[id]";
           $dbImages = dbselectmulti($sql);
           if($dbImages['numrows']>0)
           {
               foreach($dbImages['data'] as $image)
               {
                   if($image['for_print'])
                   {
                       $adImage = stripslashes("/uploads/".$image['path'].$image['filename']);
                       $adImageName = $image['filename'];
                       $liner['image'] = $adImage;
                       $liner['image_filename']=$adImageName;
                       exit;
                   }
               }
           }
           
           $body=$ad['ad_text'];
           
           if($adImage!='')
           {
               $body = "***PLACE IMAGE $adImageName***\r\n".$body;
           }
           $liner['body'] = $body;
           $liners->addLiner($liner);
            
       }
       if($settings['individiual']==true)
       {
            $outFiles = $liners->outputFile($filename,$category);
            
            if(!empty($outFiles))
            {
                foreach($outFiles as $label=>$file)
                {
                    print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br>"; 
                }
            }
       }  
   } 
   if($settings['individiual']==false)
   {
       $filename = "all_liners";
       $outFiles = $liners->outputFile($filename,'All ads');
       if(!empty($outFiles))
        {
            foreach($outFiles as $label=>$file)
            {
                print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br>"; 
            }
        }
   }
    
}
