<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Email Templates</h1>
    </div>
   <?php
   switch($action)
   {
       case "edit": record(); break;
       case "Save": save_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    $sql = "SELECT * FROM email_templates WHERE id=$id";
    $dbRecord = dbselectsingle($sql);
    $record = $dbRecord['data'];
    print "<div class='col-xs-12 col-md-9'>";
    print "<form method=post class='form-horizontal'>\n";
    print "<h4>$record[email_name]</h4>";
    make_text('subject',stripslashes($record['subject']),'Subject','Subject line fot the email');
    make_textarea('body',stripslashes($record['body']),'Body','Body of the email message.');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    print "</div>";
    print "<div class='col-xs-12 col-md-3'>";
    print "<div class='well'>";
    print "<h4>Tokens</h4>
    <p>%name% - user full name</p>
    <p>%first% - user first name</p>
    <p>%last% - user last name</p>
    <p>%street% - user street</p>
    <p>%city% - user city</p>
    <p>%state% - user state</p>
    <p>%zip% - user zip</p>
    <p>%phone% - user phone</p>
    <p>%date% - current date</p>
    <p>%email% - user email</p>
    <p>%site_name% - site name</p>
    <p>%site_url% - site url</p>
    <p>%contact_email% - site contact email</p>
    <p>%contact_phone% - site contact phone</p>
    <p>%confirm_link% - registration confirmation link</p>
    <p>%reset_link% - password reset link</p>
    <p>%approve_link% - admin ad approval link</p>
    <p>%bill% - billing record (will add an invoice)</p>
    <p>%ad% - the ad itself that can be shared</p>
    <p>%message% - a message to be included (typically from a user)</p>
    ";
    print "</div></div>\n";
}
function list_records()
{
    $sql="SELECT * FROM email_templates";
    $dbRecords=dbselectmulti($sql);
    tableStart("","Template");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $email=stripslashes($record['email_name']);
            print "<tr>\n";
            print "<td>$email</td>\n";
            print "<td>
            <a href='?action=edit&id=$id' class='btn'>Edit</a>
            </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}
  
function save_record()
{
    $record=$_POST;
    $id = intval($_POST['id']);
    $subject=addslashes($_POST['subject']);
    $body=addslashes($_POST['body']);
    $sql="UPDATE email_templates SET subject='$subject', body='$body' WHERE id=$id";
    $dbUpdate=dbexecutequery($sql);
    $error=$dbUpdate['error'];
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}
