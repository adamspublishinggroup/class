<?php
include "bootstrap.php";

        
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Attention Getters</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM attention_getters WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
        $record['active']=1;
    }
    
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    print "<div class='col-xs-12 col-md-7'>";
    make_checkbox('active',$record['active'],'Active','Check if this attention getter is active');
    make_text('attn_name',stripslashes($record['name']),'Name','');
    make_file('attn_image','Graphic','Attention Getter Graphic','../uploads/attention/'.stripslashes($record['image']));
    print "</div>";
    print "<div class='col-xs-12 col-md-5'>\n";
    print "</div>";
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
}

function delete_record()
{
    $id=intval($_GET['id']);
    
    $sql="DELETE FROM attention_getters WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    $id=intval($_POST['id']);
    if($_POST['active']){$active=1;}else{$active=0;}
    $attn_name=addslashes($_POST['attn_name']);
    
    if($id==0)
    {
        $sql="INSERT INTO attention_getters (name, active) VALUES ('$attn_name', '$active')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
        $record['id']=$id;
    } else {
        $sql="UPDATE attention_getters SET name='$attn_name', active='$active' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    $target_dir = "../uploads/attention/";
    $target_file = $target_dir . basename($_FILES["attn_image"]["name"]);
    
    if(isset($_FILES) && $_FILES['attn_image']['name']!='')
    {
        $uploadOK=1;
        // Allow certain file formats
        $imageFileType =$_FILES['attn_image']['type'];
        if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
        && $imageFileType != "image/gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
            $uploadOK = 0;
        }
        if($uploadOK)
        {
            if (move_uploaded_file($_FILES["attn_image"]["tmp_name"], $target_file)) {
                $sql="UPDATE attention_getters SET image='".addslashes($_FILES['attn_image']['name'])."' WHERE id=$id";
                $dbUpdate=dbexecutequery($sql);
                $error=$dbUpdate['error'];
                
            } else {
                $error='File upload error.';
            }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    $sql="SELECT * FROM attention_getters";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new attention getter</a>","Attention Getter");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['name']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}