<?php
include "bootstrap.php"; 

function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Bill Me / Advertisers <small>For customers who can be billed</small></h1>
    </div>
   <?php
   switch($action)
   {
       case "bill": bill_ad(); break;
       case "billalluser": bill_ad(true,'user'); break;
       case "billalladvertiser": bill_ad(true,'advertiser'); break;
       case "emailinvoice": email_invoice(); break;
       case "payment": enter_payment(); break;
       case "Apply Payment": save_payment(); break;
       case "Send Bill": send_bill(); break;
       case "Save": save_recordy(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}


function enter_payment($error='')
{
    $id=intval($_GET['id']);
    
    
    
    $paymentTypes = array('card'=>'Credit Card', 'cash'=>'Cash', 'check'=>'Check', 'other'=>"Other (put in notes)", 'free'=>'Free');
    
    $sql="SELECT * FROM ads WHERE id = $id";
    $dbAd = dbselectsingle($sql);
    $ad = $dbAd['data'];
    
    print "<form method=post class='form-horizontal'>\n";
    $amount = money_format('%(#4n', $ad['ad_total']); 
    if($error!='')
    {
        print "<div class='alert alert-warning'>$error</div><br />\n";
    }        
    make_descriptor($amount,'Total Due');
    make_checkbox('admin_paid',0,'Mark as paid','Checking this box will write off this ad as paid by admin.');
    make_select('payment_type',$paymentTypes[$ad['payment_type']],$paymentTypes,'Payment Type','What is the payment type');
    make_number('payment','Enter payment amount');
    make_number('last_4','','Last 4 of credit card','Enter last 4 digits of credit card if that is how it was paid');
    make_textarea('notes',stripslashes($ad['notes']),'Notes','Notes, check #, etc.');
    make_hidden('id',$id);
    make_hidden('amount',$ad['ad_total']);
    make_submit('submit','Apply Payment');
    print "</form>\n";
}

function save_payment()
{
    $id=intval($_POST['id']);
    $payment = floatval($_POST['payment']);
    $due = floatval($_POST['amount']);
    $last4 = intval($_POST['last_4']);
    $type = addslashes($_POST['payment_type']);
    $notes = addslashes($_POST['notes']);
    
    if($payment>$due)
    {
        enter_payment("You entered a payment larger than the amount due.");
    } else {
        $dt = date("Y-m-d H:i");
        if($_POST['admin_paid'])
        {
            $sql="UPDATE ads SET admin_paid = 1, ad_paid_dt = '$dt', payment_type='$type', payment_notes='$notes' WHERE id=$id";
            $dbUpdate=dbexecutequery($sql);
        } else {
            $sql="UPDATE ads SET ad_paid_amount = ad_paid_amount + $payment, ad_paid_dt = '$dt', card_last_4 = '$last4', payment_type='$type', payment_notes='$notes' WHERE id=$id";
            $dbUpdate=dbexecutequery($sql);
        }    
    }
    redirect('?action=list');
}

function email_invoice()
{
    $invoiceID = intval($_GET['id']);
    
    $sql="SELECT * FROM invoices WHERE id = $invoiceID";
    $dbInvoice = dbselectsingle($sql);
    $invoice = $dbInvoice['data'];
    $invoiceHTML = stripslashes($invoice['invoice_html']);
    $invoiceHTML = html_entity_decode($invoiceHTML);
    
    //get the correct email address
    if($invoice['user_id']!=0)
    {
        $sql="SELECT * FROM users WHERE id=".$invoice['user_id'];
    } else {
        $sql="SELECT * FROM advertisers WHERE id=".$invoice['advertiser_id'];
    }
    $dbAccount = dbselectsingle($sql);
    $account = $dbAccount['data'];
    $emailData = array('bill'=>$invoiceHTML);
    $emailContent = email_generator(5,$emailData);
            
    $subject = $emailContent['subject'];
    $message = $emailContent['body'];
    
    
    //send confirmation email to user
    if($invoice['pdf_filename']!='')
    {
        $attachments[] = array(
            "filename"=>SITE_URL.'/uploads/bills/'.$invoice['pdf_filename'],
            "name"=>$invoice['pdf_filename'],
            "type"=>'application/pdf'
            );
    } else {
        $attachments[] = array();
    }
    
    if($account['email']!='')
    {
        send_email(stripslashes($account['email']),$subject,$message,0,$attachments);
         //update status to 2 while will mean "mailed"
        $sql="SELECT id FROM ads WHERE invoice_id=$invoiceID";
        $dbAds = dbselectmulti($sql);
        if($dbAds['numrows']>0)
        {
            $dt = date("Y-m-d H:i");
            foreach($dbAds['data'] as $ad)
            {
                $sql="UPDATE ads SET bill_emailed_date='$dt' WHERE id=$ad[id]";
                print $sql;
                $dbUpdate=dbexecutequery($sql);
            }
        }
        $sql="UPDATE invoices SET invoice_status=2 WHERE id=$invoiceID";
        $dbUpdate=dbexecutequery($sql);
        
        print "<div class='alert alert-success clearfix' role='alert'>The invoice has been sent to the customer at $account[email]. <a class='btn btn-success pull-right' href='?action=list'>Return</a></div>\n";
    } else {
        print "<div class='alert alert-warning clearfix' role='alert'>The customer does not have an email address in their file, you'll need to print and mail it. <a class='btn btn-success pull-right' href='?action=list'>Return</a></div>\n";
    }
   
}

function bill_ad($all=false,$type='')
{
   $id=intval($_GET['id']);
   
   //if all is true, we are billing for all unbilled ads on this user
   if($all)
   {
       if($type=='user')
       {
           $sql="SELECT user_id FROM ads WHERE id=$id";
           $dbAd = dbselectsingle($sql);
           $userID=$dbAd['data']['user_id'];
           $sql="SELECT * FROM users WHERE id=$userID";
           $dbUser = dbselectsingle($sql);
           $user = $dbUser['data'];
           $account['type']='user';
           $account['id']=$userID;
           $account['name'] = $user['first'].' '.$user['last'];
           $account['email'] = $user['email'];
           $account['street'] = $user['street'];
           $account['city'] = $user['city'];
           $account['state'] = $user['state'];
           $account['zip'] = $user['zip'];
           
           $sql="SELECT * FROM ads WHERE user_id=$userID AND bill_me=1 AND billed=0";
           
       } else {
           $sql="SELECT advertiser_id FROM ads WHERE id=$id";
           $dbAd = dbselectsingle($sql);
           $advertiserID=$dbAd['data']['advertiser_id'];
           
           $sql = "SELECT * FROM advertisers WHERE id=$advertiserID";
           $dbAdvertiser = dbselectsingle($sql);
           $advertiser = $dbAdvertiser['data'];
           $account['type']='advertiser';
           $account['id']=$advertiserID;
           $account['name'] = $advertiser['name'];
           $account['email'] = $advertiser['email'];
           $account['street'] = $advertiser['street'];
           $account['city'] = $advertiser['city'];
           $account['state'] = $advertiser['state'];
           $account['zip'] = $advertiser['zip'];
           
           $sql="SELECT * FROM ads WHERE advertiser_id=$advertiserID AND bill_me=1 AND billed=0";
       }
       $dbAds = dbselectmulti($sql);
       $ads=$dbAds['data'];
   } else {
       $sql="SELECT * FROM ads WHERE id=$id";
       $dbAd = dbselectsingle($sql);
       
       $userID=$dbAd['data']['user_id'];
       $sql="SELECT * FROM users WHERE id=$userID";
       $dbUser = dbselectsingle($sql);
       $user = $dbUser['data'];
       $account['type']='user';
       $account['id']=$userID;
       $account['name'] = $user['first'].' '.$user['last'];
       $account['email'] = $user['email'];
       $account['street'] = $user['street'];
       $account['city'] = $user['city'];
       $account['state'] = $user['state'];
       $account['zip'] = $user['zip'];
           
       
       $ads[]=$dbAd['data'];
   }
        
   require_once( INCLUDE_DIR . 'class.invoice.php' );
   $invoice = new Invoice();
   
   $invoice->setBillTo($account); 
   foreach($ads as $ad)
   {
       $invoice->addItem($ad);
   }
   $invoice->display(); 
   $invoiceID = $invoice->saveInvoice();
   print "<a class='btn btn-2x btn-primary' href='?action=emailinvoice&id=$invoiceID'>Email invoice</a>
   <a class='btn btn-2x btn-primary' href='printInvoice.php?id=$invoiceID' target='_blank'><i class='fa fa-print'></i> Print invoice</a>
   <a class='btn btn-2x btn-primary' href='?action=list'><i class='fa fa-recycle'></i> Return</a>
   ";
}

function delete_record()
{
    $sql="DELETE FROM ads WHERE id=$id";
    $dbDelete = dbexecutequery($sql);  
    
    
    $sql = "DELETE FROM ads WHERE id = $adId";
    $dbExec = dbexecutequery($sql);
    $sql = "DELETE FROM ad_category_xref WHERE ad_id = $adId";
    $dbDelete = dbexecutequery($sql);
    
    $sql = "SELECT * FROM images WHERE ad_id = $adId";
    $dbImages = dbselectmulti($sql);
    if($dbImages['numrows']>0)
    {
        foreach($dbImages['data'] as $image)
        {
            $file = stripslashes($image['path'].$image['filename']);
            unlink("../uploads/".$file);
        }
        $sql="DELETE FROM images WHERE ad_id = $adId";
        $dbDelete = dbexecutequery($sql);
    }
    
    redirect("?action=list");
}
 

function list_records()
{
    $sql="SELECT * FROM advertisers ORDER BY name";
    $dbAdvertisers = dbselectmulti($sql);
    $advertisers[0]='None';
    if($dbAdvertisers['numrows']>0)
    {
        foreach($dbAdvertisers['data'] as $advertiser)
        {
            $advertisers[$advertiser['id']]=stripslashes($advertiser['name']);
        }
    }
    $sql="SELECT * FROM users ORDER BY last, first";
    $dbUsers = dbselectmulti($sql);
    $users[0]='None';
    if($dbUsers['numrows']>0)
    {
        foreach($dbUsers['data'] as $user)
        {
            $users[$user['id']]=stripslashes($user['first'].' '.$user['last']);
            $usersEmails[$user['id']]=stripslashes($user['email']);
        }
    }
    
    if($_POST['advertiser_id']!=0)
    {
        $advFilter = "AND advertiser_id=".intval($_POST['advertiser_id']);
    }
    if($_POST['user_id']!=0)
    {
        $userFilter = "AND user_id=".intval($_POST['user_id']);
    }
    if($_POST['include_paid'])
    {
        $sql="SELECT * FROM ads WHERE (bill_customer = 1 OR bill_advertiser=1) $advFilter $userFilter ORDER BY created_dt DESC";
    } else {
        $sql="SELECT * FROM ads WHERE (bill_customer = 1 OR bill_advertiser=1) $advFilter $userFilter AND ad_paid_dt IS Null ORDER BY created_dt DESC";
    }
    $dbAds=dbselectmulti($sql);
    $search = "<form method=post>";
    $search.="<b>Advertiser:</b><br>";
    $search.=make_select('advertiser_id',$advertisers[$_POST['advertiser_id']],$advertisers);
    $search.="<br><b>User:</b><br>";
    $search.=make_select('user_id',$users[$_POST['user_id']],$users);
    $search.="<br><label for='include_paid'><input type='checkbox' id='include_paid' name='include_paid'> Include paid ads</label>";
    $search.="<br><br><input type=submit name='submit' value='Search' /><br />";
    $search.="</form>";
    tableStart("$search","ID,Headline,Placed,User,Advertiser,Amount,Invoiced,Emailed,Paid");
    if ($dbAds['numrows']>0)
    {
        foreach($dbAds['data'] as $ad)
        {
            $id=$ad['id'];
            $headline=stripslashes($ad['headline']);
            $user = $users[$ad['user_id']];
            $email = $usersEmails[$ad['user_id']];
            $advertiser = $advertisers[$ad['advertiser_id']];
            $placed = date("m/d/Y H:i",strtotime($ad['created_dt']));
            $amount = money_format('%(#4n', $ad['ad_total']); 
            if($ad['billed'])
            {
                $billDate = ($ad['bill_date']!='' ? date("m/d/Y H:i",strtotime($ad['bill_date'])) : '');   
                $emailDate = ($ad['bill_emailed_date']!='' ? date("m/d/Y H:i",strtotime($ad['bill_emailed_date'])) : '');   
                $paidDate = ($ad['ad_paid_dt']!='' ? date("m/d/Y H:i",strtotime($ad['ad_paid_dt'])) : '');   
                $sql="SELECT invoice_status FROM invoices WHERE id=$ad[invoice_id]";
                $dbInvoice = dbselectsingle($sql);
                $invStatus = $dbInvoice['data']['invoice_status'];
                switch($invStatus)
                {
                    case "1":
                        $invoiced = "<i class='fa fa-check'></i> $billDate";
                        $emailed = "";
                        $paid = "";
                    break;
                    
                    case "2":
                        $invoiced = "<i class='fa fa-check'></i> $billDate";
                        $emailed = "<i class='fa fa-check'></i> $emailDate";
                        $paid = "";
                    
                    break;
                    
                    case "3":
                        $invoiced = "<i class='fa fa-check'></i> $billDate";
                        $emailed = "<i class='fa fa-check'></i> $emailDate";
                        $paid = "<i class='fa fa-check'></i> $paidDate";
                    
                    break;
                }
            } else {
                $invoiced = '';
                $emailed = '';
                $paid = '';
            };
            
            print "<tr>\n";
            print "<td>$id</td>\n";
            print "<td>$headline</td>\n";
            print "<td>$placed</td>\n";
            print "<td>$user</td>\n";
            print "<td>$advertiser</td>\n";
            print "<td>$amount</td>\n";
            print "<td>$invoiced</td>\n";
            print "<td>$emailed</td>\n";
            print "<td>$paid</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=bill&id=$id' class='btn'>Bill Customer</a>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
             <li><a href='?action=billalluser&id=$id'>Bill open ads for user</a></li>
             <li><a href='?action=billalladvertiser&id=$id'>Bill open ads for advertiser</a></li>";
          if ($email!=''){print "  <li><a href='?action=emailinvoice&id=$ad[invoice_id]'>Email invoice</a></li> ";}
          print "   <li><a href='printInvoice.php?id=$ad[invoice_id]' target='_blank'><i class='fa fa-print'></i> Print invoice</a></li>
             <li><a href='?action=payment&id=$id'><i class='fa fa-credit-card'></i> Enter payment on ad</a></li>
             <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete Ad</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbCategories);
}