<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Meta Data</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    global $metaTypes;
    $categories = categories(0);
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM metadata WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
        
        //pull in all meta categories
        $sql="SELECT category_id FROM meta_category_xref WHERE meta_id='$id'";
        $dbCats=dbselectmulti($sql);
        if($dbCats['numrows']>0)
        {
            foreach($dbCats['data'] as $cat)
            {
                $cats[]=$cat['category_id'];
            }
        }
    } else {
        $cats=array();
        $id=0;
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('name',stripslashes($record['meta_name']),'Name');
    make_select('type',$metaTypes[$record['meta_type']],$metaTypes,'Metadata Type');
    ?>
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Categories</label>
        <div class="col-sm-10">
            <small>Please select one or more categories that this meta tag should apply to</small><br>
            <select id="categories" name="categories[]" multiple="multiple" style='width:80%;'>
                <?php
                if( count( $categories ) > 0 ) {
                    foreach( $categories as $cat ) {
                        if(in_array($cat['category_id'],$cats)){$checked='selected';}else{$checked='';}
                        print "<option value='$cat[category_id]' $checked>$cat[category_name]</option>\n";
                        if( count( $cat['subcats'] ) > 0 ) {
                            foreach( $cat['subcats'] as $subcat ) {
                                if(in_array($subcat['category_id'],$cats)){$checked='selected';}else{$checked='';}
                                print "<option value='$subcat[category_id]' $checked>&nbsp;&nbsp;&nbsp;&nbsp;$subcat[category_name]</option>\n";
                            }
                        }
                    }
                }
                ?>
            </select>
        </div>
    </div>
    
    <?php
    make_textarea('default',$record['meta_default'],'Default','Default value. Use true/false for checkbox, YYYY-MM-DD for dates, and for option lists, enter a list of items with a "|" delimiter (Max length 255 characters).',50,20,false);
    make_checkbox('required',$record['required'],'Required','If checked, the user will be required to fill this field out.');
    make_hidden('id',$record['id']);
    make_submit('submit','Save');
    print "</form>\n";
    ?>
    <script type="text/javascript">
        $("#categories").select2();
    </script>
    <?php
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM metadata WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    $sql="DELETE FROM meta_category_xref WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    
    //@TODO need to clean up ad records when metadata tags are deleted!
    
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $name=addslashes($_POST['name']);
    $type=addslashes($_POST['type']);
    $default=addslashes($_POST['default']);
    if($_POST['required']){$required=1;}else{$required=0;}
    $cats = $_POST['categories'];
 
    if($id==0)
    {
        $sql="INSERT INTO metadata (meta_name, meta_type, meta_default, required) VALUES ('$name', '$type', '$default', $required)";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE metadata SET meta_name='$name', meta_type='$type', meta_default='$default', required='$required' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    $sql="DELETE FROM meta_category_xref WHERE meta_id=$id";
    $dbDelete=dbexecutequery($sql);
    //create the meta_category_xref records
    if(count($cats)>0)
    {
        $catInserts=array();
        foreach($cats as $cat)
        {
            $catInserts[]="($id,$cat)";
        }
        $sql="INSERT INTO meta_category_xref (meta_id, category_id) VALUES ".implode(",",$catInserts);
        $dbInsert=dbinsertquery($sql);
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    global $types;
    $sql="SELECT * FROM metadata";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new meta tag</a>","Name,Type");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['meta_name']);
            $type=stripslashes($record['meta_type']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>$types[$type]</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=xref&id=$id'>Categories</a></li>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}