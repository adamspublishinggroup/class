<?php
include "bootstrap.php";

        
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Promotional Blocks</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}

function record($record=array())
{
    $positions = array('left'=>"Left rail",'right-top'=>"Right rail top",'right-bottom'=>"Right rail bottom",'leader'=>"Leaderboard", 'footer'=>"Footer position");
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM rail_items WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    
    print "<div class='row'>
    <div class='col-md-9'>";
    print "<form method=post class='form-horizontal' enctype='multipart/form-data'>\n";
    make_text('name',stripslashes($record['name']),'Block name','For internal purposes only');
    make_select('rail_position',$positions[$record['rail_position']],$positions,'Placement','Select a placement');
    make_text('link',stripslashes($record['link']),'Link for action','Click-through url for action');
    make_text('headline',stripslashes($record['headline']),'Headline','Headline of item');
    make_textarea('content',stripslashes($record['content']),'Content of item');
    make_file('filename','Use an image',"../uploads/rail-items/".stripslashes($record['filename']));
    make_text('button_text',stripslashes($record['button_text']),'Button Text','Text for action button');
    make_color('button_color',$record['button_color'],'Button Color','Color of action button');
    make_color('button_hover_color',$record['button_hover_color'],'Button Hover Color','Hover Color of action button');
    make_color('button_text_color',$record['button_text_color'],'Button Text Color','Text Color of action button');
    make_color('background_color',$record['background_color'],'Background Color','Background color of item');
    make_color('border_color',$record['border_color'],'Border Color','Color of border');
    make_color('headline_color',$record['headline_color'],'Text Color','Color of headline text');
    make_color('text_color',$record['text_color'],'Text Color','Color of body copy');
    make_textarea('html_only',$record['html_only'],'HTML Only','Paste in a block of html/javascript',50,20,false);
    make_checkbox('active',$record['active'],'Active','Check if this item is active');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    print "</div>
    <div class='col-md-3'>
    <div class='well'>
    <h4>Two options for promos are a text only, or an image.</h4>
    <p>If you select an image, here are the necessary sizes:</p>
    <p>Left rail position: 236px x 300px</p>
    <p>Right rail positions: 300px x 250px</p>
    <p>Leaderboard: 728px x 90px</p>
    <p>Footer: 728px x 90px</p>
    </div>
    </div>";
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="SELECT * FROM rail_items WHERE id=$id";
    $dbImage=dbselectsingle($sql);
    $image=$dbImage['data'];
    $filename=stripslashes($image['filename']);
    unlink("../uploads/rail_items/".$filename);
    
    $sql="DELETE FROM rail_items WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    $record=$_POST;
    
    $id=intval($_POST['id']);
    $name=addslashes($_POST['name']);
    $rail_position=addslashes($_POST['rail_position']);
    $link=addslashes($_POST['link']);
    $headline=addslashes($_POST['headline']);
    $content=addslashes($_POST['content']);
    $background_color=addslashes($_POST['background_color']);
    $border_color=addslashes($_POST['border_color']);
    $button_text=addslashes($_POST['button_text']);
    $button_color=addslashes($_POST['button_color']);
    $button_hover_color=addslashes($_POST['button_hover_color']);
    $button_text_color=addslashes($_POST['button_text_color']);
    $text_color=addslashes($_POST['text_color']);
    $headline_color=addslashes($_POST['headline_color']);
    $html_only = htmlentities($_POST['html_only']);
    if($_POST['active']){$active=1;}else{$active=0;}
    
    if($id==0)
    {
        $sql="INSERT INTO rail_items (name, rail_position, link, headline, content, background_color, border_color, button_text, 
        button_color, button_hover_color, button_text_color, text_color, headline_color, active, html_only ) VALUES 
        ('$name', '$rail_position', '$link', '$headline', '$content', '$background_color', '$border_color', '$button_text', 
        '$button_color', '$button_hover_color', '$button_text_color', '$text_color', '$headline_color', '$active', '$html_only' )";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE rail_items SET name='$name', rail_position='$rail_position', link='$link', headline='$headline' content='$background_color', 
        background_color='$background_color', border_color='$border_color', button_text='$button_text', html_only='$html_only',  
        button_color='$button_color', button_hover_color='$button_hover_color', button_text_color='$button_text_color', 
        text_color='$text_color', headline_color='$headline_color', active='$active' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    $target_dir = "../uploads/rail_items/";
    $target_file = $target_dir . basename($_FILES["filename"]["name"]);
    
    if($_FILES['filename']['tmp_name']!='')
    {
        $uploadOK=1;
        // Allow certain file formats
        $imageFileType =$_FILES['filename']['type'];
        if($imageFileType != "image/jpg" && $imageFileType != "image/png" && $imageFileType != "image/jpeg"
        && $imageFileType != "image/gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed. You uploaded a $imageFileType file. ";
            $uploadOK = 0;
        }
        if($uploadOK)
        {
            if (move_uploaded_file($_FILES["filename"]["tmp_name"], $target_file)) {
                $sql="UPDATE rail_items SET filename='".addslashes($_FILES['filename']['name'])."' WHERE id=$id";
                $dbUpdate=dbexecutequery($sql);
                $error=$dbUpdate['error'];
                
            } else {
                $error='File upload error.';
            }
        }
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database or with the image.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
  
}

function list_records()
{
    $positions = array('left'=>"Left rail",'right-top'=>"Right rail top",'right-bottom'=>"Right rail bottom",'leader'=>"Leaderboard", 'footer'=>"Footer position");
    $sql="SELECT * FROM rail_items";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new item</a>","Name,Position,Active");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['name']);
            $active=($record['active'] ? "Yes" : "No");
            $position=$positions[stripslashes($record['rail_position'])];
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>$position</td>\n";
            print "<td>$active</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}