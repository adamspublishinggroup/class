<?php
include "bootstrap.php";

    
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Store &amp; Rack Locations</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": record(); break;
       case "edit": record(); break;
       case "Save": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }  
}


function record($record=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM locations WHERE id=$id";
        $dbRecord = dbselectsingle($sql);
        $record = $dbRecord['data'];
    } else {
        $id=0;
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('location_name',stripslashes($record['location_name']),'Locations','Name of the location');
    make_phone('phone',$record['phone'],'Phone');
    make_text('street',stripslashes($record['street']),'Street','Street Address');
    make_text('city',stripslashes($record['city']),'City');
    make_state('state',stripslashes($record['state']),'State');
    make_text('zip',stripslashes($record['zip']),'Zip','');
    make_hidden('id',$id);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="DELETE FROM locations WHERE id=$id";
    $dbDelete=dbexecutequery($sql);
    redirect("?action=list");
}

function save_record()
{
    
    $id=intval($_POST['id']);
    
    $location=addslashes($_POST['location_name']);
    $phone=addslashes($_POST['phone']);
    $street=addslashes($_POST['street']); 
    $city=addslashes($_POST['city']); 
    $state=addslashes($_POST['state']); 
    $zip=addslashes($_POST['zip']);
     
     //update the ad record with the lat/ln
    $geo=geocode($street.",".$city." ".$state." ".$zip);
    $lat=$geo['lat'];
    $lon=$geo['lon'];
     
    
    if($id==0)
    {
        $sql="INSERT INTO locations (location_name, street, city, state, zip, phone, lat, lon) VALUES 
        ('$location', '$street', '$city', '$state', '$zip', '$phone', '$lat', '$lon')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        $id = $dbInsert['insertid'];
    } else {
        $sql="UPDATE locations SET location_name='$location', street='$street', city='$city', state='$state', zip='$zip', phone='$phone', lat='$lat', lon='$lon' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        record($record);
    } else {
        redirect("?action=list");
    }
}

function list_records()
{
    $sql="SELECT * FROM locations ORDER BY location_name";
    $dbRecords=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new locations</a>","Locations Name");
    if ($dbRecords['numrows']>0)
    {
        foreach($dbRecords['data'] as $record)
        {
            $id=$record['id'];
            $name=stripslashes($record['location_name']);
            print "<tr>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbRecords);
}