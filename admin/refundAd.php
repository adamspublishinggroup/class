<?php
include "bootstrap.php";
require (VENDOR_DIR.'/autoload.php' ); 
use Omnipay\Omnipay;
use Omnipay\Common\CreditCard;
if($_POST)
{
    $adID=intval($_POST['ad_id']);
    $sql="SELECT * FROM ads WHERE id=$adID";
    $dbAd = dbselectsingle($sql);

    $ad = $dbAd['data'];

    $refundAmount=floatval($_POST['refund_amount']);
    $transaction_id=stripslashes($ad['transaction_id']);

    if($_POST['leave_published']){$leave_published=1;}else{$leave_published=0;}

    if($refundAmount>$ad['ad_paid_amount'])
    {
        print "<div class='alert alert-warning' role='alert'>You can not refund more than the amount paid!</div>\n";
        refund_form($id);
    } else {
        $stripe = Omnipay::create('Stripe');
        $stripe->setApiKey(STRIPE_KEY);
        // Send purchase request
        $transaction = $stripe->refund(array(
            'amount'                   => $refundAmount,
            'transactionReference'     => $transaction_id,
        ));
        $response = $transaction->send();
        if ($response->isSuccessful()) {
            print "<div class='alert alert-warning' role='alert'>Refund transaction was successful.</div>\n";
            $refund_id = $response->getTransactionReference();
            $reason=addslashes($_POST['refund_reason']);
            $dt=date("Y-m-d H:i");
            $sql="UPDATE ads SET refunded = 1, refund_amount=$refundAmount, refund_dt='$dt', refund_reason='$reason' WHERE id=$adID";
            $dbUpdate=dbexecutequery($sql);
        } else {
            print "<div class='alert alert-warning' role='alert'>".$response->getMessage()."</div>\n";
        }
    }
}
function page()
{
   refund_form($_GET['id']);     
}

function refund_form($id)
{
    $sql="SELECT * FROM ads WHERE id=$id";
    $dbAd = dbselectsingle($sql);
    $ad = $dbAd['data'];
     
    $paidAmount = $ad['ad_paid_amount'];
    print "<form method=post>\n";
    make_number('refund_amount',$paidAmount,'Refund Amount','Total amount to be refunded (can not exceed amount paid of '.money_format('%(#4n', $paidAmount).')');
    make_textarea('refund_reason','','Reason','Why are you refunding this ad?');
    make_checkbox('leave_published',0,'Leave published','Check if you want to leave the ad published');
    
    make_hidden('ad_id',$id);
    make_submit('submit','Process Refund');
    print "</form>\n";
}