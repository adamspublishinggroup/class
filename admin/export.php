<?php
include "bootstrap.php";
function page()
{
   if($_POST)
   {
       prepareOutput();
   } else {
       $startDate = date("m/d/Y");
       $endDate = date("m/d/Y",strtotime("+1 month"));
       $order = array(
           'featurealpha'=>'Alphabetical (featured ads first)',
           'featuredatedesc'=>'Start Date Desc (featured ads first)',
           'featuredateasc'=>'Start Date Asc (featured ads first)',
           'featurerand'=>'Random (featured ads first)',
           'alpha'=>'Alphabetical (featured ads mixed in)',
           'datedesc'=>'Start Date Desc (featured ads mixed in)',
           'dateasc'=>'Start Date Asc (featured ads mixed in)',
           'rand'=>'Random'
       );
       $outputs = array(
        'indesign'=>'Indesign Tagged Text',
        'quark'=>'Quark Xpress Tagged Text',
        'text'=>'Text',
        'csv' =>'CSV File'
       )
       
        ?>
        <div class="page-header">
          <h1>Export Ads</h1>
        </div>
       <?php
       $sql="SELECT * FROM addons WHERE active=1";
       $dbAddons = dbselectmulti($sql);
       $addons[0] = "Select an addon or leave unchanged for all ads";
       if($dbAddons['numrows']>0)
       {
           foreach($dbAddons['data'] as $addon)
           {
               $addons[$addon['id']]=$addon['addon_short'];
           }
       }
       
       $sql="SELECT id, category_name FROM categories WHERE active=1 ORDER BY category_name";
       $dbCategories = dbselectmulti($sql);
       $cats[0] = "Export all categories";
       if($dbCategories['numrows']>0)
       {
           foreach($dbCategories['data'] as $cat)
           {
               $cats[$cat['id']]=$cat['category_name'];
           }
       }
       
       
       print "<form method=post class='form-horizontal'>\n";
       ?>
       <div class="form-group">
            <label for="startDate" class="col-sm-2 control-label">Find ads in this range</label>
            <label for="startDate" class="col-sm-1 control-label">Start Date</label>
            <div class="col-sm-2">
                <div class="input-group date" id="startDate" style='width: 150px;'>
                    <input type="text" name="startDate" value='<?= $startDate ?>' />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <label for="endDate" class="col-sm-1 control-label">End Date</label>
            <div class="col-sm-2">
                <div class="input-group date" id="endDate"  style="width:150px;">
                    <input type="text" name="endDate" value='<?= $endDate ?>' />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
       </div>
       <?php
       
       /*
       * create an array of "publish" dates based on this sites establish pub DOW. Lets run out 2 months in advance
       */
       $cDate = time();
       $endDate = strtotime("+2 months");
       $dates[0]="Select based on date range set above";
       while($cDate<$endDate)
       {
           if(date("w",$cDate)==PRINT_DOW)
           {
               $dates[date("Y-m-d",$cDate)]=date("m/d/Y",$cDate);
           }
           $cDate = strtotime(date("Y-m-d",$cDate)." +1 day");
       }
       make_select('output_type',$outputs['indesign'],$outputs,'Output Format','What format would you like the export in?');
       make_select('print_date',$dates[0],$dates,'Print Date','Select ads for this print date.');
       make_select('category',$cats[0],$cats,'Category','Which category do you want?');
       make_select('addon',$addons[0],$addons,'Addons','Export ads based on a specific addon?');
       make_select('order',$order[0],$order,'Order','How do you want the ads ordered?');
       make_checkbox('individual',1,'Individual','Check to have each category in its own file.');
       make_checkbox('print',1,'Print Ads','Check to select all ads that are set up for print.');
       make_checkbox('paid',1,'Paid','Include only ads that are paid, or are bill me/advertiser.');
       make_submit('submit','Export Ads');
       print "</form>\n";
       ?>
    <script type="text/javascript">
        $(function () {
            $('#startDate').datetimepicker({
                format: 'MM/DD/YYYY'
            });
            $('#endDate').datetimepicker({
                format: 'MM/DD/YYYY',
                useCurrent: false //Important! See issue #1075
            });
            $("#startDate").on("dp.change", function (e) {
                $('#endDate').data("DateTimePicker").minDate(e.date);
            });
            $("#endDate").on("dp.change", function (e) {
                $('#startDate').data("DateTimePicker").maxDate(e.date);
            });
        });
    </script> 
       <?php 
   }  
}


function prepareOutput()
{
   global $config;
   print "Preparing the exports...<br />";
   $dateFilter = "";
   $printFilter = "";
   $paidFilter = "";
   $catFilter = "";
   $addonFilter = "";
   $order = "";
   $startDate=date("Y-m-d",strtotime($_POST['startDate']));    
   $endDate=date("Y-m-d",strtotime($_POST['endDate']));    
   $printDate = $_POST['print_date'];
   
   $cat = addslashes($_POST['category']);
   $addon = addslashes($_POST['addon']);
   $order = addslashes($_POST['order']);
   if($_POST['individual']){$individual = true;}else{$individual=false;}
   if($_POST['paid']){
       $paidFilter = true;
   }
   if($_POST['print']){
       $printFilter = "AND print=1";
   }
   $settings = array ('individual'=>$individual);
   
   
   //now fetch the liners
   if($print_date!=0)
   {
       $dateFilter = "AND FIND_IN_SET('$printDate',print_days)";
   } else {
       $dateFilter = "AND start_date<='$endDate' AND end_date>='$startDate'";
   }
   
   switch($order)
   {
       case "featurealpha";
        $orderFilter = "A.featured DESC, A.headline";
       break;
       
       case "featuredatedesc";
        $orderFilter = "A.featured DESC, A.start_date DESC";
       break;
       
       case "featuredateasc";
        $orderFilter = "A.featured DESC, A.start_date ASC";
       break;
       
       case "featurerand";
        $orderFilter = "A.featured DESC, RAND()";
       break;
       
       case "alpha";
        $orderFilter = "A.headline";
       break;
       
       case "datedesc";
        $orderFilter = "A.start_date DESC";
       break;
       
       case "dateasc";
        $orderFilter = "A.start_date ASC";
       break;
       
       case "random";
        $orderFilter = "RAND()";
       break;
       
   }
   
   
   $sql = "SELECT id, category_name FROM categories WHERE active=1 ".($_POST['category']!=0 ? "AND id=".intval($_POST['category']) : "");
   $dbCats = dbselectmulti($sql);
   if($dbCats['numrows']>0) {
       foreach($dbCats['data'] as $cat)
       {
            //we are going to do this for each category
            if($addon == $config['featured_ad_on'])
            {
                $sql = "SELECT A.*, B.category_id FROM ads A 
    LEFT OUTER JOIN ad_category_xref B ON A.id = B.ad_id AND B.category_id=$cat[id]
    WHERE A.published = 1 AND A.featured=1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            } elseif($addon!=0)
            {
                $sql = "SELECT A.*, B.addon_id, C.category_id FROM ads A 
    LEFT OUTER JOIN ad_addons B ON A.id = B.ad_id AND B.addon_id=$addon 
    LEFT OUTER JOIN ad_category_xref C ON A.id = C.ad_id AND C.category_id=$cat[id]
    WHERE A.published = 1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            } else {
                $sql = "SELECT A.*, B.category_id FROM ads A 
    LEFT OUTER JOIN ad_category_xref B ON A.id = B.ad_id AND B.category_id=$cat[id]
    WHERE A.published = 1 $dateFilter $printFilter
    ORDER BY $orderFilter";
            }
            
            $dbAds = dbselectmulti($sql); 
            $catAds[$cat['category_name']] = $dbAds['data'];   
       }
       switch($_POST['output_type'])
       {
           case 'indesign':
            exportIndesign($catAds,$settings);
           break;
           
           case 'quark':
            exportQuark($catAds,$settings);
           break;
           
           case 'csv':
            exportCSV($catAds,$settings);
           break;
           
           case 'text':
            exportText($catAds,$settings);
           break;
           
       }
   }
   
   print "<br /><br /><br /><a href='export.php' class='btn btn-success'>Export completed. Click to run another.</a>";
}  

function exportQuark($catAds,$settings)
{
    print "<div class='alert alert-info' role='alert'>This feature is coming.</div>\n";
}

function exportCSV($catAds,$settings)
{
   include(INCLUDE_DIR."class.export.php");
   print "Preparing the CSV export...<br />";
   if(!$settings['individual'])
   {
        $export = new Export('csv');
   }
   //loop through each category and process each ad
   foreach($catAds as $category=>$ads)
   {
       print "Generating file for $category...<br />";
       $filename = strtolower(str_replace(array(" ",",",".","!","?","*","/","\\","'","\"","#","\$","%","@","(",")","[","]","<",">","+","="),"",$category));
       
       if($settings['individual'])
       {
            $export = new Export('csv');
       }
       
       foreach($ads as $ad)
       {
           //print "&nbsp;&nbsp;&nbsp;Adding record for ad id: $ad[id] of ".count($ads)." total ads...<br />";
           //reset variables
           $liner = array();
           $adImage = '';
           $adImageName = '';
           
           if($ad['feature_headline'])
           {
               $liner['headline']=$ad['headline'];
           } 
           
           $liner['featured']=$ad['featured'];
           
           //look for images
           $sql="SELECT * FROM images WHERE ad_id=$ad[id]";
           $dbImages = dbselectmulti($sql);
           if($dbImages['numrows']>0)
           {
               foreach($dbImages['data'] as $image)
               {
                   if($image['for_print'])
                   {
                       $adImage = stripslashes("../uploads/".$image['path'].$image['filename']);
                       $adImageName = $image['filename'];
                       $liner['image'] = $adImage;
                       $liner['image_filename']=$adImageName;
                       continue;
                   }
               }
           }
           
           $body=$ad['ad_text'];
           
           
           $export->addLiner($liner);
            
       }
       if($settings['individual'])
       {
            print "Generating individual export file for $category...<br />";
            $outFiles = $export->outputFile($filename,$category);
            
            if(!empty($outFiles))
            {
                foreach($outFiles as $label=>$file)
                {
                    print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br /><br />"; 
                }
            }
       }  
   } 
   if(!$settings['individual'])
   {
       print "Generating an export zip that contains all images and ads...<br />";
       $filename = "all_liners";
       $outFiles = $export->outputFile($filename,'All ads');
       if(!empty($outFiles))
        {
            foreach($outFiles as $label=>$file)
            {
                print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br /><br />"; 
            }
        }
   }
}

function exportText($catAds,$settings)
{
    print "<div class='alert alert-info' role='alert'>This feature is coming.</div>\n";
}

function exportIndesign($catAds,$settings)
{
   include(INCLUDE_DIR."class.export.php");
   print "Preparing the InDesign export...<br />";
   if(!$settings['individual'])
   {
        $export = new Export('indesign');
   }
   //loop through each category and process each ad
   foreach($catAds as $category=>$ads)
   {
       print "Generating file for $category...<br />";
       $filename = strtolower(str_replace(array(" ",",",".","!","?","*","/","\\","'","\"","#","\$","%","@","(",")","[","]","<",">","+","="),"",$category));
       
       if($settings['individual'])
       {
            $export = new Export('indesign');
       }
       
       foreach($ads as $ad)
       {
           //print "&nbsp;&nbsp;&nbsp;Adding record for ad id: $ad[id] of ".count($ads)." total ads...<br />";
           //reset variables
           $liner = array();
           $adImage = '';
           $adImageName = '';
           
           if($ad['feature_headline'])
           {
               $liner['headline']=$ad['headline'];
           } 
           
           $liner['featured']=$ad['featured'];
           
           //look for images
           $sql="SELECT * FROM images WHERE ad_id=$ad[id]";
           $dbImages = dbselectmulti($sql);
           if($dbImages['numrows']>0)
           {
               foreach($dbImages['data'] as $image)
               {
                   if($image['for_print'])
                   {
                       $adImage = stripslashes("../uploads/".$image['path'].$image['filename']);
                       $adImageName = $image['filename'];
                       $liner['image'] = $adImage;
                       $liner['image_filename']=$adImageName;
                       continue;
                   }
               }
           }
           
           $body=$ad['ad_text'];
           
           if($adImage!='')
           {
               $body = "***PLACE IMAGE $adImageName***\r\n".$body;
           }
           $liner['body'] = $body;
           
           $export->addLiner($liner);
            
       }
       if($settings['individual'])
       {
            print "Generating individual export file for $category...<br />";
            $outFiles = $export->outputFile($filename,$category);
            
            if(!empty($outFiles))
            {
                foreach($outFiles as $label=>$file)
                {
                    print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br /><br />"; 
                }
            }
       }  
   } 
   if(!$settings['individual'])
   {
       print "Generating an export zip that contains all images and ads...<br />";
       $filename = "all_liners";
       $outFiles = $export->outputFile($filename,'All ads');
       if(!empty($outFiles))
        {
            foreach($outFiles as $label=>$file)
            {
                print "<a href='$file' class='btn btn-success'>Right-click to download the file for $label</a><br /><br />"; 
            }
        }
   }
    
}

