<?php
include "bootstrap.php";   
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Categories</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": edit_category(); break;
       case "edit": edit_category(); break;
       case "Save": save_category(); break;
       case "delete": delete_category(); break;
       default: list_categories(); break;
   }  
}

function edit_category($category=array())
{
    $maps = array(0=>"No map", 1=>"Yardsales", 2=>"Real Estate");
    $sql="SELECT * FROM maps";
    $dbMaps = dbselectmulti($sql);
    $maps[0]="No map";
    if($dbMaps['numrows']>0)
    {
        foreach($dbMaps['data'] as $map)
        {
            $maps[$map['id']]=stripslashes($map['map_name']);
        }
    }
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM categories WHERE id=$id";
        $dbCategory = dbselectsingle($sql);
        $category = $dbCategory['data'];
    } else {
        if(!isset($category['active'])){$category['active']=1;}
        if(!isset($category['category_order'])){$category['category_order']=1;}
        if(!isset($category['parent_id'])){$category['parent_id']=intval($_GET['parent_id']);}
    }
    print "<form method=post class='form-horizontal'>\n";
    make_checkbox('active',$category['active'],'Active','Check if this category is active');
    make_checkbox('free_ads',$category['free_ads'],'Free','Check if ads in this category should be free');
    make_text('category_name',stripslashes($category['category_name']),'Category Name');
    make_textarea('category_description',stripslashes($category['category_description']),'Description','This will be shown to site visits and can include any helpful information.');
    make_select('category_map',$maps[$category['category_map']],$maps,'Tied to map','Select the correct map if this category will appear on one');
    make_text('category_code',stripslashes($category['category_code']),'System Code','Enter the front end code for importing ads to this category');
    make_number('category_order',$category['category_order'],'Order','Sorting order for categories. By default everything is 1, then sorted alphabetically. If you want a special order, top is 1, down to whatever number.');
    make_hidden('id',$category['id']);
    make_hidden('parent_id',$category['parent_id']);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_category()
{
    $id=intval($_GET['id']);
    $sql="SELECT parent_id FROM categories WHERE id=$id";
    $dbCat = dbselectsingle($sql);
    $parentID = $dbCat['data']['parent_id'];
    
    $sql="DELETE FROM categories WHERE id=$id";
    $dbDelete = dbexecutequery($sql);
    $sql="DELETE FROM meta_category_xref WHERE category_id=$id";
    $dbDelete = dbexecutequery($sql);
    $sql="DELETE FROM ad_category_xref WHERE category_id=$id";
    $dbDelete = dbexecutequery($sql);
    redirect("?action=list&parent_id=$parentID");
}

function save_category()
{
    $category=$_POST;
    
    $id=intval($_POST['id']);
    $parentID=intval($_POST['parent_id']);
    $categoryOrder=intval($_POST['category_order']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($_POST['free_ads']){$free=1;}else{$free=0;}
    $categoryName=addslashes($_POST['category_name']);
    $categoryDescription=addslashes($_POST['category_description']);
    $code=addslashes($_POST['category_code']);
    $map=intval($_POST['category_map']);
    if($id==0)
    {
        $sql="INSERT INTO categories (parent_id, category_name, category_description, category_order, active,free_ads, 
        category_map, category_code) VALUES ('$parentID', 
        '$categoryName', '$categoryDescription', '$categoryOrder', $active, $free, '$map', '$code')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        $sql="UPDATE categories SET category_name='$categoryName', category_description='$categoryDescription', category_order='$categoryOrder', 
        active='$active',free_ads=$free, category_map='$map', category_code='$code' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        category($category);
    } else {
        redirect("?action=list&parent_id=$parentID");
    }
}

function list_categories()
{
    $parentID=intval($_GET['parent_id']);
    
    $sql="SELECT * FROM categories WHERE parent_id=$parentID ORDER BY category_order ASC, category_name DESC";
    $dbCategories=dbselectmulti($sql);
    $actions = "<a href='?action=add&parent_id=$parentID'>Add new category</a>";
    if($parentID!=0)
    {
        $actions.="<br><a href='?action=list'>Return to top level</a>";
    }
    tableStart($actions,"Active,Name");
    if ($dbCategories['numrows']>0)
    {
        foreach($dbCategories['data'] as $category)
        {
            $id=$category['id'];
            $parentId=$category['parent_id'];
            $name=stripslashes($category['category_name']);
            if($category['active']){$active='True';}else{$active='No';}
            print "<tr>\n";
            print "<td>$active</td>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>";
           if(NESTED_CATEGORIES){ print " <li><a href='?action=list&parent_id=$id'>Sub-categories</a></li> "; }
         print "   <li><a href='?action=delete&id=$id&parent_id=$parentId' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbCategories);
}