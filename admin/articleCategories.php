<?php
include "bootstrap.php";
function page()
{
   ?>
    <div class="page-header">
      <h1>Article Categories</h1>
    </div>
   <?php
   if($_POST)
   {
       $action=$_POST['submit'];
   } else {
       $action = $_GET['action'];
   }
   switch($action)
   {
       case "add": edit_record();break;
       case "edit": edit_record();break;
       case "Save Category": save_record(); break;
       case "delete": delete_record(); break;
       default: list_records(); break;
   }
     
}

function edit_record()
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql="SELECT * FROM article_categories WHERE id=$id";
        $dbCat = dbselectsingle($sql);
        $record = $dbCat['data'];
    }
    print "<form method=post class='form-horizontal'>\n";
    make_text('category',stripslashes($record['category']),'Category');
    if($id==0)
    {
        make_text('slug',stripslashes($record['slug']),'Slug','Used for their url (no spaces, alpha-numeric characters only, all lowercase)');
    } else {
        make_descriptor(stripslashes($record['slug']),'Slug');    
    }
    make_checkbox('active',$record['active'],'Active','Check to make this category visible to the publish');
    make_hidden('id',$id);
    make_submit('submit','Save Category');
    print "</form>\n";
}


function save_record()
{
    $id=intval($_POST['id']);
    $category=addslashes($_POST['category']);
    
    $slug = strtolower(str_replace(array(" ","*",".",",","?","\"","\\","/","#","!","@","\$","%","^","(",")","'","-","+","_","=","|","{","[","]","}","<",">"),"",$_POST['slug']));
    
    if($id==0)
    {
    $sql="INSERT INTO article_categories (category, slug, active) VALUES 
   ('$category', '$slug', '$active')";
   $dbInsert=dbinsertquery($sql);
   $categoryID=$dbInsert['insertid'];
         
    $baseDir = "../articles/".$slug;
    if(!file_exists($baseDir))
    {
        if(mkdir($baseDir, 0755))
        {
            print "Made directory called $baseDir<br>";
        //create the category index 
        $pIndex = <<<INDEX
<?php
\$page = 'articles';
require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/article-template.php' );
require_once( '../../boot_close.php' );
INDEX;
        if(file_put_contents($baseDir."/index.php", $pIndex))
        {
            print "Successfully wrote category index file<br>";
        } else {
            print "Error writing the category index file<br>";
        }
        
        } else {
            print "Failed to create directory $baseDir/read";
        }
    } else {
        print "Directory existed";
    }
    } else {
        $sql="UPDATE article_categories SET category='$category', active='$active' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        
    }
    redirect("?action=list");
}

function delete_record()
{
    $id=intval($_GET['id']);
    $sql="SELECT * FROM article_categories WHERE id=$id";
    $dbCat = dbselectsingle($sql);
    $cat = $dbCat['data'];
    
    $slug=stripslashes($cat['slug']);
    
    if(unlink("../articles/$slug/read/index.php"))
    {
        if(unlink("../articles/$slug/read"))
        {
            if(unlink("../articles/$slug/index.php"))
            {
               if(unlink("../articles/$slug"))
               {
                  $sql="DELETE FROM article_categories WHERE id=$id";
                  $dbDelete = dbexecutequery($sql);      
               }    
            }  
        }
    }
    
    redirect("?action=list");
}

function list_records()
{
   $sql="SELECT id, category, slug FROM article_categories ORDER BY category";
   $dbAdvertisers=dbselectmulti($sql);
   if($dbAdvertisers['numrows']>0)
   {
       tableStart("<a href='?action=add'>Add new category</a>","ID,Category");
       foreach($dbAdvertisers['data'] as $adv)
       {
           $id=$adv['id'];
           print "<tr>";
           print "<td>".$adv['id']."</td>";
           print "<td>".$adv['category']."</td>";
           print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-dark dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>
            <li><a href='?action=delete&id=$id' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
           print "</tr>";    
       }
       tableEnd($dbAdvertisers);
   } else {
       print "<div class='alert alert-warning'><b>No Categories</b> No categories have been created yet. <a href='?action=add'>Create the first one!</a></div>";
   } 
}