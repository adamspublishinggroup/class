<?php
  /*
  *  Import text files with ads
  */                           
  
  //we'll need to import them to a generic user account and assign them to a specified category
  
  
include "bootstrap.php";
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Import Classified Ads</h1>
    </div>
   <?php
   switch($action)
   {
       case "Import": save_record(); break;
       default: record(); break;
   }  
}

function record()
{
    //get categories
    $sql="SELECT * FROM categories ORDER BY category_name";
    $dbCats = dbselectmulti($sql);
    $cats[0]='Select category or leave here to pull category from liner';
    if($dbCats['numrows']>0)
    {
       foreach($dbCats['data'] as $cat)
       {
           $cats[$cat['id']]=stripslashes($cat['category_name']);
       } 
    }
    
    $sources = array(
        'lagrande' => "Lagrande Text import",
        'buysgalore' => "Buys Galore csv import",
        'quark'=> "Quark XPress Tagged Text",
        'vdata'=> "Vision Data XML"
    );
    print "<div class='col-xs-12 col-md-8'>\n";
    print "<form method=post enctype='multipart/form-data'>\n";
    make_select('source',$sources[0],$sources,'Source','Select the source type for the file');
    make_select('category_id',$cats[0],$cats,'Category','Select category to import listings to');
    make_checkbox('remove',0,'Remove existing','Check this to delete all previously IMPORTED ads for this category when importing the new ones');
    make_file('listings','Listings File');
    make_submit('submit','Import');
    print "</form>\n";
    
    print "</div>\n";
    
    print "<div class='col-xs-12 col-md-4 well'>\n";
    print "<h4>Format Options</h4>";
    print "<p><b>Text file</b> - formatted with headline on one, body on next, empty line between ads</p>";
    print "<p><b>CSV</b> - data mapped to columns, text in quotes, see <a href='/admin/exampleCSV.csv'>example file here.</a> </p>";
    print "<p><b>Vision Data XML</b> - specific Vision Data XML format</p>";
    print "<p><b>Quark Tagged Text</b> - tagged text from Quark Xpress</p>";
    print "</div>";
} 

function save_record()
{
    $category = $_POST['category_id'];
    if($_POST['remove']){
        if($category!=0)
        {
            $sql= "UPDATE ads SET status = 9 WHERE imported = 1 AND id IN (SELECT ad_id FROM ad_category_xref WHERE category_id=$category)";
            $dbUpdate = dbexecutequery($sql);
        } else { 
            $sql= "UPDATE ads SET status = 9 WHERE imported = 1";
            $dbUpdate = dbexecutequery($sql);
        }
    }
    if(isset($_FILES))
    {
        $ads = array();
        switch ($_POST['source'])
        {
            case 'lagrande':
                $ads = lagrandeImporter();
                
            break;
            
            case 'buysgalore':
                $ads = buysGaloreImporter();
                if(count($ads)>0)
                {
                    $created = date("Y-m-d H:i");
                    $inserts = array();
                    
                    foreach($ads as $ad)
                    {
                        $advertiser_id = 0;
                        $userID = 0;
                        print "Processing:<br><pre>";
                        print_r($ad);
                        print "</pre>\n";
                        //is it a company?
                        if($ad['company']!='')
                        {
                            //see if it exists
                            $sql="SELECT * FROM advertisers WHERE name='".addslashes($ad['company'])."'";
                            $dbAdvertiser = dbselectsingle($sql);
                            if($dbAdvertiser['numrows']>0)
                            {
                                $advertiser_id = $dbAdvertiser['data']['id'];
                            } else {
                                $slug = $ad['company'];
                                $slug = strtolower(str_replace(array(" ","*",".",",","?","\"","\\","/","#","!","@","\$","%","^","(",")","'","-","+","_","=","|","{","[","]","}","<",">"),"",$slug));
    
                                //create it
                                $sql="INSERT INTO advertisers (name, slug, active) VALUES ('".addslashes($ad['company'])."', '$slug', 1)";
                                $dbInsert = dbinsertquery($sql);
                                $advertiser_id = $dbInsert['insertid'];
                                
                                if($advertiser_id!=0)
    {
        $baseDir = "../advertisers/".$slug;
    if(!file_exists($baseDir))
    {
        if(mkdir($baseDir, 0755))
        {
            //print "Made directory called $baseDir<br>";
        //create the index 
        $pIndex = <<<INDEX
<?php
\$page = 'advertiser-listing';
require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/advertiser-listings-template.php' );
require_once( '../../boot_close.php' );
INDEX;
        if(file_put_contents($baseDir."/index.php", $pIndex))
        {
            //print "Successfully wrote listing index file<br>";
        } else {
            //print "Error writing the listing index file<br>";
        }
        //check for detail directory, or create
        if(!file_exists($baseDir."/detail/"))
        {
            mkdir($baseDir."/detail/",0755);
        $pDetail = <<<DETAIL
<?php
\$page = 'advertiser-detail';
require_once( '../../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/advertiser-detail-template.php' );
require_once( '../../../boot_close.php' );
DETAIL;
        file_put_contents($baseDir."/detail/index.php", $pDetail);
        
        }
        } else {
            //print "Failed to create directory $baseDir/detail";
        }
    } else {
        //print "Directory existed";
    }
    }
                                
                                
                            }
                        }
                        
                        
                        //lets see if the user exists
                        $name = explode(" ",str_replace("/ ","/",$ad['contact']));
                        $first = addslashes($name[0]);
                        $last = addslashes($name[1]);
                        $sql="SELECT * FROM users WHERE first = '$first' AND last = '$last'";
                        $dbUser = dbselectsingle($sql);
                        if($dbUser['numrows']>0)
                        {
                            $userID = $dbUser['data']['id'];
                        } else {
                            //create the user
                            $phone = str_replace("-","",$phone);
                            $address = explode(",",$address);
                            $street = trim($address[0]);
                            $city = trim($address[1]);
                            $stateBlock = explode(" ",trim($address[2]));
                            $state = trim($stateBlock[0]);
                            $zip = trim($stateBlock[1]);
                            $token = generate_random_string(32);
                            $passwordClear=generate_random_string(8);
                            $createdDT = date("Y-m-d H:i");
                            $password=password_hash($passwordClear,PASSWORD_DEFAULT);
                            $token = generate_random_string(32);
                            $sql="INSERT INTO users (username, first, last, email, phone, password, token, verified, level, created_dt, temporary, advertiser_id, street, city, state, zip, gender, birthdate, bio, bill_me, 13_check, veteran, email_flags, trusted, senior) VALUES  ('$userName', '$first', '$last', '', '$phone', '$password', '$token', 1, 0,  '$createdDT', 0,  $advertiser_id, '$street', '$city', '$state', '$zip', 'NA', '1969-01-01', '', '', 1, 0, 0, 0, 0)";
                            $dbInsert=dbinsertquery($sql);
                            $userID=$dbInsert['insertid'];
                              
                        }
                        
                       //now we save the ad itself
                       $start = $ad['start'];
                       $end = $ad['stop'];
                       $adText = addslashes($ad['body']);
                       $parts = explode(" ",$ad['body']);
                       $headline = $parts[0].' '.$parts[1].' '.$parts[2].' '.$parts[3];
                       $headline = addslashes(strip_tags($headline));
                       $boldCount = $ad['bold_count']; 
                       $italicCount = $ad['italic_count']; 
                       $underlineCount = $ad['underline_count']; 
                       $adTotal = ($ad['total']==''? 0: $ad['total']);
                       $billCustomer = ($ad['bill_customer']==''? 0: $ad['bill_customer']);
                       $billAdvertiser = ($ad['bill_advertiser']==''? 0: $ad['bill_advertiser']);
                       
                       $adRecord = "($advertiser_id,$userID,'$start', '$end', '$headline','$adText','','0', '', 1, '$created', '$wordCount', '$boldCount','$underlineCount', '$italicCount', 1, 1, 1, $adTotal, 1, $billAdvertiser, $billCustomer, '$ad[notes]')";
                            
                        $sql="INSERT INTO ads (advertiser_id,user_id, start_date, end_date, headline, ad_text, internet_text, feature_headline, keywords, status, created_dt, word_count, bold_count, underline_count, italic_count, published, admin_placed, imported, ad_total, admin_paid, bill_advertiser, bill_customer, notes) VALUES $adRecord";
                        $dbAd = dbinsertquery($sql);
                        if($dbAd['error']=='')
                        {
                            print "Created record with $sql.<br>";
                            $category = $ad['category'];
                            $imported++;
                            $adID=$dbAd['insertid'];
                            $sql="INSERT INTO ad_category_xref (ad_id, category_id) VALUES ($adID, $category)";
                            $dbInsert=dbinsertquery($sql);
                            
                        } else {
                            print $dbAd['error']."<hr>";
                        } 
                    }
                }
                print "Inserted a total of $imported ads.";
            break;
            
            case 'quark':
                $ads = quarkImporter();
            break;
            
            case 'vdata':
                $ads = visionDataImporter();
            break;
            
        }
    }
    
    
    print "<a href='?action=import' class='btn btn-primary'>Import another file</a>";
}


function lagrandeImporter()
{
    $ads = array();
    $listings = file_get_contents($_FILES["listings"]["tmp_name"]);
    $listings = str_replace("�","'",$listings);
    $listings = str_replace("�� "," ",$listings);
    $listings = str_replace("� "," ",$listings);
    $lines = explode("\n",$listings);
    if(count($lines)==0){$lines[0]=$listings;} //for a text file with only one line
    $i=0;
    $inad = false;
    $headline = '';
    $body = '';
    $featureHead = 0;
    if(count($lines)==1)
    {
        $body = $lines[0];
        $parts = explode(" ",$body);
        $headline = $parts[0].' '.$parts[1];
        $featureHead = 0;
        $ads[$i]=array('headline'=>addslashes($headline),'body'=>addslashes($body), 'feature_headline'=>$featureHead);
            
    } else {
        foreach($lines as $line)
        {
            if(trim($line)=='')
            {
                if($headline=='')
                {
                    $parts = explode(" ",$body);
                    $headline = $parts[0].' '.$parts[1];
                    $featureHead = 0;
                }
                
                $ads[$i]=array('headline'=>addslashes($headline),'body'=>addslashes($body), 'feature_headline'=>$featureHead);
                $headline = '';
                $body = '';
                $nohead = 0;
                $i++;
                $inad = false;
                
            } else {
                $inad = true;
                if($body=='')
                {
                    $body = $line;
                } else {
                    $headline = $body;
                    $featureHead = 1;
                    $body = $line;
                    
                }
            }
                
        }
    }
    
    if(count($ads)>0)
    {
        $created = date("Y-m-d H:i");
        $inserts = array();
        
        foreach($ads as $ad)
        {
            if(trim($ad['body'])!='')
            {
                if(isset($ad['start']))
                {
                    $start = date("Y-m-d",strtotime($ad['start']));
                    if($ad['stop']!='')
                    {
                        $end = date("Y-m-d",strtotime($ad['stop']));
                    } else {
                        $end = date("Y-m-d",strtotime("+3 weeks"));
                    }
                } else {
                    $start = date("Y-m-d");
                    $end = date("Y-m-d",strtotime("+3 weeks"));
                }
                if($ad['feature_headline'])
                {
                    $boldCount = count(explode(" ",$ad['headline']));
                }
                $wordCount = count(explode(" ",$ad['body']));
                $adRecord = "(1,'$start', '$end', '$ad[headline]','$ad[body]','','$ad[feature_headline]', '', 1, '$created', '$wordCount', '$boldCount', 1, 1, 1)";
                
                $sql="INSERT INTO ads (user_id, start_date, end_date, headline, ad_text, internet_text, feature_headline, keywords, status, created_dt, word_count, bold_count, published, admin_placed, imported) VALUES $adRecord";
                $dbAd = dbinsertquery($sql);
                if($dbAd['error']=='')
                {
                    $imported++;
                    $adID=$dbAd['insertid'];
                    $sql="INSERT INTO ad_category_xref (ad_id, category_id) VALUES ($adID, $category)";
                    $dbInsert=dbinsertquery($sql);
                } else {
                    print $dbAd['error']."<br>";
                }
            }
        }
        print "Imported $imported ads<br><br>";
    }
    
}

function buysGaloreImporter()
{
    include("../includes/parsecsv.lib.php");
    
    //grab all categories with id and category code
    $categories = array();
    $sql = "SELECT id, category_name FROM categories";
    $dbCats = dbselectmulti($sql);
    if($dbCats['numrows']>0)
    {
        foreach($dbCats['data'] as $cat)
        {
            $categories[$cat['id']]=$cat['category_name'];
        }
    }
    
    
    //format is:
    //      Company,Contact,Address,Phone,Category,Ad Text,Start Date,Stop Date,Attention Getter/ Photo,JUMBO Classified,Center,Bold,Underline,Italic,Border,Shaded,Extra Words,Price per week,Total,Payment Terms,Notes,reminder
    $ads = array();
    
    $csv = new parseCSV($_FILES["listings"]["tmp_name"]);
    $listings = $csv->data;
    $i=1;
    if(count($listings)>0)
    {
        foreach($listings as $listing)
        {
            $ad = array();
            $addon = array();
            
            $ad['company']=$listing['Company'];
            $ad['contact']=$listing['Contact'];
            $ad['address']=$listing['Address'];
            $ad['phone']=$listing['Phone'];
            //see if we can match a category
            if(in_array($listing['Category'],$categories))
            {
                $catID = array_search($listing['Category'],$categories);
            } else {
                $catID = 0;
            }
            $ad['category']=$catID;
            $ad['body']=$listing['Ad Text'];
            $ad['word_count'] = count(explode(" ",$listing['Ad Text']));
            $ad['start']=date("Y-m-d",strtotime($listing['Start Date']));
            if($listing['Stop Date']=='')
            {
                $ad['stop']=date("Y-m-d",strtotime("+3 weeks"));
            } else {
                $ad['stop']=date("Y-m-d",strtotime($listing['Stop Date'])); 
            }
            //figure out the addons
            if($listing['Bold']!='')
            {
                $body = $listing['Ad Text'];
                //usually has price in front
                $styledParts = explode("-",trim($listing['Bold']));
                if(floatval($styledParts[0])==$styledParts[0])
                {
                    //then the first part is a number, we just want the second part
                    $updatePart = $styledParts[1];
                } else {
                    $updatePart = trim($listing['Bold']);
                }
                if(trim($updatePart) != '')
                {
                    $body = str_replace(trim($updatePart),"<b>".trim($updatePart)."</b>",$body);
                    $ad['bold_text']=$updatePart;
                    $ad['body']=$body;
                    $ad['bold_count']=count(explode(" ",$updatePart));
                }
            } 
            if($listing['Underline']!='')
            {
                $body = $listing['Ad Text'];
                //usually has price in front
                $styledParts = explode("-",trim($listing['Underline']));
                if(floatval($styledParts[0])==$styledParts[0])
                {
                    //then the first part is a number, we just want the second part
                    $updatePart = $styledParts[1];
                } else {
                    $updatePart = trim($listing['Underline']);
                }
                if(trim($updatePart) != '')
                {
                    $body = str_replace(trim($updatePart),"<u>".trim($updatePart)."</u>",$body);
                    $ad['underline_text']=$updatePart;
                    $ad['body']=$body;
                    $ad['underline_count']=count(explode(" ",$updatePart));
                }
            } 
            if($listing['Italic']!='')
            {
                $body = $listing['Ad Text'];
                //usually has price in front
                $styledParts = explode("-",trim($listing['Italic']));
                if(floatval($styledParts[0])==$styledParts[0])
                {
                    //then the first part is a number, we just want the second part
                    $updatePart = $styledParts[1];
                } else {
                    $updatePart = trim($listing['Italic']);
                }
                if(trim($updatePart) != '')
                {
                    $body = str_replace(trim($updatePart),"<i>".trim($updatePart)."</i>",$body);
                    $ad['italic_text']=$updatePart;
                    $ad['body']=$body;
                    $ad['italic_count']=count(explode(" ",$updatePart));
                }
            } 
            
            if($listing['Jumbo Classified']!='')
            {
                $addon['jumbo']=1;
            } 
            if($listing['Center']!='')
            {
                $addon['centered']=1;
            } 
            if($listing['Border']!='')
            {
                $addon['border']=1;
            } 
            if($listing['Shaded']!='')
            {
                $addon['shaded']=1;
            } 
            $ad['addons']=$addon;
            $ad['notes'] = $listing['Notes']."<br>".$listing['reminder'];
            if($listing['Payment Terms']=='bill')
            {
                $ad['ad_paid']=1;
                if($listing['Company']=='')
                {
                    $ad['bill_customer']=1;
                } else {
                    $ad['bill_advertiser']=1;
                }
                
            }  
            if($listing['Payment Terms']=='cash')
            {
                $ad['ad_paid']=1;
                $ad['payment_type']='cash';
            }  
            if(substr($listing['Payment Terms'],0,3)=='cc#')
            {
                $ad['ad_paid']=1;
                $ad['payment_type']='card';
            }  
            $ad['total']=$listing['Total'];
            $ads[]=$ad;
            $i++;   
        }
    }
    /*
    print "<pre>";
    print_r($ads);
    print "</pre>\n";
    */
    return $ads;
}

function quarkImporter()
{
    //format is:
    //      start-date,stop-date,"headline","ad-text",[optional]feature-headline,[optional]feature-ad
}

function visionDataImporter()
{
    
}