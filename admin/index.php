<?php
include "bootstrap.php";
function page()
{
    //get # of ads created per date
    $sql="SELECT count(id) AS adCount, DATE_FORMAT(created_dt, '%Y-%m-%d') AS cdate FROM ads WHERE published=1 AND status=1 AND created_dt>='".date("Y-m-d",strtotime("-1 month"))."' GROUP BY DATE_FORMAT(created_dt, '%m/%d/%Y')";
    $dbAdCounts = dbselectmulti($sql);
    $lineData = "";
    foreach($dbAdCounts['data'] as $data){$lineData.="['$data[cdate] 12:00AM',$data[adCount]],";}
    $lineData = trim($lineData,","); 
    
    
    //build up some stats
    
    
    //how many users in the past 24 hours
    $sql="SELECT count(id) as newusers FROM users WHERE created_dt>='".date("Y-m-d H:i",strtotime("-24 hours"))."'";
    $dbNewUsers = dbselectsingle($sql);
    $newUsersCount = $dbNewUsers['data']['newusers']; 
    
    //ads created but not published
    $sql="SELECT count(id) as adcount FROM ads WHERE status=1 AND ad_paid_dt = Null AND created_dt>='".date("Y-m-d H:i",strtotime("-24 hours"))."'";
    $dbUnpaidAds = dbselectsingle($sql);
    $unpaidAds = $dbUnpaidAds['data']['adcount'];
    
    //bill me ads that haven't been billed
    $sql="SELECT count(id) as adcount FROM ads WHERE bill_customer = 1 AND billed = 0";
    $dbUnbilled = dbselectsingle($sql);
    $unbilled = $dbUnbilled['data']['adcount'];
    
    //free ads this week
    $sql="SELECT count(id) as adcount FROM ads WHERE free_ad = 1 AND created_dt>='".date("Y-m-d H:i",strtotime("-1 week"))."'";
    $dbFreeAds = dbselectsingle($sql);
    $freeAds = $dbFreeAds['data']['adcount'];
    
    //ads in last week
    $sql="SELECT count(id) as adcount FROM ads WHERE created_dt>='".date("Y-m-d H:i",strtotime("-1 week"))."'";
    $dbPastDayAds = dbselectsingle($sql);
    $pastWeekAds = $dbPastDayAds['data']['adcount'];
    
    //ads in past 24 hours
    $sql="SELECT count(id) AS adCount, DATE_FORMAT(created_dt, '%Y-%m-%d %H:%i') AS ctime FROM ads WHERE published=1 AND status=1 AND created_dt>='".date("Y-m-d",strtotime("-1 day"))."' GROUP BY DATE_FORMAT(created_dt, '%Y-%m-%d')";
    $dbAdCounts  = dbselectmulti($sql);
    $hourData = "";
    foreach($dbAdCounts['data'] as $data){
        $hourData.="['$data[ctime]',$data[adCount]],";
    }
    $hourData = trim($hourData,","); 
    
    //ads in past 24 hours as bar
    $sql="SELECT count(id) AS adCount, DATE_FORMAT(created_dt, '%H') AS ctime FROM ads WHERE published=1 AND status=1 AND created_dt>='".date("Y-m-d",strtotime("-1 day"))."' GROUP BY DATE_FORMAT(created_dt, '%Y-%m-%d')";
    $dbAdCounts  = dbselectmulti($sql);
    $startHour = date("h",strtotime("-1 day"));
    for($i=1;$i<=24;$i++)
    {
        $found = false;
        if($startHour<23)
        {
            $startHour++;
        } else {
            $startHour = 0;
        }
        if($startHour>=0 && $startHour<12)
        {
            $htype='am';
        } else {
            $htype='pm';
        }
        if($startHour>12)
        {
            $displayHour = $startHour-12;
        } else {
            $displayHour = $startHour;
        }
        foreach($dbAdCounts['data'] as $adC)
        {
            if($adC['ctime']==$startHour)
            {
                $found = true;
                $hourBarData.="$adC[adCount],";
                $countTick.="'$displayHour$htype',";
            }
        }
        if(!$found)
        {
           $hourBarData.="0,";
           $countTick.="'$displayHour$htype',"; 
        }
    }
    $hourBarData=trim($hourBarData,",");
    $countTick=trim($countTick,",");
    
    //flagged ads
    $sql="SELECT COUNT(id) as flagged FROM ads WHERE flagged_dt>='".date("Y-m-d H:i",strtotime("-3 days"))."'";
    $dbFlaggedAds = dbselectsingle($sql);
    $flaggedAds = $dbFlaggedAds['data']['flagged'];
    
    //flagged users
    $sql="SELECT COUNT(id) as flagged FROM users WHERE email_flags>0 AND last_email_flag>='".date("Y-m-d H:i",strtotime("-3 days"))."'";
    $dbFlaggedUsers = dbselectsingle($sql);
    $flaggedUsers = $dbFlaggedUsers['data']['flagged'];
    
    ?>
   <div class='row'>
       <div class="page-header">
          <h1>Dashboard</h1>
        </div>
       <div class='col-sm-12'>
        <h4>Chart of submissions by day for last 30 days</h4>
       <div id="adsByDate" style="height:300px; width:100%"></div>

       <script>
       $(document).ready(function(){
           var datePlot = [<?= $lineData ?>];
           var adsByDatePlot = $.jqplot ('adsByDate', [datePlot], {
              title: 'Ads placed in last 30 days',
              axes: {
                // options for each axis are specified in seperate option objects.
                xaxis: {
                  renderer:$.jqplot.DateAxisRenderer,
                  tickOptions:{formatString: '%b %#d'},
                  min:'<?php echo date("Y-m-d",strtotime("-1 month"))." 12:00AM" ?>', 
                  tickInterval:'1 day'
                },
                yaxis: {
                  label: "# of Ads"
                }
              },
              series:[{lineWidth:4, markerOptions:{style:'square'}}]
            });
           /*
           var hourPlot = [<?= $hourData ?>];
           var adsByHourPlot = $.jqplot ('adsByHour', [hourPlot], {
              title: 'Ads placed in last 24 hours',
              axes: {
                // options for each axis are specified in seperate option objects.
                xaxis: {
                  renderer:$.jqplot.DateAxisRenderer,
                  tickOptions:{formatString:'%H'},
                  tickInterval:'1 hour'
                },
                yaxis: {
                  label: "# of Ads"
                }
              },
              series:[{lineWidth:4, markerOptions:{style:'square'}}]
            });
            */
            $.jqplot.config.enablePlugins = true;
            var s1 = [<?= $hourBarData ?>];
            var ticks = [<?= $countTick ?>];   
            var hourPlotBar = $.jqplot('adsByHour', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                animate: !$.jqplot.use_excanvas,
                seriesDefaults:{
                    renderer:$.jqplot.BarRenderer,
                    pointLabels: { show: true }
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });
       });
       </script>
       
       </div>
   </div>
   
   <div class='row' style='border-top:1px solid black;padding-top:10px;margin-top:10px;'>
       <div class='col-sm-6'>
        <h4>Ads posted in past 24 hours</h4>
        <div id="adsByHour" style="height:200px; width:100%"></div>
       </div>
       <div class='col-sm-6'>
        <h4>Current week stats (#liners, #paid, etc)</h4>
        <table class='table table-bordered table-striped table-condensed'>
            <tr><td>New users</td><td><?= $newUsersCount ?></td></tr>
            <tr><td>Abandoned Ads</td><td><?= $unpaidAds ?></td></tr>
            <tr><td>Open "Bill Me" Ads</td><td><?= $unbilled ?></td></tr>
            <tr><td>Ads in past week</td><td><?= $pastWeekAds ?></td></tr>
            <tr><td>New flagged ads</td><td><?= $flaggedAds ?></td></tr>
            <tr><td>New flagged users</td><td><?= $flaggedUsers ?></td></tr>
        </table>
       </div>
   
   </div>
   <div class='row'>
    <div class='col-xs-12'>
        <div class='btn-group'>
            <a href='billMe.php' class='btn btn-primary'>Bill Me Ads</a>
            <a href='flaggedUsers.php' class='btn btn-primary'>Flagged Users</a>
            <a href='flaggedAds.php' class='btn btn-primary'>Flagged Ads</a>
        </div>
    </div>
   </div>
   <?php   
}