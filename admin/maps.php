<?php
include "bootstrap.php";   
function page($action='list')
{
   ?>
    <div class="page-header">
      <h1>Maps</h1>
    </div>
   <?php
   switch($action)
   {
       case "add": edit_category(); break;
       case "edit": edit_category(); break;
       case "Save": save_category(); break;
       case "delete": delete_category(); break;
       default: list_categories(); break;
   }  
}

function edit_category($category=array())
{
    $id=intval($_GET['id']);
    if($id!=0)
    {
        $sql = "SELECT * FROM maps WHERE id=$id";
        $dbCategory = dbselectsingle($sql);
        $category = $dbCategory['data'];
    } 
    print "<form method=post class='form-horizontal'>\n";
    make_checkbox('active',$category['active'],'Active','Check if this map is active');
    make_text('map_name',stripslashes($category['map_name']),'Map Name');
    make_hidden('id',$category['id']);
    make_submit('submit','Save');
    print "</form>\n";
    
}

function delete_category()
{
    $sql="DELETE FROM maps WHERE id=$id";
    $dbDelete = dbexecutequery($sql);  
    redirect("?action=list");
}

function save_category()
{
    
    $id=intval($_POST['id']);
    $name=addslashes($_POST['map_name']);
    if($_POST['active']){$active=1;}else{$active=0;}
    if($id==0)
    {
        $sql="INSERT INTO maps (active, map_name) VALUES ('$active', '$name')";
        $dbInsert=dbinsertquery($sql);
        $error=$dbInsert['error'];
        
    } else {
        $sql="UPDATE maps SET map_name='$name', active='$active' WHERE id=$id";
        $dbUpdate=dbexecutequery($sql);
        $error=$dbUpdate['error'];
    }
    if($error!='')
    {
        print "<div class='alert alert-danger' role='alert'>There was a problem updating the database.<br>$error</div>";
        category($category);
    } else {
        redirect("?action=list&parent_id=$parentID");
    }
}

function list_categories()
{
    
    $sql="SELECT * FROM maps";
    $dbCategories=dbselectmulti($sql);
    tableStart("<a href='?action=add'>Add new map</a>","Active,Name");
    if ($dbCategories['numrows']>0)
    {
        foreach($dbCategories['data'] as $category)
        {
            $id=$category['id'];
            $name=stripslashes($category['map_name']);
            if($category['active']){$active='True';}else{$active='No';}
            print "<tr>\n";
            print "<td>$active</td>\n";
            print "<td>$name</td>\n";
            print "<td>
        <div class='btn-group'>
          <a href='?action=edit&id=$id' class='btn'>Edit</a>
          <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
            <span class='caret'></span>
            <span class='sr-only'>Toggle Dropdown</span>
          </button>
          <ul class='dropdown-menu'>";
         print "   <li><a href='?action=delete&id=$id&parent_id=$parentId' class='delete'><i class='fa fa-trash'></i> Delete</a></li>
          </ul>
        </div>
        </td>";
            print "</tr>\n";
        }
    }
    tableEnd($dbCategories);
}