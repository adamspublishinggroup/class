<?php

require_once("../includes/bootCore.php");

$gate  = $_POST['gate'];


if($_GET['mode']=='test')
{
	$gate = $_GET['gate'];
	$GLOBALS['user']['id'] = 8;
	$_POST['adId']=$_GET['adId'];
}


// verify user is logged in
if( $gate == 'verifyUser' ) {
    function verifyUser() {
        $return = array();
        $userId = $GLOBALS['user']['id'];
        if( $userId && $userId > 0 ) {
            $return['status'] = 'success';
        } else {
            $return['status'] = 'fail';
            $return['msg'] = "It doesn't appear you are logged in.\nPlease log in to use this feature.";
        }
        echo json_encode($return);
    }
    verifyUser();
}



// verify user is logged in
if( $gate == 'deleteAd' ) {
	$adId = intval($_POST['adId']);
    function deleteAd($adId) {
		$return = array();

        $sql = "DELETE FROM ads WHERE id = $adId";
        $dbExec = dbexecutequery($sql);
        $sql = "DELETE FROM ad_category_xref WHERE ad_id = $adId";
        $dbDelete = dbexecutequery($sql);
        
        $sql = "SELECT * FROM images WHERE ad_id = $adId";
        $dbImages = dbselectmulti($sql);
        if($dbImages['numrows']>0)
        {
            foreach($dbImages['data'] as $image)
            {
                $file = stripslashes($image['path'].$image['filename']);
                unlink("../uploads/".$file);
            }
            $sql="DELETE FROM images WHERE ad_id = $adId";
            $dbDelete = dbexecutequery($sql);
        }
        
        if( $dbExec['error'] == '' ) {
            //success
            $return['status'] = 'success';                                              
        } else {
            //fail
            $return['status'] = 'fail';
            $return['msg'] = "We were unable to delete ad.\nRefresh the browser and try again.\nIf the issue persists, please contact us.";
        }

        echo json_encode($return);
	}
	deleteAd($adId);
}


// report ad as inappropriate
if( $gate == 'report' ) {
	$adId = intval($_POST['adId']);
	function report($adId) {
		$return = array();
        $dt = date("Y-m-d H:i:s");
		$sql = "UPDATE ads SET flagged = flagged + 1, flagged_dt = '$dt' WHERE id = $adId";
		$dbExec = dbexecutequery($sql);

		if( $dbExec['error'] == '' ) {
			//success
			$return['status'] = 'success';
			$return['newGate'] = 'unreport';
			$return['html'] = 'Reported <i class="fa fa-check" aria-hidden="true"></i>';
		} else {
			//fail
			$return['status'] = 'fail';
			$return['msg'] = "We were unable to report this ad.\nRefresh the browser and try again.\nIf the issue persists, please contact us.";
		}

		echo json_encode($return);
	}
	report($adId);
}
// allow un-report immediately after reporting (if by mistake)
if( $gate == 'unreport' ) {
	$adId = intval($_POST['adId']);
	function unreport($adId) {
		$return = array();

		$sql = "UPDATE ads SET flagged = flagged - 1, flagged_dt = Null WHERE id = $adId";
		$dbExec = dbexecutequery($sql);

		if( $dbExec['error'] == '' ) {
			//success
			$return['status'] = 'success';
			$return['newGate'] = 'report';
			$return['html'] = 'Report';
		} else {
			//fail
			$return['status'] = 'fail';
			$return['msg'] = "We were unable to un-report this ad.\nRefresh the browser and try again.\nIf the issue persists, please contact us.";
		}

		echo json_encode($return);
	}
	unreport($adId);
}


// save ad
if( $gate == 'save' ) {
	$adId = intval($_POST['adId']);
	function save($adId) {
		$return = array();
		$userId = $GLOBALS['user']['id'];
		if( $userId && $userId > 0 ) {
			$dt = date("Y-m-d H:i:s");
			$sql = "INSERT INTO ads_saved ( user_id, ad_id, added_dt ) VALUES ( '$userId', '$adId', '$dt' )";
			$dbInsert = dbinsertquery($sql);
			if( $dbInsert['numrows'] > 0 ) {
				//success
				$return['status'] = 'success';
				$return['newGate'] = 'unsave';
				$return['html'] = 'Saved <i class="fa fa-check" aria-hidden="true"></i>';
			} else {
				//fail
				$return['status'] = 'fail';
				$return['msg'] = "We were unable to unsave this ad for you.\nRefresh the browser and try again.\nIf the issue persists, please contact us to see if there is a problem with your account.";
			}
		} else {
			$return['status'] = 'fail';
			$return['msg'] = "It doesn't appear you are logged in.\nPlease log in to use this feature.";
		}

		echo json_encode($return);
	}
	save($adId);
}

// unsave ad
if( $gate == 'unsave' ) {
	$adId = intval($_POST['adId']);
	function unsave($adId) {
		$return = array();
		$userId = $GLOBALS['user']['id'];
		if( $userId && $userId > 0 ) {
			$sql = "DELETE FROM ads_saved WHERE user_id=$userId AND ad_id=$adId";
			$dbDelete = dbexecutequery($sql);
			if( $dbDelete['error'] == '' ) {
				//success
				$return['status'] = 'success';
				$return['newGate'] = 'save';
				$return['html'] = 'Save';
			} else {
				//fail
				$return['status'] = 'fail';
				$return['msg'] = "We were unable to unsave this ad for you.\nRefresh the browser and try again.\nIf the issue persists, please contact us to see if there is a problem with your account.";
			}
		} else {
			$return['status'] = 'fail';
			$return['msg'] = "It doesn't appear you are logged in.\nPlease log in to use this feature.";
		}

		echo json_encode($return);
	}
	unsave($adId);
}

// remove ad from saved ads area
if( $gate == 'remove' ) {
	$adId = intval($_POST['adId']);
	function remove($adId) {
		$return = array();
		$userId = $GLOBALS['user']['id'];
		if( $userId && $userId > 0 ) {
			$sql = "DELETE FROM ads_saved WHERE user_id=$userId AND ad_id=$adId";
			$dbDelete = dbexecutequery($sql);
			if( $dbDelete['error'] == '' ) {
				//success
				$return['status'] = 'success';
				$return['newGate'] = 'save';
				$return['html'] = 'Removed <i class="fa fa-check" aria-hidden="true"></i>';
			} else {
				//fail
				$return['status'] = 'fail';
				$return['msg'] = "We were unable to remove this ad for you.\nRefresh the browser and try again.\nIf the issue persists, please contact us to see if there is a problem with your account.";
			}
		} else {
			$return['status'] = 'fail';
			$return['msg'] = "It doesn't appear you are logged in.\nPlease log in to use this feature.";
		}

		echo json_encode($return);
	}
	remove($adId);
}

// email an ad to a friend
if( $gate == 'email' ) {
	function email() {
		$userId = $GLOBALS['user']['id'];
		if( $userId && $userId > 0 ) {
			$form_adId = intval($_POST['adId']);
			$form_sender = $_POST['sender_name'];
			$form_recipient = $_POST['recipient_email'];
			$form_message = $_POST['message'];
			$form_token = $_POST['cToken'];

			$thisAd = getSingleAd( $form_adId, false );

			$messageContent = '';
			$adId = 0;
			if( !empty( $thisAd ) ) {

				// data from db
				$adId = $thisAd['id'];
				$adUserId = $thisAd['user_id'];
				$adStart = $thisAd['start_date'];
				$adEnd = $thisAd['end_date'];
				$adTitle = $thisAd['headline'];
				$adRaw = $thisAd['ad_text'];
				$adText = strip_tags( $adRaw, '<p><a><b><i><u><strike><strong><em><br>' );
				$adKeywords = $thisAd['keywords'];
				$adStatus = $thisAd['status'];
				$adCats = $thisAd['cats'];
				$adImages = $thisAd['images'];
				$hasImg = false;
				if( !empty($adImages) ) {
					$hasImg = true;
				}
				$colorPri1 = '#DA225E';
				$emailOptions = array();
				$sql = "SELECT * FROM email_info WHERE id = 1";
				$dbArr = dbselectsingle($sql);
				if( $dbArr['numrows'] > 0 ) {
					$emailOptions = $dbArr['data'];
				}
				$hasPreCopy = false;
				$hasPostCopy = false;
				$preCopy = trim($emailOptions['header']);
				$preCopy = strip_tags( $preCopy, '<p><a><b><i><u><strike><strong><em><br>' );
				if( $preCopy > '' ) { $hasPreCopy = true; }
				$postCopy = trim($emailOptions['footer']);
				$postCopy = strip_tags( $postCopy, '<p><a><b><i><u><strike><strong><em><br>' );
				if( $postCopy > '' ) { $hasPostCopy = true; }

				if( $adStatus == 1 ) {

					$messageContent .= '<table border="0" cellpadding="0" width="600" cellspacing="0">';
					$messageContent .= '<tbody>';
					$messageContent .= '<tr><td>This ad was sent to you on the behalf of: <b>' . $form_sender . '</b></td></tr>';
					$messageContent .= '<tr><td>' . $form_message . '&nbsp;</td></tr>';
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '</tbody>';
					$messageContent .= '</table>';

					$messageContent .= '<center>';

					$messageContent .= '<table width="600" border="0" cellpadding="0" cellspacing="0">';
					$messageContent .= '<tbody>';
					$messageContent .= '<tr><td width="600" height="100"><a href="' . SITE_URL . '"><img src="' . SITE_URL . '/img/email/header-logo.jpg" width="600" height="100" alt="' . SITE_NAME . '" border="0" style="display: block;"></a></td></tr>';
					$messageContent .= '</tbody>';
					$messageContent .= '</table>';

					$messageContent .= '<table border="0" cellpadding="0" width="600" cellspacing="0">';
					$messageContent .= '<tbody>';
					$messageContent .= '<tr>';
					$messageContent .= '<td width="49" height="30"><img src="' . SITE_URL . '/img/email/nav-left.png" width="49" height="30" style="display: block;" border="0" alt="' . SITE_NAME . '"></td>';
					$messageContent .= '<td width="107" height="30"><a href="' . SITE_URL . '/"><img src="' . SITE_URL . '/img/email/nav-classifieds.png" width="107" height="30" style="display: block;" border="0" alt="Classifieds"></a></td>';
					$messageContent .= '<td width="70" height="30"><a href="' . SITE_URL . '/maps/"><img src="' . SITE_URL . '/img/email/nav-maps.png" width="70" height="30" style="display: block;" border="0" alt="Maps"></a></td>';
					$messageContent .= '<td width="116" height="30"><a href="' . SITE_URL . '/place-an-ad/"><img src="' . SITE_URL . '/img/email/nav-place.png" width="116" height="30" style="display: block;" border="0" alt="Place an Ad"></a></td>';
					$messageContent .= '<td width="99" height="30"><a href="' . SITE_URL . '/static/about/"><img src="' . SITE_URL . '/img/email/nav-about.png" width="99" height="30" style="display: block;" border="0" alt="About Us"></a></td>';
					$messageContent .= '<td width="111" height="30"><a href="' . SITE_URL . '/static/contact/"><img src="' . SITE_URL . '/img/email/nav-contact.png" width="111" height="30" style="display: block;" border="0" alt="Contact Us"></a></td>';
					$messageContent .= '<td width="48" height="30"><img src="' . SITE_URL . '/img/email/nav-right.png" width="48" height="30" style="display: block;" border="0" alt="' . SITE_NAME . '"></td>';
					$messageContent .= '</tr>';
					$messageContent .= '</tbody>';
					$messageContent .= '</table>';

					$messageContent .= '<table width="600" border="0" cellpadding="0" cellspacing="0" style="border-right: 1px solid #cccccc; padding-right: 6px; border-left: 1px solid #cccccc; padding-left: 6px; border-bottom: 1px solid #cccccc;">';
					$messageContent .= '<tbody>';
					if( $hasPreCopy ) {
						$messageContent .= '<tr><td>&nbsp;</td></tr>';
						$messageContent .= '<tr><td>' . $preCopy . '</td></tr>';
					}
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '<tr><td><a href="' . SITE_URL . '/detail/?ad=' . $adId . '" style="display:block;text-align:center;text-transform:uppercase;font-size:36px;margin:0;color: ' . $colorPri1 . ';font-family:"Oswald",sans-serif;text-decoration:none;line-height:40px;">' . $adTitle . '</a></td></tr>';
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '<tr><td align="right">This ad runs from <b>' . $adStart . '</b> to <b>' . $adEnd . '</b></td></tr>';
					if( !empty( $adCats ) ) {
						$catCount = count($adCats);
						$i = 1;
						$messageContent .= '<tr><td align="right">';
							$messageContent .= 'Filed in ';
							foreach( $adCats as $adCat ) {
								$catDetails = getCatDetails($adCat['category_id']);
								$messageContent .= '<a href="' . SITE_URL . '/category/?c=' . $catDetails[0]['id'] . '">' . $catDetails[0]['category_name'] . '</a>';
								if( $i >= $catCount ) { $messageContent .= '.'; } else { $messageContent .= ', '; }
								$i++;
							}
						$messageContent .= '</td></tr>';
					}
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					if( $hasImg ) {
						$messageContent .= '<tr><td><a href="' . SITE_URL . '/detail/?ad=' . $adId . '" style="display:block;max-width:600px;max-height:400px;"><img src="' . SITE_URL . '/uploads/' . $adImages[0]['display'] . '" border="0" style="display:block;margin: 0 auto;width:auto;height:auto;max-width:600px;max-height:400px;" alt="' . $adTitle . '"></a></td></tr>';
					}
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '<tr><td>' . $adText . '</td></tr>';
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					$messageContent .= '<tr><td><a href="' . SITE_URL . '/detail/?ad=' . $adId . '" style="display: block; text-align: center; font-size: 13px; letter-spacing: 0.03em; text-transform: uppercase; font-weight: 700; color: ' . $colorPri1 . '">View Ad Online</a></td></tr>';
					$messageContent .= '<tr><td>&nbsp;</td></tr>';
					if( $hasPostCopy ) {
						$messageContent .= '<tr><td>' . $postCopy . '</td></tr>';
						$messageContent .= '<tr><td>&nbsp;</td></tr>';
					}
					$messageContent .= '</tbody>';
					$messageContent .= '</table>';

					$messageContent .= '</center>';

				} else {
					// then this ad is no longer valid and cannot be emailed
					$return['status'] = 'fail';
					$return['msg'] = "Sorry, the ad you are trying to email is no longer available or does not exist.";
					echo json_encode($return);
					exit;
				}

			} else {
				// reply ad not found
				$return['status'] = 'fail';
				$return['msg'] = "Sorry, the ad you are viewing does not exist.";
				echo json_encode($return);
				exit;
			}

			// validate recaptcha
			$response = getCaptchaResponse( $form_token );
			// send email
			if( $response['content']['success'] ) {
				$headers = 'From: ' . MAIL_FROM_NAME . ' <' . MAIL_FROM_ADDRESS . '>' . "\r\n";
				$headers .= 'Reply-To: ' . MAIL_FROM_NAME . ' <' . MAIL_FROM_ADDRESS . '>' . "\r\n";
				$headers .= 'X-Mailer: PHP/' . phpversion();
				$to = $form_recipient;
				$subject = 'Sent on behalf of ' . $form_sender . ': ' . MAIL_FROM_SUBJECT;
				$message = $messageContent;

				$mailed = send_email( $to, $subject, $message, $adId );
				if( !empty($mailed) && $mailed['status'] == 1 ) {
					site_log( $thisAd['id'], 'email', $thisAd['advertiser_id'] );
					$return['status'] = 'success';
				} elseif( !empty($mailed) && $mailed['status'] == 2 ) {
					$return['status'] = 'fail';
					$return['msg'] = "There was a problem sending the email.\nReason: No email address.\n\nPlease correct the form and try again.";
				} elseif( !empty($mailed) && $mailed['status'] == 3 ) {
					$return['status'] = 'fail';
					$return['msg'] = "There was a problem sending the email.\nReason: There was a problem sending the email.\n\nPlease refresh your browser and try again.\nIf the problem persists, feel free to contact us for support.";
				} elseif( !empty($mailed) && $mailed['status'] == 4 ) {
					$return['status'] = 'fail';
					$return['msg'] = "There was a problem sending the email.\nReason: This email recipient has chosen to opt-out of receiving emails from our site.";
				} elseif( !empty($mailed) && $mailed['status'] == 5 ) {
					$return['status'] = 'fail';
					$return['msg'] = "There was a problem sending the email.\nReason: Your account has exceeded the number of allowable abuse flags.\n\nPlease contact us for assistance, as we are able to audit the findings and reset the account flags if we feel they are unwarranted.";
				} else {
					// send mail failed for an unknown reason
					$return['status'] = 'fail';
					$return['msg'] = "Sorry, your email did not go through.\nPlease try again.\nIf the problem persists, feel free to contact us for support.";
				}

			} else {
				// reply failed recaptcha
				$return['status'] = 'fail';
				$return['msg'] = "Sorry, the recaptcha answer did not come through.\nPlease try again.\nIf the problem persists, feel free to contact us for support.";
			}
		} else {
			// user is not logged in
			$return['status'] = 'fail';
			$return['msg'] = "It doesn't appear you are logged in.\nPlease log in to use this feature.";
		}
		echo json_encode($return);
	}
	email();
}
