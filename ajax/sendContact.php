<?php
require_once("../includes/bootCore.php");
//handle contact request
if($_GET['mode']=='test')
{
    $userName = 'Jim Bob';
    $userEmail = 'jim@bobs.net';
    $userMessage = 'This is a message.';
    $testing = true;
} else {
    $userName = addslashes($_POST['name']);
    $userEmail = addslashes($_POST['email']);
    $userMessage = addslashes($_POST['message']);
}
$json = array();

if( ($_POST && trim($userEmail) != '') || $testing ) {

	$emailData = array(
			'name'		=> $userName,
			'email'		=> $userEmail,
			'message'	=> $userMessage
		);
	if($testing)
    {
        print_r($emailData);
    }
    $emailContent = email_generator(6,$emailData);
	$subject = $emailContent['subject'];
	$message = $emailContent['body'];
    if($testing)
    {
        print_r($emailContent);
    }
	//send form data to admin
	$success = send_email( CONTACT_EMAIL, $subject, $message );

	if( !$success ) {
		echo json_encode(array('mail_send'=>'error'));
	} else {
		echo json_encode(array('mail_send'=>'success'));
	}

} else {
	echo json_encode(array('mail_send'=>'Fail: No POST or no email address'));
}
