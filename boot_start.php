 <?php
global $page;
global $currPage;
global $page_title;
global $page_meta;
global $extraBodyClasses;

require_once("includes/bootCore.php");

$catId = false;
$ads = false;
$displayAds = false;
if( !empty($_GET["c"]) ) {
    $catId = htmlspecialchars($_GET["c"]);
    $thisCat = category($catId);
    $page_title = $thisCat[0]['category_name'];
    $page_h1_title = $thisCat[0]['category_name'];
}

/*
//check to see if we are being loaded in a sub directory of advertisers
*/
$path = $_SERVER['REQUEST_URI'];

if(stripos($path,"advertisers")>0)
{
    $pathUrlParts = explode("/advertisers/",$path);
    $path = $pathUrlParts[1];
    $path = explode("/",$path);
    $path = $path[0];
    if($path!='')
    {
        $al = getAdvertiserInfo($path);
        $alId=$al['id'];
        $page_title=stripslashes($al['name']);
        $page_h1_title=stripslashes($al['name']);
        $page_keywords=stripslashes($al['keywords']);
        $page_meta=stripslashes($al['meta_desc']);
        $ads = getAdvertisersAds($alId);
        $displayAds = getAdvertisersDisplays($alId);
        $dispAdsFirst = $al['disp_ads_first'];
    }
}

$ad = false;
if( !empty($_GET["ad"]) ) {
    $adId = htmlspecialchars($_GET["ad"]);
    $ad = getSingleAd($adId);
    $page_h1_title = $ad['headline'];
}
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <?php if( $page_meta != '' ){ ?>
        <meta name="description" content="<?= $page_meta; ?>">
        <?php } ?>
        <meta name="author" content="Joe Hansen <jhansen@pioneernewsgroup.com> and Rich McGee <rmcgee@pioneernewsgroup.com>">

        <!-- Icons -->
        <?php header_icons(); ?>

        <!-- Fonts -->
        <link href="<?= HEADING_FONT_LINK ?>" rel="stylesheet">
        <link href="<?= BODY_FONT_LINK ?>" rel="stylesheet">
        <link href="<?= PREVIEW_FONT_LINK ?>" rel="stylesheet">

        <!--
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
        -->
        <link rel="stylesheet" href="/css/bootstrap.min.css">
        <link rel="stylesheet" href="/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/css/datatables.css">
        <link rel="stylesheet" href="/css/dataTables.TableTools.css">
        <link rel="stylesheet" href="/css/bootstrap.datatables.min.css">
        <link rel="stylesheet" href="/css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="/css/colorpicker.css">
        <link rel="stylesheet" href="/css/jquery.tagsinput.min.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/summernote.css">
        <link rel="stylesheet" href="/css/select2.min.css">
        <link rel="stylesheet" href="/css/fileinput.min.css">
        <link rel="stylesheet" href="/css/cropper.min.css">
        <link rel="stylesheet" href="/css/slick.css">
        <link rel="stylesheet" href="/css/tooltipster.main.min.css">
        <link rel="stylesheet" href="/css/tooltipster.bundle.min.css">
        <?php
        $files = glob( ABS_PATH . '/sass/scss_cache/*.css' );
        $files = array_combine( $files, array_map( 'filemtime', $files ) );
        arsort( $files );
        $latestFile = key($files);
        $styleFile = basename($latestFile);
        if( MODE == 'testing' ) {
            ?><link rel="stylesheet" href="/sass/style.php/app.scss" data-mode="t"><?php
        } else {
            ?><link rel="stylesheet" href="/sass/scss_cache/<?= $styleFile ?>" data-mode="p"><?php
        } ?>

        <script src="/js/jquery-3.2.0.min.js"></script>
        <!--
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
        -->
        <script src="/js/bootstrap.min.js"></script>
        <script src="/js/summernote.min.js"></script>
        <script src="/js/jquery.tagsinput.min.js"></script>
        <script src="/js/jquery.colorpicker.js"></script>
        <script src="/js/jquery.cookie.js"></script>
        <script src="/js/moment.min.js"></script>
        <script src="/js/jquery.dataTables.min.js"></script>
        <script src="/js/jquery.dataTables.TableTools.js"></script>
        <script src="/js/dataTables.bootstrap.min.js"></script>
        <script src="/js/bootstrap-datetimepicker.min.js"></script>
        <script src="/js/bootbox.min.js"></script>
        <script src="/js/fileinput.min.js"></script>
        <script src="/js/cropper.min.js"></script>
        <script src="/js/jquery.validate.min.js"></script>
        <script src="/js/additional-methods.min.js"></script>
        <script src="/js/pwstrength-bootstrap.min.js"></script>
        <script src="/js/jquery.maskedinput.min.js"></script>
        <script src="/js/select2.full.min.js"></script>
        <script src="/js/jquery.gmap.min.js"></script>
        <script src="/js/slick.min.js"></script>
        <script src="/js/jquery.colorbox-min.js"></script>
        <script src="/js/jquery.sortable.js"></script>
        <script src="/js/masonry.pkgd.min.js"></script>
        <script src="/js/tooltipster.main.min.js"></script>
        <script src="/js/tooltipster.bundle.min.js"></script>
        <script src="/js/index.js"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <?php if( $GLOBALS['config']['billing_interface'] == 'Authorize.net' ) { ?>
            <?php if( MODE == 'testing' ) { ?>
                <!-- For Sandbox/Testing, use: -->
                <script type="text/javascript" src="https://jstest.authorize.net/v1/Accept.js" charset="utf-8"></script>
            <?php } else { ?>
                <!-- For Production, use: -->
                <script type="text/javascript" src="https://js.authorize.net/v1/Accept.js" charset="utf-8"></script>
            <?php } ?>
        <?php } ?>
        <title><?= ( $page_title ? $page_title . ' | ' . SITE_NAME : SITE_NAME ) ?></title>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		<script type="text/javascript">
			var adActionsAjaxPath = '/ajax/functions_ad_actions.php';
			var formSubmitAjaxPath = '/ajax/sendContact.php';
		</script>

        <?php if(GOOGLE_DFP_KEY != '') {
            //check to see if we have any slots to add
            $sql = "SELECT * FROM dfp_ads";
            $dbDFP = dbselectmulti($sql);
            if( $dbDFP['numrows'] > 0 ) {
                ?>
                <script async='async' src='https://www.googletagservices.com/tag/js/gpt.js'></script>
                <script>
                    var googletag = googletag || {};
                    googletag.cmd = googletag.cmd || [];
                </script>
                <script>
                    googletag.cmd.push(function() {
                        <?php foreach($dbDFP['data'] as $dfpAd) {
                            print "googletag.defineSlot('/".GOOGLE_DFP_KEY."/".stripslashes($dfpAd['slot_name'])."', ".stripslashes($dfpAd['slot_size']).", 'div-gpt-ad-".GOOGLE_DFP_KEY."-".$dfpAd['id']."').addService(googletag.pubads());\n";
                            ?>
                            googletag.pubads().enableSingleRequest();
                            googletag.pubads().collapseEmptyDivs();
                            googletag.enableServices();
                        <?php } ?>
                    });
                </script>
                <?php 
            }
        } ?>

        <?php
        if( !empty($_GET["ad"]) ) {
            facebookOG($ad);
        }
        ?>
        <?php
        if (function_exists('siteHead')) {
            siteHead();
        }
        ?>
    </head>

    <body class="page_<?= $page; ?> <?= $extraBodyClasses; ?>">
