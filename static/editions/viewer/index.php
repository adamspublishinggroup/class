<?php
$page = 'edition-viewer';
$page_title = 'Edition Pageflip';
$page_h1_title = 'Edition Pageflip';
$page_meta = '';
$extraBodyClasses = "loadingInProgress";


require_once( '../../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/edition-template.php' );
require_once( '../../../boot_close.php' );
