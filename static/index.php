<?php
// this is a placeholder if people go to this url, it will redirect to the about page
$page = 'static';
$page_title = 'About Us';
$page_h1_title = 'About Us';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/default-template.php' );
require_once( '../../boot_close.php' );
