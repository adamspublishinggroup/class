JS Libraries in use:

File Uploader:
http://plugins.krajee.com/file-input

Payment Gateway:
https://omnipay.thephpleague.com/simple-example/

Image Manipulation:
http://image.intervention.io/getting_started/installation

Image Cropper
https://github.com/fengyuanchen/cropper

Google Mapping
http://labs.mario.ec/jquery-gmap/

Graphing Library
http://www.jqplot.com/

DateTime Picker
eonasdan.github.io/bootstrap-datetimepicker/

DataTables
datatables.net

CLEditor WYSIWYG Text Editor
http://premiumsoftware.net/cleditor/gettingstarted

Bootstrap MultiFile upload 
http://plugins.krajee.com/file-input

Bootstrap Wizard
http://vinceg.github.io/twitter-bootstrap-wizard

Password Strength Checker
https://github.com/ablanco/jquery.pwstrength.bootstrap/blob/master/OPTIONS.md

jQuery Validator
https://jqueryvalidation.org/documentation/

jQuery MaskedInput
http://digitalbush.com/projects/masked-input-plugin/

jQuerty Tags
https://github.com/xoxco/jQuery-Tags-Input

color picker
http://www.eyecon.ro/bootstrap-colorpicker/

Moment, jQuery 3.2 and Bootstrap 3.3

PDF Viewer
https://github.com/erayakartuna/pdf-flipbook

PDF Generator
https://tcpdf.org/     https://github.com/tecnickcom/tcpdf
https://github.com/spipu/html2pdf

DateTimePicker
http://eonasdan.github.io/bootstrap-datetimepicker/

Sortable
http://johnny.github.io/jquery-sortable/

Convert classes to inline style in an element (like an invoice html template)
http://premailer.dialect.ca/


Instructions on posting to facebook via facebook php api, including how to generate permanent access token
https://adamboother.com/blog/automatically-posting-to-a-facebook-page-using-the-facebook-sdk-v5-for-php-facebook-api/


ICO generator
https://github.com/chrisbliss18/php-ico

Testing Rewrites with .htaccess
http://htaccess.mwl.be/

Zip class
https://github.com/alexcorvi/php-zip/blob/master/src/Zip.php

Front site wysiwyg editor
https://summernote.org