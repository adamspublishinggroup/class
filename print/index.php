<?php
$page = 'print-detail';
$page_title = 'Print Ad Details';
$page_h1_title = 'Print Ad Details';
$page_meta = '';

require_once( '../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/print-template.php' );
require_once( '../boot_close.php' );
