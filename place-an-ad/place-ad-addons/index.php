<?php
$page = 'place-ad';
$currPage = 'addons';
$page_title = 'Select Addons for your ad | Place Ad';
$page_h1_title = 'Place Ad - Addons';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/place-ad-template.php' );
require_once( '../../boot_close.php' );
