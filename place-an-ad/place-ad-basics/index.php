<?php
$page = 'place-ad';
$currPage = 'basics';
$page_title = 'Basics | Place Ad';
$page_h1_title = 'Place Ad - Basics';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/place-ad-template.php' );
require_once( '../../boot_close.php' );
