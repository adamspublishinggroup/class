<?php
$page = 'place-ad';
$currPage = 'preview';
$page_title = 'Ad Preview | Place Ad';
$page_h1_title = 'Place Ad - Preview';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/place-ad-template.php' );
require_once( '../../boot_close.php' );