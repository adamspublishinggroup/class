<?php
$page = 'place-ad';
$currPage = 'summary';
$page_title = 'Ad Summary | Place Ad';
$page_h1_title = 'Place Ad - Summary';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/place-ad-template.php' );
require_once( '../../boot_close.php' );
