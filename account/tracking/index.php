<?php
$page = 'dashboard';
$currPage = 'tracking';
$page_title = 'Ad Tracking | User Dashboard';
$page_h1_title = 'Ad Tracking';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/dashboard-template.php' );
require_once( '../../boot_close.php' );
