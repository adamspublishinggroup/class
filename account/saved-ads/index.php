<?php
$page = 'dashboard';
$currPage = 'saved-ads';
$page_title = 'Saved Ads | User Dashboard';
$page_h1_title = 'Saved Ads';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/dashboard-template.php' );
require_once( '../../boot_close.php' );
