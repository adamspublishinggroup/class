<?php
$page = 'dashboard';
$currPage = 'settings';
$page_title = 'Settings | User Dashboard';
$page_h1_title = 'Account Settings';
$page_meta = '';

require_once( '../../boot_start.php' );
require_once( INCLUDE_DIR . 'page-templates/dashboard-template.php' );
require_once( '../../boot_close.php' );
